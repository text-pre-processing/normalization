#!/usr/bin/env python
# coding: utf-8

import os
import sys
import string
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize, sent_tokenize
from TABICON.MyListArgs import *

try:
    word_tokenize("Texto de ejemplo")
except Exception as e:
    print("Recursos no instalados")
    import nltk 
    nltk.download('punkt')

class Normalizacion:

	def __init__(self, argv):
		"""
		Constructor de la clase que inicializa los parámetros de línea de
		comandos
		"""
		args = []
		for element in range(1,len(argv)):
			args.append(argv[element])
		param = MyListArgs(args)

		configFile = param.valueArgsAsString("-CONFIG","")
		if configFile != "":
			param.addArgsFromFile(configFile)

		sintaxis="-INDIRNORM:str -INEXT:str -OUTDIRNORM:str -LOWCASE:bool -ELIMPUN:bool -ELIMSW:bool [-LANGUAGE:str] -LENGTH:int [-CHARSETNORM:str] [-SPLITSENT:bool]"

		self.input_dir = param.valueArgsAsString("-INDIRNORM","")
		self.extension = param.valueArgsAsString("-INEXT","")
		self.dir_output = param.valueArgsAsString("-OUTDIRNORM","")
		self.low_case = param.valueArgsAsBoolean("-LOWCASE",True)
		self.elim_punt = param.valueArgsAsBoolean("-ELIMPUN",True)
		self.elim_SW = param.valueArgsAsBoolean("-ELIMSW",True)
		self.language = param.valueArgsAsString("-LANGUAGE", "english")
		self.long_w = param.valueArgsAsInteger("-LENGTH", 0)
		self.codec_c = param.valueArgsAsString("-CHARSETNORM", "utf-8")
		self.splits = param.valueArgsAsBoolean("-SPLITSENT", False)


	def get_Files(self):
		"""
		Este método lee un directorio y retorna una lista de archivos
		contenidos en el mismo
		:param dir_root: Es el nombre y ruta del directorio a procesar
		:return: Una lista de los nombres de los archivos contenidos
		en el directorio
		"""
		files = list(os.listdir(self.input_dir))
		return [w for w in files if w.endswith(self.extension)]


	def read_Text_File_NoNull(self, filename, codif="utf8"):
		"""
		Lee un archivo de texto y regresa una arreglo con las lineas 
		de ese archivo, pero filtra las lineas que solo estan en blanco 
		(es decir que no tienen algo).
		:return: Regresa un arreglo con las lineas del archivo leido
		"""
		lista = []
		with open(filename, "r", encoding=codif) as archivo:
			for linea in archivo.readlines():
				if linea.strip() != "":
					lista.append(linea)
		return lista


	def remove_puntuation(self, content, puntuation=list(string.punctuation)):
		"""
		Esta función permite remover de una cadena de texto los signos 
		de puntuación establecidos como parámetro
		:param palabras: es la cadena de palabras de entrada
		:param puntuation: es la lista de signos de puntuación de un idioma
		:return: Una lista de términos sin signos de puntuación
		"""
		puntuation.append("``")
		puntuation.append("''") 
		puntuation.append("...")
		puntuation.append("\"\"")
		puntuation.append("--")
		puntuation.append("“")
		puntuation.append("n't")
		puntuation.append("'s")
		puntuation.append("”")
		only_palabras = []
		for j in content:
			only_palabras.append([w for w in j if w not in puntuation])
		return only_palabras

		
	def tokenize_per_words(self, content_file):
		"""
		Esta función segmenta las oraciones de un texto de entrada por 
		palabra, toma en cuenta el contenido de un documento por oración 
		y lo tokeniza en función a las palabras de la cadena de texto
		:param content_file: es una lista que contiene el texto a procesar
		:return: Regresa una lista de palabras tokenizadas
		"""
		list_words = []
		for i in content_file:
			list_words.append(nltk.word_tokenize(i))
		return list_words


	def remove_not_alpha(self, content_file):
		"""
		Esta función realiza la eliminación de todos los signos que no 
		pertenecen al conjunto de caracteres alfanuméricos y ni al conjunto
		de números. Toma como entrada una lista que contiene una lista de
		cadenas a procesar, para filtar solo los elementos numéricos y 
		alfabéticos.
		:param content_file: es una lista que contiene el texto a procesar
		:return: Regresa una lista de palabras alfanuméricas
		"""
		list_words = []
		for i in content_file:
			list_words.append([w for w in i if w.isalpha() or w.isdigit()])
		return list_words


	def to_lower_case(self, content_file):
		"""
		Función que pasa el contenido de una lista de cadenas a minúsculas
		:param content_file: es el contenido del documento en lista
		:return: una lista de palabras transformadas a minúsculas
		"""
		list_lower = []
		for i in content_file:
			list_lower.append([w.lower() for w in i])
		return list_lower


	def to_upper_case(self, content_file):
		"""
		Función que pasa el contenido de una lista de cadenas a mayúsculas
		:param content_file: es el contenido del documento en lista
		:return: una lista de palabras transformadas a mayúsculas
		"""
		list_upper = []
		for i in content_file:
			list_upper.append([w.upper() for w in i])
		return list_upper
		

	def Write_String_file(self, outfile, contenido):
		"""
		Crea un archivo de texto en el cual se escribe el contenido 
		de la cadena
		:param contenido: Arreglo de cadenas a escribir
		:return: True si pudo crear el archivo junto con su contenido
		"""
		with open(outfile, "w", encoding="utf8") as archivo:
			for linea in contenido:
				archivo.write(linea + "\n")
				#print linea


	def true_or_false(self, arg):
		"""
		Función que permite evaluar el parámetro de salida como un valor
		booleano.
		:param arg: Es la cadena del argumento booleano
		:return: True o False si coincide con la opción solicitada,
		en caso contrario se retornará None
		"""
		ua = str(arg).upper()
		if 'TRUE'.startswith(ua):
			return True
		elif 'FALSE'.startswith(ua):
			return False
		else:
			return None


	def remove_stop_words(self, content, language="english"):
		"""
		Función que permite eliminar las palavras vacías (obtenidas de las 
		librerías de NLTK a partir de una lista de palabras.
		:param content: Es la lista de palabras de cada linea de texto
		:return: Una lista de palabras que no corresponden a la lista de
		 stopwords
		"""
		sw = set(stopwords.words(language))
		only_palabras = []
		for j in content:
			only_palabras.append([w for w in j if w not in sw])
		return only_palabras


	def delim_words(self, content,limit=100):
		"""
		Función que delimita en un número de palabras tomando como
		referencia un texto de entrada.
		:param limit: Es el número límite de palabras permitido
		:return: Una lista de palabras comprendidas en el número
		especificado
		"""
		fi_wo = []
		words = 0
		for j in content:
			l_words = [w for w in j]
			list_aux = []
			for word_i in l_words:
				if words < limit:
					list_aux.append(word_i)
					words += 1
			fi_wo.append(list_aux)
		return fi_wo


	def untilcrearcarpetas(self, path):
		"""
		Crea una carpeta y no avanza hasta que crea la carpeta.
		:param path: Ruta de donde se va a crear la carpeta
		:return: True cuando halla creado la carpeta
		"""
		while not os.path.exists(path):
			os.makedirs(path)


	def split_by_sentences(self, content_file):
		"""
		Función usada para delimitar oraciones de un archivo 
		de texto. Esta función es útil cuando el contenido 
		del archivo no se encuentra segmentado por oraciones.
		:param content_file: es una lista que contiene oraciones.
		:return: 
		"""
		return sent_tokenize(" ".join(content_file))


if __name__ == '__main__':
	norm = Normalizacion(sys.argv)

	if norm.elim_SW == True:
		nltk.download('stopwords')

	files_corpus = norm.get_Files()
	norm.untilcrearcarpetas(norm.dir_output)

	for file_int in files_corpus:
		print(file_int)
		content_file = norm.read_Text_File_NoNull(norm.input_dir+os.sep+file_int, norm.codec_c)
		if norm.splits == True:	
			content_file = norm.split_by_sentences(content_file)

		content_file = norm.tokenize_per_words(content_file)

		if norm.low_case == True:
			content_file = norm.to_lower_case(content_file)
		elif norm.low_case == False:
			content_file = norm.to_upper_case(content_file)

		if norm.elim_punt == True:
			content_file = norm.remove_puntuation(content_file)

		if norm.elim_SW == True:
			content_file = norm.remove_stop_words(content_file, norm.language)

		if norm.long_w > 0:
			content_file = norm.delim_words(content_file, int(norm.long_w))
		
		salida = ""
		list_salida = []
		for out in content_file:
			for i_ou in out:
				salida += i_ou + " "
			list_salida.append(salida)
			salida = ""

		norm.Write_String_file(norm.dir_output+os.sep+file_int, list_salida)
