# Normalization

## Autor de este repositorio
Ing. Jonathan Rojas Simón (ids_jonathan_rojas@hotmail.com)

## Descripción general

Componente desarrollado en python 3.7.3 que normaliza un conjunto de documentos. Las características que se considera este componente para la normalización del texto son:
- Eliminación de palabras vacías (Stopwords).
- Elininación de signos de puntuación.
- Conversión de caracteres (mayúsculas o minúsculas).
- Delimitación del texto a un número de palabras definido.

# Dependencias

Para usar este componente debes instalar los siguientes intérpretes y librerías:

- Python 3.7
- TABICON37
- regex
- numpy
- nltk

Para conocer los nombres de librerías y componentes usados, es necesario consultar el archivo requeriments.txt

## Sintaxis

-INDIRNORM:str -INEXT:str -OUTDIRNORM:str -LOWCASE:bool -ELIMPUN:bool -ELIMSW:bool [-LANGUAGE:str] -LENGTH:int [-CHARSETNORM:str]

### Descripción de parámetros

- -INDIRNORM   -> Directorio donde se encuentran los documentos (archivos) de entrada a normalizar.
- -INEXT       -> Cadena que indica la extensión de los archivos de entrada (p. ej. ".txt").
- -OUTDIRNORM  -> Directorio donde se generarán los archivos de salida normalizados.
- -LOWCASE     -> Variable booleana que indica la transformación del texto:
                   * True  = Conversión del texto a minúsculas.
                   * False = Comversión del texto a mayúsculas.
- -ELIMPUN     -> Variable booleana que indica la eliminación de signos de puntuación:
                   * True  = Eliminación de signos de puntuación.
                   * False = NO aplica la eliminación de signos de puntuación.
- -ELIMSW      -> Variable booleana que indica la eliminación de palabras vacías (StopWords - SW).
                   * True  = Eliminación de palabras vacías.
                   * False = NO aplica la eliminación de palabras vacías.
- -LANGUAGE    -> Lenguaje utilizado de los documentos de entrada (p. ej. "english", "spanish", etc.).
- -LENGTH      -> Especifica la longitud (a número de palabras) del texto normalizado.
- -CHARSET     -> Tipo de codificación a utilizar (p. ej. "utf-8").
