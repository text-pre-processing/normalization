Los funcionarios administrativos y del Congreso est�n de acuerdo en que la CIA hizo su trabajo.
Desde la semana pasada, los analistas de inteligencia hab�an advertido al presidente Bush que lo que originalmente parec�a un farol militar se estaba convirtiendo r�pidamente en el tipo de despliegue de tropas que podr�a usarse para invadir Kuwait o incluso Arabia Saudita.
El director de la Agencia Central de Inteligencia, William Webster, incluso entreg� a mano ciertos detalles nuevos sobre la acumulaci�n iraqu� para el asesor de seguridad nacional Brent Scowcroft.
Entre las evaluaciones que transmiti� se encontraba una conclusi�n de que, para esta semana, la fuerza del presidente Saddam Hussein ser�a capaz de penetrar en Kuwait o Arabia Saudita.
La agencia carec�a de evidencia contundente de una intenci�n de invadir, dicen los funcionarios de inteligencia.
Pero a medida que la acumulaci�n creci�, la CIA se alarm� m�s, en parte porque el Sr. Hussein es muy impredecible y en parte porque, a diferencia de muchos otros l�deres �rabes, se sabe que lleva a cabo sus amenazas.


