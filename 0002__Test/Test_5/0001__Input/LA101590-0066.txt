Leonard Bernstein, el hombre de m�sica renacentista que se destac� como pianista, compositor, director y maestro y, tambi�n, el extravagante maestro de ceremonias de su propio circo sin parar, muri� el domingo en su departamento de Manhattan.
Ten�a 72 a�os.
Bernstein, conocido y amado por el mundo como "Lenny", muri� a las 6:15 p.m.En presencia de su hijo, Alexander, y el m�dico, Kevin M. Cahill, quien dijo que la causa de la muerte fue complicaciones del fracaso pulmonar progresivo.
Por consejo de Cahill, el director hab�a anunciado el martes que se retirar�a.
Cahill dijo que el enfisema progresivo complicado por un tumor pleural y una serie de infecciones pulmonares hab�an dejado a Bernstein demasiado d�bil para continuar trabajando.
En los �ltimos meses, Bernstein cancel� las actuaciones con una frecuencia creciente.
Su �ltima aparici�n en direcci�n fue en Tanglewood, Massachusetts, el 19 de agosto.
Bernstein fue el primer director de orquesta nacido en Estados Unidos en dirigir una importante orquesta sinf�nica, a menudo uni�ndose a su Filarm�nica de Nueva York para tocar sus propias piezas, mientras conduc�a desde el piano.
Grab� otros nichos en la historia componiendo la "historia del lado oeste" indeleble y ense�ando una generaci�n sobre m�sica cl�sica a trav�s de la innovadora serie de televisi�n "Omnibus".
Al exhibir talento y experiencia notables en cuatro �reas que la mayor�a de los artistas desean que pose�an en simplemente uno, Bernstein a�n podr�a haber seguido siendo un m�sico oscuro sin el toque teatral �nico que dominaba su vida personal y profesional.
Con eso, se convirti� en una personalidad, bien conocida incluso por las personas que nunca compraron un boleto para una actuaci�n musical o vieron un programa de televisi�n serio.
La persona derviche, incluida su advenediza gimnasia en el podio, nunca disminuy� a lo largo de su larga vida en el centro de atenci�n.
Hizo que la m�sica cl�sica sea comprensible y sabrosa para las masas.
Y levant� la m�sica popular a un avi�n m�s alto, infundiendo artistas y oyentes con su alegr�a man�aca en crear un sonido tonal.
"Algunos directores se suavizan con la edad", coment� el cr�tico de m�sica Martin Bernheimer cuando Bernstein dirigi� la Filarm�nica de Los �ngeles en UCLA en 1986.
"(Pero) Bernstein, a los 68 a�os, sigue siendo una combinaci�n fren�tica de cohete en �rbita, maestro aer�bico, s�per juggler, �dolo matinee, ca��n en auge, mima hist�rico, tragedia desgarradora, bola de rebotes, escritor de cielo, m�quina remachadora, mawkish Sentishaista yDanseur ignorble ".
Al describir al director en el mismo concierto, Bernheimer se refiri� a �l como "el encogimiento de hombros, saltando, suspirando, altando, brotando, agachado, mecedora, rodando, delimitando, balance�ndose, saltando, sacudiendo, apu�alando, golpeando, rutando y gru�endo maestroExcelsis ".
Los cr�ticos tambi�n estuvieron r�pidamente de acuerdo en que si su espect�culo envidiado y a menudo criticado sea enmascarado en mascarado, descuidada o inepta de musicalidad, Bernstein nunca podr�a haber seguido siendo un director de conductor internacionalmente buscado durante cinco d�cadas.
Sab�a lo que estaba haciendo, y los m�sicos que acompa��, escribi�, dirigi�, dirigi� o dieron conferencias y ense�adas lo admiraban como uno de los suyos.
Louis Bernstein (llamado porque su abuela materna insisti�) naci� el 25 de agosto de 1918 en Lawrence, Massachusetts, de dos inmigrantes jud�os rusos.
Su padre, Samuel Joseph Bernstein, era emprendedor de productos para el cuidado del cabello de las mujeres y un erudito talm�dico.
Su madre, Jennie Resnick Bernstein, quien lo sobrevive, dijo que su hijo siempre ten�a una oreja para la m�sica.
"Cuando ten�a 4 o 5 a�os, tocaba un piano imaginario en su alf�izar".
Los padres prefirieron el nombre de "Leonard" y llamaron al ni�o eso.
Cuando su maestro de jard�n de infantes le pidi� a "Louis Bernstein" que se pusiera de pie, permaneci� sentado y mir� alrededor de la habitaci�n para ver qui�n comparti� su apellido.
Bernstein cambi� su nombre legalmente a los 16 a�os, cuando obtuvo su primera licencia de conducir.
Su talento musical mega surgi� tard�amente y casi por accidente.
Cuando Bernstein ten�a 10 a�os, una t�a divorciada almacen� su viejo piano vertical con sus padres, y el ni�o que sol�a jugar en el alf�izar de la ventana qued� fascinado con �l.
Pidi� lecciones, y pronto estaba jugando mejor que su maestro, la hija de un vecino que cobr� $ 1 por lecci�n.
A los 12 a�os, estaba estudiando en el Conservatorio de M�sica de Nueva Inglaterra y hab�a determinado, a pesar de las objeciones de su padre, esa m�sica, en ese momento tocando el piano, ser�a su carrera.
Los impresionantes talentos instintivos de Bernstein para la lectura a primera vista, el recuerdo de puntajes complicados y la improvisaci�n se hicieron evidentes mientras tocaba, y la m�sica alterada, cl�sica, jazz y popular.
Produjo sus propios espect�culos y versiones de "The Mikado" y "Carmen", y actu� como solista de piano con su orquesta escolar y la Orquesta Sinf�nica del Estado.
Se deleit� con la m�sica mientras sobresal�a en el atletismo y las materias cl�sicas ense�adas en la Escuela Latina de Boston de 300 a�os.
En la Universidad de Harvard, Bernstein estudi� piano y composici�n, pero desarroll� un gran inter�s en componer solo despu�s de conocer al compositor estadounidense Aaron Copland.
Un fan�tico y practicante de su m�sica, Bernstein conoci� a Copland en un incidente t�pico de que solo se engendra.
Invitado a un recital de baile en Nueva York, Bernstein se sent� en el balc�n junto a un hombre que no reconoci�.
Invitado despu�s a una fiesta de cumplea�os post-recital para Copland, Bernstein comand� el piano y jug� las "variaciones de piano" de Copland.
Su actuaci�n cautiv� al hombre que hab�a conocido en el balc�n, que por supuesto era Copland.
Se hicieron amigos de toda la vida.
Copland present� a Bernstein a varios compositores y le consigui� su primer trabajo: transcribir m�sica para el editor Boosey y Hawkes.
Ir�nicamente, Copland posteriormente inst� al desgarrado Bernstein a hacer la conducci�n, en lugar de componer o incluso jugar al piano, su carrera.
La conducci�n surgi� como una posibilidad para Bernstein ese mismo a�o, 1937, su segundo a�o en Harvard, cuando conoci� al director griego Dimitri Mitropoulos durante la visita del maestro al campus.
Mitropoulos estaba tan impresionado con la interpretaci�n de Bernstein que lo invit� a asistir a los ensayos con la Sinfon�a de Boston.
El maestro tambi�n habl� con Bernstein en privado en su vestidor despu�s de conciertos, prometiendo mantenerse en contacto.
"La influencia de Mitropoulos en mi vida, en mi vida en mi vida es enorme y generalmente muy subestimada o no conocida en absoluto", escribi� Bernstein a�os despu�s, despu�s de que sus mentores murieron todos, "porque normalmente los dos grandes conductores con los que estudi� sonLos que reciben el cr�dito por cualquier destreza conductora que tenga, a saber, Serge Koussevitzky y Fritz Reiner.
...Pero mucho antes de conocer a cualquiera de ellos, hab�a conocido a Dimitri Mitropoulos...Y verlo llevar a cabo esas dos semanas de ensayos y conciertos con la Sinfon�a de Boston sent� alg�n tipo de pasi�n conductora y base en mi psique que ni siquiera sab�a hasta muchos a�os despu�s ".
Se gradu� de Harvard y fuera del trabajo despu�s de un verano en Nueva York, Bernstein le pregunt� a Mitropoulos qu� deb�a hacer.
"Debes ser un director", respondi� Mitropoulos, inst�ndolo a estudiar en Juilliard o con Reiner en el Instituto Curtis en Filadelfia.
Fue oto�o y las clases de conducci�n en Juilliard estaban llenas, por lo que Bernstein fue a Filadelfia durante dos a�os.
Durante los veranos, estudi� con Koussevitzky, director de la Sinfon�a de Boston, en la nueva escuela Koussevitzky comenzaba en la casa de verano de la orquesta, Tanglewood.
"Se convirti� en un padre sustituto para m�", dijo Bernstein m�s tarde sobre Koussevitzky, su tercer mentor conductor importante.
"No ten�a hijos propios y yo ten�a un padre a quien me encantaba mucho, pero que no era por esta cosa musical en absoluto.
...Y entonces encontr� otro padre: Primero Mitropoulos, luego Reiner, y ahora Koussevitzky.
Pero la relaci�n Koussevitzky fue muy especial, muy c�lida ".
M�s tarde, Bernstein se convirti� en el asistente de Koussevitzky y regres� a Tanglewood anualmente para conducir y ense�ar.
�l vener� tanto al maestro que estaba casado con zapatos Koussevitzky y traje blanco y siempre conduc�a usando sus enlaces de brazalete.
Fue Koussevitzky, y Fate, quien organiz� la ascensi�n mete�rica de Bernstein a un conductor de clase mundial a la edad inaudita de 25 a�os.
En su 25 cumplea�os, el 25 de agosto de 1943, Bernstein le dijo a Bernstein por los Koussevitzkys que deber�a visitar a Artur Rodzinsky, director musical reci�n nombrado de la Filarm�nica de Nueva York, en su granja de Stockbridge, Connecticut.
"Voy a necesitar un director asistente", le dijo Rodzinsky.
"He pasado por todos los conductores que conozco en mi mente y finalmente le pregunt� a Dios a qui�n tomar�a y Dios dijo: 'Toma Bernstein'", dijo, otorgando la posici�n como un favor no a Dios sino a Koussevitzky.
Notable como su nombramiento consideraba que era estadounidense y tan joven, Bernstein ten�a pocas posibilidades de tomar el podio.
En la historia de la orquesta, ning�n director asistente hab�a sido convocado.
Pero para la historia de "Lucky Lenny" hizo una excepci�n.
El 14 de noviembre, menos de tres meses despu�s de que Bernstein consigui� el trabajo, el director invitado Bruno Walter cay� enfermo y Rodzinsky estaba nevado en su granja.
Jennie Tourel hab�a cantado "I Ody Music" de Sung Bernstein en su debut en el ayuntamiento la noche anterior, en s� mismo un evento lo suficientemente grande como para llevar a sus padres a la ciudad, y �l hab�a tocado para ella y la fiesta posterior al recital.
Sin ensayo, una resaca y tres horas de sue�o, Bernstein realizar�a un programa complejo transmitido en todo el pa�s en CBS Radio.
El New York Times narr� el evento �pico en la p�gina uno y declar� en un editorial: "Sr.
Bernstein tuvo que tener algo que se acercara al genio para aprovechar al m�ximo su oportunidad.
...Es una buena historia de �xito estadounidense.
El c�lido y amigable triunfo llen� el Sal�n Carnegie y se extendi� sobre las ondas ".
Bernstein no deb�a obtener su propia orquesta hasta que se hizo cargo de la Filarm�nica de Nueva York en 1957-58.
No ten�a tiempo para uno.
Ten�a demasiada demanda en todo el mundo como el director invitado de Wunderkind.
Exitoso como pianista, compositor y director de orquesta, Bernstein, seg�n Joan Peyser en una controvertida biograf�a, consult� a los psiquiatras debido a su conflicto interno sobre las tres actividades.
"Es imposible para m� tomar una elecci�n exclusiva entre las diversas actividades", escribi� Bernstein en 1946.
"Lo que me parece bien en cualquier momento es lo que debo hacer.
...Los fines son m�sica misma...y los medios son mi problema privado ".
El piano se convirti� en un solo en el escenario o un pasatiempo de fiesta famoso.
Bernstein se concentr� seriamente en componer mientras desarrollaba y comercializaba su carrera en direcci�n.
Su primera composici�n importante, una sinfon�a titulada "Jeremiah", se introdujo en 1942, y su primer ballet, "Fancy Free" y Related First Musical ", en la ciudad", se debutaron en 1944.
Intent� ser un compositor cl�sico, ganando aplausos por las sinfon�as ("Jerem�as" fue seguido por "la era de la ansiedad" en 1949 y "Kaddish" en 1963), Sonatas y las �peras "problemas en Tahit�" en 1953 y "A" ALugar tranquilo "en 1983.
Al escribir m�sica, Bernstein logr� un mayor �xito en Broadway, e incluso en Hollywood, que en Lincoln Center.
Sigui� "On the Town" con los musicales "Wonderful Town" en 1953, la partitura para la pel�cula "en el paseo mar�timo" en 1954, y el aclamado pero menos popular "Candide" en 1956 en 1956.
Su mejor y mejor trabajo, "West Side Story", debut� en 1957.
"Si puedo escribir una �pera estadounidense real y en movimiento que cualquier estadounidense pueda entender (y una que, a pesar de un trabajo musical serio) ser� un hombre feliz", dijo Bernstein en 1948.
Cuando muchos sugirieron una d�cada m�s tarde que hab�a logrado su sue�o con "West Side Story", no estaba de acuerdo, diciendo: "Hay momentos en los que lo creo, pero como un total no puede ser una �pera.
Porque en el desenlace, en el dram�tico desentra�amiento, la m�sica se detiene.
"Pero no me encanta", agreg�, deleitando en la adulaci�n su adaptaci�n del conflicto de Romeo y Julieta hab�a producido.
"No lo convierte en un hijastro o una fundaci�n".
En la ense�anza, su cuarta �rea de especializaci�n, Bernstein ense�� regularmente en Tanglewood y gan� su lugar en anales acad�micos con sus conferencias de Charles Eliot Norton sobre tonalidad en Harvard en 1973.
Fue en televisi�n en las d�cadas de 1950 y 1960, con sus "conciertos de j�venes" de la Filarm�nica y "Omnibus", que Bernstein ense�� a la naci�n.
"Una evaluaci�n de Bernstein debe incluir su talento y contribuci�n como maestro y popularidad de m�sica, un papel que lo ha separado m�s de otros artistas", escribi� el director, historiador y presidente de Bard College, Leon Botstein, en Harper's en 1983.
Instintivamente experto en la televisi�n, Bernstein se convirti� en un prototipo para Carl Sagan, Alistair Cooke y otros ahora familiarizados en programas instructivos en el sistema de transmisi�n p�blica.
Los programas de Bernstein, dijo Botstein, "mostraron su don para analizar y entusiasmar la m�sica cl�sica sin sacrificar la integridad de la partitura, su complejidad o su genio simple.
Nadie antes o desde que Bernstein ha sido tan efectivo, art�stica y comercialmente, en el proselitismo y traer m�sica seria a una audiencia masiva ".
Los cl�sicos de televisi�n ganaron a Bernstein un codiciado premio Peabody.
Bernstein pudo aplicar su habilidad innata para comercializar su arte que lo hab�a hecho rico en la luchadora Filarm�nica de Nueva York.
Present� conciertos gratuitos en el parque y puso a la orquesta en la televisi�n, ampliando su audiencia y triplicando la asistencia a los conciertos pagados.
Dej� la orquesta en 1969, despu�s de un mandato r�cord de 11 a�os al tim�n, para tener m�s tiempo para componer y conducir hu�spedes.
Si dos anillos del Circus de Bernstein descansaban en m�sica popular y cl�sica, el tercero estaba anclado en su vida personal.
Conocido por sus abrazos emocionales de ambos sexos, Bernstein amaba a las multitudes y manten�a a amigos y familiares con �l durante toda la noche en su apartamento de Manhattan o en la casa de Connecticut Country.
Un fuerte fumador y bebedor que festej� o trabaj� hasta el amanecer y durmi� hasta el mediod�a, afirm� que compuso o estudi� puntajes incluso cuando estaban rodeados de personas.
"Dios sabe, ya deber�a estar muerto", dijo mientras se acercaba a 70, caracter�sticamente arrogante sobre su salud.
"Yo fumo.
Bebo.
Me quedo despierto toda la noche.
Estoy comprometido en todos los frentes.
...Me dijeron que si no dejara de fumar estar�a muerto a los 35 a�os.
Bueno, venc� al rap ".
Al aumentar las cejas del mundo de la m�sica alta, Bernstein hizo campa�a con otras celebridades por los derechos civiles de los negros en la d�cada de 1960 y contra la Guerra de Vietnam en la d�cada de 1970.
Criticado por "Radical Chic", su filosof�a pol�tica izquierdista lo llev� a organizar un partido de recaudaci�n de fondos para los Black Panthers en 1970.
En 1989, rechaz� una Medalla Nacional de las Artes en protesta por la subvenci�n retirada de $ 10,000 (m�s tarde restablecida) del National Endowment for the Arts to a Art Show on SIDA.
Aunque Peyser hizo un caso fuerte en su biograf�a de 1987 de que Bernstein ten�a muchos asuntos homosexuales (Bernstein prometi� a sus hijos que nunca leeran el libro), no hab�a duda de que adoraba a su familia.
El 9 de septiembre de 1951, se cas� con la actriz chilena Felicia Montealegre, y, a pesar de una breve separaci�n y la posterior reconciliaci�n en 1976, permaneci� dedicada a ella y entr� en una depresi�n severa cuando muri� de c�ncer de pulm�n en 1978.
A Bernstein tambi�n le sobreviven dos hijas, Jamie y Nina, as� como su hermana, Shirley, y su hermano, Burton.
La portavoz Margaret Carson dijo que el funeral ser�a privado.
Se opuso firmemente a retirarse, hasta el martes pasado, Bernstein continu� su direcci�n de invitados, componer y grabar, tal vez encabezando sus m�s de 200 discos con lo mejor de su mejor, una regrabaci�n de "West Side Story" en 1985.
El esfuerzo se convirti� en el r�cord m�s vendido de Deutsche Grammaphon y le pidi� a Will Crutchfield a escribir en el New York Times que Bernstein estaba en una clase con Giuseppe Verdi.
El presidente franc�s Francois Mitterrand salud� a "West Side Story" de nuevo en 1986 cuando convirti� a Bernstein en un comandante de la Legi�n de Honor.
Despu�s de una serie nacional de conciertos y fiestas que celebran su 70 cumplea�os en 1988, Bernstein dijo con modestia poco caracter�stica: "No tengo m�s solicitudes del destino ... excepto por el tiempo.
He logrado m�s de lo que ten�a derecho a esperar.
Nadie ha tenido tanta suerte como yo ".
Superstar Lenny: Demasiado talento, muy poco tiempo, dice Martin Bernheimer.A23


