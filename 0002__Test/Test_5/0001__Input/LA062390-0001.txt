Poco despu�s de las 2 p.m.El mi�rcoles, el residente de San Diego, Mohammad Nyakoui, recibi� una llamada de su esposa, que pasa el verano en la ciudad costera del mar de Caspian de Rasht, Ir�n, con su familia.
"Le pregunt� qu� estaba haciendo, y ella dijo que estaba viendo la Copa del Mundo", recuerda Nyakoui.
Unos 15 minutos despu�s de colgar, Rasht y otras ciudades en la regi�n norte de Ir�n fueron sacudidas por un terremoto que mat� a miles.
De vuelta a casa en San Diego, pasaron varias horas antes de que Nyakoui escuchara noticias del terremoto.
Cuando lo hizo, una aritm�tica r�pida en la zona de tiempo demostr� que hab�a hablado con su esposa unos minutos antes de que ocurriera el terremoto.
"Estaba muy preocupado", dijo Nyakoui.
"Intent� a trav�s de la Cruz Roja llamar a Ir�n, pero nos dijeron que no pod�an hacer nada antes de que pasaran 48 horas".
Uno de los aproximadamente 25,000 iran�es de San Diego en el mismo estado ansioso, Nyakoui fue m�s afortunado que la mayor�a: su esposa logr� llamarlo por segunda vez ese d�a, aproximadamente ocho horas despu�s del terremoto, para decirle que ella y sus dos hijos estaban a salvo.
Las l�neas telef�nicas permanecieron atascadas el viernes, dijeron otros iran�es-estadounidenses en San Diego.
"Hay una sensaci�n de p�nico.
Es simplemente imposible pasar ", dijo Houshang Ghashhai, quien ense�a ciencias pol�ticas en la Universidad Estatal de San Diego.
Sin embargo, algunos de los inmigrantes preocupados estaban convirtiendo su miedo en acci�n al movilizar los esfuerzos de ayuda para Ir�n.
Hamid Biglari, un f�sico te�rico de UC San Diego, fue uno de los que organizaron un grupo local para reunir contribuciones para el Fondo de las Naciones Unidas para el terremoto de Ir�n.
El grupo ha configurado dos oficinas y l�neas telef�nicas.
Est�n recolectando medicina, comida y una variedad de suministros vivos, que incluyen mantas, ropa de luz y linternas.
Los suministros ser�n transportados por las Naciones Unidas, dijo Biglari.
Otro grupo que recolecta suministros m�dicos es Southwest Medical Teams, una organizaci�n cuyo �ltimo esfuerzo internacional importante fue enviar voluntarios y suministros m�dicos a Armenia despu�s del terremoto de 1988 all�.
Los equipos m�dicos del suroeste no enviar�n ning�n voluntario esta vez, dijo el director Barry La Forgia.
"Eso es por consejo del Departamento de Estado.
No pod�an asegurar nuestra seguridad all�, y no queremos poner en peligro a ninguno de nuestros voluntarios ", dijo La Forgia.
Donde enviar ayuda
Dos grupos est�n tomando donaciones para el alivio del terremoto iran� en San Diego:
Fondo de las Naciones Unidas para el terremoto de Ir�n (467-1120 o 456-4000): recolectando medicamentos antibi�ticos y analg�sicos, recipientes de agua de pl�stico plegables, l�minas de pl�stico, generadores port�tiles de 220 voltios de menos de 10 kilovatios, carpas, mantas, alimentos secos, secos,Ropa de luz, linternas y equipos de iluminaci�n.
Traiga art�culos o cheques por correo a 4540 Kearny Villa Road, Suite 214, San Diego 92123;o a 7509 Girard Ave., Suite A, La Jolla 92037.
Los cheques deben hacerse a la U.N.
Fondo para el terremoto de Ir�n.
Southwest Medical Teams (284-7979)-Hasta el jueves, coleccionar suturas, guantes quir�rgicos, antibi�ticos, analg�sicos (incluida la aspirina), vitaminas, soluciones oft�lmicas y bolsas de recolecci�n de sangre vac�as.
El env�o se enviar� el 2 de julio.
Las donaciones en efectivo se utilizar�n para pagar para volar los suministros a Ir�n.
Checks de correo a 3547 Camino Del Rio South, Suite C, San Diego 92108, se�alando que la donaci�n es para el Fondo de Ayuda de Terremotos Ir�n.
Otras agencias que aceptan donaciones para v�ctimas de terremotos iran�es:
Agencia de Desarrollo Adventista y Ayuda
12501 Old Columbia Pike
Silver Spring, Md. 20904
(301) 680-6380
Cruz Roja Americana
Desastre del terremoto de Ir�n
CORREOS.Box 37243
Washington, D.C. 20013
(800) 842-2200
Banco Melli Ir�n
Asistencia de alivio del terremoto de Ir�n
Cuenta No. 5000
628 Madison Ave.
Nueva York, N.Y. 10022
Comit� de EE. UU. Para UNICEF
333 E. 38th st.
Nueva York, N.Y. 10016
(212) 686-5522


