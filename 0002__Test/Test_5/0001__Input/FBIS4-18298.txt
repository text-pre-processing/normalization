Bfn
[Art�culo de Colin Brown: "Los aliados del gabinete cierran rangos pero traen
El escaso alivio para los ministros del gabinete mayor "] [texto] se unieron ayer a John Major al insistir en que no habr�a elecciones de liderazgo este oto�o.
Pero el espect�culo concertado de apoyo no pudo disipar las dudas entre los parlamentarios conservadores sobre sus posibilidades de quedarse en el cargo.
Los parlamentarios de derecha confirmaron los hallazgos en una encuesta independiente del domingo/NOP [Encuesta Nacional de Opini�n] que Michael Heseltine era el favorito para reemplazar al Sr. Major, si se ve obligado a salir.
Pero est�n calculando que Michael Portillo funcionar� duro para asegurar pol�ticas euroesc�pticas del eventual ganador.
Algunos de los aliados m�s cercanos del Sr. Major estaban hirviendo con ira con Heseltine y Kenneth Clarke, el canciller, por "poseear" antes del retiro da�ino la semana pasada por el primer ministro sobre los poderes de veto de Gran Breta�a en la Uni�n Europea ampliada.
"�Por qu� Clarke y Heseltine estaban tratando de sonar m�s portilloesco que Portillo?
La respuesta es bastante obvia ", dijo un ministro del gabinete.
El liderazgo espera que, en el receso de Pascua, las circunscripciones conservadoras insten a sus parlamentarios a que detengan la lucha perjudicial que creen que est� eclipsando la lucha del gobierno en la econom�a, la ley y el orden y la educaci�n.
Ese mensaje fue reforzado por tres ministros del gabinete que rompieron sus domingos de Pascua para apoyar al Sr. Major.
John Patten, el Secretario de Estado de Educaci�n, dijo: "Es un hombre de gran resistencia.
Debe ser dif�cil para �l.
A nadie le gusta el nivel de cr�tica que ha estado recibiendo.
Pero continuar� con un programa de 20 a 30 a�os que comenz� en 1979 hasta las pr�ximas elecciones generales ".
Michael Howard, el Secretario del Interior, dijo: "No hay posibilidad de ning�n concurso de liderazgo.
John Major dirigir� al Partido Conservador con �xito a las pr�ximas elecciones generales ".
Desestim� una encuesta de 100 parlamentarios que mostr� que 41 de ellos cre�an que habr�a un cambio de liderazgo.
Lord Wakeham, l�der de la C�mara de los Lores, us� una carta a su antigua circunscripci�n de Colchester South para emitir un llamado general a los conservadores por lealtad al Sr. Major.
"Nuestro apoyo para �l no solo debe ser lealtad a un ganador probado, sino tambi�n respaldar a un hombre que tiene ideas, ambiciones y capacidad para llevar a Gran Breta�a al �xito en el siglo XXI", dijo Lord Wakeham.
Sus llamadas fueron descritas como "ritualistas" por un MP Tory de derecha l�der.
"John Major se ha hecho un gran da�o.
No habr� un golpe de estado la pr�xima semana.
Depender� de c�mo lo hagamos en las elecciones locales y europeas.
Pero habr� un candidato a caballo de acecho este oto�o.
Hay pocas dudas sobre eso ", dijo.
Sin embargo, Tory Right Wingers dijo anoche que apoyar�an al Sr. Major porque no hab�a un candidato de derecha cre�ble.
El Sr. Portillo, de 40 a�os, era demasiado joven y todav�a consideraban al Sr. Heseltine como un intervencionista de izquierda.
Si se produce un desaf�o, presionar� el Sr. Portillo para que se mantenga para asegurar compromisos clave, incluida la entrada brit�nica en una sola moneda europea.
Tres de los parlamentarios conservadores que se rumorea como posibles acechando caballos, Teresa Gorman, Tony Marlow y Sir Richard Body, parec�an descartar.
Las probabilidades contra Major Surviving se han acortado de Evens a 11/10 contra las �ltimas 24 horas.
El primer ministro dijo a los periodistas en su club conservador Huntingdon el fin de semana que no se ver�a obligado a salir por la especulaci�n.
"No me ha detenido en los �ltimos dos a�os.
No me va a detener ahora ".


