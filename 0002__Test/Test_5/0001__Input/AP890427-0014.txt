Bob Thomas ha cubierto Hollywood para Associated Press durante 45 a�os.
Ella nunca supo lo genial que era.
En medio de los egos del mundo del tama�o de un dirigible, Lucille Ball se destac� como una baliza de naranja.
Ella nunca se jact�.
Ella nunca ejerci� su considerable poder.
Ella siempre parec�a asombrada, incluso avergonzada, cuando los honores inevitablemente ven�an en su camino.
Cuando recibi� un premio humanitario de clubes de variedades en un especial de CBS hace cinco a�os, me dijo: `` Llega un momento en su vida en el que obtienes muchas placas.
Es muy halagador y lo seguir�, especialmente si beneficia a los ni�os ".
Cada vez que fue elogiada por `` Amo a Lucy '', siempre respond�a: `` Bueno, todo el cr�dito deber�a ir a (escritores) Madelyn Pugh y Bob Carroll jr ''.
O, `` Desi fue un genio: fue responsable del �xito del programa ''.
O cit� a los coprotagonistas Desi, Vivian Vance y Bill Frawley.
S�, la escritura y la producci�n y el lanzamiento de `` I Love Lucy '' fueron maravillosas, pero la actuaci�n de Lucy agreg� la fuerza cu�ntica.
Su pantomima inspirada no pod�a ser escrita por nadie: el creciente p�nico a medida que se aceleraba la l�nea de ensamblaje de chocolate;Su cat�logo de expresiones de dolor despu�s de probar la medicina de patente que estaba demostrando.
A diferencia de muchas estrellas, Lucille Ball rara vez se entreg� al autoan�lisis.
Pero en una ocasi�n, revel� por qu� sus sentimientos sobre el triunfo `` I Love Lucy '' podr�a haber sido ambivalente: vivir en los �ltimos cinco a�os del espect�culo, dijo: `` Quit� cualquier disfrute que podr�a haber tenido.
El consumo de alcohol y la caro de Desi fueron una verg�enza terrible, mala para la autoestima de una mujer.
La charla en la ciudad era "pobre Lucy".
Como no pod�a enfrentar a la gente, me convert� en un recluso durante m�s de 5 a�os.
`` Nunca demostr� mi infelicidad en el estudio, y en realidad, trabajar juntos fue una bendici�n.
Desi ten�a todo tan bien organizado que era f�cil hacer el trabajo ".
Lucy lleg� tarde a alcanzar su potencial de comedia.
Ten�a 40 a�os cuando `` I Love Lucy '' comenz� en CBS y revolucion� la televisi�n.
Y aunque pas� muchos a�os en Hollywood, `` ninguno de los estudios dise�� comedias para m� como lo hizo MGM para Red Skelton, Paramount por Bob Hope y Sam Goldwyn lo hizo por Danny Kaye '', dijo una vez.
`` La televisi�n era el medio para m�, el medio para cualquiera que quisiera ser bien conocido mucho m�s r�pido que esperar un a�o y medio para que saliera una pel�cula '', dijo en una entrevista tarde en la vida.
`` La televisi�n es un bar�metro instant�neo, especialmente la forma en que lo hicimos.
`` Todo era nuevo.
Nadie sab�a nada sobre televisi�n.
�Nadie estaba all� para decir: "No, no hagas eso, est� mal"!
Nunca supimos lo que estaba mal y lo que estaba bien.
Todos sent�amos nuestro camino.
Desi fue muy innovador y muy consciente del espect�culo y ten�a la intenci�n de seguir adelante y hacerlo: 'eso suena bien, �vamos'!
Entonces fuimos''.
Siempre el cr�dito para otra persona.
Pero millones y millones de observadores de televisi�n saben lo contrario.


