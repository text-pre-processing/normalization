Una efusi�n de simpat�a de los presidentes a los vecinos de la infancia sigui� la muerte de Lucille Ball, pero muchos dijeron que la Reina Madre de la Comedia vivir� a trav�s de las repeticiones de televisi�n y su influencia en la comedia.
El Daffy y pelirrojo comediante cuyos esquemas de hacojas volvieron a su familia de televisi�n loca y encantada de los espectadores durante cuatro d�cadas murieron el mi�rcoles de una arteria abdominal ruptura en el Centro M�dico Cedars-Sinai.
Ten�a 77 a�os y muri� ocho d�as despu�s de la cirug�a card�aca de emergencia.
Se plane� un entierro privado, seg�n los informes, sin servicios funerarios de acuerdo con los deseos de Miss Ball.
`` Dios la tiene ahora y, gracias a la televisi�n, la tendremos para siempre '', dijo el comediante y amigo de toda la vida Bob Hope.
Las tablas de los hospitales fueron inundadas con llamadas de condolencia y miles de cartas despu�s de la muerte de Miss Ball, dijo el portavoz Ron Wise.
La familia de la se�orita Ball solicit� que cualquier flores enviada al centro m�dico en su memoria se distribuya a otros pacientes.
La se�orita Ball mantuvo su sentido del humor incluso cuando estaba enfermo de cr�tica.
Despu�s de su operaci�n la semana pasada, sus primeras palabras para su hija, Lucie, fueron: `` �No sabr�as _ este es el d�a en que me iba a arreglar el cabello ''.
La se�orita Ball hab�a estado mejorando constantemente de la cirug�a card�aca, por lo que su muerte fue un shock.
`` Hab�a estado caminando, su esp�ritu estaba despierto.
Su familia estaba con ella '', dijo Wise.
`` Ella respondi� tan bien como cualquiera podr�a responder a ese tipo de cirug�a ''.
Dijo que se despert� la madrugada del mi�rcoles quej�ndose de un dolor repentino en su espalda, y en cuesti�n de segundos entr� en un paro card�aco completo causado por una aorta ruptura.
La se�orita Ball y su difunto ex esposo, Desi Arnaz, protagonizaron `` I Love Lucy '' de 1951 a 1957.
En el innovador espect�culo, todav�a visto en sindicaci�n en todo el mundo, el difunto Vivian Vance y William Frawley interpretaron a sus vecinos, Fred y Ethel Mertz.
`` Probablemente mucho de lo que ella hizo con lo que hago '', dijo la actriz-comediana Jane Curtin, estrella de la comedia de situaci�n `` Kate and Allie ''.
`` Pero no se pudo ayudar, porque memoric� sus shows ''.
El programa, uno de los m�s populares en la historia de la televisi�n, se distingui� por el momento impecable de Miss Ball, expresiones con cara de goma, lamentos de boca ancha y extravagantes ca�das.
`` Trabajar con Lucy fue como recibir un M.A. o un Ph.D.en comedia '', record� Joan Rivers.
`` Lo que olvid� sobre el momento y la entrega c�micos es m�s de lo que la mayor�a de nosotros aprenderemos en la vida ''.
El comediante Whoopi Goldberg dijo: `` Lucy fue genial porque representaba a todas las personas.
Ella nos mostr� debilidades humanos y nos dio la oportunidad de re�rnos de nosotros mismos ".
La se�orita Goldberg dijo que su episodio favorito implic� una visita de los personajes de `` I Love Lucy '' a Hollywood, donde Lucy y Ethel vieron al actor William Holden en un restaurante.
`` As� reaccionar�amos todos al ver a una estrella de cine en un restaurante '', dijo la se�orita Goldberg.
`` Ella lo estaba mirando en un espejo compacto e intentando mirarlo subrepticiamente.
Y ese es solo un ejemplo de c�mo todos pueden identificarse con Lucy ''.
Bob Rosati, que vive por la calle de la casa de Beverly Hills, Miss Ball, comparti� con su esposo Gary Morton, dijo que estaba viendo una repetici�n de `` Amo a Lucy '' el mi�rcoles por la ma�ana cuando la pantalla se qued� negra y las palabras `` en memoriade Lucille Ball, 1911-1989 '' apareci�.
`` Sent� que hab�a perdido a alguien en mi familia.
No pod�a creerlo '', dijo Rosati, de 40 a�os.
`` Estaba viendo a Lucy como lo hago todas las ma�anas.
Ella es una de las damas m�s admiradas de esta ciudad ''.
Estaba entre un grupo de fan�ticos que se pararon reverentemente fuera de la casa de Miss Ball en Roxbury Drive mientras los floristas entregaban cestas de flores a la casa.
Las camionetas de la gira pasaron en una procesi�n sin parar.
En la caminata de la fama de Hollywood Boulevard, los arreglos florales se colocaron en las dos estrellas de Miss Ball, una para su trabajo cinematogr�fico, uno para la televisi�n.
Budd Friedman, propietario de los clubes de comedia de improvisaci�n, dijo: `` El mundo de la comedia ha perdido a su primera dama.
Su muerte es una p�rdida irreparable ...
a nuestra industria ''.
El actor Danny Devito record� haber recibido una felicitaci�n personal de Miss Ball en 1981 cuando gan� un Emmy por la serie de televisi�n `` Taxi ''.
`` Hizo que hubiera sido bendecido '', record�.
Los admiradores incluyeron al presidente Bush, en el sur de California en el momento del fallecimiento de Miss Ball y el ex presidente Reagan.
`` Su cabello rojo, sus payasadas en la pantalla, su tiempo y su entusiasmo por la vida la convirtieron en una instituci�n estadounidense '', dijo la ex presidenta y Nancy Reagan en un comunicado.
`` Solo la menci�n de su nombre trae una sonrisa.
... amamos a Lucy y la extra�aremos profundamente ''.
`` Lucille Ball pose�a el regalo de la risa '', dijo Bush.
`` Pero ella tambi�n encarnaba un tesoro a�n mayor _ el regalo del amor.
Ella apel� a los impulsos m�s suaves del esp�ritu humano.
Ella no era simplemente una actriz o una comediante.
Ella era Lucy y era amada ''.
`` Para los baby boomers, falta una parte feliz de nuestra infancia '', dijo `` Saturday Night Live '', la comediante Victoria Jackson, quien naci� dos a�os despu�s de `` I Love Lucy '' se sali� del aire como una serie regular.
`` No me hizo re�r a carcajadas, recuerdo, ella me hizo feliz con la vida '.
Dijo el c�mic Tim Conway: `` Vamos a extra�ar a esta dama, pero en realidad no, porque ella todav�a est� aqu�.
Todas esas cosas maravillosas que hicieron juntos ...
Esos maravillosos programas de televisi�n ...
todav�a est�n disponibles ''.
Aunque la se�orita Ball no hab�a visitado su ciudad natal de Jamestown, Nueva York, desde 1956 y no ten�a familia all�, se hab�a mantenido en contacto con amigos.
Irene Rosseti, que ahora vive en Celoron, Nueva York, dijo que la se�orita Ball recientemente le hab�a enviado una carta a ella y a su esposo en su 50 aniversario de bodas.
`` Aunque era una gran estrella, todav�a era Lucy para nosotros '', record�.
El portavoz del hospital Wise dijo que la familia ha pedido que se env�en donaciones a la Fundaci�n Lucille Ball, bajo el cuidado de Irella-Manella, 1800 Ave. of the Stars, Suite 900, Los �ngeles, CA.90067.


