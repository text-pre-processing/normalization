Es la persona m�s importante que jam�s haya salido de esta ciudad, e incluso cuando el mundo considera su aptitud f�sica para la presidencia, Huntington celebra su vida en lo que se anuncia ambiciosamente como la principal exhibici�n de recuerdos de Dan Quayle.
Para aquellos que dicen que Quayle no merece su propia exposici�n, los devotos de Quayle dicen con una mezcla de desaf�o y dignidad que es, despu�s de todo, el vicepresidente de los Estados Unidos.
"Estamos preservando la historia", dice David Schenkel, el coordinador de la exposici�n.
"Dentro de un siglo, alguien puede querer hacer una disertaci�n con �l";Si es as�, Huntington estar� listo.
Los familiares, ex compa�eros de clase y vecinos de Quayle de toda la vida han donado anuarios, boletas de calificaciones, poemas, cartas, su uniforme de liga peque�a, una taza de caf� que bebi�, una silla en la que una vez se par� durante la campa�a presidencial de 1988 y casi cualquier otra cosa que hayaTocado, escribi�, firmado o posado para.
La madre del vicepresidente, Corinne, don� un mech�n de su cabello de beb�.
La reina de Indianapolis 500 de 1972 produjo una imagen de Quayle, luego un asistente del gobernador de Indiana Edgar Whitcomb, que la acompa�a a la carrera.
Y una ex ama de llaves se convirti� en un pedazo de alfombra de 20 a�os que salv� de uno de los pisos de Quayle.
La exposici�n, que se extiende hasta el 20 de mayo, fue reunida por la Fundaci�n Commemorativa de Dan Quayle, una especie de club de fan�ticos convertidos en arquits dedicados a documentar la vida y los tiempos de la 44� y, dicen, vicepresidente menos entendida.
"Ha tomado una mala reputaci�n", dijo Schenkel, miembro de la Junta de la Fundaci�n.
"Si la gente lo conoc�a, ver�an que es una persona normal.
Si tuvieran la oportunidad de sentarse con Dan como vecino, ver�an los valores extremadamente buenos en el hombre ". Estos son los fieles, que desde hace mucho tiempo han dejado de esperar" buena prensa "a su hombre.
En cambio, miden informes de noticias sobre �l por su relativa negatividad, describiendo la mejor cobertura como "m�s optimista" o "a mitad de camino decente";Defienden la edad relativamente joven de Quayle, que cumpli� 44 a�os en febrero ("todo lo que necesita es un peque�o condimento");Su experiencia limitada ("Mira lo que dijeron sobre Truman: era un Haberdasher"), y su pasi�n por el golf ("Tiene que tener alguna forma de relajaci�n").
Pero tienen cuidado de no estirar los superlativos.
La descripci�n "agradable" aparece mucho.
"No es el tonto que los presentadores del programa de entrevistas piensan que es", dice Dottie Watson, presidenta de las mujeres republicanas del condado de Huntington.
"Es una persona muy agradable, una persona capaz.
Todos los que conozco dicen que puede hacerlo "; Eso no quiere decir que algunas personas no tengan algunos consejos amistosos para Quayle.
"Si redujo la velocidad, hablara una octava m�s baja y piense en lo que est� diciendo, no creo que la gente lo tome como lo hace", dijo Jean Anne Drabenstot, due�a de Nick's Kitchen, el restaurante favorito de Quayle.
Hayden Schenkel, presidente del Partido Dem�crata del Condado de Huntington y primo lejano de David Schenkel, dice: "Algunas personas se estremec�an y temblar�an si accidentalmente se convirtiera en presidente de los Estados Unidos.
La mayor�a de los dem�cratas e incluso algunos de los republicanos dir�an que est�n holgados a muerte para tenerlo como vicepresidente, y est�n 100 por ciento detr�s de �l.
Pero con el debido respeto, seguramente esperamos que nada le pase al presidente Bush ". Quayle ha rechazado una invitaci�n para asistir al tributo, citando una agenda ocupada.
No obstante, la mayor�a de los 17,000 residentes de Huntington se han recuperado alrededor de su hijo natal para la exposici�n, que rastrea su vida desde la infancia hasta la facultad de derecho, desde el Senado hasta la Vicepresidencia.
La exposici�n gratuita ha convertido una habitaci�n gris en la biblioteca p�blica de la ciudad en un museo virtual de Dan Quayle.
La pantalla se junta piezas de una vida que hasta la �ltima d�cada era sorprendentemente ordinaria.
Est�n Dan Quayle y su hermano Chris montando a caballo y con sombreros de vaquero, o sentados frente a la chimenea en pijama en Navidad.
Est� Dan Quayle con su clase de Escuela B�blica de Vacaciones, Dan Quayle con el equipo de golf de la escuela secundaria, Dan Quayle a los 5 o 6 a�os de edad con Sharon McEachern cuando fueron nombrados rey y reina del festival de nataci�n en el Fort Wayne Country Club.
Todos son de 50 a�os, muy estadounidense, casi surrealista en su inocencia.
Tambi�n hay ventanas en el funcionamiento de la familia Quayle.
Por ejemplo, su padre, James, firm� las boletas de calificaciones cuando Dan trajo a casa pobres calificaciones, y su madre firm� cuando trajo a casa buenas.
Y hay cartas y poemas que escribi�, que solo demuestran que uno no tiene que ser un Yeats para hacerlo en la pol�tica.
Dan escribi� este verso bastante reflexivo el 22 de noviembre de 1960, cuando ten�a 13 a�os:;Mi pap� es muy muy bonito;Pero no est� hecho de az�car y especias.
A veces act�a como si hubiera sido perturbado;Pero en otras ocasiones es tan alegre como un p�jaro.
La exhibici�n tambi�n incluye el anuncio peri�dico de 1972 de la boda de Dan y Marilyn Quayle, la teja de madera "Quayle & Quayle" que la pareja colg� cuando abri� su pr�ctica legal aqu�, y una variedad de recuerdos de campa�a, incluida la bolsa oficial de enfermedades del aire de 1988 Republican National Republican.Convenci�n.
En �ltima instancia, la Fundaci�n quiere construir un repositorio para los recuerdos.
Ya ha organizado un recorrido a pie por los sitios hist�ricos de Quayle, como la casa blanca de estilo rancho donde pas� sus primeros d�as y la cocina de Nick, donde le gusta pedir lomo, papas fritas y un batido.
"Queremos que todos sepan que es un ganador: no me importa lo que digan los peri�dicos", dijo Marge Hiner, una de las organizaciones de exhibiciones.
"Qu� modelo a seguir para nuestros hijos, saber que puede ser una persona normal, provenir de una familia normal y convertirse en vicepresidente de los Estados Unidos".


