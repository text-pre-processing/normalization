El presidente Corazon Aquino acord� permitir que Fernando Marcos regrese a Filipinas para que el gobierno pueda probarlo por cargos de robar miles de millones de d�lares, dijeron los peri�dicos el s�bado.
El Bolet�n de Manila y la Cr�nica de Manila citaron al secretario de prensa de la Sra. Aquino, Teodoro Benigno, diciendo que la Sra. Aquino asegur� a dos abogados suizos antes de visitar Suiza a principios de este mes.
Los informes no dijeron cuando los cargos penales contra Marcos, el predecesor expulsado de Aquino, se presentar�n o cu�ndo la Sra. Aquino le permitir� regresar.
La Sra. Aquino se ha negado a permitir que Marcos ingrese a Filipinas, citando razones de seguridad.
Marcos vive en el exilio en Hawai.
El gobierno de la Sra. Aquino ha dicho que presentar� cargos penales contra Marcos, a quien ha acusado de robar hasta $ 10 mil millones en fondos del gobierno.
Pero el gobierno no lo ha hecho debido a la negativa de la Sra. Aquino a dejar que Marcos ingrese al pa�s.
La ley filipina no permite el juicio en ausencia.
Los abogados suizos est�n ayudando al gobierno de Filipino a trazar dinero que Marcos y su familia supuestamente colocaron en cuentas bancarias suizas.
Marcos ha estado viviendo en Hawai desde que fue derribado por la revuelta civil-militar que llev� a la Sra. Aquino al poder en febrero de 1986.
El gobierno suizo ha congelado los activos de Marcos y sus asociados y no ha publicado informaci�n sobre ellos al gobierno filipino debido a los desaf�os legales planteados por los abogados de Marcos y los bancos suizos.
Los abogados suizos no identificados dijeron que la informaci�n no se entregar� `` a menos y hasta que haya una garant�a de que se presentar�n cargos formales contra �l '', dijo Benigno.
La ley suiza permite el levantamiento de las leyes de secreto bancario solo en caso de enjuiciamiento penal.
La Sra. Aquino ha dicho que permitir�a a Marcos regresar ahora solo si �l y su familia juraron lealtad a su gobierno y devolvieron el dinero que supuestamente rob� durante su regla de 20 a�os.
Ella dijo m�s tarde que cumplir�a con cualquier decisi�n judicial si los abogados de Marcos en Filipinas plantearon el asunto de su regreso a los tribunales.
Benigno fue citado diciendo que la Sra. Aquino `` tiene que aceptar la posibilidad de que Marcos regrese a casa ''.


