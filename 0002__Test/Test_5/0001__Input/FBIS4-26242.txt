BFN [Texto] Changsha, 19 de junio (Xinhua) - Esta capital de la provincia de Hunan del centro de China ha sido golpeada por inundaciones a ra�z de una sucesi�n de tormentas de lluvia en los tramos superior y medio del r�o Xiangjiang.
El nivel del agua del r�o, que fluye a trav�s de Changsha, todav�a est� aumentando bruscamente, seg�n fuentes locales.
A las cuatro de la tarde de esta tarde, el nivel registrado en la estaci�n de monitoreo Changsha hab�a aumentado a 38.93 m, unos 0.56 m m�s altos que la cifra m�s alta jam�s registrada.
La inundaci�n ha causado serias p�rdidas a las personas a lo largo de ambas orillas del r�o, especialmente en Changsha, seg�n las fuentes.
En las primeras horas de ayer, seis pueblos y un campo de pesca en el municipio de Yulushan se inundaron cuando un terrapl�n se derrumb�.
La mayor�a de las 15,000 personas afectadas por la inundaci�n all� han sido evacuadas a lugares m�s seguros.
A las seis de la tarde de ayer, 19 diques en el �rea de Changsha se hab�an derrumbado, y un total de 128,000 residentes urbanos y rurales se hab�an visto afectados.
Un colapso parcial del terrapl�n de Grand Longwangang en el campo de pesca Xihu en el distrito de Wangyuehu de Changsha ha puesto en peligro las vidas y la propiedad de 60,000 personas.
Pero gracias a los esfuerzos de rescate concertados de 2.000 cuadros locales, residentes y soldados no hubo p�rdida de vidas.
El gobierno de la ciudad de Changsha ha pedido a los residentes dentro de las �reas en peligro de extinci�n que se trasladen y retiren sus propiedades a lugares m�s seguros, en caso de que ocurra las inundaciones m�s graves.
A las primeras horas de esta ma�ana, las carreteras y las superficies de los puentes en el distrito de Wangyuehu hab�an sido inundados.
Las aguas de la inundaci�n tambi�n hab�an llegado al bulevar a lo largo del r�o Xiangjiang, la calle Xiahe y la parte occidental de la calle Zhongshan en Changsha propiamente dicho.
Hasta ahora, m�s de 500,000 soldados y residentes locales se han mudado para ayudar a combatir las inundaciones.
Mientras tanto, el Comit� de la Ciudad de Changsha del Partido Comunista celebr� una reuni�n de emergencia, instando a todos los departamentos y a la gente de toda la ciudad a llevar adelante el esp�ritu de la lucha continua y unirse como uno en la lucha contra las inundaciones.
Wang Maolin, Secretario del Comit� del Partido Provincial de Hunan, que ahora se encuentra en la ciudad sur de la provincia de Zhuzhou para dirigir los esfuerzos contra la inundaci�n all�.
En una llamada telef�nica al gobierno de la ciudad de Changsha, pidi� a los funcionarios locales que adopten medidas efectivas, garanticen la seguridad de las vidas y la propiedad de los residentes de Changsha y logren un �xito general del trabajo para conquistar la inundaci�n.


