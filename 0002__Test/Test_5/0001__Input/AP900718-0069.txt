El gobierno impuso hoy el gobierno federal directo en Cachemira, donde las fuerzas de seguridad indias est�n luchando por dejar una rebeli�n de los separatistas musulmanes.
La acci�n sigue al matar el martes de al menos 29 militantes musulmanes por las fuerzas de seguridad indias.
Mientras tanto, en la capital paquistan� de Islamabad, India y Pakist�n abrieron las negociaciones hoy destinadas a reparar relaciones amenazadas por la rebeli�n en la disputada regi�n norte.
Las relaciones entre Pakist�n e India empeoraron en enero cuando los soldados indios tomaron medidas en�rgicas contra los militantes musulmanes en Cachemira que buscan la independencia de la India o la uni�n con Pakist�n.
Al menos 792 personas han sido asesinadas en Cachemira desde entonces.
India ha acusado a Pakist�n de armarse y entrenar a los militantes, un cargo que Pakist�n ha negado.
Antes de que se impusiera el gobierno federal directo en el estado de Jammu-Kashmir, fue administrado temporalmente por un gobernador, un representante federal, desde el 18 de enero.
El anuncio del gobierno federal reflej� la creencia de que el estado de Jammu-Kashmir era demasiado inestable para ser dirigido por un gobierno local electo.
El martes, al menos 29 militantes fueron asesinados por las fuerzas de seguridad indias en Cachemira, la parte norte de Jammu-Kashmir dominada por musulmanas y el centro de la campa�a secesionista.
Las autoridades, que no pueden ser identificadas bajo reglas informativas, dijeron que 21 militantes fueron asesinados en una batalla con la Fuerza de Seguridad Fronteriza en Poonch, a 400 millas al noroeste de Nueva Delhi.
Horas despu�s, en la misma �rea, las tropas mataron a ocho militantes que se deslizaron a trav�s de la frontera desde Pakist�n, dijeron los funcionarios.
Los guardias tambi�n arrestaron a cuatro militantes y confiscaron 14 rifles autom�ticos, dos ametralladoras y dos lanzadores de cohetes, dijeron los funcionarios.
Los funcionarios indios sostienen que los militantes de Cachemira se cruzan regularmente a Pakist�n a trav�s de la frontera y regresan por la misma ruta despu�s de recibir el entrenamiento de armas.
Pakist�n niega que est� entrenando o ayudando a los militantes, pero llama a la lucha separatista una lucha por la autodeterminaci�n.
India y Pakist�n han peleado dos guerras anteriores sobre Cachemira, en 1948 y 1965.
Ambos fueron ganados por India.
Alrededor del 65 por ciento de los 6 millones de personas de Jammu-Kashmir son musulmanes, lo que lo convierte en el �nico estado mayoritario musulm�n de la India.
A nivel nacional, los musulmanes representan el 12 por ciento de los 880 millones de personas de la India.
Los hind�es representan el 82 por ciento de la poblaci�n.
Los separatistas de Cachemira exigieron anteriormente uni�n con Pakist�n, un pa�s isl�mico, pero recientemente han estado luchando por la independencia total y el estado de un pa�s neutral.


