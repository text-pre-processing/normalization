El gerente de la tienda de Wal-Mart en esta comunidad en las afueras de Dallas cuenta una historia que da una idea del hombre considerado responsable de un fen�meno minorista.
El gerente Don Bost recuerda un d�a el verano pasado cuando un secretario telefone� de la sede corporativa de Wal-Mart Stores Inc. en Bentonville, Ark. El presidente Sam Walton estaba a punto de aterrizar en un aeropuerto cercano y el secretario pregunt� si alguien podr�a buscarlo y traerlo y traerlo y traerlo�l a la tienda De Soto.
Pagar llamadas inesperadas en las tiendas es una forma en que Walton se ha mantenido en contacto cercano con los pasos cotidianos.
Su ejemplo es seguido por otros ejecutivos de Wal-Mart y la mayor�a hace un punto de pasar mucho tiempo en el campo.
Dice Ed Nagy, gerente de distrito para 9 tiendas en el �rea metropolitana de Dallas, `` No es como 'uh-oh, alguien viene', porque siempre est�n fuera '".
Seg�n todas las cuentas, la personalidad de Sam Walton impregna las operaciones en toda la empresa.
Todos, desde el presidente y director de operaciones de Wal-Mart, David Glass, hasta un trabajador de la tienda por hora, pueden citar f�cilmente el evangelio seg�n Sam, como se le llama.
Parafraseando las principales reglas de venta minorista de SAM, `` La regla n. � 1 es que el cliente siempre tiene raz�n.
La Regla No.2 dice que en caso de que el cliente est� equivocado, vuelva a la Regla No.1 ''.
Si los principios suenan anticuados y caseros, son porque as� es Sam, las personas que lo conocen dicen.
�l evita el centro de atenci�n que el �xito de su compa��a le ha arrojado y ha decidido rechazar casi todas las solicitudes de prensa de entrevistas.
Sin embargo, est� feliz de charlar con la gente en sus tiendas.
Tambi�n le recuerda regularmente a Wal-Mart `` Associates '' que la hospitalidad a los clientes es crucial.
Lo predica en persona o a trav�s de una conexi�n satelital de alta tecnolog�a con sus tiendas.
Los empleados de Wal-Mart se hicieron conocidos como Asociados bajo un esfuerzo de la empresa para demostrar que se preocupaba por los empleados, en parte para frustrar un esfuerzo de organizaci�n sindical a fines de la d�cada de 1960.
Uno de los temas m�s sensibles para Walton es la fortuna que ha acumulado.
Un asociado cercano dice que Sam `` probablemente dar�a algo '' para no ser nombrado en la lista de la revista Fortune de las personas m�s ricas del pa�s.
La revista ha estimado que la participaci�n de su familia inmediata en las acciones de Wal-Mart tiene un valor de aproximadamente $ 6.8 mil millones.
`` Sam Walton est� m�s interesado en su rev�s que en su cuenta bancaria '', dice Don Shinkle, portavoz de Wal-Mart.
Samuel Moore Walton cumple 71 a�os el 29 de marzo.
Naci� en Kingfisher, Okla.
, y creci� como hijo de un banquero de mortogages de granja.
Se gradu� de la Universidad de Missouri con un t�tulo en econom�a en 1940 y se uni� a J.C. Penney Co. como aprendiz de gesti�n en Des Moines, Iowa.
Despu�s de cumplir tres a�os en el ej�rcito, fue a Arkansas y abri� una tienda de cinco y dime de Ben Franklin, convirti�ndose en el mayor franquiciado de la compa��a.
Las semillas del Imperio Wal-Mart se sembraron cuando Sam y su hermano James L., o Bud, abrieron la primera ciudad de descuento de Wal-Mart en 1962.
El resto probablemente ganar� una entrada en la historia del comercio minorista de EE. UU.
Bud es vicepresidente senior y miembro de la junta de Wal-Mart.
Tres de los cuatro hijos adultos que Sam y su esposa Helen ten�an involucrados con la compa��a en diversas capacidades.
Sam no ha disminuido mucho, disfrutando de la caza y el tenis tanto como siempre, y se dice que no sufre despu�s de los efectos de un combate con leucemia, ahora en remisi�n.
Pasa menos tiempo en la oficina desde que entreg� la mayor�a de las responsabilidades a David Glass.
Sam sigue siendo presidente de la junta.
El vidrio no duda cuando se le pregunta si Wal-Mart puede florecer sin Sam.
La respuesta es un s� enf�tico porque, dice, `` tenemos grandes personas ''.
La familia Walton siempre estar� involucrada, agrega.
Al comentar sobre el futuro, Glass dice: `` Nos juzgaremos por lo que producimos hoy.
Ma�ana, lo que hemos hecho en el pasado no importar� ''.


