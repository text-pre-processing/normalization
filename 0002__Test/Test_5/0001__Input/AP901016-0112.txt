Cr�tico y novelista A.S.Byatt el martes gan� el Premio Booker, el premio literario m�s prestigioso de Gran Breta�a, por su historia de dos j�venes acad�micos que investigan la vida de un par de poetas victorianos imaginarios.
Los cinco jueces deliberaron dos horas antes de otorgar el premio de $ 39,000 a Antonia Byatt por `` posesi�n '', uno de los seis finalistas en la competencia de 21 a�os.
El ganador de Booker del a�o pasado fue Kazuo Ishiguro por su novela, `` Los restos del d�a ''.
El ejecutivo de televisi�n Sir Denis Forman, presidente del panel del juez, anunci� el premio en un banquete en el Guildhall isabelino de Londres, sede del Lord Mayor de la ciudad.
`` Hubo un apoyo individual muy fuerte para varios libros en la lista corta y finalmente 'posesi�n' por A.S.Byatt fue el ganador por una votaci�n mayoritaria '', dijeron los jueces.
`` El panel era un�nime, sin embargo, al aceptar que el est�ndar de las otras cinco novelas era excepcionalmente alto y esto tom� su decisi�n inusualmente interesante e inusualmente dif�cil ''.
La Sra. Byatt, quien a principios de este mes gan� el Premio Lingus de Irish Times-Aer de $ 43,000 por ficci�n internacional por el mismo trabajo, ha publicado cinco novelas desde 1964.
En `` posesi�n '', cuenta c�mo dos estudiantes de posgrado juntan la relaci�n y las vidas de dos poetas victorianos imaginarios de fragmentos de sus cartas y retraciendo los viajes de los poetas en Inglaterra y Francia.
La influencia del pasado en la vida es un tema en todo el libro.
La Sra. Byatt, nacida en 1936, es hija de una abogada y hermana de la novelista Margaret Drabble.
Su primera novela, `` Shadow of a Sun '', se refer�a a los esfuerzos de una mujer para escapar de la sombra de su padre novelista.
Un trabajo de 1978, `` The Virgin in the Garden '', hace alusiones aleg�ricas a Shakespeare y otros genios literarios ingleses en una historia de una obra realizada en una casa de campo.
La Sra. Byatt ense�� literatura inglesa y estadounidense en University College de Londres hasta 1983, cuando dej� de escribir a tiempo completo.
Otros finalistas de este a�o fueron Pen�lope Fitzgerald, ganador del premio Booker de 1979, con `` la puerta de los �ngeles '', el novelista irland�s John McGahern `` Entre Women 'Women', '' `` Solomon Gursky, nacido en Canad�, '`` Solomon Gursky estaba aqu�' ',' '`` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` de Brian Moore, de Brian Moore.Mentiras de silencio, 'y `` una aventura terriblemente grande' 'de Beryl Beryl Bainbridge.
Fueron elegidos de una lista de 113 libros publicados en el Reino Unido en 1989.
El Premio Booker est� patrocinado por Booker, un negocio internacional de alimentos y agricultura, y administrado por Book Trust.
Es el premio literario principal de Gran Breta�a.


