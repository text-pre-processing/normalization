Parientes y amigos afligidos se reunieron en Memorial Services el s�bado para llorar a los cinco ni�os escolares asesinados por un vagabundo de camuflaje y suicida.
`` Aunque hay algunas personas en los Estados Unidos que hacen cosas malas, la mayor�a de las personas en los Estados Unidos hacen cosas buenas '', dijo Patricia Busher, directora de la Escuela Primaria Cleveland, donde los asesinatos y las heridas de 29 estudiantes yUn maestro ocurri� el martes pasado.
`` La tragedia que ocurri� en nuestra escuela podr�a haber sucedido en cualquier lugar.
No hay forma de protegernos de este tipo de tragedia ...
Esta tragedia sin sentido.
Con el tiempo, parte de la vanguardia del dolor desaparecer� '', dijo Busher.
Su discurso, en un servicio conmemorativo al que asistieron 200 personas para los cuatro ni�os camboyanos asesinados, fue traducido mientras hablaba en el idioma camboyano.
`` La tragedia te ha seguido '', dijo el senador estatal John Garamendi a las familias, refugiados de un pa�s donde m�s de 1 mill�n de personas murieron en ejecuciones y hicieron cumplir las dificultades despu�s de una adquisici�n comunista de 1975.
El servicio, celebrado en la Iglesia Metodista Central, fue para Oeun Lim, de 8 a�os, Rathanan, o Ram Chun, de 8 a�os, y Sokhim AN, de 6 a�os.
Durante la ma�ana, se llev� a cabo una misa f�nebre cat�lica para Thuy Tran, de 6 a�os, hija de refugiados vietnamitas.
Se plane� otro servicio conmemorativo para los cuatro camboyanos el domingo en una morgue.
El gobernador George Deukmejian plane� hablar en un servicio el lunes por la ma�ana para los cinco ni�os en el Auditorio C�vico de Stockton.
Se pidi� a las personas que asist�an a ese servicio que seguiran una tradici�n cultural del sudeste asi�tico y usaban cintas en blanco y negro.
Patrick Edward Purdy, quien se convirti� en un arma en s� mismo despu�s de disparar m�s de 100 rondas de un rifle de asalto semiautom�tico AK-47 en la Primaria Cleveland, donde hab�a asistido a la escuela primaria, fue enterrado el viernes en una tumba sin marcar en una parcela familiar en Lodi.
Diez de los ni�os heridos y el maestro herido permanecieron hospitalizados el s�bado.
Tres ni�os fueron dados de alta el viernes.
La polic�a de Stockton dijo que han localizado a todos los parientes cercanos de Purdy, excepto su madre, identificada como Kathy Snyder, de 43 a�os.
La polic�a dijo que ha tenido cuatro apellidos diferentes y que cuando renov� su licencia de conducir hace un a�o, us� una direcci�n en La Mesa que result� ser un cobertizo qu�mico.


