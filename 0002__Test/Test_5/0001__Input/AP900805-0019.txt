La radio del nuevo gobierno instalado de Kuwait instalado afirm� que las tropas iraqu�es comenzaron a retirarse de Kuwait el domingo, pero Estados Unidos dijo que dudaba que una retirada estaba realmente en marcha.
El anuncio de radio se produjo tres d�as despu�s de que Iraq invadi� Kuwait y un d�a despu�s de que Bagdad dijo que hab�a reemplazado la monarqu�a derrocada del reino rico en petr�leo con un nuevo gobierno compuesto por nueve oficinas del ej�rcito kuwait�.
Iraq tambi�n dijo el s�bado que estaba creando un nuevo ej�rcito kuwait� y que los 100,000 iraqu�es, el n�mero aproximado de soldados con los que Bagdad se hizo cargo de Kuwait, se hab�a ofrecido como voluntario para la nueva fuerza militar.
En otros desarrollos: _ La Casa Blanca continu� buscando el retiro inmediato e incondicional de todas las tropas iraqu�es, y agradeci� a las muchas naciones que han condenado la invasi�n e impuestas sanciones econ�micas a Bagdad.
Despu�s de varios d�as de debate, Jap�n prohibi� todas las importaciones de petr�leo de Irak y Kuwait y detuvo las exportaciones japonesas a ambos pa�ses.
Tokio dijo que suspender�a todos los pr�stamos a Irak y Kuwait, prohibir�a la inversi�n japonesa all� y congelar�a los activos iraqu�es en Jap�n.
Jap�n, que importa el 99 por ciento de su petr�leo, hab�a dependido de Irak y Kuwait durante m�s del 11 por ciento de sus importaciones de petr�leo.
La Casa Blanca elogi� la medida de Jap�n y dijo que el primer ministro Toshiki Kaifu llam� al presidente Bush el domingo en Camp David para informarle sobre las sanciones.
Seg�n los informes, las decenas de miles de tropas iraqu�es permanecieron en masa cerca de la frontera de Kuwait con Arabia Saudita, Washington dijo el domingo que no hab�a indicios de que ning�n soldado se hubiera mudado a Arabia Saudita.
La Casa Blanca dijo que 11 trabajadores petroleros estadounidenses se detuvieron despu�s de que la invasi�n hab�a sido tra�da a Bagdad, habl� con EE. UU.
Funcionarios de la embajada, y estaban en buena forma.
Dijo que explica a todos los estadounidenses desaparecidos.
Un informe no confirmado dijo que un avi�n de combate de la Fuerza A�rea del Emir en Kuwait bombarde� una sede militar iraqu� cerca de la ciudad de Kuwait.
No estaba claro de d�nde proviene el avi�n o si su ataque reportado caus� alg�n da�o.
La radio del nuevo gobierno de Kuwait, monitoreado en Bahrein, cit� a su `` corresponsal militar '' diciendo que el retiro comenz� a las 8 a.m.
(1 a.m.
EDT).El presidente iraqu�, Saddam Hussein, permiti� que los periodistas con sede en Bagdad se llevaran al sur a Basra durante la noche para observar las primeras tropas iraqu�es regresan a casa.
Pero en la Casa Blanca, el secretario de prensa Marlin Fitzwater dijo el domingo por la ma�ana: `` No tenemos informaci�n independiente o confirmable sobre este informe (del comienzo del retiro).
Las acciones de Saddam Hussein en el pasado han planteado serios escepticismo sobre sus intenciones ".
Iraq declar� anteriormente que el gobierno provisional instalado por Bagdad es capaz de garantizar la seguridad en el pa�s ocupado.
Bagdad Television ha dicho que el nuevo gobierno est� encabezado por el Col.
Ala Hussein Ali y que ocupa los puestos del primer ministro, el comandante en jefe de las Fuerzas Armadas, el Ministro de Defensa y el Ministro del Interior.
Los otros ocho miembros eran tenientes coroneles y mayores.
Pero la embajada de Kuwait en Amman, Jordania, descart� el domingo al nuevo gobierno como `` una lista de nombres falsos '' y dijo que Ali es el yerno de Saddam.
Un informe similar apareci� en uno de los principales peri�dicos de El Cairo, Al-Ahram.
Cit� la embajada de Kuwait en T�nez que dijo que Ali trabaja con la administraci�n de Saddam en Bagdad.
La agencia de noticias iraqu� llev� a una declaraci�n de Kuwait-Datelined que dijo que era del gobierno provisional, negando a cualquiera de sus miembros eran iraqu�es.
En Jordania, el primer ministro Mudo Badran dijo que su naci�n no reconocer� al nuevo gobierno porque eso obstaculizar�a los esfuerzos �rabes para poner fin al conflicto.
Mientras tanto, los buques de guerra estadounidenses, franceses y brit�nicos, al vapor hacia el Golfo P�rsico.
El presidente Bush dijo el viernes que si Iraq se mudara contra Arabia Saudita, atacar�a a los Estados Unidos
`` intereses vitales ''.
El s�bado, Bush dijo en Washington que el uso de la fuerza para resolver la crisis sigue siendo una opci�n.
Arabia Saudita env�a 1,2 millones de barriles de petr�leo a los Estados Unidos diariamente.
Fitzwater dijo el domingo que no hab�a indicios de que las fuerzas iraqu�es se hab�an mudado a Arabia Saudita, a pesar de que se ha informado que decenas de miles de ellas est�n a 5 a 10 millas de la frontera saudita.
En Washington, el embajador iraqu� Mohammed al-Mashat el s�bado neg� los informes estadounidenses y otros informes extranjeros que sugirieron que Iraq planea invadir Arabia Saudita.
Saddam orden� la invasi�n de Kuwait despu�s de acusarlo de robar petr�leo de un campo que se extiende a horcajadas en su frontera en disputa.
Tambi�n culp� a Kuwait por una ca�da en los precios del petr�leo que le cost� a Iraq $ 14 mil millones, diciendo que contribuy� al exceso de petr�leo haciendo trampa en sus cuotas de producci�n de carteles del petr�leo de la OPEP.


