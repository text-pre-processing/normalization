Una enorme nube de vapor cargado de cenizas se elev� hasta 11,500 pies sobre el volc�n hoy, extendiendo cenizas en un radio de seis millas, dijo el Instituto Filipino de Vulcanolog�a y Sismolog�a.
Si est� en erupci�n, la ceniza caliente mortal producida por el volc�n podr�a barrer a trav�s de la base de aire de Clark contigua, dijo.
Tambi�n est�n en peligro los 350,000 residentes de la ciudad de Angeles, donde se encuentra la base a�rea, a unas 50 millas al norte de Manila.
Incluso si el volc�n no estalla, es probable que la amenaza de erupci�n mantenga la base cerrada al menos durante semanas, dijo Lipman.
Los cient�ficos dudan de que sea m�s que una coincidencia que la erupci�n ocurra al mismo tiempo que el volc�n Monte Unzen en Jap�n, dijo.
Los cinco cient�ficos del USGS se encuentran entre las pocas personas restantes en la Base A�rea Clark, que fue evacuada junto con miles de filipinos que viven cerca del volc�n.
Los investigadores, junto con 10 cient�ficos filipinos y sus equipos, se mudaron hoy a un b�nker en el borde este de la base a�rea, a 12 millas del volc�n, donde deber�an estar a salvo de las avalanchas de cenizas.
El monte Pinatubo es t�pico de cientos de volcanes que conforman el "anillo de fuego" en las costas del Pac�fico de Asia y las Am�ricas.
(Estos volcanes, incluido el Monte Lassen en el condado de Shasta, y Mount Rainier y Mount St. Helens en Washington, todos en la Cordillera Cascade, surgen donde una de las inmensas placas de la costa de la Tierra est� buceando lentamente bajo otra);Se cree que la �ltima erupci�n de Pinatubo, hace 600 a�os, produjo al menos tanta roca fundida, medio kil�metro c�bico, como lo hizo Mount St. Helens cuando estall� en 1980.
Hasta que se llam� la atenci�n de los volcan�logos por un grupo de terremotos debajo del volc�n esta primavera, no se hab�a estudiado bien.
Otros volcanes recientes que estallaron en la misma escala fueron El Chichon en M�xico en 1983 y Nevado del Ruiz en Colombia en 1985.
Si tiene una erupci�n masiva, el principal medio de Pinatubo para causar la muerte y la destrucci�n ser�an avalanchas infernales de cenizas calientes: 1,000 grados o m�s, fluyendo y dudando a hasta 100 mph, envolviendo y dando vueltas a todo en su camino.
"En el peor de los casos, una erupci�n realmente grande, toda la base de la Fuerza A�rea Clark podr�a estar cubierta" con cenizas brillantes, dijo Lipman.
Solo ser atrapado en una nube de cenizas que soplan la avalancha es fatal.
Eso es lo que mat� a 38 personas en Jap�n la semana pasada.
Tal ceniza tambi�n causar�a estragos en los motores de aviones en la base a�rea, dijo Lipman.
Los aviones que vuelan a trav�s de nubes de cenizas volc�nicas, indistinguibles en el radar de las nubes de lluvia, corren el riesgo de que sus motores se estancenan por la arena.
Hasta ahora, los vientos han volado a cenizas lejos de la base.
Pero los vientos del patr�n meteorol�gico del monz�n de verano, que se espera que comiencen cualquier d�a, enviar�an cenizas directamente hacia la base.
La actividad del volc�n, medida por sism�metros que detectan leves terremotos en su sistema de plomer�a de roca fundida, est� aumentando de una manera que sugiere que una gran erupci�n es inminente, dijo Lipman.
Los cient�ficos tambi�n est�n monitoreando los metros de inclinaci�n en las laderas del volc�n, para verlo hincharse mientras el magma se acumula en el interior.
En los d�as despejados vuelan sobre el cr�ter para medir las emisiones de azufre.
Sus predicciones pueden ser una falsa alarma: el volc�n puede haber hecho lo mejor.
Pero tomar�a varias semanas de terremotos y emisiones de azufre antes de que los cient�ficos confiaran razonablemente de que los militares podr�an regresar a la base.
Los volcan�logos no conocen ninguna raz�n por la que la actividad del volc�n podr�a estar vinculada al volc�n en Jap�n, que es de tama�o comparable.
"Ning�n cient�fico acreditado que conozco" est� estableciendo una conexi�n entre los dos, dijo Lipman.
Se�al� que m�s de 500 volcanes han estallado en tiempo hist�rico.
"Dos de 500 no son grandes".


