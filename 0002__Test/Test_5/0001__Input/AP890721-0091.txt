Los mineros de carb�n en Siberia terminaron su huelga hoy despu�s de exigir promesas de mejores alimentos, viviendas y condiciones de trabajo, pero la ola de disturbios que lanzaron continu� en otras regiones clave del carb�n.
Algunos de los 150,000 mineros que se hicieron huelga la semana pasada en la regi�n de la cuenca de carb�n de Kuznetsk de Siberia regresaron al trabajo el jueves, y el resto regres� hoy, dijeron los l�deres de huelga y la agencia oficial de noticias TASS.
A pesar de los anuncios de que el presidente Mikhail S.
Las amplias concesiones de Gorbachev _ incluyendo otorgar a los mineros un mayor control sobre su industria _ Aplicar en todo el pa�s, cientos de miles de mineros se mantuvieron fuera del trabajo en otro lugar.
En la regi�n de carb�n m�s grande, la cuenca de carb�n de Donetsk de Ucrania, m�s de 300,000 mineros estaban en huelga, dijo a Izvestia el viceministro de la industria del carb�n, Alexander P. Fisun.
En la regi�n de Karaganda de la Rep�blica del Sur de Kazajst�n, el tercer �rea productora de carb�n de la Uni�n Sovi�tica, las 26 minas estaban paralizadas, y los mineros hicieron m�s de 70 demandas, dijo Izvestia.
Tambi�n se informaron huelgas en la cuenca de carb�n de Pechora en la Rep�blica Rusa, y Chervonogrado en el oeste de Ucrania, dijeron medios oficiales.
Los disturbios laborales son los peores de la Uni�n Sovi�tica en seis d�cadas.
Gorbachov ha expresado simpat�a por los huelguistas y ha dado gran parte de la culpa de las condiciones de trabajo y vida de los mineros a los l�deres de los sindicatos sancionados oficialmente.
A�n as�, �l y otros l�deres comunistas han advertido que si los ataques no terminan r�pidamente, la escasez de carb�n cerrar� gran parte de la industria del pa�s.
El mi�rcoles, el Comit� de huelga de Kuznetsk y los miembros de una comisi�n gubernamental de alto nivel firmaron un acuerdo que cumpli� con muchas demandas.
A los trabajadores se les prometi� salarios m�s altos, mayores entregas de carne, az�car, jab�n, ropa, muebles y otros bienes de consumo, m�s construcci�n de viviendas y _ m�s importante _ una voz mayor para administrar su industria.
`` Es una victoria sobre el sistema que hemos tenido en la Uni�n Sovi�tica durante los �ltimos 70 a�os, un sistema en el que trabajamos duro pero tenemos poco a cambio '', dijo Pyotr A. Menayev, un ingeniero de Taldinski severnyAbra la mina de pozo en las afueras de Prokopyevsk.
El miembro del comit� de huelga, Vyacheslav G. Akulov, enfatiz� que los mineros hab�an acordado `` interrumpir la huelga, no terminar con ella ''.
Dijo que los mineros mantendr�an al gobierno en su palabra: `` Si el gobierno no guarda sus promesas, volveremos a huelga ''.
El comit� de huelga de 26 miembros se reuni� hoy con media docena de representantes al Parlamento Nacional y pidi� que las elecciones municipales avanzaran desde la primavera hasta la primera mitad de noviembre.
Las �ltimas elecciones locales en Kuznetsk se celebraron hace dos a�os, antes de que las reformas de Gorbachov hicieran las elecciones m�s democr�ticas al ofrecer m�ltiples candidatos.
La ola de huelga comenz� hace 11 d�as en Kuznetsk, el segundo campo de carb�n m�s grande del pa�s.
Tass dijo el jueves por la noche que las huelgas hab�an terminado en seis minas en la regi�n de Donetsk, pero que la mayor�a de las minas en el �rea permanecieron en huelga.
Los mineros en tres ejes en Chervonogrado, cerca de la frontera polaca, se unieron a la huelga el jueves por la noche, dijo Anatoly M. Dotsenko, portavoz del grupo de relojes Helsinki ucraniano.
La industria del carb�n emplea a m�s de 1 mill�n de trabajadores.
Tass cit� hoy al ministro de carb�n, Mikhail I. Shchadov, diciendo que el gobierno acord� hacer que todas las operaciones de carb�n sovi�ticas sean independientes econ�mica y legalmente.
Dijo que las autoridades hab�an acordado pagos adicionales por turnos de noche y noche y dar a los domingos de los trabajadores.
A partir del 1 de agosto, las minas y otras industrias en el `` complejo minero de carb�n '' obtendr�n el derecho de vender los precios negociados en los productos de la URSS y en el extranjero que hacen m�s all� del plan estatal ", Tass cit� el acuerdo comoestipulaci�n.
Dijo que los gerentes locales podr�n aumentar los precios del carb�n para reflejar los costos de producci�n reales.
Gorbachev est� tratando de terminar la pr�ctica sovi�tica desde hace mucho tiempo de establecer precios de los bienes que no tienen relaci�n con su valor de libre mercado y alentar y terminar con el control central de la econom�a.
Akulov dijo que a partir del 1 de enero, la cuenca de carb�n de Kuznetzk se volver� econ�micamente aut�noma.
`` El ministerio de miner�a de carb�n continuar� administrando las minas, pero los mineros tendr�n una voz mucho mayor estableciendo los niveles de producci�n y asignando las ganancias '', dijo.
`` Nuestra pol�tica se ha vuelto m�s libre bajo perestroika '', dijo Akulov sobre el programa de reforma econ�mica de Gorbachov.
`` Pero nuestras condiciones de vida no han mejorado ''.
Hasta que Gorbachev alcanz� el poder en 1985, los ataques no fueron tolerados.


