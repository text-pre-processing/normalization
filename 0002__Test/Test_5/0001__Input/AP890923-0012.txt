El bombardeo del IRA que mat� a 10 hombres y destroz� un edificio en la Royal Marines Music School indign� a los brit�nicos y agit� las recriminaciones sobre los est�ndares de seguridad en la escuela.
Una gran explosi�n en la sala de personal de un cuartel el viernes por la ma�ana nivel� el edificio de tres pisos cuando los hombres de la banda tomaron un descanso para tomar un caf� entre las sesiones de pr�ctica en el campo de desfile de la escuela.
Doce de las 22 personas heridas fueron hospitalizadas, ocho de ellas en estado grave, dijo la polic�a.
Dijeron que nueve de los muertos eran de bandas y otro hombre no se identific� de inmediato.
Docenas de casas vecinas en la tranquila ciudad costera del sureste de Deal fueron da�ados.
Aunque los alumnos de la escuela tienen entre 16 y 20 a�os, el coronel John Ware, director principal de m�sica, dijo que los hombres de banda asesinados fueron entrenados, m�sicos adultos.
Ten�an filas militares, pero ninguno hab�a sido entrenado como soldado de lucha.
El exhibido ej�rcito republicano irland�s, involucrado en una campa�a de 20 a�os para expulsar a los brit�nicos de la provincia de Irlanda del Norte, se dio cuenta de una llamada telef�nica a Irlanda Internacional, una agencia de noticias de Dubl�n.
Los residentes del �rea contiguo a la escuela de marines dijeron el viernes que a pesar de las llamadas del gobierno de alerta p�blica intensificada contra los terroristas, la seguridad en la escuela hab�a disminuido notablemente.
`` Cualquiera en el trato le dir� lo f�cil que es ingresar a la base '', dijo Fred Verge, un veterano del ej�rcito de 70 a�os.
La gente local coloc� coronas y ramos de flores en la entrada del cuartel.
`` Hay una gran sensaci�n de ira '' en la comunidad, dijo el jefe de polic�a de Kent, Paul Condon.
La Royal Marines Music School es uno de los 30 establecimientos militares en Gran Breta�a que utilizan firmas de seguridad privadas, seg�n muestran las cifras del Ministerio de Defensa.
Fuentes de defensa no identificadas citadas por el peri�dico del Times dijeron hoy que los guardias civiles fueron respaldados por marines armados que patrullaban regularmente dentro del per�metro.
El Secretario de Defensa, Tom King, prometi� una revisi�n de seguridad y dijo que ya se hab�an tomado nuevas medidas en la base.
�l no elabor�.
Martin O'Neill, el portavoz del Partido Laborista de la oposici�n en defensa, dijo el viernes: `` Se deben hacer preguntas serias sobre la pol�tica de centavo del Ministerio de Defensa, que bien puede poner nuestros servicios armados en un riesgo terrible ''.
M�s tarde, O'Neill le dijo a British Broadcasting Corp. Television que el IRA estaba `` preparado para sacar a cualquiera.
La naturaleza cobarde de sus ataques es tal que cada establecimiento militar est� bajo amenaza ".
Dijo que las empresas de seguridad privadas fueron asignadas a �reas consideradas bajos riesgos para ataques.
`` Si designamos los lugares como categor�as de bajo riesgo, entonces es probable que sean atacados, dada la forma en que el IRA se ve obligado a operar '', agreg� O'Neill.
`` Los grandes premios ya no est�n disponibles para ellos, por lo que est�n eliminando los que consideran ...
vulnerable''.La primera ministra Margaret Thatcher recibi� noticias del bombardeo mientras a bordo de un avi�n sobre Siberia en el camino de Jap�n a Mosc�.
Ella lo llam� un `` indignaci�n profundamente seria ''.
King visit� la escena poco despu�s de la explosi�n.
`` S� que las personas que lo cometieron, los padrinos que los enviaron, en realidad saben en sus corazones que no va a hacer ninguna diferencia;que es solo matar por matar '', dijo.
El Ministro de Defensa, ira claramente escrita en su rostro, dijo que, en lugar de erosionar la voluntad de las fuerzas de seguridad, el ataque `` Reduce su determinaci�n para garantizar que el terrorismo no pueda ganar ''.
El primer ministro Charles Haughey de Irlanda conden� el ataque y le envi� a la Sra. Thatcher un mensaje de simpat�a en `` Esta indignaci�n ...
lo que ha causado una p�rdida de vida severa y tr�gica y muchas lesiones ".
El reclamo principalmente de IRA cat�lico romano no dio detalles de la operaci�n, pero dijo que fue una respuesta a un `` mensaje de guerra '' entregado por la Sra. Thatcher cuando visit� Irlanda del Norte hace nueve d�as.
Fue una referencia aparente a su discurso alabando al regimiento de defensa del Ulster reclutado localmente como `` un grupo muy, muy valiente de hombres '' que han perdido a 179 miembros en ataques de IRA desde 1970.
El UDR principalmente protestante es considerado por muchos cat�licos como una fuerza sectaria, y actualmente est� bajo una nube por acusaciones de que algunos reclutas filtraron nombres, direcciones y fotos de IRA sospechosos de asesinos protestantes.
Las instalaciones militares brit�nicas son un objetivo frecuente de la campa�a de IRA.
El ataque del viernes fue el peor desde julio de 1982 cuando 11 militares resultaron heridos fatalmente en dos bombardeos en el centro de Londres.
La escuela de m�sica entrena a los j�venes reclutas para las siete bandas de Royal Marines.
Hasta 250 hombres j�venes, la mayor�a de las edades de 16 a 20 a�os, se someten a entrenamiento militar y musical all�, trabajando con hombres de banda profesionales mayores.


