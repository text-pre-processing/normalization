La cubierta de la lente en el telescopio espacial Hubble se abri� hoy despu�s de una ma�ana de problemas preocupantes, y Mission Control dijo a los astronautas Discovery que ya no son necesarios para rescatar el instrumento de $ 1.5 mil millones.
`` Has sido lanzado del soporte de Hubble;Es solo '', dijo el control de misiones a los astronautas cuando se abri� la puerta de apertura de aluminio.
`` Gracias, se�or, esa es una gran noticia '', dijo el comandante de descubrimiento Loren J. Shriver.
`` Hay apretones de manos y sonrisas aqu� arriba '', dijo Steve Hawley, quien hab�a estado en los controles cuando el telescopio fue liberado en su propia �rbita el mi�rcoles.
`` Apuesto a que tambi�n es as� all� abajo ''.
La exitosa apertura de la cubierta de lente allan� el camino para que los astronautas regresen a la Tierra el domingo.
Hab�an ido al espacio el martes despu�s de un retraso de dos semanas y enviaron el telescopio de su bah�a de carga �til el mi�rcoles.
`` El Hubble est� abierto para los negocios '', dijo Mission Control a los astronautas.
Sin embargo, el telescopio todav�a ten�a problemas.
Cuando se abri� la puerta, dos de sus cuatro giroscopios estabilizadores de posici�n dejaron de funcionar y el sistema electr�nico entr� en un `` modo seguro '' autom�tico en el que se detiene todo el movimiento.
El equipo del Centro de Vuelo Espacial Goddard en Maryland, que gu�a el telescopio, dijo que el movimiento de apertura de la puerta sacudi� el telescopio m�s de lo esperado y que el sistema reaccion� correctamente al apagar.
A primera hora de la tarde, se inici� un procedimiento paso a paso para volver a poner en l�nea a la electr�nica del telescopio.
Steve Terry, un alto funcionario de Ingenier�a de Hubble, dijo que las tolerancias para los sistemas de telescopios se hab�an establecido particularmente estrechas para las primeras operaciones y esa fue la causa de muchas mal funcionamiento.
Adem�s de los problemas esta ma�ana, hay dos cortes de comunicaciones el jueves por un total de varias horas.
`` Somos cautelosos, y eso es natural '', dijo.
`` Tenemos una nave espacial muy cara aqu� y no queremos hacer nada para poner en peligro la utilidad.
... Llegaremos al punto en que sabemos exactamente qu� har� la nave espacial y establecer� los l�mites adecuadamente ''.
La NASA anunci� que `` en este momento el telescopio es seguro y estable ''.
El descubrimiento del transbordador espacial, que llevaba el telescopio a la �rbita, estaba a 50 millas detr�s del telescopio, listo para darle una mano si es necesario.
M�s tarde en el d�a, el barco deb�a hacer un motor que lo pusiera en un camino preliminar para el hogar.
Ese camino tomar�a el descubrimiento dos millas debajo del telescopio.
La cubierta de la lente, un di�metro de 10 pies `` puerta de aperatura '', cubre la abertura al escudo de luz del telescopio.
Cuando est� cerrado, el telescopio es ciego.
La cubierta de la lente se hab�a cerrado _, excepto por una peque�a cu�a de apertura _ desde que el equipo de Discovery el mi�rcoles de $ 1.5 mil millones fue retirado por el equipo de Discovery.
Ahora que se abre, Starlight puede golpear la lente por primera vez y puede hacer su negocio de fotografiar y analizar el universo de manera y con sensibilidad nunca antes posible.
La puerta est� hecha de l�minas de aluminio de panal cubierto en el exterior con material reflectante solar.
El interior est� pintado de negro para absorber la luz perdida.
Si el comando de apertura de la puerta hab�a fallado, los cinco astronautas Discovery que desplegaron Hubble estaban preparados para regresar al telescopio.
Los especialistas en misi�n Bruce McCandless y Kathryn Sullivan habr�an realizado una caminata espacial para abrir la puerta el s�bado si es necesario.
Si hubiera sido necesario, el rescate habr�a retrasado el aterrizaje, programado para el domingo en la Base de la Fuerza A�rea Edwards, California, por un d�a.
Los astronautas se despertaron a las 2:30 a.m. EDT a los suaves sonidos de la canci�n de los Beach Boys, `` Kokomo ''.
`` Para el teclado de Max Q, esa es la forma en que se deber�a hacer una canci�n realmente '', Control de misi�n por radio al especialista en misi�n Steve Hawley, miembro de la banda de tastronautas llamada Max Q.
`` Tu pr�xima pr�ctica es el domingo por la noche en el gimnasio, no llegues tarde ''.
Max Q es un acr�nimo de la NASA para el punto en el ascenso del transbordador cuando la presi�n aerodin�mica est� en su mayor punto.
`` Eso suena como la forma en que lo hacemos '', respondi� Hawley.
`` Si nos llevas a casa el domingo, ir� a practicar ''.
En una �rbita lo suficientemente alta como para evitar la atm�sfera distorsionadora de la Tierra, Hubble permitir� a los astr�nomos estudiar estrellas y galaxias tan distantes que su luz ha tardado 14 mil millones de a�os en llegar a la Tierra.
Es capaz de detectar objetos 50 veces m�s d�biles y con una claridad 10 veces mayor que el mejor observatorio terrestre.
Durante su vida laboral de 15 a�os, se espera que Hubble ayude a determinar la edad y el tama�o del universo.
Puede resolver los misterios de los cu�sares, p�lsares y agujeros negros e incluso encontrar estrellas con planetas que posiblemente puedan apoyar la vida.
El martes, la NASA planea lanzar su primera imagen de prueba del telescopio, un cl�ster estrella abierto en Constellation Carina.
Seguir�n datos significativos en un mes o dos.
El telescopio de 12 toneladas y 43 pies de largo, aproximadamente del tama�o de un autob�s, lleva el nombre del astr�nomo Edwin P. Hubble, quien muri� en 1953.
El Hubble nacido en Missouri descubri� durante la d�cada de 1920 que el universo se est� expandiendo, un hallazgo que dio lugar a la teor�a El universo fue creado hace unos 15 mil millones de a�os por una tremenda explosi�n.


