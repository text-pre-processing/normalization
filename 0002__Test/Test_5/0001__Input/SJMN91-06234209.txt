Gorbachov estaba listo para abordar un avi�n en Crimea, donde estaba de vacaciones en el momento del golpe de estado del lunes, para el vuelo de dos horas a Mosc�, dijo un diputado al ministro de Defensa ruso Konstantin Kobets.
Los informes anteriores de que el avi�n ya hab�a despegado eran incorrectos, dijeron las autoridades.
El l�der sovi�tico, en un comunicado para la radio y la televisi�n, dijo que estaba completamente al mando de la situaci�n y que los l�deres de los golpes de golpe asumir�an "plena responsabilidad" por sus acciones.
En los Estados Unidos, el presidente Bush elogi� el regreso al poder de Gorbachev y dijo que los l�deres de los golpes hab�an "subestimado el poder de la gente, subestim� qu� gusto de libertad y democracia trae";El golpe de estado de tres d�as, uno de los desarrollos m�s tumultuosos en la historia sovi�tica reciente, se desenred� en una serie de desarrollos r�pidos y dram�ticos hoy:;(Compruebe) Los tanques y los transportistas de personal blindado se alejaron del parlamento ruso, escena de la peor violencia del golpe.
Una columna de 2 1/2 milelones de tanques y camiones que sal�an de Mosc�.
(Verifique) Los l�deres de la Legislatura Nacional exigieron la restituci�n de Gorbachov.
La legislatura completa anul� los decretos de emergencia de los l�deres de golpes.
(verificar) se levantaron las restricciones de prensa.
"Lo siento, camaradas, �pero estoy muy emocionado"!dijo un locutor de televisi�n sovi�tico, sonriendo ampliamente despu�s de que se le permiti� por primera vez criticar las acciones de los l�deres de golpes.
(Verifique) El Partido Comunista, aparentemente buscando salvar su propia imagen, denunci� la adquisici�n.
(Verifique) En las rep�blicas b�lticas secesionistas, que hab�an dado la peor parte de la represi�n posterior a la placa, algunas tropas sovi�ticas comenzaron a regresar a sus bases permanentes.
Letonia se uni� a la vecina Estonia hoy para declarar la independencia inmediata de la Uni�n Sovi�tica.
El fracaso del golpe podr�a dar un golpe fatal a los esfuerzos organizados de alto nivel para frustrar los impulsos de la reforma en la Uni�n Sovi�tica por parte de Gorbachov y Yeltsin y alterar significativamente el equilibrio de poder que ha caracterizado el poder sovi�tico para la era de Gorbachov de seis a�os.
Sin embargo, el curso futuro del pa�s depende de qui�n permanece en el poder.
Aunque Yeltsin apoy� a Gorbachov durante el golpe, los dos hombres mantienen diferentes enfoques para las reformas sovi�ticas.
Yeltsin se dirigi� hoy al parlamento ruso y dijo que la Uni�n Sovi�tica acababa de "pasar por un momento m�s tr�gico en su historia, y quiz�s en el momento m�s tr�gico";El ministro de Relaciones Exteriores de Gorbachev, Alexander Bessmertnykh, quien se hab�a retirado durante el golpe, reapareci� hoy y calific� la toma de adquisici�n en la historia sovi�tica que "se debe cambiar";La Legislatura Nacional invalid� a todos los decretos hechos por los l�deres de los golpes, dijo la legisladora nacional Yuri Karyakin a la Legislatura rusa.
Karyakin dijo que el Consejo Ejecutivo de la Legislatura exigi� el retorno al poder de Gorbachov.
Con la desaparici�n del golpe, el Partido Comunista se movi� para salvar su propia credibilidad.
Llam� a la adquisici�n inconstitucional y exigi� una reuni�n con Gorbachov, el jefe del partido, dijo la agencia oficial de noticias Tass.
El partido renunci� a cualquier conexi�n con los l�deres de golpes.
Miles de personas hab�an mantenido su vigilia en el edificio del Parlamento ruso despu�s de enfrentamientos durante la noche con tropas sovi�ticas que dejaron al menos cuatro muertos.
Animaron cuando el anuncio de la partida del comit� de golpes se produjo sobre un altavoz.
"Esto muestra la fuerza de nuestra democracia", dijo Leonid Abramtsov, quien marchaba con una banda de adolescentes agitando la bandera roja, blanca y azul cielo de la Rep�blica rusa.
Seg�n los informes, se dijo que el avi�n llevaba a los l�deres del golpe de estado se dirig�a a la ciudad de Bishkek, anteriormente llamado Frunze, la capital de Kirgizia, a 2,100 millas al sureste de Mosc�, seg�n Radio Rusia, que habla para Yeltsin.
No se sab�a de inmediato por qu� los l�deres de golpe viajar�an a Kirgizia.
Pero las fuentes en el aeropuerto le dijeron a Associated Press que solo el jefe de KGB, Vladimir Kryuchkov, estaba en un avi�n, y se dirig�a a Crimea.
Seg�n los informes, fue acompa�ado por el l�der del Partido Comunista No. 2 Vladimir Ivashko.
La agencia de informaci�n rusa dirigida por la rep�blica dijo que el vicepresidente Gennady Yanayev, jefe nominal del comit� de golpes, todav�a estaba en su oficina del Kremlin.
El informe no pudo confirmarse de forma independiente, y las discrepancias no pudieron aclararse.
A pesar de la fuerza aportada por los l�deres del golpe, no pudieron desalojar a Yeltsin del Parlamento ruso, donde reuni� a la oposici�n, para hacer cumplir el toque de queda o evitar grandes manifestaciones en todo el pa�s.
El estado de emergencia fue declarado despu�s de elementos de l�nea dura en el ej�rcito y el Partido Comunista expulsado de Gorbachov, declarando que estaba enfermo.
La junta parec�a incapaz de hacer frente a decenas de miles de personas que erigieron barricadas alrededor de la fortaleza de Yeltsin en el Parlamento.
Tambi�n hubo enormes manifestaciones contra los grupos en Mosc�, Leningrado, Kishiniev y otras ciudades, as� como ataques de mineros de carb�n en Siberia.
Temprano hoy, antes de que el golpe se desmoronara, las columnas de tanques y los transportistas de personal blindado se hab�an movido alrededor del centro de Mosc�.
Una patrulla de los veh�culos blindados qued� atrapada por manifestantes furiosos justo despu�s de la medianoche mientras intentaba romper una barricada.
Dos personas fueron confirmadas disparadas o aplastadas mientras los veh�culos intentaron abrirse camino, y los testigos dijeron que una tercera persona fue asesinada.
Un funcionario de la Rep�blica de Rusia, el coronel Viktor Samailov, dijo a la agencia de noticias independiente Interfax que cuatro personas fueron disparadas o aplastadas hasta la muerte en el choque.


