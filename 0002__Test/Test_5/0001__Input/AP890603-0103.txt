Las tropas chinas irrumpieron en la Plaza Tiananmen la madrugada del domingo, disparando y golpeando a las multitudes de manifestantes prodemocr�ticos en enfrentamientos que los testigos y los m�dicos dijeron que mataron al menos a 32 personas.
Cientos resultaron heridos.
Las tropas cortaron una franja violenta a trav�s del coraz�n de Beijing para llegar a la plaza, rodando a trav�s de barricadas y creciendo masas en las calles circundantes.
A las 3:30 a.m., los soldados y la polic�a antidisturbios rodearon a Tiananmen, boxeando a varios miles de estudiantes y seguidores concentrados en torno a un monumento a h�roes revolucionarios en el centro.
Media hora despu�s, las luces fueron salidas.
La plaza estaba en la oscuridad.
`` Tienes que renunciar a toda esperanza '', dijeron los estudiantes sobre su altavoz en el monumento.
`` Tienes que dar tu vida al movimiento ''.
Los estudiantes cantaron el `` Internacional '', el himno comunista.
A las 4:30 a.m., los transportistas de personal blindado ingresaron a la plaza y las tropas comenzaron a moverse lentamente hacia varios miles de estudiantes agrupados alrededor de un monumento a h�roes revolucionarios en el centro.
Los disparos espor�dicos a�n se pod�an escuchar en las afueras de la plaza cuando las tropas dispararon a multitudes de miles.
Se cre�a que el peaje de muerte y v�ctimas estaba aumentando r�pidamente.
Ambulancias y pedicabs de bicicleta fueron de ida y vuelta transportando a los heridos.
Un hombre fue visto disparado en la espalda, otro en la cabeza.
Un altavoz oficial en la plaza anunci� que las tropas se estaban mudando para despejar el �rea mientras los soldados se preparaban para avanzar desde los cuatro lados.
Ho Te-Chien, un cantante taiwan�s que desert� a China en 1983 y ha estado activo en el movimiento estudiantil, inst� a los estudiantes a irse.
`` No tenemos miedo de morir, pero ya hemos perdido demasiada sangre '', dijo sobre un altavoz estudiantil.
Una r�plica de 33 pies de altura de la Estatua de la Libertad, establecida por los estudiantes la semana pasada, permaneci� en pie.
Las calles principales que conducen al Tiananmen, el Centro Pol�tico Simb�lico de China, fueron selladas.
Miles huyeron de la plaza, donde cientos de miles de personas se hab�an reunido para apoyar a los estudiantes en la tercera semana de una sentada.
El asalto se produjo dos semanas despu�s de que el primer ministro Li Peng declarara la ley marcial en Beijing y enviara tropas a la ciudad.
Cientos de miles de residentes respondieron bloqueando las carreteras con camiones y sus cuerpos.
En Washington, el Secretario de Estado James A. Baker III dijo el s�bado que la situaci�n en China se hab�a convertido en `` fea y ca�tica.
`` Esto perturbar� al gobierno de los Estados Unidos y molestar� considerablemente al pueblo estadounidense '', dijo.
El Secretario insinu� que podr�a haber repercusiones para el uso de la violencia del gobierno chino.
El enfrentamiento entre tropas y manifestantes hab�a sido tenso pero en gran medida pac�fico durante semanas, con soldados aparentemente reacios o incapaces de moverse contra la multitud hasta que estall� la primera violencia generalizada el s�bado por la tarde.
Un m�dico del Hospital Fuxing en el oeste de Beijing, donde las tropas comenzaron a disparar contra la gente de la calle, dijo que el hospital hab�a recibido 15 muertos y tantos heridos que ten�an que ser puestos en garajes.
Un m�dico de otro hospital dijo que 12 personas murieron all�.
Un soldado fue asesinado cuando fue atropellado por un portador de personal blindado corriendo hacia la plaza, y otro hombre fue reportado a tiros a tiros en un choque callejero en otro lugar.
Un visitante sueco, Tom Hansson, dijo que vio a tres personas matadas a tiros en enfrentamientos al sur de la plaza.
`` �El gobierno es fascista!
�C�mo pueden hacerme esto?
�C�mo pueden hacer esto a la gente ''?Grit� a un hombre de 28 a�os que recibi� un disparo en la pierna.
La mayor�a de los disparos en la plaza arrojaban rocas a las tropas y personas cerca de sus l�neas del frente.
Un altavoz del gobierno en la plaza acus� a los estudiantes de `` agitaci�n contrarrevolucionaria '' y dijo que los militares estaban tomando medios necesarios para poner fin a su rebeli�n.
El s�bado se informaron enfrentamientos espor�dicos y docenas de lesiones cuando los soldados hicieron varios intentos para llegar a la plaza.
Se retiraron moment�neamente bajo barreras de rocas, zapatos y otros objetos, luego reanudaron su empuje m�s violento alrededor de las 8 p.m.S�bado, disparar armas y usar gases lacrim�genos y clubes mientras se mov�an hacia Tiananmen.
Miles de tropas armadas con rifles marcharon por el lado este de la plaza, golpeando a las personas que se interpusieron en su camino.
Los estudiantes les arrojaron c�cteles Molotov y otros objetos.
Un transportista de personal blindado se meti� en un autob�s en llamas en la avenida Changan, la calle principal que corr�a a lo largo de la plaza, y fue incendiado por los estudiantes.
Dos personas dentro fueron arrastradas por las multitudes y golpeadas severamente.
Otros transportistas de personal blindado condujeron por la avenida rompiendo divisores de carril y otras barreras establecidas por los ciudadanos para bloquear el camino del ej�rcito.


