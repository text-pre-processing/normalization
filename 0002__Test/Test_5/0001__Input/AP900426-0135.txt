Los controladores terrestres en el Centro de Vuelo Espacial Goddard finalmente se pusieron en contacto hoy con las dos antenas principales en el telescopio espacial Hubble de $ 1.5 mil millones despu�s de intentar m�s de seis horas para bloquearse.
Los aplausos estallaron en el Centro de Control del Hubble en Greenbelt, Maryland, cuando se resolvi� la falla que estrope� el primer d�a completo de Hubble en el espacio.
`` Tenemos confirmaci�n de que hemos logrado la comunicaci�n con el sat�lite de seguimiento y retransmisi�n de datos a trav�s del sistema de antena de alta ganancia '', dijo Dave Drachlis de la NASA.
Los ingenieros hab�an dicho todo el tiempo que cre�an que las antenas estaban funcionando bien y que el problema estaba apuntando incorrectamente el sat�lite o el TDRS, un sat�lite de comunicaciones en �rbita que act�a como una centralita.
En el intento exitoso, casi duplicaron el ancho de las vigas de las antenas para tomar m�s territorio, desde un barrido de 6.2 grados a 10.8 grados.
Las antenas de baja ganancia del telescopio, que reciben y transmiten informaci�n a una velocidad mucho m�s lenta y pueden usarse en emergencias, funcionaban bien.
El descubrimiento del transbordador espacial estaba a 46 millas de distancia, disponible para remediar ciertos problemas.
La interrupci�n no era un problema para el cual los caminantes espaciales habr�an sido convocados.
Los cinco astronautas estaban ocupados haciendo fotos de tormentas sobre Texas desde su percha de 380 millas de altura y haciendo experimentos.
Steve Terry, director de un equipo que calibra varios aspectos del telescopio, dijo que la primera interrupci�n fue causada por un error humano y no fue un problema grave.
Comenz� entre la 1 y la 1:30 a.m. EDT y dur� 45 minutos a una hora, dijo.
La segunda falla ocurri� a las 6:44 a.m. cuando los controladores intentaron enviar una se�al a las dos antenas de alta ganancia del telescopio a trav�s de los TDR.
`` Esta vez fue un problema de se�alizaci�n '', dijo Jim Elliott, un portavoz de Goddard.
`` Es una nueva nave espacial y creemos que la inexperiencia que se ocupa de ella fue la causa ''.
El telescopio, que se embarc� en una b�squeda de 15 a�os para New Worlds, fue liberado de la bah�a de carga del transbordador el mi�rcoles despu�s de un enganche de �ltima hora que casi envi� a dos astronautas en una caminata espacial de emergencia.
El control de la misi�n despert� a la tripulaci�n temprano hoy con una fuerte interpretaci�n de la canci�n `` Shout '', un favorito de la casa de la fraternidad.
`` Buenos d�as, descubrimiento.
Supongo que est�s despierto despu�s de esa canci�n '', dijo Kathy Thornton, comunicadora de transbordador.
`` Hay muchas personas felices aqu� abajo.
Ayer vimos un gran despliegue y Hubble tuvo una buena noche mientras dorm�as ".
`` Apreciamos las buenas palabras de todos '', respondi� el comandante de descubrimiento Loren Shriver.
Despu�s del despliegue del mi�rcoles, Shriver respald� suavemente el descubrimiento del telescopio y puso la transmisi�n a trav�s de una serie de maniobras, la �ltima completada esta ma�ana, a `` Park '' Discovery en �rbita a 46 millas detr�s de Hubble.
`` El telescopio realmente se ve�a genial mientras volamos de �l, y seguramente esperamos que haga un buen trabajo '', dijo Shriver el mi�rcoles.
`` Bueno, seguro que ahora es, Loren, y gracias por todo el gran trabajo que ha realizado '', respondi� el control de la misi�n.
`` Galileo est� muy orgulloso de ti ''.
Los cient�ficos han aclamado la nave espacial no tripulada m�s cara de la NASA _ como el mayor avance de la astronom�a desde que Galileo levant� un peque�o telescopio a sus ojos hace casi 400 a�os.
Discovery y sus cinco astronautas seguir�n el telescopio mientras los controladores en el Centro Goddard se aseguran de que Hubble est� funcionando correctamente, como responder a los comandos direccionales.
El siguiente hito es el viernes, cuando se abre la puerta de apertura de 10 pies del telescopio, exponiendo el espejo finamente pulido de 94.5 pulgadas a Starlight.
Luego, el martes, la NASA espera lanzar la primera imagen del telescopio: un cl�ster de estrellas abiertas en Constellation Carina.
Los datos seguir�n en un mes o dos.
Si la puerta de la apertura no se abre correctamente, el descubrimiento podr�a volver al telescopio para que dos astronautas puedan realizar una caminata espacial para arreglarlo.
El gerente del proyecto de Hubble, Fred Wojtalik, dijo que el telescopio se probar� hoy por su capacidad de maniobrar antes de que se abriera la puerta de la apertura.
`` Queremos tener el control total, as� que nunca ponemos en peligro al Hubble poniendo la puerta de apertura cerca de la luz del sol '', dijo.
Demasiada luz podr�a da�ar los instrumentos sensibles del telescopio.
El mi�rcoles, el especialista en misi�n Steve Hawley us� el brazo robot de 50 pies del transbordador para comprender suavemente el telescopio de 12 toneladas y izarlo desde la bah�a de carga.
Una de las matrices solares del telescopio se visti� de su m�stil como se esperaba y comenz� a proporcionar energ�a a las bater�as del telescopio, que de lo contrario solo ten�a ocho horas de carga.
Pero hubo problemas con el segundo panel.
No se mover�a cuando se liberaron los pestillos, y luego se movi� alrededor de un quinto de su longitud de 39 pies y se detuvo nuevamente.
El panel sali� todo el camino en el tercer intento.
Seg�n el intento final, los astronautas Bruce McCandless y Kathryn Sullivan estaban en sus trajes espaciales en la esclusa de aire, a 30 minutos de tomar herramientas en la mano y ir en una llamada de mantenimiento del espacio exterior.
Wojtalik dijo que la causa del problema es desconocida.
El procedimiento de 8 horas se realiz� a 380 millas sobre la Tierra, 70 millas m�s altas de lo que cualquier transporte ha volado.
La �rbita fue dictada por la necesidad de colocar el Hubble sobre la atm�sfera distorsionadora de la Tierra.
Los astr�nomos esperan usar el telescopio para mirar hacia atr�s al comienzo del tiempo, estudiando estrellas y galaxias tan distantes que su luz ha tardado 14 mil millones de a�os en llegar a la Tierra.
Esperan determinar c�mo y cu�ndo se form� el universo, actualmente estimado en hace 15 mil millones de a�os.
El telescopio originalmente estaba programado para el lanzamiento en 1983, pero se retras� por problemas t�cnicos y el accidente de Challenger.
Discovery, que se estrell� en �rbita el martes, est� programado para aterrizar el domingo en la Base de la Fuerza A�rea Edwards, California.


