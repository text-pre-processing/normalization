El hurac�n Gilbert maltrat� los resorts de la pen�nsula de Yucat�n hoy con vientos de 160 mph y lluvias torrenciales.
Miles de personas huyeron de �reas costeras para refugios hacia el interior.
`` El viento estaba soplando ventanas en todas partes '' en Canc�n, dijo un funcionario sobre el complejo donde miles de estadounidenses de vacaciones cada a�o.
Los residentes a lo largo de la costa del Golfo de Texas, a 560 millas al norte, almacenaron alimentos y suministros y se prepararon para evacuar.
Robert Sheets, director del Centro Nacional de Huracanes en Florida, dijo temprano hoy que espera que Gilbert toque tierra en la costa de Texas-Louisiana en 48 a 60 horas.
El hurac�n, uno de los m�s fuertes de la historia, es `` extremadamente peligroso '', Estados Unidos
Dijo el Servicio Meteorol�gico Nacional.
Los vientos de la tormenta alcanzaron las 175 mph hoy, pero luego cayeron ligeramente a 160 mph, dijo el servicio meteorol�gico.
Las compa��as petroleras evacuaron a miles de trabajadores de plataformas en el Golfo de M�xico, seg�n informes de Nueva Orleans.
Gilbert, que ha matado al menos a 11 personas, se mud� sobre la isla de Cozumel a las 10 a.m.es de EDT, dijo Jos� Pereira, portavoz de la Oficina del Gobernador en M�rida, capital del estado de Yucat�n.
Pereira, quien habl� por tel�fono desde la Ciudad de M�xico, dijo que las fuertes lluvias cayeron sobre la pen�nsula y las comunicaciones con Canc�n y Cozumel estaban fuera.
Canc�n est� a 240 millas al este de Medira.
Cozumel est� a 12 millas de la costa de Yucat�n.
Los equipos militares y civiles evacuaron a m�s de 16,000 personas de las zonas costeras en la costa superior de Yucat�n entre Puerto Progreso a R�o Lagartos, dijo Pereira.
La mayor�a est�n en refugios en las ciudades del interior, dijo.
`` Muchas personas eran reacias a abandonar sus hogares '', dijo Pereira.
`` Ten�an que estar convencidos de la gravedad de la amenaza ''.
Funcionarios en M�rida hablaron por �ltima vez con personas en Canc�n antes de hoy antes de que golpeara el centro de la tormenta.
`` La electricidad est� baja '', dijo.
`` Hablamos con ellos temprano en la ma�ana y dijeron que era impresionante _ todo oscuro.
La gente ten�a miedo.
El viento estaba soplando ventanas en todas partes ''.
Las fuertes lluvias comenzaron en M�rida alrededor de las 6 a.m. EDT, y se esperaba que la tormenta arrojara de 5 a 10 pulgadas en el �rea.
El Servicio Meteorol�gico Nacional en Miami dijo que el ojo de la tormenta estaba sobre el noreste de Yucat�n al mediod�a EDT.
El centro de la tormenta se ubic� cerca de la latitud 20.7 Norte y la longitud 87.3 oeste, o a unas 150 millas al este de M�rida.
El hurac�n se estaba moviendo hacia el oeste del noroeste a 15 mph.Gilbert golpe� la Rep�blica Dominicana, Jamaica y las Islas Caim�n bajas el domingo, lunes y martes.
Al menos 11 personas fueron reportadas asesinadas, y al menos 60,000 quedaron sin hogar en Jamaica.
Los equipos de rescate trabajaron desesperadamente para restaurar los servicios p�blicos y las comunicaciones en las �reas destrozadas.
En una llamada telef�nica hoy temprano, la recepcionista Pablo Torres del Hotel Carrillos en Canc�n dijo que unas 150 personas, la mayor�a de ellos turistas, estaban abarrotados en el vest�bulo de sof�s y sillas.
`` Estamos llenos, y ahora no hay un turista en la zona del hotel de la playa '', dijo.
`` El sonido del viento afuera es horrible.
No podr�as irte incluso si quisieras ''.
Dijo que los vientos derribaron postes y �rboles de servicios p�blicos y que las hojas de lluvia estaban golpeando la ciudad.
Jennie Valdez, representante de los Estados Unidos
C�nsul dijo que no sab�a cu�ntos turistas hab�a en Canc�n, pero que las cifras del gobierno estiman entre 40,000 y 65,000 al mes.
Canc�n tiene una poblaci�n de aproximadamente 100,000, dijo.
Tambi�n se informaron fuertes vientos en Valladolid, una ciudad de 80,000 ubicadas a 80 millas al suroeste de Canc�n, dijo Jos� Joaquin Martil, presidente local de la Cruz Roja.
El primer ministro jamaicano, Edward Seega, dijo el martes por la noche que al menos seis personas fueron asesinadas, y se estima que 60,000 quedaron sin hogar en `` el peor desastre natural en la historia moderna de Jamaica ''.
Funcionarios de defensa civil en la Rep�blica Dominicana, Sideswiped el domingo por la tormenta, informaron que cinco personas eran conocidas muertas.
El Centro de Huracanes dijo que Gilbert era la tormenta m�s intensa registrada en t�rminos de presi�n barom�trica.
Se midi� a 26.31 pulgadas, rompiendo las 26.35 pulgadas registradas para el hurac�n de 1935 que devast� las teclas de Florida.
`` Esa es la presi�n m�s baja jam�s medida en el hemisferio occidental '', dijo el pronosticador Mark Zimmer.
Gilbert es una tormenta de categor�a 5, el tipo de hurac�n m�s fuerte y mortal.
Tales tormentas tienen vientos m�ximos sostenidos superiores a 155 mph y pueden causar da�o catastr�fico.
Solo dos huracanes de categor�a 5 han golpeado a los Estados Unidos _ la tormenta de 1935 que mat� a 408 personas en Florida y el hurac�n Camille que devast� la costa de Mississippi en 1969 y mat� a 256 personas.


