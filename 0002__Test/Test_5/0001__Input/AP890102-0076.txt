Un ferry de pasajeros se hundi� de la costa del Caribe mientras fue remolcado al puerto, y 59 personas se ahogaron y seis desaparecen, dijeron hoy las autoridades y los informes de noticias.
Capit�nAnibal Giron Arreola, portavoz de la base naval de Puerto Barrios, dijo que el ferry Justo Justo Rufino Barrios II se hundi� el domingo por la tarde en la Bah�a de Amatique.
Dijo por tel�fono que 59 personas murieron.
Juan Jose Gaytan, reportero de Radio Portena en Puerto Barrios, dijo que el Justo Rufneo Barrios II se qued� sin combustible a mitad de camino en una carrera regular de 16 millas desde la ciudad de Livingston, al otro lado de la bah�a, hasta Puerto Barrios.
Gaytan dijo que el bote se hundi� mientras estaba siendo remolcado por un remolcador.
La embajada espa�ola en la ciudad de Guatemala confirm� el hundimiento y dijo que un ciudadano espa�ol de 40 a�os, Vicente Daudi, estaba entre los muertos.
La embajada inform� que su esposa, Rosa Maria Arnal, y un hijo sobrevivieron al naufragio, pero faltaban dos hijas.
Daudi estaba trabajando con una misi�n de ayuda t�cnica y desarrollo econ�mico en espa�ol en Guatemala, dijo la embajada.
Un funcionario de la morgue de Puerto Barrios dijo que se han identificado 13 cuerpos.
Adem�s de Daudi, las nacionalidades de las v�ctimas no se conocieron de inmediato.
Gaytan dijo que los botes de patrulla de la Marina, los buques de pesca y la artesan�a privada han estado buscando en la bah�a sobrevivientes y cuerpos en recuperaci�n.
Dijo que no ten�a otros detalles sobre el naufragio.
Puerto Barrios, a 187 millas al noreste de la capital, es el principal puerto de la costa este de Guatemala.


