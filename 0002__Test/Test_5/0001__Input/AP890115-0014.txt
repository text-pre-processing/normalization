Las muertes por enfermedad del coraz�n y los vasos sangu�neos cayeron un 24 por ciento en la �ltima d�cada, pero sigue siendo, con mucho, el mayor asesino de la naci�n, quit�ndose la vida cada 32 segundos, dijo el domingo la Asociaci�n Americana del Coraz�n.
`` La buena noticia es que continuamos viendo una mejora en la tasa de mortalidad por parte del mayor asesino de nuestra poblaci�n '', dijo el Dr.
Bernadine Healy, de la Cleveland Clinic Foundation, quien es presidente de la Asociaci�n del Coraz�n.
`` Esas tendencias son dram�ticas y no parecen estar revertiendo '', dijo.
`` La mala noticia es que la enfermedad card�aca todav�a est� matando a casi 1 mill�n de estadounidenses al a�o, y tenemos un largo camino por recorrer ''.
La asociaci�n public� las estad�sticas en su foro anual para escritores cient�ficos.
Los expertos atribuyen la disminuci�n de las tasas de mortalidad a una combinaci�n de h�bitos de vida m�s saludables, incluidos menos cigarrillos y mejores alimentos, y un mejor tratamiento m�dico.
`` El p�blico debe apreciar el progreso que se ha realizado en enfermedades card�acas en los �ltimos 20 a�os '', dijo el Dr. Myron L. Weisfeldt de la Universidad Johns Hopkins.
`` Es casi incre�ble.
Casi no hay una forma de enfermedad card�aca que no podamos abordar sin un tratamiento significativo ".
Sin embargo, Weisfeldt, quien es presidente electo de la Asociaci�n del Coraz�n, tambi�n advirti� que queda mucho trabajo, tanto para mejorar la atenci�n como alentar a las personas a cuidar mejor su salud.
`` Creo que podemos prevenir al menos el 50 por ciento de la enfermedad card�aca isqu�mica en los Estados Unidos para el a�o 2000 si dejamos de fumar, tratan el colesterol si es superior a 220 e identificamos y tratamos la hipertensi�n '', dijo.
La enfermedad card�aca isqu�mica es la obstrucci�n de los vasos sangu�neos que alimentan al coraz�n.
Subyace a la mayor�a de los ataques card�acos, la dolencia card�aca m�s letal m�s letal.
Las cifras de la asociaci�n indicaron que en 1986 _ el a�o m�s reciente para el cual hay estad�sticas _ Se estima que 978,500 estadounidenses murieron de ataques card�acos, accidentes cerebrovasculares y otras enfermedades del coraz�n y los vasos sangu�neos.
El c�ncer, el asesino n�mero 2, se quit� 466,000 vidas.
Entre 1976 y 1986, la tasa de mortalidad de todas las formas de enfermedad cardiovascular cay� un 24 por ciento.
Declin� un 28 por ciento para los ataques card�acos y el 40 por ciento para los accidentes cerebrovasculares.
Las cifras de la Asociaci�n muestran que m�s de uno de cada cuatro estadounidenses sufre alguna forma de enfermedad cardiovascular, y casi la mitad de ellas finalmente mueren por ella.
Alrededor del 60 por ciento de las muertes por ataque card�aco ocurren antes de que la v�ctima llegue al hospital.
Los estudios muestran que aproximadamente la mitad de todas las v�ctimas de ataque card�aco esperan m�s de dos horas antes de llegar a una sala de emergencias.
Weisfeldt dijo que un objetivo de salud importante es hacer que las personas vayan al hospital dentro de las tres o cuatro horas de la primera se�al de ataques card�acos.
Durante este per�odo, todav�a hay tiempo para darles medicamentos para disolver los co�gulos de sangre que est�n causando sus ataques card�acos.
Este tratamiento puede salvar vidas y reducir la discapacidad de los ataques card�acos.
Otros objetivos importantes, dijo, incluyen encontrar formas de mejorar la efectividad de la angioplastia, una t�cnica que utiliza globos para abrir arterias card�acas obstruidas y encontrar t�cnicas quir�rgicas y m�dicas para identificar y proteger a las personas en riesgo de paro card�aco.
La Asociaci�n del Coraz�n estima que la enfermedad cardiovascular costar� $ 88.2 mil millones este a�o en gastos m�dicos y salarios perdidos.
Entre otras estad�sticas liberadas por la asociaci�n: _ La enfermedad cardiovascular m�s com�n es la presi�n arterial alta, lo que afecta a 60 millones de estadounidenses.
_Apbout 200,000 de las muertes por enfermedad cardiovascular cada a�o ocurren en personas menores de 65 a�os.
_As hubo 1.418 trasplantes de coraz�n en 1987, casi nueve veces m�s que en 1983.
_Ablo 2.5 millones de estadounidenses tienen angina, el dolor en el pecho resultante de las arterias card�acas obstruidas.
Alrededor de 300,000 casos nuevos ocurren anualmente.
_ Los 1,5 millones de estadounidenses tendr�n ataques card�acos este a�o.


