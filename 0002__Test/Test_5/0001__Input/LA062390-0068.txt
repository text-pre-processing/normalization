Dejando de lado a�os de animosidad, el gobierno de los Estados Unidos se uni� a grupos humanitarios privados el viernes para canalizar medicamentos, carpas, ropa, alimentos y otros materiales de ayuda para ayudar a Ir�n afectado por el terremoto.
Funcionarios del Departamento de Estado dijeron que el gobierno otorg� suministros por valor de $ 300,000 a la Cruz Roja Americana para su env�o a Ir�n, incluidos 1,000 cascos, 1,000 pares de guantes de cuero, 10,000 m�scaras faciales, 2,940 mantas de lana y aproximadamente 500 carpas.
El ej�rcito de los EE. UU., Que a menudo se utiliza para transportar suministros de alivio a los pa�ses necesitados, estaba listo para ofrecer aviones de transporte y apoyo log�stico para cualquier ayuda de EE. UU. Solicitada por Ir�n.
Sin embargo, un funcionario del Pent�gono dijo que es poco probable que el Departamento de Estado y la Casa Blanca autorizaran un avi�n militar de suministros.
"Est� bastante claro que los iran�es no nos quieren, y hay tantas alternativas, como las cartas privadas, realmente no es necesario", dijeron los funcionarios.
"En un caso como este, realmente ser�amos m�s irritantes que una ayuda".
El terremoto, que sacudi� la regi�n norte de Ir�n el jueves temprano, dej� casi 29,000 muertos, por conteo oficial y aproximadamente 400,000 sin hogar.
Sin embargo, se espera que el n�mero de muertos aumente, a medida que los rescatistas contin�an trabajando.
El jueves por la noche, la Media Luna Roja iran� pidi� ayuda de la Cruz Roja y la Red Crescent Society, un paraguas con sede en Ginebra para organizaciones humanitarias internacionales.
El presidente iran�, Hashemi Rafsanjani, quien recorri� la regi�n afectada el viernes, tambi�n solicit� ayuda internacional.
Pat Davis, un portavoz de la Cruz Roja Americana en Washington, dijo el viernes que aunque la Media Media Roja est� altamente organizada y bien equipada, la gran cantidad de personas muertas y sufrientes ha obligado a la Agencia Iran� a solicitar asistencia internacional.
La Cruz Roja Americana respondi� movilizando sus oficinas locales para solicitar dinero, informando a sus operadores telef�nicos gratuitos para solicitar donaciones destinadas a las v�ctimas del terremoto y enviar $ 50,000 en efectivo a la Red Crescent Society para que se usen para ayudar a los iran�es necesitados.
Algunas organizaciones de ayuda jud�a se encontraron en una posici�n inc�moda debido a la hostilidad de Ir�n hacia Estados Unidos e Israel.
Enfrentados con los informes de que Ir�n no dar�a la bienvenida a la asistencia de Israel, los grupos no estaban seguros de si deber�an ofrecer asistencia.
Andy Griffel, director ejecutivo del Servicio Mundial Jud�o Americano, dijo que los directores de la organizaci�n de asistencia participaron en una conferencia telef�nica el viernes por la ma�ana para debatir si recolectar dinero para las v�ctimas iran�es.
Despu�s de una discusi�n en�rgica, el grupo decidi� ayudar a monitorear la situaci�n de cerca y estar preparado para cambiar su decisi�n.
"Somos una organizaci�n humanitaria ... sin tener en cuenta las consideraciones pol�ticas", dijo Griffel.
"Pero si van a politizarlo, definitivamente reconsideraremos nuestra participaci�n".
Amir Zamani, un portavoz de la Misi�n Iran� en las Naciones Unidas, dio la bienvenida a las "muchas ofertas de muchas organizaciones" que su oficina ha recibido.
"Ir�n necesita asistencia, y tales organizaciones pueden enviarla".
Reconociendo las tensas relaciones de Ir�n con muchas naciones, Zamani dijo que su pa�s est� tratando de aliviar el antagonismo para lidiar con el desastre en el pa�s.
Como ejemplo, dijo que "no se requerir�n formalidades" para aviones de ayuda que vuelan a Ir�n.
Sin embargo, agreg� que los env�os de ayuda ser�n coordinados por el gobierno y enrutados a trav�s de Teher�n porque no hay otros aeropuertos en el pa�s que puedan acomodar grandes aviones de transporte.
Mientras tanto, otros grupos de EE. UU. Se apresuraban a proporcionar productos de emergencia a Ir�n.
Dwain Schenck, un trabajador de ayuda para la Fundaci�n Americares, dijo que el grupo de ayuda con sede en Connecticut espera enviar 80,000 libras de suministros m�dicos, dos m�dicos, una enfermera y aproximadamente media docena de trabajadores de ayuda a Ir�n hoy.
"Esperamos estar all� unos cuatro d�as m�s o menos", dijo.
El escritor del personal de Times, Charles J. Hartley, contribuy� a este informe.
Donde enviar ayuda
Estas son algunas de las agencias que aceptan donaciones para v�ctimas de terremotos iran�es:
Agencia de Desarrollo Adventista y Ayuda
12501 Old Columbia Pike
Silver Spring, Md. 20904
(301) 680-6380
Servicio Mundial Jud�o Americano
1290 Avenue of the Americas, piso 11
Nueva York, N.Y. 10104
Cruz Roja Americana
Desastre del terremoto de Ir�n
CORREOS.Box 37243
Washington, D.C. 20013
(800) 842-2200
Cruz Roja Americana (local)
2700 Wilshire Blvd.,
Los �ngeles, California 90057
Am�ricos
161 Cherry st.
New Canaan, Conn. 06840
(800) 486-HELP
Alianza Bautista Mundial
Fondo de terremotos de Ir�n
6733 Curran st.
McLean, Va. 22101
Direct Relief International
2801-B de la facci�n st.
Santa B�rbara, California 93105
(805) 687-3694
Banco Melli Ir�n
Asistencia de alivio del terremoto de Ir�n
Cuenta No. 5000
628 Madison Ave.
Nueva York, N.Y. 10022
Banco Melli Ir�n (local)
818 Wilshire Blvd.
Los �ngeles, California 90017)
Alivio mundial luterano
390 Park Ave. S
Nueva York, N.Y. 10016
Comit� de EE. UU. Para UNICEF
333 E. 38th st.
Nueva York, N.Y. 10016
(212) 686-5522
Preocupaci�n mundial
CORREOS.Caja 33000
Seattle, WA.98133
Visi�n Mundial
919 W. Huntington Drive
Monrovia, California 91016
(818) 357-7979


