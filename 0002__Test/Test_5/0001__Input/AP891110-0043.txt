La estatuilla del Oscar, una de las im�genes m�s reconocibles en el mundo del entretenimiento, no tiene protecci�n contra los derechos de autor, dictamin� un juez federal.
La estatua del Premio de la Academia Peque�a es parte del dominio p�blico, EE. UU.
El juez de la corte de distrito Laughlin Waters dijo en un fallo publicado el jueves.
La decisi�n fue un rev�s para la Academia de Artes y Ciencias de la Cine de pel�culas, que hab�a demandado a un fabricante de un trofeo incentivo con sede en Chicago similar al Oscar.
El presidente de la academia, Karl Malden, dijo que el fallo `` viene como un shock para m� ''.
La academia dijo que apelar�a.
La academia afirm� que el premio Star, el trofeo parecido por las promociones de la casa creativa, viol� las leyes de derechos de autor, diluy� la marca registrada de la academia y representaba una competencia desleal.
El premio Star representaba a un hombre desnudo y musculoso muy parecido al Oscar, solo dos pulgadas m�s cortos y sosteniendo una estrella en lugar de una espada.
Ten�a un acabado de oro similar al Oscar, y se encontraba en una tapa de oro circular montada en una base cil�ndrica.
Aunque Waters reconoci� que el Trofeo Creative House es `` muy similar '' al Oscar, rechaz� todas las afirmaciones legales de la academia, diciendo que la estatuilla se convirti� en parte del dominio p�blico antes del 1 de enero de 1978, la fecha efectiva deLa Ley de Derechos de Autor de 1976.
El premio principal de la industria del cine se distribuy� desde la primera ceremonia en 1929 hasta aproximadamente 1941 sin la marca `` c '' que indica un derechos de autor.
El juez tambi�n dictamin� que el Oscar no solo era un premio honorario, sino que tambi�n se us� para promover la industria del cine y, por lo tanto, no ten�a un `` prop�sito limitado '', seg�n lo requerido por la Ley de Derechos de Autor.
La academia se�al� que el fallo se aplic� solo a la estatuilla y que las marcas registradas y de servicio de la academia para el Oscar y los nombres y s�mbolos de los premios de la Academia no se ven afectados.
`` Estamos sorprendidos de que el Tribunal basara su decisi�n en los eventos que ocurrieron hace medio siglo, con vistas a la moda meticulosa en la que la academia ha protegido la integridad de su s�mbolo institucional durante d�cadas '', dijo Malden en un comunicado.


