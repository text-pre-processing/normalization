La primera ministra Margaret Thatcher ha debilitado su posici�n y comenz� la carrera para sucederla al decir que planea renunciar despu�s de ganar otro t�rmino, dijeron los l�deres de la oposici�n y los observadores pol�ticos.
Un peri�dico pro-Thatcher dijo hoy que simplemente hab�a declarado lo obvio y que sus declaraciones no eran `` causa de asombro ''.
La Sra. Thatcher dijo al corresponsal del domingo en una entrevista publicada el domingo que esperaba llevar a su partido conservador a una cuarta elecci�n sucesiva, pero dijo que era poco probable que intentara un quinto.
`` Creo que la gente pensar�a que era hora de que alguien m�s llevara la antorcha '', dijo al peri�dico.
El Sunday Times de Londres dijo en un comentario: `` Sra.
Thatcher ha cometido un error colosal ''.
`` Ella ha dado a los corredores por la sucesi�n conservadora sus �rdenes iniciales a�os antes de la carrera real '', dijo el peri�dico.
Las pr�ximas elecciones deben celebrarse en el verano de 1992, pero se espera que la Sra. Thatcher la llame en 1991.
La Sra. Thatcher, quien cumpli� 64 a�os el mes pasado, nunca antes hab�a dejado en claro en p�blico cu�ndo planea retirarse como l�der del partido.
Fue elegida en 1979, la primera primera ministra en Europa, y gan� la reelecci�n en 1983 y en 1987, cuando dijo que planeaba `` Continuar y seguir ''.
A principios de este a�o, la Sra. Thatcher super� el mandato liberal de Lord Asquith 1908-1916 como primer ministro para convertirse en el primer ministro del siglo XX del siglo XX de Gran Breta�a.
La popularidad de la Sra. Thatcher es la m�s baja de cualquier primer ministro desde que comenzaron las encuestas de opini�n en Gran Breta�a hace 50 a�os, y los colegas de alto rango le han advertido p�blicamente que cambie su estilo de liderazgo despu�s de la renuncia sorpresa de su jefe del Tesoro, Nigel Lawson.
Lord Callaghan, ex primer ministro laborista a quien la Sra. Thatcher derrot� en 1979, dijo que sus comentarios hab�an `` considerablemente debilitado su posici�n ''.
Si su partido gana las pr�ximas elecciones, se espera que la Sra. Thatcher renuncie antes de que su t�rmino finalice para permitir que su sucesor elija un momento para llamar a una nueva elecci�n.
`` Supongo que el Partido Tory querr� a alguien como l�der que pueda llevarlo a trav�s del pr�ximo parlamento '', dijo Callaghan.
Si la Sra. Thatcher renuncia mientras est� en el cargo, su sucesor eligido por el partido se convierte autom�ticamente en primer ministro con la aprobaci�n de la Reina Isabel II, pero la Convenci�n dicta que busca un mandato en una elecci�n general lo antes posible.
El Sunday Times especul� que renunciar�a en 1994.
La Sra. Thatcher celebrar�a su 69 cumplea�os ese a�o y su esposo, Denis, tendr� 79 a�os y `` su edad puede haber sido un factor en su decisi�n '', dijo el peri�dico.
El peri�dico predijo que el Partido Laborista de la Oposici�n intentar� promover al l�der Neil Kinnock como `` El ocupante en espera de 10 Downing St., que est� listo, capaz y dispuesto a completar un t�rmino completo ''.
Kinnock llam� a la Sra.
Los comentarios de Thatcher `` La entrevista de alguien que se prepara para dejar de fumar.
Mentalmente, Margaret Thatcher se est� preparando para empacar sus maletas ".
En un editorial de hoy, el Pro-Thatcher Daily Telegraph dijo que la Sra. Thatcher estaba simplemente declarando lo obvio al indicar que no buscar�a un quinto mandato.
`` Incluso si no luchara contra una quinta elecci�n, la Sra. Thatcher a�n pod�a disfrutar de los siete a�os m�s a cargo '', dijo.
`` Para entonces, ella estar�a en sus 70 a�os, y su devoto marido una d�cada mayor ...
No obstante, �qu� otro pol�tico se esperar�a que entretuviera una investigaci�n sobre sus intenciones en siete a�os?
�Y puede ser realmente una causa de asombro que la Sra. Thatcher no est� dispuesta a comprometerse con la actividad pol�tica a mediados de los 70 y m�s all�? ''


