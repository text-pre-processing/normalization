El l�der de Alemania Oriental derrocado, Erich Honecker, fue arrestado y llevado a prisi�n hoy, y un fiscal dijo que lo juzgar� por alta traici�n en marzo.
Honecker, de 77 a�os, fue detenido despu�s de su liberaci�n del Hospital Charite del Este de Berl�n, donde se hab�a sometido a una cirug�a por un tumor renal maligno, dijo la agencia oficial de noticias ADN.
El fiscal jefe Hans-Judergen Joseph dijo al Parlamento que Honecker y los ex miembros del Politbur� Erich Mielke, Guenter Mittag y Joachim Herrmann ser�an juzgados por la Corte Suprema por cargos de traici�n en marzo.
Los cuatro han sido expulsados de la fiesta.
Mielke era el jefe de seguridad de Honecker, Mittag supervis� la econom�a y Herrmann encabez� el departamento de propaganda del partido.
Once miembros del Politbur� expulsado de Honecker ya est�n en prisi�n en espera de juicio.
El abogado de Honecker, Wolfgang Vogel, le pidi� al fiscal jefe que libere al ex l�der de la prisi�n de Rummelburg en funci�n de la declaraci�n de un m�dico de que est� demasiado enfermo para permanecer en la c�rcel, dijo ADN.
Vogel es el abogado m�s destacado de Alemania Oriental.
Uno de los m�dicos de Honecker, Horst Vogler, dijo que el ex l�der estaba `` muy deprimido '' y su estado mental `` deteriorado '' 'despu�s de dos operaciones quir�rgicas importantes desde agosto.
La televisi�n de Alemania Occidental mostr� a un impasible Honecker sacado del hospital por su esposa, Margot, en la oscuridad previa al amanecer.
Ella lo bes� antes de que �l fuera llevado en una gran limusina.
La hija de Honecker, el yerno y el nieto m�s joven tambi�n vinieron a verlo en el hospital, ADN cit� a Vogler como dijo.
El cargo de alta traici�n conlleva una cadena perpetua m�xima.
La convicci�n sol�a llevar la pena de muerte, pero eso fue abolido en reformas que siguieron a la expulsi�n de Honecker el 18 de octubre.
Alemania Oriental ha sido atrapada en un frenes� virtual para eliminar la corrupci�n y el abuso de cargos.
Honecker, el l�der estalinista de l�nea dura que gobern� Alemania Oriental durante 18 a�os, hab�a sido declarado demasiado enfermo para resistir el prisi�n.
ADN dijo que el Director de la Cl�nica de Urolog�a Charite, el Dr. Peter Althaus, repiti� hoy que, en su opini�n, Honecker a�n no est� lo suficientemente bien como para ser encarcelado.
El domingo, el peri�dico Bild de circulaci�n de Alemania Occidental dijo que Honecker ser�a arrestado pero que ser�a retenido en un hospital de la prisi�n debido a su condici�n.
ADN no dijo si la prisi�n de Rummelsburg est� equipada con un centro m�dico.
A principios de este mes, la Iglesia Luterana de Alemania Oriental se hab�a ofrecido a poner a Honecker en una casa para los ancianos tras su liberaci�n del hospital.


