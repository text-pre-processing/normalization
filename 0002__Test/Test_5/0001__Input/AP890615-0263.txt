El fundador de Wal-Mart, Sam Walton, entr� para ayudar a los empleados de salida en su tienda en esta ciudad de Panhandle de Florida cuando un fallo electr�nico cerr� las cajas registradoras.
Walton, de 71 a�os, considerado como una de las personas m�s ricas del mundo, tom� una notas el martes por la noche y comenz� a escribir a mano los precios de la mercanc�a para los clientes para que sus facturas pudieran fijarse en las calculadoras r�pidamente.
`` La gente comenz� a reconocerlo y venir y saludarlo '', dijo el gerente de la tienda Paul Sentin.
`` Se estrech� las manos y habl� con todos los que pudo.
Los registros estuvieron fuera durante unos 45 minutos, y no creo que tengamos un cliente quejarse ".
Walton, conocido por su estilo de hogar, hizo una visita sorpresa a la tienda que m�s tarde el martes organiz� un concierto de la cantante country Jana Jea en su estacionamiento.
Walton a menudo asiste a eventos promocionales para su cadena con sede en Arkansas, y Sivery dijo que sospechaba que el jefe podr�a aparecer.
Walton continu� hablando con los clientes durante el concierto.
Tambi�n se uni� al cantante en el escenario para cantar un dueto y lider� a los clientes en la alegr�a de Wal-Mart.
`` Estaba all� diciendo: 'Dame una W, dame una A', y los clientes acudieron con �l '', dijo Senty.
`` Incluso lleg� a hacer un colapso de la animadora ''.


