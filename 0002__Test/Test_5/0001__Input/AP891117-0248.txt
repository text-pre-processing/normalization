Beer Gridiron ser� un lugar ocupado y costoso en la televisi�n este enero.
Los dos cerveceros m�s grandes del pa�s, Anheuser Busch Cos. Inc. y Miller Brewing Co., se est�n preparando para vender su cerveza al organizar juegos simulados de f�tbol en comerciales de televisi�n junto con el Super Bowl XXIV en Nueva Orleans.
Los ejecutivos de ambas compa��as dicen que est�n tratando de aprovechar las semanas de exageraci�n en torno al juego.
Ambos beermakers afirman que sus paquetes son las promociones m�s grandes del Super Bowl, y cada uno planea gastar m�s de $ 7 millones.
Anheuser Busch est� organizando una campa�a publicitaria de `` Bud Bowl II '' que culmina durante el Super Bowl del 28 de enero, con un juego con equipos animados de botellas de Budweiser y Bud Light Botence en un campo de f�tbol del tama�o de una pinta.
El primer juego de este tipo se organiz� durante el �ltimo Super Bowl.
`` Estamos comenzando un mes antes, hemos agregado un lugar de promoci�n adicional y vamos a tener muchos m�s materiales de exhibici�n en las tiendas '', dijo Tom Sharbaugh, director de la marca del grupo de Budweiser.
Miller, con sede en Milwaukee, no dir� exactamente cu�nto est� gastando en su campa�a de Bowl de 12 semanas y pre-Super centrada en un juego de f�tbol entre un equipo `` gusto '' y un escuadr�n `` menos relleno ''.
Esos equipos estar�n compuestos por jugadores actuales y anteriores de la Liga Nacional de F�tbol como Bert Jones, L.C.Greenwood y Joe Klecko.
`` Estamos gastando m�s de $ 7 millones, puedo decirle eso '', dijo Richard Burton, gerente de publicidad de Lite Beer.
`` Estamos hablando de talento de la vida real ''.
La campa�a de Miller culmina el 14 de enero, dos semanas antes del Super Bowl, principalmente porque Anheuser Busch compr� todos los lugares de televisi�n del Super Bowl asignados para publicidad de cerveza.
Los comerciales de televisi�n de Miller ya han comenzado a transmitirse.
Anheuser Busch comenzar� sus anuncios esta semana con un lugar con el locutor deportivo Brent Musberger.
Ambas compa��as tambi�n tienen promociones de participaci�n de fan�ticos.
Anheuser Busch espera recibir entre 400,000 y 500,000 cuadros de puntuaci�n de los fan�ticos a los que se les pide que escriban el puntaje del Bud Bowl despu�s de cada trimestre.
Los fan�ticos que env�an sus cuadros de puntuaci�n recibir�n un PIN conmemorativo.
Miller tiene un sorteo de consumidores que ofrece viajes pagados al Super Bowl.


