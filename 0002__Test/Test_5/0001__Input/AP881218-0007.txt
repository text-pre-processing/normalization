Un ferry sobrecargado que lleva a los escolares en una excursi�n volc� y se hundi� en el sur de China, matando a 55 personas y dejando a siete desaparecidos, dijo el servicio de noticias de China en un despacho visto el domingo.
Los 183 pasajeros y cuatro miembros de la tripulaci�n `` superaron enormemente '' la capacidad del ferry y el barco volc� el mi�rcoles antes de que navegara 200 yardas, seg�n el informe.
No dijo cu�ntas personas el bote fue dise�ado para sostener.
Dijo que para el viernes, 125 sobrevivientes hab�an sido rescatados, se encontraron 55 cuerpos y que faltaban siete personas.
El accidente ocurri� en la isla de Hainan, seg�n el informe.
El servicio de noticias dijo que los pasajeros eran 183 estudiantes y maestros de una escuela secundaria y primaria en el condado de Qiongzhong, que iban a tomar el ferry para visitar una estaci�n de energ�a hidroel�ctrica.
El 22 de septiembre, otro ferry sobrecargado se hundi� en la regi�n aut�noma de Guangxi Zhuang que bordea Vietnam, dejando a 61 personas muertas y una desaparecida.
El Ministerio de Comunicaciones, responsable de la navegaci�n del agua interior, anunci� en agosto que hab�a comenzado una investigaci�n sobre la seguridad de los buques fluviales de China para determinar si los est�ndares deber�an ser apretados.
El movimiento sigui� al volcado del 21 de julio de un ferry en el r�o Min en el suroeste de la provincia de Sichuan en la que murieron 133 personas, y el 25 de julio que se hund�a de un bote de pasajeros en el r�o Yangtze en el que 71 personas se ahogaron.


