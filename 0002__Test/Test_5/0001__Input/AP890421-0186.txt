El acorazado da�ado USS Iowa se llev� a casa desde Puerto Rico el viernes, mientras que los investigadores a bordo entrevistaron a los tripulantes y revisaron una video aficionada de la explosi�n que mat� a 47 marineros.
Ens.Karl Johnson, un portavoz de la Flota Atl�ntica, dijo que la Marina no lanzar�a la posici�n exacta del barco, que se esperaba que llegara a su puerto de origen aqu� alrededor de las 5:30 p.m.Domingo.
El Washington Post inform� en sus ediciones del s�bado que los tripulantes en las c�maras superiores de la torreta de la pistola fueron asesinados por la explosi�n y el fuego mientras los marineros trabajaban varios niveles debajo asfixiados cuando la explosi�n absorbi� todo el ox�geno de los espacios.
Los investigadores de la Marina tambi�n dijeron que la explosi�n dej� dos barriles de pistola completamente cargados en la torreta, inform� el peri�dico.
La explosi�n se produjo despu�s de que dos de las tres armas hab�an sido cargadas y justo despu�s de que la tripulaci�n recibi� permiso para cargar y disparar la pistola central, dijo el peri�dico.
Despu�s de extinguir el fuego, los hombres de la tripulaci�n trabajaron r�pidamente para eliminar las conchas y el polvo de las otras dos armas antes de que pudieran explotar, dijo el peri�dico.
Mientras tanto, NBC News inform� que la causa exacta de la explosi�n en una de las tres torretas de pistola de 16 pulgadas del barco el mi�rcoles puede nunca ser conocida porque la destrucci�n dentro de la torreta fue muy completa.
La Marina lanz� fotograf�as del esfuerzo de lucha contra incendios, que las autoridades, seg�n las autoridades, requirieron dos horas para apagar el incendio y enfriar la torreta antes de que alguien pudiera entrar.
Durante el viaje a casa, los investigadores a bordo estaban revisando una cinta de video de la torreta tomada por un oficial de Iowa que hab�a querido registrar el despido de las grandes armas cuando ocurri� la explosi�n, dijo el teniente.
Cmdr.James Cudler, un portavoz del Pent�gono.
Dijo que los investigadores no hab�an decidido si lanzar la cinta al p�blico.
Los investigadores tambi�n estaban entrevistando a los miembros de la tripulaci�n.
El mayor Tom Johnston, un portavoz de la flota del Atl�ntico en Norfolk, dijo que no ten�a informaci�n sobre la causa de la muerte de los 47 tripulantes. Los funcionarios de Navy se han negado a especular sobre la causa de la explosi�n.
En Norfolk, los servicios conmemorativos para las 47 v�ctimas se establecen tentativamente para las 9 a.m. del lunes.
El presidente George Bush va a asistir.
Johnson dijo que el barco est� humeando bajo su propio poder, y repiti� el informe diario de la Marina de que el barco es de navegaci�n y sin peligro.
El acorazado de 887 pies y 58,000 toneladas tiene una armadura de hasta 17 pulgadas de espesor.
Funcionarios de la Marina han dicho que la explosi�n en la torreta de armas No. 2 da�� solo la torreta.
Los principales miembros del Comit� de Servicios Armados del Senado dijeron el viernes que el panel celebrar� audiencias sobre la explosi�n ardiente.
La explosi�n ocurri� durante la pr�ctica de armas de mares abiertos a 300 millas al norte de Puerto Rico.
El Pent�gono dijo que la explosi�n atraves� la pistola media de la torreta de tres armas, y que esa pistola no hab�a sido disparada antes de la explosi�n.
El Pent�gono ha ordenado que no se dispare ning�n arma de 16 pulgadas hasta que se complete la investigaci�n.
Los barcos con esas armas _ los acorazados Iowa, Nueva Jersey, Missouri y Wisconsin _ fueron construidos durante la Segunda Guerra Mundial.
Algunos cr�ticos han dicho que las armas de 16 pulgadas est�n pasadas de moda y peligrosas.


