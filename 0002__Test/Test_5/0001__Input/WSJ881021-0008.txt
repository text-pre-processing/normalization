Di lo que quieras sobre Albert Goldman, el autor de la nueva biograf�a, "La vida de John Lennon" (Morrow, $ 22.95), pero tienes que entregarla: este tipo es un ambicioso s�rdido.
La mayor�a de los bi�grafos pop m�s vendidos lo juegan a salvo: eligen una celebridad de respetables, por no decir pr�stino, reputaci�n, y luego proceden a ennegrecerlo, preferiblemente contando cuentos de exceso sexual, dependencia qu�mica, falta de acompa�amiento, crudo comercial.Tratos, incluso, en algunos casos, dislexia.
El retrato resultante de un ogro disipado que no puede deletrear se presenta debidamente en forma dura al p�blico, lo que a su vez est� tan sorprendido y disgustado que se obliga a comprar la biograf�a por millones.
Una f�rmula no se puede perder, seguramente.
La v�ctima anterior de Albert Goldman en el g�nero fue Elvis Presley.
Ahora el Sr. Presley era, si perdonaba la expresi�n, un objetivo gordo para un escritor del Sr.
M�todos de Goldman.
A pesar de su t�tulo, el "Rey del Rock 'n' Roll" era en verdad m�s una criatura del antiguo espect�culo, una estrella cuya imagen sana, creada cuidadosamente por P.R. Wizards, estaba madura para desacreditar.
Pero al elegir al Sr. Lennon como su pr�ximo objetivo, el Sr. Goldman parece estar tratando de desacreditar a los desaconsejables.
Aparte de su pol�tica pueril, Sr.
El tema favorito de Lennon en las miles de entrevistas que dio fue su propia historia escu�lida: su voraz uso de drogas, su perfidia a los empleados, su inalcanzable promiscuidad sexual, etc.
Por su propia contabilidad, entonces, el Sr. Lennon no estaba a punto de destronar a Ward Cleaver como el padre estadounidense.
Su p�blico lo amaba a pesar de todo, o tal vez por todo.
�El Sr. Lennon lleg� a la celebridad durante un momento en que las transgresiones como intemperancia y adulterio (t�rminos pintorescos)!estaban perdiendo su aguij�n social.
Su franqueza, si eso era, y no una simple logorrea, redimi� todo.
Y as� es por franqueza insuficiente que el ingenioso Sr. Goldman, caliente para desacreditar, ataca su tema.
El leitmotif del Sr.
Las confesiones de Lennon fueron que sus peores d�as estaban detr�s de �l, y que con Yoko Ono hab�a aprendido finalmente a premiar las virtudes de Quietud y Domesticidad.
Ahora el Sr. Goldman est� aqu� para anunciar: no le dijo la mitad.
Por lo tanto, este estofado de 718 p�ginas de sordidez sin alivio, filtrando con las "primeras" que ahora se han publicitado ampliamente en la revista People, "West 57th" y otros lugares acad�micos: que el Sr. Lennon ten�a algo para los ni�os j�venes;que su matrimonio con la Sra. Ono fue atrapado por la misma fidelidad y rencor que caracteriz� su primer matrimonio;que ten�a un goloso que simplemente no renunciar�a.
Las revelaciones miserables dribla como una fuga qu�mica, adelgazando cada p�gina.
Sorprendentemente, entonces, el arte de SleageMonger ha triunfado: Sr.
John Lennon de Goldman es a�n peor que John Lennon de John Lennon.
Gran parte de este Muck logra salpicar a la Sra. Ono, a quien el Sr. Goldman retrata como un manipulador c�nico cuyo gran talento radica en explotar al Sr. Lennon para continuar su propio ansia de fama y riqueza.
El Sr. Goldman, por desgracia, permite pasar la iron�a obvia de que este es un boceto de miniatura bastante fino de s� mismo.
Los guardianes de Lennon Flame, por supuesto, se han apresurado a se�alar esto.
La Sra. Ono y su hijo han tomado el camino preferido por las celebridades ofendidas, han llegado a los programas de entrevistas para mostrar sus heridas.
Y la revista Rolling Stone, Long the House Organ for the Lennon Publicity Machine, public� recientemente una extensa refutaci�n del libro, que se vio socavado por un tono de indignaci�n prigish que bordea el c�mic.
Criticar "La vida de John Lennon" por insuficiente respeto al legado de The Fallen Beatle est� m�s bien, como condenar a Charles Manson por sus malos modales en la mesa.
El libro puede ser inexacto, ya que los cr�ticos han cobrado.
Pero uno solo puede esperar que las objeciones sobre su veracidad no oscurecen las innumerables razones para odiar al Sr.
Libro de Goldman.
Hay, para empezar, la prosa descuidada y pomposa, tan sugerente de la sala de fumar en la escuela de posgrado: el Sr. Goldman es el tipo de escritor que se refiere a las canciones pop como "piezas", como si fueran conciertos.
Existe su enfoque de conocimiento, m�s moderno, para drogas, sexo, m�sica, que le da licencia para adivinar al Sr.
Lennon es cada movimiento.
Y est� la organizaci�n inc�moda, en la que los eventos menores asumen grandes proporciones por la simple raz�n de que reflejan mal al Sr. Lennon, mientras que los eventos importantes se saltan a la ligera porque, �sale el avance!- Podr�an revelar al Sr. Lennon en una luz halagadora.
Todo lo cual indica por qu� "la vida de John Lennon" es un libro tan miserable: no porque haga que un buen hombre se vea mal, sino porque no muestra otro prop�sito que hacer un hombre, ya sea un hombre bueno o uno malo, Probablemente los laicos nunca lo sabremos, nos vemos mal.
John Lennon, mientras estaba vivo, hizo suficiente de eso �l mismo para satisfacer a todos menos los apetitos m�s bajos.
--- El Sr. Ferguson es asistente de editor gerente del American Spectator.


