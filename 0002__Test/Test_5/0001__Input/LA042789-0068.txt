Lucille Ball, cuya muerte el mi�rcoles por la ma�ana a la edad de 77 a�os ser� la p�rdida de espect�culo m�s ampliamente y profundamente sentida en la memoria reciente, fue el mejor regalo de las pel�culas para la televisi�n.
Result� ser una de las mejores y m�s prof�ticas horas de Sam Goldwyn cuando la firm� como una de sus chicas Goldwyn para aparecer en "esc�ndalos romanos" con Eddie Cantor en 1933.
Ella hab�a mordido partes en dos pel�culas anteriores, pero los "esc�ndalos romanos" fue el que le dio un punto de apoyo m�s firme en las laderas resbaladizas de una carrera de Hollywood.
Las pel�culas la prepararon para la televisi�n.
Ella perfeccion� su oficio incomparable como comediante en docenas de pel�culas, incluidas tres de Fred Astaire y uno de los hermanos Marx ("Servicio de Room").
Muchas de las otras pel�culas causaron pocas ondas en la amplia superficie de la historia del cine.
Pero en esos a�os, Lucy estaba refinando su imagen p�blica duradera y entra�able como una mujer alegre y descarada que era m�s probable (al menos en los primeros d�as) para terminar con el amigo divertido del h�roe que con el mismo h�roe.
Sin embargo, como sucedi� a menudo con los artistas de apoyo, su entusiasmo, una lengua comprensamente afilada y una visi�n bastante salada del mundo la hac�an m�s interesante que los directores de corte cuadrado, y condujo a papeles principales.
Mirar las repeticiones de "Amo a Lucy" hoy, como todos lo hacen, una y otra vez, es darse cuenta de cu�n excelentemente son profesionales que son.
Hoy en d�a, muchas comedias de situaci�n todav�a usan la t�cnica de tres c�maras, disparando frente al p�blico en vivo, que Desi Arnaz fue pionero.
La idea ahora parece tan inevitable como la rueda o las magdalenas pre-cortadas, y sin embargo, se pregunta si habr�a funcionado casi tan bien o que se atribuy� sin la gran experiencia de Lucy como una comediante f�sica y verbal.
Ella lo hizo todo, incluso las alcaparras m�s locas y locas, parece que estaba sucediendo ahora, espont�nea, no realizadas, accidentales, las consecuencias inevitables de la personalidad dispersa y adorable de Lucy.
Sus l�minas, el propio Desi, Vivian Vance y William Frawley, eran magn�ficos farfeurs, pero Lucy era la dinamo que hizo que todo funcionara.
La nave era invisible, las habilidades tan perfeccionadas que se ocultaban totalmente.
Fue una ilusi�n brillante, generando un encanto que ocult� el trabajo duro y la ingeniosa escritura y edici�n, as� como la actuaci�n.
El resultado fue probablemente la mejor y ciertamente la serie individual m�s duradera en la historia de la televisi�n hasta ahora.
El atractivo de los programas de Lucy tambi�n fue el m�s universal que la televisi�n estadounidense ha proporcionado hasta ahora.
Eran totalmente, protot�picamente estadounidenses, y eso es fundamental para su popularidad en el extranjero.
Sin embargo, han sido abrazados como el tipo de acontecimientos de clase media que posiblemente podr�a surgir casi en cualquier lugar donde haya una clase media.
Lucy era toda Americana pero, igualmente, ciudadana del mundo.
Su exitosa carrera en pel�culas, en la que finalmente obtuvo la facturaci�n de estrellas, si no el estatus de superestrella, le dio una identificaci�n invaluable cuando comenz� "I Love Lucy".
La iron�a es que, creada por las pel�culas, deb�a lograr la fama y la fortuna en la televisi�n en una escala que la hab�a eludido en las pel�culas.
De hecho, cuando Lucy comenz� a comandar nuestros corazones en "I Love Lucy" en 1951, las pel�culas ya estaban en problemas considerables, su p�blico se qued� en casa en grandes cantidades para ver a los gustos de Lucy en sus salas de estar.
Como siempre, su momento era impecable.
Lucille Ball no era Lucy, por supuesto.
Ella hab�a venido un camino duro de Jamestown, Nueva York, en una l�nea de trabajo en la que no sobreviviste al ser disperso.
Era inteligente, trabajadora y perfeccionista que sab�a que cualquier otra cosa que fuera, la comedia no era accidental.
Por otra parte, las c�maras, la televisi�n o el cine tienen propiedades de rayos X, y los millones que la vieron y la amaban no se equivocaban al pensar que percib�an a una mujer generosa y amorosa, as� como a un payaso glorioso que sab�a que el mundo estaba mejorre�r .


