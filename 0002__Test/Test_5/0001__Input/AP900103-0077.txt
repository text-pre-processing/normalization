El ex l�der de Alemania Oriental, Erich Honecker, puede ser trasladado a un monasterio para protegerlo de un posible linchamiento de ciudadanos enfurecidos, dijo un peri�dico el mi�rcoles.
En una historia de primera plana, el peri�dico Bild de circulaci�n masiva dijo `` Lynch Danger _ Church quiere otorgar asilo de Honecker ''.
Desde que Honecker fue expulsado el 18 de octubre, el ex l�der del Partido Comunista y otros funcionarios del partido han sido investigados por cargos de corrupci�n y viviendo en lujo a costa del estado.
En diciembre, siete ex miembros del Politbur� fueron arrestados, y Honecker fue puesto bajo arresto domiciliario en el �rea de vivienda del gobierno de Wandlitz en las afueras de Berl�n Oriental.
Las casas de Wandlitz, incluida la en la que Honecker a�n vive, se est�n convirtiendo en un centro de rehabilitaci�n m�dica para ni�os.
Se espera que Honecker se vea obligado a salir del complejo en febrero.
Bild dijo que ten�a informaci�n que `` fuentes de la Iglesia Luterana y Cat�lica han ofrecido Honecker _ que est� amenazado por la ira p�blica _ Protecci�n y refugio ''.
`` Esta es la �nica posibilidad de proteger a Erich Honecker de la ira del pueblo de Alemania Oriental '', dijo Bild la fuente.
Fueron las iglesias las que se convirtieron en los lugares para la reuni�n de grupos prodemocr�ticos que finalmente trajeron el derrocamiento de su liderazgo de 18 a�os.
En las �ltimas semanas, los grupos de Alemania Oriental han pedido castigar a Honecker y trasladarlo a cuartos m�s peque�os `` con un ba�o externo '', dijo Bild.
El liderazgo actual teme que en tal situaci�n, Honecker `` podr�a ser atacado y convertirse en v�ctima de Lynch Justice '', dijo Bild.
Honecker, de 77 a�os, se someti� a una cirug�a de ves�cula biliar en agosto, y los medios de comunicaci�n de Alemania Oriental informaron que permanece gravemente enfermo.
`` Las fuentes dicen que el ex l�der enfermo puede colocarse en una casa operada por la iglesia para los ancianos, o incluso en un monasterio '', inform� Bild.


