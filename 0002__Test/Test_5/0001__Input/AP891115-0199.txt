Las tormentas el�ctricas causaron estragos en el Valle del Medio Mississippi y el sudeste el mi�rcoles con tornados letales, terminan hasta 100 mph y se alzan hasta el tama�o de las pelotas de b�isbol.
Al menos cuatro tornados golpearon el �rea de Huntsville, Alabama.
Un tornado se estrell� contra un distrito residencial, un �rea comercial y una escuela, matando al menos a dos personas y causando docenas de heridas, dijo la polic�a.
El sur de Illinois tambi�n fue duro por las tormentas, con una mujer asesinada por un �rbol que ca�a, unas 20 personas heridas y las l�neas el�ctricas cayeron.
En otros lugares, la nieve cay� de las Monta�as Rocosas del Norte a los Grandes Lagos superiores, mientras que el clima inusualmente suave continu� en el este.
Las personas en Alorton, Illinois, un pueblo de 2.700 a unas cinco millas al sureste de East St. Louis, resultaron heridas cuando los �rboles cayeron en sus casas o hogares m�viles, dijo Bill Gamblin de la Agencia de Servicios de Emergencia y Desastres del Condado de St. Clair.
En Parks College de Cahokia, Illinois, a ocho millas al sur de East St. Louis, las perchas de aviones fueron retorcidas y aplanadas y peque�os aviones arrojados por el aeropuerto por fuertes vientos, dijo el Servicio Meteorol�gico Nacional.
Las tormentas en el �rea generaron viento estimado en 80 a 100 mph, dijo el servicio meteorol�gico.
Las tormentas el�ctricas se desarrollaron junto y por delante de un fuerte frente fr�o y barrieron hacia el este de Missouri y Arkansas.
Las tormentas el�ctricas que cruzaron el noreste de Arkansas azotaron un viento fuerte que soplaban techos de metal que se instalaron en un gimnasio de la escuela secundaria en Lepanto y se arroj� a un granizo de hasta 2 pulgadas de di�metro en Leachville.
Las tormentas el�ctricas pesadas, posiblemente un tornado, golpearon la ciudad de Palmetto en el norte de Georgia el mi�rcoles por la noche, causando da�os extensos, dijeron las autoridades.
En el centro y suroeste de Ohio, las tormentas el�ctricas que empacan hasta 66 mph destruyeron graneros y un remolque a casa y arrancaron los techos de varios edificios.
Algunas lesiones pero no se informaron muertes.
Una tormenta de la tarde en el centro-este de Alabama form� un tornado que da�� al menos una casa, y la Oficina del Sheriff del Condado de Clay dijo que un n�mero desconocido de personas resultaron heridos en una casa.
Las tormentas el�ctricas que se extendieron por Tennessee salieron de un tornado en el sudeste de Madison Country, y dej� caer el granizo tan grande como las pelotas de b�isbol al norte de Jackson, dijo el servicio meteorol�gico.
Pedes de granizo tan grandes como las pelotas de tenis cayeron cerca de Huntingburg, Indiana, el servicio meteorol�gico dijo.
La lluvia m�s pesada durante las seis horas hasta la 1 p.m.EST era 1.22 pulgadas en Per�, Ind.
Enrolle hasta 65 mph techos da�ados, l�neas el�ctricas y �rboles en partes de Kentucky y Louisiana, dijo el servicio meteorol�gico.
Las tormentas el�ctricas severas tambi�n se desarrollaron sobre Alabama y Carolina del Sur durante la tarde, y el Servicio Meteorol�gico inform� un tornado al norte de Greenwood, S.C.
Otras duchas y tormentas el�ctricas, con lluvia localmente pesada, estaban ampliamente dispersas a lo largo de la costa del norte del Atl�ntico.
Nieve ligera asociada con un frente fr�o �rtico extendido desde el este de Wyoming y el noreste de Colorado al oeste de Minnesota.
La nieve tambi�n cay� sobre partes del norte de Wisconsin y el Upper Michigan.
Hasta 1 a 2 pies de nieve fue posible el jueves por la ma�ana a lo largo de la costa del lago Superior de Wisconsin.
Fuerte viento del noroeste que introduce el aire fr�o en los estados del centro-norte ridizado por encima de 40 mph durante la tarde, creando lecturas de enfriamiento del viento m�s fr�a que 25 grados bajo cero.
La nieve, el fuerte viento y las temperaturas en r�pida ca�da asociadas con el frente fr�o produjeron brevemente las condiciones de Nebraska en Nebraska el mi�rcoles por la ma�ana.
El clima inusualmente c�lido continu� en el este con lecturas de la tarde en los a�os 60 en Nueva Inglaterra, y en los a�os 80 sobre Florida, el sur de Texas y el sur de Louisiana.
El m�nimo del mi�rcoles por la ma�ana para los 48 estados inferiores fue de 9 grados por encima de cero en Ely, Nev.


