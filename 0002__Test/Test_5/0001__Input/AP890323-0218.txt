La selecci�n del Oscar ha sido objeto de escrutinio a medida que los estudios de cine llegan a nuevos longitudes para sacar los votos de la Academia de Artes y Ciencias de Pineos.
Y con la inundaci�n de los votos de los Oscar, desde tazas de caf� gratuitas hasta cenas de buffet de cortes�a, los miembros de la academia reconocen que el comit� realiza alguna votaci�n de Oscar, en violaci�n de las reglas.
Los secretarios, c�nyuges y amigos, dicen varios miembros de la academia, est�n marcando las papeletas en lo que se supone que es la competencia de premios m�s estimada de la industria del entretenimiento.
`` Es como una elecci�n local de jueces '', dijo el miembro de la academia Harry Clein, quien dijo que fue testigo de la votaci�n del equipo.
`` Mucha gente no sabe qui�nes son los candidatos.
Entonces echan un vistazo a qui�n est� a su alrededor y ven cu�l es el consenso.
`` En cuanto a las elecciones, no est� mal.
Pero nunca he pasado por una elecci�n en Argentina ''.
Dan Lyle, quien supervisa la votaci�n del Oscar para la firma de contabilidad Price, Waterhouse Inc., dijo que no hay garant�a de que los miembros de la academia realmente hayan completado sus propias boletas.
`` Todo lo que sabemos es que se ha devuelto una boleta adecuada '', dijo.
`` Los miembros est�n en el sistema de honor para completar la votaci�n en s� ''.
M�s de 4,600 miembros recibieron boletas este a�o para los 61� Premios Anuales de la Academia, que se transmitir�n el 29 de marzo desde el Auditorio Santuario en Los �ngeles.
Los miembros de la academia votan en su �rea de especialidad.
La rama de actores de la academia, por ejemplo, es el �nico grupo que selecciona nominados en las cuatro categor�as de actuaci�n.
Dado que la votaci�n se realiza en secreto, los miembros no firman sus boletas _ La autenticidad de la boleta se garantiza �nicamente por un n�mero de control num�rico, dijo Lyle.
Las boletas se invalidan solo si hay m�s de un voto en una categor�a determinada, y solo los votos en esa categor�a espec�fica no se cuidan, dijo.
Las nominaciones para la actuaci�n, la direcci�n, la direcci�n del arte y todas las categor�as especializadas son manejadas por ramas y comit�s seleccionados compuestos por miembros de la academia.
Todos los miembros votantes de la Academia pueden participar en las nominaciones a Mejor Pel�cula y pueden votar por los premios finales en todas las categor�as.
Los ganadores de la mejor pel�cula documental, el mejor cortometraje animado y el mejor cortometraje de acci�n en vivo son elegidos solo por aquellos miembros que han visto todos los nominados.
El constituyente de la academia, Julian Blaustein, dijo que la mayor�a de sus colegas se toman muy en serio la votaci�n.
`` Ni siquiera voto (en una categor�a) si no estoy familiarizado con las im�genes en esa categor�a.
... y nunca le he dicho a alguien que vote por una pel�cula, incluso cuando hice algunos de ellos ".
Mientras tanto, otras reglas de votaci�n de los Oscar tal vez se est�n rompiendo, especula el veterano miembro de la Academia Millard Kaufman.
`` Aunque se le pide que vote en las nominaciones para cinco im�genes, creo que muchas personas se dan cuenta de que tienen una mejor oportunidad si solo votan por su propia foto y dejan las otras cuatro en blanco '', dijo.
Dado el tremendo impacto tanto una nominaci�n al Oscar como una victoria en el Oscar en la fortuna de una pel�cula, no es del todo sorprendente que cierto grado de artificio pueda rodear a los Oscar.
`` El �ltimo emperador '' recibi� $ 18.9 millones, cerca de la mitad del bruto de la pel�cula, despu�s de que gan� el Trofeo Mejor Pel�cula del a�o pasado.
M�s del 70 por ciento de los ingresos generados por la mejor imagen de 1986, `` pelot�n '', se produjo despu�s de que la pel�cula fue nominada.
En promedio, un premio a Mejor Pel�cula vale aproximadamente $ 20 millones en estos d�as.
Una campa�a promocional de Oscar de $ 500,000 montada por un estudio, por lo tanto, puede ser una buena inversi�n.
En el caso de los premios de este a�o, una oleada de anuncios de peri�dicos orientados a la academia, banquetes, regalos de cortes�a y correos de fotos vinculados a cinco lanzamientos tard�os parece haber sido notablemente exitoso.
Este a�o, todos los nominados a Mejor Pel�cula y la gran mayor�a de los principales nominados de actuaci�n provienen de pel�culas lanzadas en diciembre: `` The Accidental Tourist '', `` Liaisons peligrosas ',' `` Rain Man '' y `` Working Girl ''todos fueron lanzados despu�s del 16 de diciembre.
Si bien estas cinco pel�culas pueden ser buenas, la exclusi�n casi completa de las pel�culas del resto del a�o ha destacado el dominio de los estudios al marketing de los Oscar.
`` Creo que los premios de la Academia son basura '', dijo Terry Gilliam, director de `` Brasil '' y las `` las aventuras del bar�n Munchausen ''.
`` Se trata de vender pel�culas de todo se trata '', dijo.
`` Es una forma m�s de azotar algunas pel�culas.
Cuando 'Brasil' fue nominado (en 1986), me pidieron que fuera miembro de la Academia.
Y no me unir�a.
Simplemente no quiero ser parte de eso en absoluto ''.
Y al menos un destacado miembro de la academia, el director Carl Reiner, dice que la manipulaci�n de los votantes del Oscar ha alcanzado nuevas profundidades.
Reiner dijo que el esfuerzo total de Universal Studios para embolsar votos para Sigourney Weaver, un nominado a la mejor actriz para `` gorilas en la niebla '', podr�a llevarlo a votar en contra de la pel�cula.
`` Tengo en mi casa m�s reproducciones de color de Sigourney Weaver y un hermoso gorila de lo que necesito '', dijo Reiner sobre material promocional enviado a su hogar.
`` Tenemos que decirle a la academia que detenga esta publicidad.
Me estoy apagando en las fotos que me gustan ''.


