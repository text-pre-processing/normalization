BFN [Texto] Guangzhou, 23 de junio (Xinhua) - El m�ximo pico de la inundaci�n en los r�os Xijiang y Beijiang ha pasado, dijo Zhu Senlin, gobernador de la provincia de Guangdong de China del Sur de China.
Mientras inspeccionaba el control de inundaciones y el trabajo de ayuda en la ciudad de Qingyuan en el r�o Beijiang ayer, atribuy� el �xito de Guangdong al combatir esta inundaci�n, casi la m�s grande en 100 a�os, a los esfuerzos concertados de los armadores estacionados en los residentes de Guangdong y locales.
M�s de 200 personas perdieron la vida en el desastre natural, que destruy� 189,000 habitaciones y arruin� cultivos en 1,2 millones de hect�reas.
La inundaci�n fue causada por tormentas torrenciales sucesivas en los valles del r�o Xijiang y Beijiang a principios y mediados de junio.
Las principales estaciones de monitoreo de inundaciones en los dos r�os registraron sus niveles de agua m�s altos, los cuatro metros por encima de la marca de peligro.
Los gobiernos locales en varios niveles de la provincia han prestado mucha atenci�n al trabajo de control de inundaciones, y los principales funcionarios del gobierno y el Partido Comunista de diferentes localidades han ido al frente de combate de inundaciones.
No se informaron infracciones de terraplenes importantes o embalses a pesar de la inundaci�n m�s grave en cien a�os, protegiendo efectivamente la seguridad de la capital provincial, Guangzhou y el delta del r�o Pearl (Zhujiang).
Pero las p�rdidas causadas por la inundaci�n fueron bastante graves, dijo el gobernador.
Seg�n �l, 11 millones de personas en las nueve ciudades y 55 condados de la provincia fueron afectadas, y m�s de 200 personas murieron en el desastre natural, con 189,000 habitaciones destruidas y 1.2 millones de hect�reas de cultivos arruinados.
Las p�rdidas econ�micas directas se establecieron en 10.2 mil millones de yuanes.
El gobernador advirti� que, aunque el peligro de inundaci�n hab�a retrocedido, la determinaci�n de luchar contra posibles inundaciones no pod�a aflojar, ya que esto es solo el comienzo: la principal temporada de inundaciones, que generalmente comienza a fines de julio y principios de agosto, a�n no ha llegado.
Inst� a los funcionarios locales a estar en constante alerta contra posibles inundaciones posibles y ser meticuloso sobre las medidas de prevenci�n y control de inundaciones, mientras hac�a todo lo posible para ayudar a las v�ctimas de las inundaciones, ayud�ndoles a reanudar la producci�n lo antes posible y mantener la estabilidad social.


