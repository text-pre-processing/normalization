Los miembros del partido gobernante dijeron hoy que el gobierno, temiendo un ataque a�reo estadounidense, estaba distribuyendo armas autom�ticas a decenas de miles de partidarios y prepar�ndose para evacuar la capital iraqu�.
Un boicot del petr�leo iraqu� oblig� a Bagdad hoy a cerrar una de las dos tuber�as en Turqu�a que estaban bombeando la mayor parte del aceite exportado de Irak, dijeron funcionarios turcos.
Un funcionario brit�nico dijo que las tropas iraqu�es reunieron a los visitantes estadounidenses y brit�nicos en Kuwait y los llevaron a Irak.
El portavoz del Ministerio de Relaciones Exteriores dijo que la mayor�a eran pasajeros de un vuelo de British Airways varado en Kuwait desde la invasi�n iraqu� el jueves.
`` No hay evidencia de que est�n de ninguna manera en problemas '', dijo el funcionario.
Se inform� que Arabia Saudita reforzaba sus tropas a lo largo de la frontera kuwait� despu�s de las preocupaciones de que Iraq puede tratar de invadirlo.
Fuentes del Pent�gono dijeron que el USS Independence y su grupo de batalla de transportistas ahora est�n en estaci�n en el Mar Ar�bigo, una posici�n que le permitir�a lanzar aviones de combate a la regi�n del Golfo P�rsico.
Los diplom�ticos con sede en Gulf dijeron que los soldados de la fuerza de despliegue r�pido estadounidense, formados para reaccionar ante las crisis en el Medio Oriente, hab�an sido enviados al �rea.
En otros desarrollos: _ El nuevo gobierno de Kuwait instalado por Iraq insinu� que podr�a tomar rehenes o confiscar la propiedad de las naciones que toman medidas punitivas contra Iraq.
El presidente iraqu�, Saddam Hussein, dijo que los 100,000 soldados que invadieron Kuwait comenzaron a retroceder el domingo.
El presidente Bush dijo que Irak estaba mintiendo.
_ Los precios de los perosos aumentaron m�s de $ 3 por barril en News Europe y Jap�n se un�an al embargo del petr�leo iraqu�.
Las existencias se desplomaron en todo el mundo.
Los funcionarios de los hospitalides dijeron que al menos 300 soldados kuwait�es e iraqu�es fueron asesinados durante la invasi�n.
En Londres, los kuwait�es que llegaron a un avi�n especial dijeron que 700 kuwaitis murieron.
El embargo del petr�leo iraqu� creci� durante el fin de semana para incluir Jap�n y la Comunidad Europea.
Junto con los Estados Unidos, compraron m�s de la mitad del aceite exportado de Iraq, y la mayor parte bombe� por Turqu�a.
Un funcionario de la compa��a estatal de tuber�as turcas Botas dijo que el bombeo se detuvo en una de las dos tuber�as turcas a las 5 p.m.Iraq redujo el flujo en la segunda tuber�a m�s ancha al 70 por ciento de la capacidad, dijeron.
No hubo comentarios de Iraq.
Las dos tuber�as bombean crudo iraqu� de los campos petroleros de Kirkuk en el norte de Irak a una terminal en el extremo noreste del Mediterr�neo.
Llevan 70 millones de toneladas de crudo anualmente, proporcionando la salida para la mayor parte de las exportaciones iraqu�es.
El funcionario de Botas dijo que todos los pa�ses que participan en el embargo compran su crudo iraqu� de la salida turca.
Iraq tambi�n exporta petr�leo a trav�s de una tuber�a de Arabia Saudita y a trav�s de petroleros en el norte del Golfo Persa.
Un portavoz del Ministerio de Relaciones Exteriores dijo hoy diciendo que un total de 366 personas hab�an sido reunidas por tropas iraqu�es en Kuwait, en su mayor�a pasajeros de un vuelo de British Airways varados.
Estaban siendo transportados en autob�s a la frontera iraqu�.
Bagdad no dio ninguna raz�n para su acci�n.
Inmediatamente despu�s de la invasi�n del jueves, 11 estadounidenses fueron detenidos por tropas iraqu�es.
Subieron a salvo en Bagdad.
El nuevo ministro de Relaciones Exteriores de Kuwait, instalado de Kuwait, el teniente.
El coronel Walid Sa'oud Mohammed Abdullah, advirti� contra m�s sanciones en un comunicado transmitido el domingo por la radio iraqu�.
`` Los pa�ses que recurren a medidas punitivas contra el Gobierno de Kuwait Provisional e Irak fraternal deber�an recordar que tienen intereses y nacionales en Kuwait '', dijo.
Los soldados iraqu�es invadieron Kuwait despu�s de quejarse de que su violaci�n de las cuotas de producci�n de la OPEP estaba reduciendo los precios mundiales del crudo, reduciendo los ingresos del petr�leo iraqu�.
Saddam tambi�n acus� a Kuwait de robar petr�leo de un campo que incluye la reclamaci�n de ambas partes.
La respetada encuesta econ�mica de Medio Oriente inform� hoy que las instalaciones de exportaci�n de petr�leo de Kuwait se hab�an cerrado.
Tambi�n dijo que hab�a dudas considerables, Arabia Saudita, con las reservas de petr�leo m�s grandes del mundo, estar�a dispuesta a compensar la posible escasez mundial de petr�leo.
Kuwait e Iraq juntos representan una quinta parte de los 23.5 millones de barriles producidos diariamente por las 13 naciones de la OPEP.
Las tensiones permanecieron altas en la regi�n;Un funcionario estadounidense dijo en Washington que las tropas iraqu�es en la frontera kuwait�-saudita aparentemente estaban cavando.
Un t�cnico estadounidense en una base secreta de Arabia Saudita dijo que el reino hab�a enviado 200 a 300 tanques hacia su frontera con Kuwait.
Fuentes de la industria petrolera en Arabia Saudita llegaron por tel�fono desde Bahrein, dijo que las tropas sauditas estaban entrando en la regi�n de Khafji, cerca de la frontera.
Los residentes llegaron por tel�fono informaron una intensa actividad a�rea cerca del aeropuerto saudita durante la noche.
El Washington Post inform� hoy que Egipto estaba movilizando algunos elementos de sus fuerzas armadas para ayudar a Arabia Saudita en caso de invasi�n.
Funcionarios estadounidenses dijeron que no hab�a indicios de que las tropas iraqu� se estaban preparando para invadir Arabia Saudita.
Pero Bush envi� al Secretario de Defensa Dick Cheney al reino para conferir a sus l�deres, y la Casa Blanca dijo que el Secretario de Estado James A. Baker ir�a esta semana a Turqu�a.
Saadi Mehdi Saleh, el comandante del ej�rcito iraqu�, fue citado por el peri�dico Al-Iraq diciendo decenas de miles de voluntarios se hab�a unido al ej�rcito para defender lo que llam� `` Iraq y la revoluci�n en Kuwait ''.
Los estadounidenses, fue citado diciendo: `` Deber�a entender que la gente del gran l�der Saddam Hussein no puede asustarse ''.
Los miembros locales del partido gobernante Baath dijeron que los preparativos estaban en curso para evacuar a todos los 4 millones de personas de Bagdad a campamentos fuera de la ciudad.
Se les dijo que los EE. UU.
La Fuerza A�rea podr�a atacar o que Israel podr�a atacar con armas qu�micas o nucleares.
Los miembros del partido tambi�n dijeron que el partido hab�a distribuido armas, principalmente rifles autom�ticos AK-47, a decenas de miles de personas en todo el pa�s.
State Radio dijo que Saddam hab�a ordenado la formaci�n de 11 nuevas divisiones del ej�rcito iraqu�, que se espera generar 100,000 tropas.
La fuerza militar m�s formidible del mundo �rabe ya se estima en 1 mill�n de hombres.
En un punto de control fronterizo a 30 millas al sur de la ciudad portuaria iraqu� de Basora, docenas de tanques, transportistas de personal blindado y camiones que transportaban cientos de soldados cruzados a Irak el domingo.
Pero miles de soldados iraqu� permanecieron en Kuwait, controlando los principales edificios gubernamentales, centros comerciales y puertos.
Los diplom�ticos en la regi�n dijeron que un nuevo ej�rcito kuwait� se avecin� a trav�s de Kuwait.
La fuerza comprende alrededor de 80,000 tropas, en su mayor�a iraqu�es.
El gobierno militar kuwait� de nueve miembros, anunciado por Irak el s�bado, sigui� siendo un misterio.
El Ministerio de Informaci�n no ha mostrado fotograf�as de los l�deres nombrados y el Ministerio de Informaci�n no pudo ubicar a ninguno de ellos.
Los diplom�ticos kuwait�es dijeron que todos eran iraqu�es, incluido un yerno de Saddam, un funcionario de los medios iraqu� de cargo negados.


