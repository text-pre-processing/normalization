El novelista australiano Peter Carey recibi� el codiciado Premio Booker para la Ficci�n el martes por la noche por su historia de amor, `` Oscar y Lucinda ''.
Un panel de cinco jueces anunci� por unanimidad la adjudicaci�n del premio de $ 26,250 despu�s de una deliberaci�n de 80 minutos durante un banquete en el antiguo Guildhall de Londres.
Los jueces hicieron su selecci�n de 102 libros publicados en Gran Breta�a en los �ltimos 12 meses y que leen en sus hogares.
Carey, que vive en Sydney con su esposa e hijo, dijo en un breve discurso que, como los otros cinco finalistas, le hab�an pedido que asistiera con un breve discurso en su bolsillo en caso de que ganara.
`` Si hubiera sabido que iba a ganar, creo que habr�a tomado un poco m�s de problemas con mi letra '', dijo mientras miraba sus notas.
No tuvo �xito en la competencia de premios en 1985 cuando su novela, `` Illywhacker '', se encontraba entre los �ltimos seis.
Carey calific� el premio un `` gran honor '' y agradeci� a los patrocinadores del premio por `` provocando tanta discusi�n apasionada sobre literatura _ Quiz�s habr� m�s ma�ana ''.
`` Todos los que aman los buenos libros se benefician de esto, seguramente '', agreg�.
La novela ganadora es una opci�n adecuada para el a�o bicentenario de Australia.
Cuenta un romance entre un jugador ingl�s y una heredera australiana que se re�ne en el viaje inaugural de un barco de vapor a Australia a mediados del siglo XIX.
La historia teje en la historia australiana de su fundaci�n en 1788 por inmigrantes brit�nicos.
El presidente del panel, Michael Foot, escritor y ex l�der del Partido Laborista de la oposici�n, dijo que los jueces acordaron que las entradas de este a�o hab�an sido `` excepcionalmente fuertes '' y que elegir las �ltimas seis novelas era extremadamente dif�cil.
`` Esperamos que no haya cr�ticas, pero si lo hay, entonces la gente deber�a comenzar a leer los libros por s� mismos '', dijo Foot.
Carey fue el �nico no brit�n en los �ltimos seis.
Hijo de un traficante de autom�viles, naci� en Bacchus Marsh, Victoria, y asisti� a la Escuela de Gram�tica de la Iglesia de la Inglaterra de Geelong, Melbourne, donde el Pr�ncipe de Gales tambi�n era un alumno como un joven de 18 a�os en 1966.
Carey era un redactor publicitario antes de recurrir a escribir libros en 1965.
El primer premio Booker fue otorgado en 1969.
Se administra por Book Trust, una organizaci�n ben�fica educativa que promueve libros, y est� patrocinado por Booker, un negocio internacional de alimentos y agricultores.


