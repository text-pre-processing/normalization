Cachemira ex�tica, un para�so tur�stico de hoteles de casas flotantes y jardines de los magnate de cuyo nombre el ingl�s hizo `` cachemira '', se ha convertido en una zona de guerra de separatismo y enemistad religiosa.
Los jeeps militares llevan altos funcionarios del gobierno y funcionarios alrededor de Srinagar en estos d�as peligrosos, mientras los soldados y los soldados paramilitares intentan detener una campa�a de violencia por parte de los separatistas musulmanes en el valle de Cachemira.
Oficial de inteligencia M.K.Kaul ordena a uno de los jeeps.
Al comienzo de un viaje por la ciudad de 1 mill�n de personas, recurri� a los cuatro soldados en los asientos traseros del Jeep y orden�: `` Revise sus armas.
Est� listo para disparar ''.
Kaul se instal� en el asiento delantero, una pistola de 9 mm en su mano derecha.
`` Estos son d�as malos '', murmur�.
`` Estos terroristas ac�rrimos pueden atacar desde cualquier lugar.
A sus hombres, dijo: `` Disparos si los ves ''.
El lunes, se levant� un toque de queda todo el d�a desde las 5 a.m. hasta el mediod�a y las tiendas se abrieron a multitudes de clientes.
Soldados en bunkers de sacos de arena, armados con ametralladoras ligeras, intersecciones principales protegidas.
Un portavoz del gobierno estatal de Jammu-Kashmir dijo que la situaci�n era pac�fica y `` absolutamente normal '' en Srinagar, la ciudad m�s grande del estado.
Al menos 72 personas han sido asesinadas desde que comenz� la represi�n el 20 de enero en un esfuerzo por frenar los ataques contra funcionarios y edificios gubernamentales.
La mayor�a de los muertos han sido militantes musulmanes disparados por tropas indias.
`` No estamos luchando contra una guerra ni tratando de suprimir ning�n movimiento popular '', dijo Jagmohan, gobernador de Jammu-Kashmir, el �nico estado en la India predominantemente hind� con una mayor�a musulmana.
`` Estamos luchando contra un pu�ado de terroristas que est�n decididos a crear un problema de ley y orden '', dijo Jagmohan, un hind� con reputaci�n de dureza.
Otros funcionarios en Cachemira dicen que en privado la situaci�n es cr�tica, tal vez la amenaza m�s grave que India ha enfrentado en 42 a�os de independencia de Gran Breta�a.
`` Parece que todos aqu� se han convertido en un fundamentalista con una creencia firme de que el d�a no est� muy lejos cuando se vuelven independientes '', dijo un oficial de polic�a estatal.
Cachemira era un reino separado, gobernado por un maharaj� hind�, cuando Gran Breta�a renunci� al dominio colonial el 15 de agosto de 1947, y el subcontinente se dividi� en l�neas religiosas en India y Pakist�n.
El rey hind� opt� por unirse a India, no a Pakist�n, y la disputa sobre Cachemira ha exacerbado las relaciones desde entonces.
Pakist�n tom� la parte occidental de Cachemira e India el este.
Cada uno acus� al otro de ocupaci�n forzada.
Han luchado por las guerras sobre Cachemira y la pregunta fronteriza sigue sin resolverse.
Los observadores de la ONU est�n estacionados en la l�nea de tregua y las dos partes ocasionalmente intercambian disparos.
Las autoridades no dir�n cu�ntos refuerzos se han enviado al valle de Cachemira desde el 20 de enero, pero los informes de varias fuentes indican 30,000 y 40,000 soldados y tropas paramilitares se han desplegado.
Es la poblaci�n musulm�n del valle que inclina la escala sectaria en el estado de Jammu-Kashmir.
De los casi 6 millones de personas del estado, el 64 por ciento son musulmanes y 32 por ciento de hind�es.
La mayor�a de los hind�es viven en el �rea del sur de Jammu.
Srinagar tiene 150 mezquitas.
`` Este es el mejor caldo de cultivo para el fundamentalismo musulm�n '', dijo un funcionario federal que ayuda a coordinar las nuevas medidas de seguridad.
Casi cada mezquita tiene un altavoz para convocar a los fieles a la oraci�n _ y, m�s recientemente, para exhortar a los oyentes a defender su fe y Cachemira.
`` Los residentes son constantemente bombardeados con sermones isl�micos y se les recuerda que luchar contra una 'Jihad' (Guerra Santa Isl�mica) es el deber m�s sagrado de cada musulm�n '', dijo el funcionario federal, bajo condici�n de anonimato.
La electricidad se cort� en la ciudad el viernes, que era tanto el s�bado musulm�n como el D�a Nacional de la Rep�blica de la India en el D�a de la Rep�blica de la India, pero muchos altavoces de la mezquita continuaron operando porque los militantes los conectaron con las bater�as del autom�vil.
Los informes de inteligencia sugieren que el gobierno tiene una ventaja porque los separatistas no est�n unidos.
El grupo m�s poderoso es el frente de liberaci�n de Jammu-Kashmir, pero hay varios otros, incluidos los Allah Tigers, Hezb-e-Mujahed Hezb-e-Islami.
India afirma que los fundamentalistas obtienen dinero y armas de Pakist�n.
Los pakistan�es lo niegan.
Jagmohan, el gobernador, insiste en que el problema principal en Cachemira es el desarrollo econ�mico, no el separatismo.
`` La gente no quiere violencia o el llamado Cachemira independiente '', dijo.
`` La necesidad de la hora es el desarrollo.
Que vengan m�s turistas.
Deje que se construyan nuevas escuelas.
Deje que los nuevos hospitales surjan ''.
Mientras hablaba, los oficiales militares esperaron en la habitaci�n siguiente para discutir una nueva estrategia para aplastar la rebeli�n en lo que los folletos tur�sticos llaman para�so.
En este momento, el para�so parece perdido, al menos temporalmente.


