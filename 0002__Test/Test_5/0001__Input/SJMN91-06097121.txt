Otra v�ctima fue el Dr. Nicholas Davies, presidente electo del American College of Physicians, un grupo de la industria de 60,000 miembros, dijo el Dr. Louis Felder, que trabaj� con Davies en el Hospital Piedmont en Atlanta.
Ninguno de los 20 pasajeros o tres miembros de la tripulaci�n a bordo del vuelo 2311 de Atlantic Southeast Airlines sobrevivi�.
El accidente seguido de un d�a la colisi�n del helic�ptero del avi�n que mat� al senador John Heinz de Pensilvania y a otras seis personas.
Tower, de 65 a�os, y su hija Marian, de 35 a�os, abordaron el Turbopropproppop 120 de 30 pasajeros en Atlanta y se dirigieron a Sea Island, frente a la costa de Georgia, para una fiesta en honor de la Torre.
Las autoridades dijeron que no sab�an la causa del accidente.
El avi�n estaba descendiendo en clima despejado, dijeron las autoridades, cuando cay� a las 2:47 p.m.A unas tres millas al noreste de la pista en el Brunswick-Glynco Jetport.
Los testigos dijeron que el avi�n parec�a haberse ca�do directamente del cielo.
"Simplemente baj� directamente.
No hubo cultivos de �rboles ", dijo el teniente Jack Hopper, del Departamento de Polic�a del Condado de Glynn, citando testigos.
"Comenz� en su nariz, y sab�a que estaba en problemas", dijo James Griner, quien vive a un cuarto de milla del aeropuerto.
"Fui al avi�n, pero no pude llegar por el fuego.
...Cuando llegu� a �l, parec�a compactado ". El vicepresidente senior de Atlantic Southeast, John Beiser, dijo que los funcionarios no ten�an indicios del piloto de que el vuelo estaba en problemas.
Donald Day, el gerente de asuntos del consumidor de la aerol�nea, describi� la aeronave como "extremadamente sofisticada y eficiente en combustible, uno de los columnas de nuestra flota de viajeros".
Dijo que el avi�n era nuevo y hab�a estado en servicio en diciembre.
El Embraer 120 de fabricaci�n brasile�a es fabricado por la compa��a aeron�utica brasile�a SA.
En 1984, un Embraer 110 Bandeirante, un modelo anterior de los 120, se estrell� en Jacksonville, Florida, matando a 13 personas.
Posteriormente, la Administraci�n Federal de Aviaci�n orden� una inspecci�n de todos los Embrayers para posibles remaches cortados o desgastados en las secciones de cola.
La muerte de Tower provoc� palabras de dolor y alabanza entre sus antiguos colegas en Capitol Hill, incluso aquellos que ayudaron a descarrilar su elevaci�n al gabinete en 1989.
"Me entristece profundamente la p�rdida de John Tower, un amigo y patriota duro y capaz", dijo el senador Lloyd Bentsen, D-Texas, quien rompi� filas con la mayor�a de los dem�cratas para apoyar la nominaci�n del presidente Bush del veterano del Senado de 24 a�osser secretario de defensa.
"Era inteligente y ten�a independencia de juicio";El presidente del Comit� de Servicios Armados del Senado, Sam Nunn, D-GA.
, quien dirigi� la exitosa lucha contra la confirmaci�n de la Torre, dijo: "Estados Unidos hab�a perdido un patriota.
...Luch� por la libertad y trabaj� diligentemente para hacer que el mundo sea m�s seguro "." Es muy triste ", dijo Bush.
"Es una p�rdida tr�gica".


