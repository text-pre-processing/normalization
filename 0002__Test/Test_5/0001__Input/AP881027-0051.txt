Se tem�a a m�s de 100 personas ahogadas despu�s de que un ferry superpoblado se hundi� en un cruce fluvial en el sur de Bangladesh, dijo hoy la polic�a.
Se recuperaron quince cuerpos y 94 personas fueron reportadas como desaparecidas en el hundimiento del mi�rcoles por la noche, donde los r�os Ilisha y Tetulia se re�nen y se unen a Meghna, dijeron la polic�a y un administrador del distrito.
Otro ferry fue atacado por los piratas fluviales cerca de Chandpur el martes por la noche, dijo la polic�a.
Seis pasajeros, incluida una mujer embarazada, fueron arrojados por la borda y no han sido encontrados, dijo la polic�a de Chandpur.
Alrededor de 200 pasajeros resultaron heridos cuando los Piratas dispararon rifles y lanzaron bombas caseras antes de salir con alrededor de $ 83,000 en moneda local, dijo la polic�a.
El ferry que se hundi� llevaba m�s del doble de su capacidad autorizada de 100 pasajeros, dijo la polic�a.
Dijeron que unas 100 personas en el techo del ferry nadaron a un lugar seguro, pero la mayor�a de las que estaban en el casco no pudieron salvarse.
El ferry viajaba desde Mehendiganj a la ciudad isle�a de Bhola, a unas 68 millas al sureste de Dhaka, cuando se hundi� en 22 pies de agua.
La polic�a dijo que se encontraron seis cuerpos en el casco del ferry, el M.L.Sonali y otros nueve fueron encontrados m�s de una milla aguas abajo.
Dos peri�dicos independientes en Dhaka, Ittefaq y Sangbad, citaron a los sobrevivientes diciendo que el ferry se descontrol� mientras intentaba negociar un turno.
El ataque pirata en el segundo ferry, el M.V.Sagar, tambi�n ocurri� en el r�o Meghna.
El Sagar estaba en camino desde Dhaka a Barisal cuando unos 50 piratas lanzaron su ataque cerca de Chandpur, a 34 millas al sureste de Dhaka.
Los Piratas, que hab�an abordado el ferry como pasajeros, abrieron fuego y explotaron bombas poco antes de la medianoche del martes mientras la gente se acostaba por la noche, dijo la polic�a de Chandpur.
Los atacantes escaparon en barcos de espera, agreg� la polic�a.
Dijeron que 44 de los pasajeros heridos fueron hospitalizados, incluidos cuatro en estado cr�tico.


