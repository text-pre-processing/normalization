Los pronosticadores meteorol�gicos hoy ofrec�an a los agricultores afectados por la sequ�a, una peque�a perspectiva de alivio en perspectivas separadas durante los pr�ximos 30 d�as y tres meses.
Una nueva evaluaci�n clim�tica de la sequ�a en curso dijo que `` muchas �reas que ahora experimentan sequ�a extrema tienen menos del 4 por ciento de posibilidades de recuperarse de la sequ�a dentro de los 3 meses ''.
El pron�stico decepcionante se produjo cuando el presidente Reagan se dirigi� al Medio Oeste para inspeccionar las granjas plagadas de sequ�as.
El martes, el Departamento de Agricultura inform� una fuerte disminuci�n en las perspectivas de varios cultivos.
Eso fue seguido el mi�rcoles por un pron�stico de 30 d�as del Servicio Meteorol�gico Nacional que dec�a que las �reas afectadas por la sequ�a desde las Dakotas East hasta Nueva York y desde la frontera canadiense hasta Oklahoma, Arkansas y Tennessee probablemente continuar�n sin lluvia.
Esa �rea, que incluye los principales estados de crecimiento de granos donde los cultivos ya est�n sufriendo, tiene un 55 por ciento de posibilidades de lluvia menos de lo normal.
Y algunos estados en el centro de la zona, que se extienden desde Virginia Occidental hasta Nebraska y de Wisconsin a Missouri, pueden ser a�n m�s secos.
Se les da una probabilidad del 60 por ciento de no tener precipitaci�n.
Una porci�n del sur de California, incluido Los �ngeles, es el �rea m�s seca en el pron�stico, con un 70 por ciento de posibilidades de tener precipitaciones por debajo de lo normal.
El resto de la costa de California tambi�n estar� seca, seg�n el pron�stico.
La perspectiva tambi�n dice que hay un 55 por ciento de posibilidades de temperaturas m�s calientes de lo normal a trav�s del coraz�n de EE. UU. Desde Nueva Inglaterra hasta Idaho y desde la frontera canadiense hasta Oklahoma.
Dentro de esa �rea, las posibilidades de altas temperaturas son superiores al 65 por ciento en Wyoming, mientras que los estados centrales del medio oeste tienen un 60 por ciento de posibilidades de temperaturas superiores al promedio.
Se predice m�s h�medo que el clima normal para el oeste y el sur de Texas, y partes de Nuevo M�xico, Arizona y Utah.
Se pronostica la precipitaci�n normal para los estados del Atl�ntico y la Costa del Golfo, junto con los estados del noroeste y del norte de la monta�a.
El servicio meteorol�gico dijo que se esperan temperaturas normales para los estados del Atl�ntico de Virginia South y para los estados de la costa del Golfo.
El oeste de Texas y Nuevo M�xico pueden ser m�s frescos de lo normal, pero se espera que el lejano Oeste vea su cuota normal del calor de verano.
La evaluaci�n de impacto de sequ�a separada, emitida por la oficina clim�tica de la Administraci�n Nacional Oce�nica y Atmosf�rica, se�ala que predecir el fin de la sequ�a es algo complejo, ya que los hechizos secos no tienen una vida normal.
La baja probabilidad del 4 por ciento de un fin dentro de los 3 meses se basa simplemente en un an�lisis estad�stico de sequ�as pasadas, dijo la agencia.
El informe se�al� que a fines de junio, el 29 por ciento de los Estados Unidos continentales estaba experimentando sequ�a severa o extrema, con partes de 35 estados afectados.
Eso pone la sequ�a actual en el mismo nivel que en 1911, pero a�n menos severa que las sequ�as de 1936, 1931, 1956 y 1934.


