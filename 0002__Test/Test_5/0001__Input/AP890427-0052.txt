Cuando era ni�a en el oeste de Nueva York, Lucille Ball vendi� hot dogs y palomitas de ma�z en un parque de diversiones mientras so�aba con protagonizar pel�culas, recordaron los amigos de la ciudad.
`` Ella siempre hablaba de ser una estrella '', dijo Pauline Lopus, quien fue a la escuela con Miss Ball y se hab�a mantenido en contacto con ella.
Miss Ball, nacida a 60 millas al sur de Buffalo en Jamestown el 6 de agosto de 1911, creci� en la cercana Celoron, una aldea rural en el lago Chautauqua.
La Sra. Lopus record� a la estrella de comedia el mi�rcoles como `` atrevido de muchos sentidos ''.
`` Ella trat� de hacer que me escapara (a Broadway) '', Sra.
Lopus agregado.
`` Pero no ten�a el nervio ''.
A los 15 a�os, la se�orita Ball trabaj� como laderas en el parque de atracciones, gritando a la gente que compre palomitas de ma�z y perros calientes hasta que perdi� la voz.
M�s tarde, protagoniz� obras de la escuela secundaria y comenz� frente a una gran audiencia en el escenario de vodevil en el desaparecido Palace Theatre de Jamestown.
Aunque la se�orita Ball no hab�a visitado Jamestown desde 1956 y no ten�a familia all�, se mantuvo en contacto con amigos.
`` Aunque era una gran estrella, todav�a era Lucy para nosotros '', dijo Irene Rosseti de Celoron.
La Sra. Rosseti dijo que la se�orita Ball recientemente le envi� a ella y a su esposo una carta en su 50 aniversario de bodas.
Miss Ball donaba regularmente dinero a organizaciones ben�ficas, eventos c�vicos y Jamestown Community College, dijo el alcalde de Jamestown, Steven Carlson.
El 20 de mayo, Miss Ball recibir�a un t�tulo honorario de la universidad y hab�a planeado regresar a Jamestown, ahora una ciudad de aproximadamente 40,000.
Los residentes hab�an colocado carteles que dec�an `` Jamestown ama a Lucy '' en escaparates y ventanas para el hogar.
Despu�s de que la se�orita Ball sufri� un ataque al coraz�n la semana pasada, los residentes decidieron firmar los carteles y enviarlos a ella como un saludo bien.
Se enviaron m�s de 150 carteles con 10,000 firmas.
El comediante de 77 a�os muri� el mi�rcoles de una aorta ruptura, la arteria principal del coraz�n, en el Centro M�dico Cedars-Sinai en Los �ngeles, donde se estaba recuperando de la cirug�a card�aca de emergencia de la semana pasada.
`` Es un d�a triste para todos nosotros '', dijo Nelson Garifi, organizador del evento de bienvenida.
`` Pero estoy seguro de que el sentimiento es compartido por muchas personas en todo el mundo ''.
Garifi, director de relaciones comunitarias del Colegio Comunitario, dijo que el t�tulo honorario se otorgar� p�stumamente.
Las generaciones futuras en Celoron tendr�n un recordatorio de su famoso residente cada vez que conduzcan o caminen por la calle, donde creci� _ pasar� a llamarse Lucy Lane.
Los funcionarios del pueblo hab�an planeado ceremonias formales que renombraron la calle cuando deb�a vencer en el �rea el pr�ximo mes.
`` Pero suceder� de todos modos '', dijo John Goodell de la Sociedad Hist�rica de la aldea el mi�rcoles.
Cuando se form� la sociedad hace aproximadamente un a�o, Goodell dijo: `` Enviamos una invitaci�n a Lucille Ball para ser miembro, y ella envi� un cheque por $ 500 por una membres�a vitalicia.
`` Era un cheque escrito en una mano muy audaz con una firma de aproximadamente media pulgada de alto ''.


