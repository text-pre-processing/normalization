Es probable que Dan Quayle sea un `` hombre en el exterior '' en la Casa Blanca de George Bush despu�s de una candidatura vicepresidencial que comenz� en un furor pero se instal� en la oscuridad, dicen los expertos.
Genado por su imagen como un novato pol�tico que necesitaba un grupo de manejadores profesionales para sobrevivir a las primeras controversias de la campa�a, Quayle entra en una administraci�n Bush en la que tiene pocos �ntimos o aliados.
Relegado en la campa�a a pueblos peque�os y �reas seguras republicanas, es probable que Quayle como vicepresidenta reciba un papel ceremonial tradicional _ Ir a reuniones pol�ticas y funerales estatales _ en lugar del papel de asesoramiento que Walter Mondale e incluso Bush sienten, algunos acad�micos sienten, sienten algunos acad�micos..
`` Dan Quayle va a retrasar la vicepresidencia alrededor de una d�cada o m�s.
Una cosa de la que los polit�logos han estado hablando es de cu�nto ha crecido la vicepresidencia.
... Con Quayle, solo se retirar� a los viejos tiempos de pol�tica y funerales '', dijo Ryan Barileaux, profesor de ciencias pol�ticas en la Universidad de Miami en Oxford, Ohio, que estudia la presidencia estadounidense.
Bush rara vez mencion� a su compa�ero de fortaleza de 41 a�os durante la campa�a.
Cuando Bush, Bush emple� las l�neas de stock que Quayle `` har�a un vicepresidente sobresaliente '' y hab�a sido `` templado por el acero '' mientras resist�a el furor temprano sobre su servicio militar, registro acad�mico y vida personal.
El presidente electo dijo a los periodistas Quayle tendr�a acceso a los mismos documentos, informaci�n e inteligencia que est�n disponibles para el presidente.
Pero no ir�a m�s all� al describir qu� tareas le dar�a a Quayle.
Quayle dice que Bush ha hablado con �l sobre encabezar un consejo espacial, y que asume que tendr� un papel en los esfuerzos antidrogas de la administraci�n.
`` Ser� un asesor muy cercano al presidente '', dijo Quayle.
Pero esa no es la forma en que todos lo ven.
Tanto Bush como Mondale llegaron a sus campa�as vicepresidenciales con reputaci�n sustancial y pudieron colocar a los empleados clave en posiciones importantes en las respectivas campa�as presidenciales, se�ala Barilleaux.
`` Dan Quayle no ha contribuido nada en el camino del personal a la campa�a de Bush.
No tiene su propia gran reputaci�n en Capitol Hill.
... Dan Quayle no tiene amigos en una casa blanca de Bush, excepto George Bush '', dice Barilleaux.
`` Le van a dar mucho escaparate, un consejo espacial, un grupo de trabajo de drogas, pero va a ser un hombre en el exterior ...
No es un hombre que ser� consultado en una crisis, excepto en una base pro forma ".
Pero Eddie Mahe Jr., un consultor republicano que trabaj� con la campa�a de Bush, dice que Bush es probable que le d� a Quayle un papel m�s activo en la administraci�n que en la campa�a.
`` La reivindicaci�n exige eso.
Ambos querr�n demostrar cu�n correcta fue la decisi�n '', dice Mahe.
Los dem�cratas retrataron a Quayle durante la campa�a como un hombre liviano, un hombre con pocos o ning�n logro legislativo, sin probar y no calificado para estar a un latido de la presidencia.
Incluso algunos miembros de la propia fiesta de Quayle estaban consternados por la elecci�n de Bush.
Quayle no ayud� a su caso al hacer una serie de famosos gaffes durante la campa�a, quiz�s la peor fue su estrecha de Holocausto y su declaraci�n, `` No viv� en este siglo ''.
Norman Ornstein, del American Enterprise Institute, dice Quayle, mientras que `` no es el intelectual l�der del mundo '', no es `` un mu�eco completo ''.
Simplemente estaba empujado a una situaci�n por la que no estaba preparado, dice Ornstein.
`` Creo que es inmaduro.
No es est�pido '', dice Ornstein.
Pero Quayle llega a la Casa Blanca con `` un estereotipo muy da�ino que se desarrolla sobre �l que se cree ampliamente en la comunidad pol�tica y que se cree por un gran segmento del electorado '', dice Ornstein.
`` Eso le dar� mucho para superar.
Le dar� un enorme impulso para demostrar su val�a.
Eso va a ser dif�cil, dada la oficina ''.
Tanto Barilleaux como Stephen Hess, de la Brookings Institution, ven los paralelos en la vicepresidencia de Quayle y los de Richard Nixon y Spiro Agnew.
La elecci�n de Nixon fue vista como un movimiento por Dwight Eisenhower para apaciguar el ala derecha del partido, y Agnew fue vista como una no entidad pol�tica.
`` La respuesta natural es usarlo (Quayle) con moderaci�n, como lo hizo Nixon con Agnew en 1968 '', dice Hess.
`` Sin embargo, Quayle tiene cuatro a�os para demostrar su val�a, y creo que ver�s historias en dos a�os sobre cu�nto aprendi�, cu�nto ha actuado de manera responsable, cu�nto ha llegado de la campa�a de 1988 ''.
Quayle admite que la campa�a fue un proceso de aprendizaje.
Despu�s de que las controversias disminuyeron, Quayle declar� su independencia de sus manejadores de arbustos, diciendo que ser�a su propio hombre.
Se volvi� m�s accesible para los medios de comunicaci�n, pero continu� adheri�ndose al horario transmitido desde Washington y entregar el mensaje guiado de la fiesta.
Pronto, dej� las p�ginas delanteras.
Y aunque ocasionalmente hablaba con frustraci�n por tener poco control sobre su horario y dejar de hacer noticias nacionales, desempe�� el papel de leal n�mero 2 e incluso anunci� su nuevo anonimato.
`` Hemos llegado como un candidato vicepresidencial tradicional porque estamos en las p�ginas traseras o no en las p�ginas en absoluto.
Eso es lo que se supone que debemos ser '', dijo a los periodistas.
Esa disposici�n a ser un jugador de equipo podr�a servirle bien en una Casa Blanca de Bush, siente Mahe, ya que Bush quiere `` un asesor privado, cuya experiencia y opini�n respetaba ...
(qui�n) nunca ha tenido otra agenda ''.
`` Dan Quayle le debe su vida pol�tica a un hombre y a un hombre sola '', dice Mahe.
`` Nunca vas a ver a Dan Quayle contando cuentos fuera de la escuela ''.


