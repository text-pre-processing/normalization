Iraq se devolvi� el oeste hoy, redondeando a cientos de ciudadanos extranjeros en Kuwait, como la U.N.
El Consejo de Seguridad abrum� el comercio amplio y las sanciones militares contra Irak para castigarlo por invadir al emirato.
Los invasores iraqu� recorrieron hoteles que buscaban a algunos de los miles de occidentales con sede en Kuwait o atrapados por la invasi�n.
M�s de 1 mill�n de extranjeros viven y trabajan en Kuwait.
Varios cientos de brit�nicos, estadounidenses y alemanes occidentales fueron agarrados en los hoteles, y algunos fueron llevados a Irak, dijeron los Ministerios de Relaciones Exteriores de Alemania Occidental y Brit�nica.
Gran Breta�a dijo que Bagdad no hab�a dado ninguna raz�n para detener a 366 personas, la mayor�a de ellos pasajeros de un vuelo de British Airways varados en el aeropuerto de Kuwait por la invasi�n.
Las sanciones de la ONU, que inclu�an una prohibici�n de las compras de petr�leo, se votaron de 13 a 0, en el consejo de 15 miembros.
Cuba y Yemen se abstuvieron de la resoluci�n, que fue propuesta por los Estados Unidos.
El gobierno de Kuwait, instalado por Iraqi, hab�a advertido anteriormente a los pa�ses extranjeros que no deber�an olvidar a sus ciudadanos en Kuwait mientras se amontonaban los castigos econ�micos, y el resumen de hoy parec�a ser una reacci�n contra las sanciones.
Las tropas iraqu�es entraron en su vecino del tama�o de Nueva Jersey el jueves, conquistando la capital y movi�ndose hacia el sur hacia la frontera de Arabia Saudita.
Las fuentes de petr�leo del Golfo P�rsico dijeron hoy que hab�an visto tropas sauditas que se mov�an hacia el norte hacia la frontera kuwait� cerca del puerto petrolero de Khafji, pero los sauditas negaron haber enviado refuerzos a la frontera.
Las autoridades iraqu�es dijeron hoy a la compa��a de tuber�as estatal de Turqu�a que una de las tuber�as gemelas que llevan el aceite de Bagdad a trav�s de Turqu�a estar� cerrado "por razones de marketing", dijeron las autoridades.
Los precios mundiales del petr�leo se elevaron a su nivel m�s alto en m�s de cuatro a�os hoy a medida que m�s pa�ses se unieron al intento de estrangular la econom�a de Bagdad y obligarlo a retirar sus tropas de invasi�n.
Los ingresos petroleros de Iraq totalizaron m�s de $ 11 mil millones el a�o pasado.
Ha estado presionando que los precios del petr�leo m�s altos reconstruyan una econom�a da�ada por la guerra de 1980-88 con Ir�n y liquidan deudas extranjeras de hasta $ 80 mil millones.
Una de las razones por las que Iraq dio para invadir Kuwait fue la reticencia kuwait� a frenar su flujo de petr�leo y elevar los precios del petr�leo, pero fue el petr�leo iraqu� que se hab�a desacelerado hoy.
"Hay muy pocos petroleros que cargan petr�leo iraqu� en Yumurtalik", dijo un alto funcionario del gobierno turco, refiri�ndose a los muelles de exportaci�n en el lado turco del mar Mediterr�neo.
En Bagdad, el presidente Saddam Hussein orden� simulacros para millones de personas en caso de que se necesitaran evacuaciones masivas.
Advirti� a los 17 millones de personas de su naci�n que estuvieran en alerta por posibles ataques estadounidenses o israel�es.
Estados Unidos advirti� a Irak contra cualquier ataque contra Arabia Saudita, y el presidente Bush ha dicho que todas las opciones est�n abiertas para Estados Unidos.
Cientos de kuwait�es recurrieron al desierto para huir a Arabia Saudita, sus hijos y algunas pertenencias abarrotadas en veh�culos con tracci�n en las cuatro ruedas.
Historias relacionadas: p�ginas 2,3


