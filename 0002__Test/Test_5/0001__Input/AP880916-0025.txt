Miles de residentes costeros desde M�xico hasta Louisiana huyeron a un terreno m�s alto el jueves cuando el feroz hurac�n Gilbert envi� la primera de sus tormentas contra Texas despu�s de golpear la pen�nsula de Yucat�n.
`` Esta es una tormenta asesina '', dijo Gordon Guthrie, director de la Divisi�n de Emergencia de Gesti�n de Florida.
`` Siento pena por cualquiera donde sea que esto llegue ''.
El n�mero de muertos por el ataque de la tormenta a trav�s de las islas del Caribe y el Yucat�n fue de al menos 47, y las estimaciones de da�os alcanzaron los $ 8 mil millones.
El primer ministro jamaicano, Edward Seega, declar� una emergencia p�blica de un mes.
Al final de la noche, una lluvia torrencial estaba cayendo en Brownsville.
A las 11 pm.CDT, el centro de tormenta estaba cerca de la latitud 22.8 norte, longitud 94.4 oeste, o a unas 275 millas al sureste de Brownsville, movi�ndose hacia el oeste-noroeste entre 12 y 15 mph, con vientos m�ximos sostenidos de 120 mph y acompa�ado por un 5 a 10 pulgadasRAVINACI�N, seg�n el Servicio Meteorol�gico Nacional.
`` Si esta moci�n contin�a, el centro del hurac�n cruzar� la costa cerca de la frontera de Texas-Mexico el viernes por la noche '', dijo el servicio meteorol�gico.
`` Si se produzca un giro del noroeste, el terreno podr�a estar m�s arriba en la costa de Texas ''.
El Servicio Meteorol�gico emiti� una advertencia de huracanes para la costa norte de M�xico y la mitad sur de la costa de Texas de 370 millas desde Brownsville a Port O'Connor, incluido Corpus Christi de 250,000 residentes.
Un reloj de hurac�n permaneci� vigente durante el resto de la costa de Texas, desde Port O'Connor North hasta Port Arthur, cerca de la frontera de Louisiana.
El servicio meteorol�gico dijo que las advertencias podr�an extenderse hacia el norte, dependiendo del camino de la tormenta de 450 millas de ancho.
El gobernador de Texas, Bill Clements, envi� unidades de la Guardia Nacional a Alice, McAllen y Corpus Christi, el primer despliegue de este tipo para el hurac�n, para ayudar en las comunicaciones y las tareas de rescate.
Clements tambi�n emiti� una proclamaci�n de emergencia que permite a las autoridades locales suspender las leyes `` preservar la salud, la seguridad y el bienestar del p�blico '', incluida las cosas como la direcci�n de los viajes en las carreteras.
Las tiendas de comestibles estaban bajas de agua embotellada, bater�as, at�n enlatado y pan mientras las personas sentaban suministros.
Los propietarios cubr�an ventanas y puertas con madera contrachapada y cinta adhesiva de huracanes a prueba de roturas.
Los trabajadores petroleros en alta mar dejaron sus plataformas en el Golfo de M�xico.
En la isla de Galveston, el administrador de la ciudad, Doug Matthews, recomend� el jueves por la noche que los 62,000 residentes de la ciudad comienzan a evacuarse.
`` Estamos tratando con la vida de las personas, por lo tanto, a las 8 en punto, recomendamos la evacuaci�n de la isla.
Creemos que no podemos esperar hasta la ma�ana para tomar esa decisi�n '', dijo Matthews, y agreg� que los obst�culos se establecer�an a la medianoche en la Interestatal 45 para limitar el acceso a la isla.
En Brownsville, la ciudad m�s meridional de Texas, los vientos comenzaron a recoger alrededor del mediod�a bajo cielos nublados.
La polic�a dijo que estaban pidiendo a los residentes que consideraran evacuar, pero que no lo ordenaban.
`` Nunca hemos evacuado la ciudad y el punto es, �a d�nde vas a mover a 100,000 personas ''?dijo el sargento.Dean Poos.
Un portavoz de autobuses de Greyhound Trailways dijo que la compa��a estaba duplicando su horario de autobuses salientes para acomodar a los que huyen de la tormenta.
Lorena Curry, quien ha vivido en Brownsville desde 1935, dijo que planea sacar la tormenta.
`` He pasado por ellos antes.
Me voy a quedar en mi casa ''.
Pero ella agreg�: `` Podr�a llevarme bien sin un hurac�n muy bien '.
Gilbert entr� en el Golfo despu�s de golpear la pen�nsula de Yucat�n en M�xico con vientos de 160 mph, lo que oblig� a decenas de miles a huir.
Despu�s de cruzar la pen�nsula, los vientos de Gilbert se debilitaron a 120 mph, pero los pronosticadores predijeron que la tormenta se intensificar�a nuevamente a medida que se mueve sobre aguas abiertas.
`` La actividad de la ducha y la tormenta el�ctrica que vemos que tiene lugar en torno al hurac�n se est� organizando mejor, m�s vigorosa, por lo que creemos que est� comenzando a fortalecerse '', dijo Bob Sheets, director del Centro Nacional de Huracanes en Miami.
Las s�banas dijeron que los vientos sostenidos del hurac�n `` ciertamente aumentar�an a 130, 140 millas por hora ''.
En el complejo costero de South Padre Island, a unas 25 millas al sureste de Brownsville, el alcalde orden� que sus 1,000 residentes evacuaran.
`` No podemos obligarlos a irse, pero no tiene sentido quedarse '', dijo el alcalde Bob Pinkerton Jr., y agreg� que el agua y la electricidad se apagar�an el jueves por la tarde.
Los vientos de la fuerza de la tormenta tropical, al menos 39 mph, se extendieron hacia afuera hasta 250 millas al norte y 200 millas al sur del centro.
La primera tierra de la tormenta a principios de esta semana dej� al menos 26 personas muertas en Jamaica, cinco en la Rep�blica Dominicana, 10 en Hait� y los seis en M�xico.
Tambi�n dej� medio mill�n de personas sin hogar en Jamaica y caus� da�os generalizados a las Islas Caim�n.
La tormenta atraves� la costa de Yucat�n al amanecer el mi�rcoles, sacudiendo playas con olas de 23 pies, �rboles desarraigantes, eliminando la electricidad y el suministro de agua y la corte de l�neas telef�nicas.
En Campeche, la capital del estado en la costa oeste de Peninusula, los barcos y las algas abuchearon las calles de cientos de metros de la orilla despu�s de que Gilbert pas�.
`` Toda la ciudad est� inundada.
Todo est� oscuro '', dijo que Ramon Castillo, un vigilante en el peri�dico Novedades de Campeche.
`` He vivido aqu� toda mi vida y nunca he visto mal tiempo como este.
La gente tiene miedo ''.
Funcionarios mexicanos informaron seis muertes, incluidos dos beb�s que se ahogaron, y al menos siete personas resultaron heridas, pero las autoridades no pudieron llegar a muchas aldeas aisladas.
Gilbert golpe� la capital provincial de M�rida y las ciudades portuarias del Golfo de Puerto Progreso, Campeche y Ciudad del Carmen, que cerraron aeropuertos y carreteras y eliminan las comunicaciones y el poder e inundando calles y carreteras.
Unas 20,000 personas fueron evacuadas de Puerto Progreso y otras ciudades costeras, dijo el jueves un portavoz del Ministerio del Interior en la Ciudad de M�xico el jueves.
Cientos de casas fueron destruidas, dijo.
En la costa norte de M�xico, la gente fue evacuada de pueblos pesqueros y pueblos a refugios en Matamoros, una ciudad de 400,000 aproximadamente 12 millas tierra adentro cerca de Brownsville.
El Centro de Huracanes dijo que Gilbert en un momento era la tormenta m�s intensa registrada en t�rminos de presi�n barom�trica, que se midi� el martes a 26.13 pulgadas, rompiendo las 26.35 pulgadas registradas para el hurac�n de 1935 que devast� las claves de Florida.
La tormenta, generada el s�bado al sureste de Puerto Rico, parec�a haber golpeado a Jamaica el m�s duro.
Seega dijo que destruy� alrededor de 100,000 de las 500,000 hogares de Jamaica.
Estim� da�os en $ 8 mil millones.
Seega dijo en una transmisi�n de radio a nivel nacional el jueves que el `` per�odo de emergencia p�blica '' de un mes, incluido un toque de queda para evitar el saqueo, se puso en vigencia `` para permitir que la normalidad se reanudara en el menor tiempo posibley para permitir que el proceso de reconstrucci�n comience ''.
El mi�rcoles, Gilbert fue clasificado como una tormenta de categor�a 5, el tipo de hurac�n m�s fuerte y mortal.
Tales tormentas tienen vientos m�ximos sostenidos superiores a 155 mph y pueden causar da�o catastr�fico.
Para el mi�rcoles por la noche, el Centro Nacional de Huracanes lo degrad� a una categor�a 4 y a una categor�a 3 para el jueves por la ma�ana.


