El extravagante desaf�o del ex ministro de Defensa Michael Heseltine con la primera ministra Margaret Thatcher por el liderazgo del Partido Conservador ha causado una sensaci�n pol�tica en Gran Breta�a.
Se programa un primer voto crucial de los miembros del Parlamento (MPS) para hoy, y si Heseltine deber�a arrebatar el control del partido de Thatcher, la cabeza de gobierno brit�nica m�s antigua en los tiempos modernos, tambi�n tendr�a que renunciar aEl trabajo del primer ministro que ha celebrado desde 1979.
Aqu� hay una muestra de comentarios editoriales sobre el concurso que ha provocado Gran Breta�a:
"La era de Thatcher en la pol�tica brit�nica casi ha terminado.
...Es m�s probable que ella est� terminada por completo.
El golpe de gracia no puede ser entregado en este momento.
La naturaleza de la �poca que viene despu�s est� lejos de ser clara.
Pero como fuerza pol�tica, es condenada en el mejor de los casos a una acci�n trasera implacable que terminar� en alg�n momento en un abismo humillante.
...
"El partido puede apoyarla hoscamente ahora, pero volver�n a hacer la pregunta nuevamente en la primavera, sabiendo que si no lo hacen, el pa�s har� la misma pregunta a�n m�s desplegable cuando llegue las elecciones generales.
...
"Una alternativa, sin embargo, la mira en la cara.
...Es para reconocer que ha pasado una efectividad y preserva sus logros al retener, liberando a sus lugartenientes e instando a su partido, en inter�s de una unidad desaparecida, a apoyar al Sr.
(Ministro de Relaciones Exteriores Douglas) Hurd ".
- Bi�grafo de Thatcher Hugo Young en The Guardian
"El concurso para el liderazgo del Partido Conservador ... no deber�a estar ocurriendo.
...
"Ser�a una tonter�a negar que, hoy, el primer ministro es pol�ticamente asediado como nunca antes.
...Pero la tarea del Partido Conservador, en el a�o y m�s que queda antes de las pr�ximas elecciones generales, debe ser reunirse para permitir que la Sra. Thatcher reconstruya su posici�n personal y le d� la confianza y el apoyo para ganar el cuarto mandato que merece".
-- El Tel�grafo diario
"�Qui�n podr�a haber adivinado ... al ver al primer ministro responder preguntas en los bienes comunes que estaba luchando por su vida?
Ella era tan r�pida y bien destacada como siempre.
...
"Malgaret Thatcher ensangrentada puede ser.
Bowed, ella ciertamente no lo es ".
-- Correo diario
"Los parlamentarios conservadores brit�nicos deben estar preparados para rechazar la Sra. Thatcher cuando votan en las elecciones de liderazgo del partido.
...Ya no est� en inter�s del Reino Unido, que ahora est�n inextricablemente vinculados con el desarrollo de una Europa fuerte y unida, para que ella permanezca en el cargo.
"El europeo no deriva la satisfacci�n de llegar a esta conclusi�n.
Adem�s de sus logros dom�sticos en revivir la econom�a brit�nica en la d�cada de 1980, la Sra. Thatcher ha contribuido mucho m�s al desarrollo de la comunidad europea de lo que a menudo se le da cr�dito.
...
"Pero el europeo cree que su visi�n de pesadilla de Europa, de personas mal intencionadas que intentan destruir la democracia, no es compartida por la mayor�a del pueblo brit�nico.
Rechazan su opini�n de que la agrupaci�n de soberan�a no es diferente de la rendici�n.
...
"El Partido Conservador se enfrenta a una marcada elecci�n.
O reelecci�n a la Sra. Thatcher y ver� que el Reino Unido se desliza en el aislamiento, ya que queda atr�s el resto de la comunidad.
O puede elegir un nuevo l�der que est� preparado para participar genuinamente en el argumento sobre la uni�n econ�mica y pol�tica ".
-- El Europeo
"Un primer ministro en total comando durante 11 a�os no pudo ser derribado, y las pol�ticas apreciadas abandonadas, sin una explosi�n dentro del partido.
Se necesitar�a un Salom�n para sanar las divisiones y evitar la guerra civil.
Francamente, no podemos ver al Sr. Heseltine en ese papel.
"El Sol tambi�n cuestiona profundamente su capacidad para ser un primer ministro exitoso o incluso adecuado.
...Este es el hombre que durante los �ltimos cinco a�os ha profesado lealtad al gobierno y, sin embargo, ha sido su fuerza m�s divisiva.
Discernimos solo una lealtad constante, y eso es para Michael Heseltine ".
-- El sol
"La Sra. Thatcher merece algo mejor que ser despedida de la manera de mala calidad y degradante, tienen la mente que tengan la mente reservada para ella.
Ser derrotado por la gente en una elecci�n general, al igual que Winston Churchill en 1945, fue un rechazo honorable.
Ser derribado por un voto de censura en la C�mara de los Comunes, al igual que Neville Chamberlain en 1940, incluso esa era una forma constitucionalmente respetable de desaparici�n pol�tica.
Pero lo que proponen hacerle a la Sra. Thatcher, sin duda, la mayor ministra en tiempos de paz de este siglo, es simplemente indigna.
Su destino podr�a ser sellado, si sus oponentes se salen con la suya, a puerta cerrada en habitaciones llenas de humo.
...A las personas de calidad siempre se les otorg� la dignidad de la ejecuci�n por hacha o escuadr�n de disparos, en lugar de la humillaci�n del ahorcamiento.
A estos conspiradores les gustar�a enviar a la Sra. Thatcher, quien casi sin ayuda ha ganado tres elecciones, por el equivalente pol�tico de la cuerda.
...
"Puedes encontrar Heseltines donde quiera que vayas.
Pero solo hay una se�ora Thatcher.
Tratar con cuidado.
Este valioso objeto es insustituible ".
- The Sunday Telegraph
"Este peri�dico ha mantenido durante mucho tiempo que el mejor resultado en las pr�ximas elecciones generales ser�a que Margaret Thatcher llevara a los conservadores a una cuarta victoria.
Lamentablemente, el curso de los eventos ha conspirado para hacerlo poco probable.
...
"A los brit�nicos nunca le han gustado Margaret Thatcher; pero mientras ella entreg� los bienes que la respetaron y votaron por ella.
Cuando su gobierno falla (como lo ha hecho recientemente), esa falta de afecto recurre al creciente resentimiento.
Por razones que probablemente sean injustas e inmerecidas, la Sra. Thatcher es ahora una v�ctima de ese estado de �nimo p�blico m�s fuerte de todos: tiempo para un cambio.
...
"La Sra. Thatcher tiene raz�n al sentirse amargada de que un primer ministro con su historial y prestigio pueda ser desafiado tan f�cilmente.
Si ella va, la forma de su expulsi�n dejar� mucho que desear;Ning�n otro partido podr�a deshacerse de su l�der en el gobierno, este lado de una elecci�n, en gran parte por razones de impopularidad.
Pero los conservadores tienen una nariz para la supervivencia en el poder sin igual por cualquier otro partido en el mundo democr�tico, y la pol�tica es un negocio implacable.
El problema antes de los MP de Tory...No es la reelecci�n de Margaret Thatcher, es la derrota del (l�der del Partido Laborista de Oposici�n) Neil Kinnock.
Eso requiere decir una despedida reacia a la Sra. Thatcher, y al respaldo de Michael Heseltine ".
- The Sunday Times


