Los deudores del Tercer Mundo tuvieron que pagar $ 50.1 mil millones m�s para atender sus deudas a los Estados Unidos y otros acreedores el a�o pasado de lo que recibieron en nuevos pr�stamos, un drenaje importante en sus econom�as ya liquidez en efectivo, inform� el domingo el Banco Mundial.
La cifra, contenida en el informe anual del Banco y se hizo p�blica antes de su reuni�n anual aqu� el 23 de septiembre, fue casi un tercio mayor que en 1987, cuando el retroceso neto totaliz� $ 38.3 mil millones.
El drenaje en efectivo ha estado creciendo constantemente desde 1984, cuando fue de $ 10.2 mil millones.
El banco tambi�n inform� que, a pesar del crecimiento econ�mico relativamente boyante en la mayor�a de los pa�ses industriales, los pa�ses en desarrollo obtuvieron una actuaci�n decididamente mixta, desde un mini-boom en las econom�as del sudeste asi�tico hasta un mayor empobrecimiento en �frica.
El banco dijo que planea aumentar sus propios compromisos para los nuevos pr�stamos a los pa�ses del Tercer Mundo a aproximadamente $ 16.4 mil millones en el a�o fiscal actual, frente a $ 14.8 mil millones en el a�o fiscal 1988.
Sin embargo, se espera que los desembolsos reales del dinero del pr�stamo permanezcan en aproximadamente $ 11 mil millones.
Las cifras sobre el desag�e en efectivo mostraron que estas "transferencias de recursos netos", como las entienden el lenguaje bancario, se est�n agitando r�pidamente, una medida de la tensi�n creciente que la carga de la deuda global est� colocando en las econom�as del Tercer Mundo.
El total del domingo es aproximadamente $ 7 mil millones m�s alto que una estimaci�n preliminar de $ 43 mil millones para 1988 que el banco public� en diciembre pasado.
"La situaci�n en el Tercer Mundo est� empeorando, no mejor", dijo un funcionario del banco.
Un portavoz del banco dijo que parte de la raz�n por la que la cifra est� tan hinchada es que algunos pa�ses, como Corea del Sur, rico en efectivo, est�n pagando sus deudas temprano.
Y se espera que el nuevo plan de EE. UU. Para ayudar a los pa�ses a reducir sus deudas recorte el total de algunos.
A�n as�, la cifra es masiva por cualquier medida.
La carga total de la deuda de los pa�ses del Tercer Mundo actualmente se estima en aproximadamente $ 993 mil millones.
El servicio de la deuda para este total, es decir, los pagos para cubrir intereses y capital, asciende a aproximadamente $ 143 mil millones al a�o.
El banco dio una variedad de razones para la disparidad en las tasas de crecimiento entre los pa�ses en desarrollo.
En general, sin embargo, dijo que los pa�ses cuyos gobiernos siguen pol�ticas econ�micas sensatas, como en el este de Asia, han atra�do una gran inversi�n y han tenido un buen desempe�o.
Pero otros, como los de Am�rica Latina, el Medio Oriente y el �frica subsahariana, que son econom�as de propiedad estatal y consumen m�s de lo que producen, no lo han hecho.
En particular, la inversi�n se ha retrasado en estos pa�ses, ya que los residentes han enviado su capital al extranjero.
El banco dijo que el continuo deterioro en la situaci�n de la deuda subraya la necesidad de m�s esfuerzos de los pa�ses industriales para ayudar a los gobiernos del Tercer Mundo a respaldar su deuda, un objetivo importante del plan de los Estados Unidos ofrecido por el secretario del Tesoro, Nicholas F. Brady.
El drenaje comenz� en 1984, sin embargo, los esfuerzos bajo el plan Brady van relativamente lentamente, y los funcionarios del Banco Mundial advirtieron que los resultados pueden no aparecer durante al menos dos o tres a�os m�s.
Algunos cr�ticos creen que el plan, dise�ado para permitir que los deudores intercambien deuda por valores, puede tener un uso limitado.
El drenaje neto de efectivo de los pa�ses deudores a los m�s ricos comenz� en 1984.
Hasta entonces, los pa�ses m�s ricos hab�an proporcionado m�s en nuevos pr�stamos a las naciones del Tercer Mundo de los que los pa�ses pagaron.
Funcionarios del Banco Mundial dijeron que es probable que las salidas impulsen a la instituci�n a intensificar su �nfasis en los programas dise�ados para aliviar la pobreza, no solo el desarrollo de la velocidad.
El banco tambi�n tiene la intenci�n de intensificar los pr�stamos para proyectos ambientales.


