Dos aviones de combate F-16 de la Fuerza A�rea de EE. UU. Se estrellaron hoy en el aire y explotaron, dijo una portavoz de la Fuerza A�rea.
El accidente ocurri� menos de dos horas despu�s de que otro F-16 se estrell� contra el Bosque Negro.
La polic�a de Alemania Occidental dijo que un piloto fue asesinado en el accidente en vuelo.
La portavoz de la Fuerza A�rea, el capit�n Gail Hayes, dijo que el avi�n estaba en una misi�n de entrenamiento cuando se estrellaron cerca de Bodenheim, a unas seis millas al sur de Mainz.
Ella dijo que el avi�n, asignado al ala de combate t�ctica 50 en la base a�rea de Hahn, se estrell� a la 1:30 p.m.
`` Hab�a una persona a bordo de cada avi�n.
Se desconoce la condici�n de los que est�n a bordo '', dijo Hayes a The Associated Press en una entrevista telef�nica de la sede de la Fuerza A�rea de EE. UU. En la Base A�rea Ramstein.
El portavoz de la polic�a de Alemania Occidental, Hugo Linxweiler, dijo al AP en una entrevista telef�nica que uno de los pilotos fue asesinado en el accidente.
`` El otro piloto pudo expulsar de manera segura '', dijo Linxweiler.
Lenxweiler dijo que no sab�a si el piloto que expuls� sufri� alguna lesi�n.
Las identidades de los pilotos no se liberaron de inmediato.
Dijo que los aviones se estrellaron a varios cientos de yardas de un �rea poblada, pero que nadie en el suelo result� herido.
Dijo que la informaci�n preliminar indicaba que uno de los F-16 embisti� al otro desde atr�s.Ambos aviones explotaron en el impacto, dijo.
El otro accidente ocurri� a unas 90 millas de distancia.
El portavoz de la Fuerza A�rea, el teniente Al Sattler, dijo que el piloto en el accidente de la Selva Negra se expuls� de manera segura antes del accidente y fue llevado a la Base A�rea Ramstein para ser examinado.
La polic�a en Karlsruhe dijo que el accidente ocurri� al mediod�a (6 a.m. EDT) cerca del pueblo de Marxzell-Burbach.
Sattler dijo que el avi�n era del ala de combate t�ctica 52, estacionado en la base a�rea de Spangdahlen.
El avi�n participaba en un ejercicio de aire t�ctico militar de la OTAN que se llevaba a cabo desde la Base de la Fuerza A�rea Canadiense en Baden-Sellenen, dijo Sattler.
La polic�a de Alemania Occidental y el personal militar estadounidense aseguraron el �rea de los accidentes, y los equipos de expertos fueron enviados a los sitios de accidentes para determinar la causa de los accidentes.


