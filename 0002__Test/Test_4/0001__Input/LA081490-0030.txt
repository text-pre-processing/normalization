Los 80,000 hombres y mujeres que luchan contra los incendios forestales de la naci�n siempre han sabido que sus vidas estaban en peligro por llamas o calor o escombros.
Pero ahora, dos nuevos estudios muestran que tambi�n enfrentan un peligro invisible: su salud est� bajo asedio del estofado venenoso de gases y holl�n en el humo de incendios forestales.
Entre los culpables se encuentran el mon�xido de carbono, que ralentiza la reacci�n y perjudica el juicio;part�culas microsc�picas de carbono que se alojan en los pulmones;Aldeh�dos y �cidos que irritan los pasajes de aire y las sustancias a base de hidrocarburos y otros productos qu�micos que pueden da�ar los genes y causar c�ncer, muestran las pruebas.
Los bomberos forestales, cuya �nica protecci�n es el pa�uelo de algod�n que cubre sus caras, pierden hasta el 10% de su capacidad pulmonar despu�s de una temporada de rutina, y el da�o persiste durante semanas, seg�n los estudios del Departamento de Servicios de Salud de California y los JohnsEscuela de Higiene y Salud P�blica de la Universidad de Hopkins en Baltimore.
Combinados, los investigadores probaron los pulmones de m�s de 100 bomberos silvestres de California antes y despu�s de las temporadas de incendios de 1988 y 1989.
Los expertos en salud tambi�n sospechan que la exposici�n al humo de incendios forestales puede acelerar el envejecimiento, impulsar ataques card�acos fatales o c�ncer y desencadenar enfermedades respiratorias como bronquitis cr�nica o asma.
"Despu�s de los incendios, tosen Black Gunk.
Luego, despu�s de una semana, piensan que han vuelto a la normalidad.
Pero nuestros estudios muestran que sus pulmones no vuelven a la normalidad ", dijo el Dr. Robert Harrison, jefe de vigilancia de salud ocupacional del Departamento de Salud de California y el m�dico a cargo de uno de los estudios.
"Para algunos de los bomberos, la ca�da en la funci�n pulmonar fue bastante sorprendente".
Las peores dosis de mon�xido de carbono y otros productos qu�micos peligrosos, que pueden ser fatales, se emiten cuando los incendios arden, la etapa en la que los bomberos pasan la mayor parte de su tiempo, dijo Darold Ward, un qu�mico del Servicio Forestal de los Estados Unidos.
Mark Linane, quien dirige un equipo nacional de lucha contra incendios conocido como los Hotshots, dijo que ha visto bomberos de la superficie tan envenenada por el mon�xido de carbono que no pueden decidir qu� zapato atar.
Los peligros son particularmente agudos en el sur de California, lo cual es propenso a incendios m�s grandes y ahumados que en cualquier otro lugar de la naci�n.
Su sequ�a de cuatro a�os se ha vuelto a secar los pastizales, y sus condiciones clim�ticas estancadas y su topograf�a pueden atrapar el humo durante d�as.
Adem�s, los funcionarios forestales predicen que la temporada de incendios de la regi�n de 1990 ser� devastadora.
Los incendios de Yosemite, que han quemado m�s de 15,000 acres y todav�a est�n fuera de control, y los recientes incendios de Santa B�rbara y Glendale, que destruyeron casi 500 casas, son solo los cientos de blazes m�s grandes del estado.
"Tenemos mucho verano por delante y ya hemos quemado 600 o 700 estructuras", dijo el subdirector Keith Metcalfe, del equipo de extinci�n de incendios regional del sur del estado en Riverside."... Debido a la sequedad, los incendios arden m�s r�pida y m�s intensamente".
Durante casi 10 a�os, los bomberos urbanos han sabido que el humo t�xico de las estructuras y los autom�viles en quema aumenta enormemente sus posibilidades de c�ncer y enfermedades card�acas.
Los pandanes que alguna vez fueron su �nica protecci�n fueron reemplazadas por tanques de aire y m�scaras hace mucho tiempo.
Pero los funcionarios del Servicio Forestal de EE. UU. Y los bomberos estatales y del condado no han podido proteger a sus tripulaciones de incendios forestales porque no conocen el equipo lo suficiente como para que los bomberos los usen mientras caminan millas en terreno ardiente o lo suficientemente efectivo como para filtrar la amplia variedad de materiales t�xicos en humo.
Adem�s, el desarrollo de protecci�n para las tripulaciones ha recibido poca atenci�n y pr�cticamente ning�n financiamiento estatal o federal.
Funcionarios de bomberos nacionales dicen que no pueden obtener ayuda del Congreso a menos que puedan probar que los bomberos est�n dejando muertos.
Pero debido a que no se han financiado estudios de mortalidad, todo lo que tienen es la vieja broma que se escucha en el campo de bomberos: solo trate de encontrar un bombero forestal que todav�a respire despu�s de 60.
"Es un crimen que seguimos dejando que estos tipos funcionen as�", dijo James Johnson, director de proyectos de control de riesgos en el Laboratorio Nacional de Lawrence Livermore en el norte de California, que est� desarrollando equipo respiratorio para sus bomberos.
"Son un grupo olvidado, un subconjunto oculto de personas que han sido ignoradas en masa.
En todos mis 18 a�os de experiencia en higiene industrial, nunca he visto algo as� ".
En el pasado, los bomberos cre�an que su tos y congesti�n eran efectos secundarios fugaces.
Pero los nuevos estudios de salud han documentado cambios fisiol�gicos en los pulmones y las v�as respiratorias que no desaparecen con el humo.
Las pruebas de Johns Hopkins, realizadas en 52 bomberos del norte de California, durante la temporada de 1988, mostraron que su funci�n pulmonar, o flujo de ox�geno, permaneci� reducida en hasta un 3% incluso ocho semanas despu�s de la exposici�n.
Los investigadores dijeron que no han determinado si los pulmones sanan entre estaciones o si el da�o se acumula.
Las pruebas del Departamento de Salud de California en 63 bomberos mostraron que perdieron hasta el 10% de su capacidad pulmonar durante una temporada de incendios de seis meses, con una p�rdida promedio del 4%.
Para muchos bomberos, la congesti�n pulmonar se convierte en bronquitis o en neumon�a caminando tres veces al a�o, dijo Linane, de 46 a�os, que ha luchado contra incendios forestales durante 28 a�os.
"Tomas antibi�ticos y finalmente desaparece", dijo Linane.
"Pero luego regresa. Y m�s a menudo".
Durante los cuatro meses de incendios en 1988 en el Parque Nacional de Yellowstone, 12,000 bomberos buscaban ayuda m�dica para problemas respiratorios, y alrededor de 600 necesitaban atenci�n de un m�dico despu�s de regresar a casa, seg�n un informe del Servicio Forestal de EE. UU.
Stan Stewart, de 37 a�os, dijo que sab�a cu�ndo se uni� al Servicio Forestal a los 17 a�os que el trabajo era peligroso.
Pero no sab�a que se sentir�a cada vez m�s enfermo cada a�o.
"El m�dico me dijo que parece que hab�a fumado 10 paquetes de cigarrillos al d�a toda mi vida.
Pero nunca he fumado ", dijo Stewart, capataz de la tripulaci�n de Hotshot en el Bosque Nacional de Los Padres, cerca de Ojai.
"Mis pulmones probablemente reciban disparos.
Estoy un poco m�s preocupado cada a�o ".
Los investigadores de salud, generalmente reacios a interferir en las decisiones pol�ticas, dijeron que se sienten lo suficientemente fuertes por el peligro de que est�n instando a los bomberos a proporcionar protecci�n respiratoria lo antes posible.
"No nos sorprende que los bomberos hayan disminuido la funci�n pulmonar.
Solo quer�amos documentarlo para que las agencias de lucha contra incendios tomen medidas ", dijo el Dr. John Balmes, especialista en pulmonar y experto en salud ocupacional en UC San Francisco que ayud� con el estudio del estado.
Harrison dijo que las agencias forestales y los departamentos de bomberos deber�an advertir al menos a sus tripulaciones, y a los posibles reclutas, del peligro y considerar los cambios rotativos con m�s frecuencia para reducir la exposici�n al humo.
Aunque los hallazgos de los estudios nacionales a�n no se han publicado, Word se ha extendido a los altos funcionarios del departamento forestal estatal, quienes dicen que ahora est�n comenzando a buscar protecci�n para los 3.500 bomberos a tiempo completo de la agencia y alrededor de 2,000 estacionales.
"No quiero esperar a que los bomberos mueran", dijo Jack Wiest, jefe de planificaci�n e investigaci�n de incendios del departamento.
"Tenemos que subir a esto ahora mismo".
Parte de la soluci�n es desarrollar respiradores livianos y duraderos.
Los tanques de aire y las m�scaras que son est�ndar para combatir incendios estructurales pesan 40 libras y duran solo 15 a 30 minutos, por lo que no son pr�cticos para los equipos de incendios forestales que caminan millas que transportan hasta 60 libras de mangueras y herramientas.
El desarrollo de la tecnolog�a, sin embargo, toma dinero, y ninguno ha sido asignado por funcionarios forestales de California.
"La protecci�n respiratoria para los bomberos forestales es inexistente.
Y estamos viendo un par de a�os, al menos, antes de que se desarrolle algo ", dijo Wiest.
El a�o pasado, los investigadores del Servicio Forestal de EE. UU. Y Johns Hopkins le pidieron al Congreso que financiara un estudio de $ 13.4 millones y cuatro a�os para analizar la amenaza para la salud y desarrollar protecci�n respiratoria.
Pero William Sommers, director de investigaci�n de incendios forestales para el grupo nacional de coordinaci�n de incendios forestales, dijo que los fondos nacionales son escasos.
Su grupo, que incluye a todas las agencias federales y estatales involucradas en la lucha contra incendios forestales, ahora tiene menos de la mitad del dinero de la investigaci�n que recibi� hace 15 a 20 a�os.
Este a�o, alrededor de $ 10 millones, menos del 1% de los d�lares federales gastados en luchar contra incendios forestales, se reservan para la investigaci�n.
Sommers dijo que ninguna otra agencia federal dedica tan poco, especialmente cuando los trabajos son potencialmente mortales.
Otros grupos ocupacionales propensos a peligros, como trabajadores qu�micos y equipos de construcci�n, est�n mucho mejor protegidos, dijo.
La mayor�a de los empleadores son requeridos por las regulaciones federales para proporcionar equipos que protejan a los trabajadores de humos peligrosos u otras amenazas.
Los propios bomberos, que tradicionalmente han blandido la actitud de que si no puede soportar el calor, sale del fuego, ahora exigen protecci�n.
Su cambio de actitud fue en gran medida como resultado del inolvidable verano de California de 1987.
En lo que se llam� el "asedio del '87", 1.500 incendios atacaron el Bosque Nacional Klamath cerca de la frontera de California-Oregon en un mes.
Una fuerte capa de inversi�n se instal� en el valle, atrapando el humo espeso durante todo el tiempo.
El sindicato de bomberos, preocupado de que las tripulaciones se estaban enfermando en n�meros r�cord, le pidi� a la Universidad Johns Hopkins que enviara a un m�dico.El Dr. Patrick Ford, entonces residente m�dico especializado en salud ocupacional, observ� el campamento base de los bomberos, que estaba envuelto por una espesa niebla de humo negro.
"Fuimos miserables pocas horas despu�s de llegar.
Ardor de la nariz, quemaduras de garganta, ojos ardientes.
Y no desapareci� ", dijo Ford, ahora m�dico de medicina ocupacional de la Marina en Filadelfia.
Los oficiales de polic�a en el campamento, llamaron para dirigir el tr�fico, estaban usando linternas al mediod�a porque no pod�an ver 20 yardas frente a ellos.
Ford record� que algunos de los oficiales usaban m�scaras de gas, sin embargo, los bomberos solo llevaban bandanales.
Las tripulaciones dorm�an en carpas improvisadas: las s�banas de algod�n se colgaban de los tendederos.
Debido a que el campamento no ofreci� un respiro del humo, los bomberos lo respiraron las 24 horas del d�a durante varias semanas.
Ford cuestion� a los bomberos sobre su salud y se sorprendi� por cu�ntos ten�an s�ntomas alarmantes.
Escuch� sus cofres y escuch� sibilancias en casi todos.
Candace Gregory, de 33 a�os, la primera mujer jefe de batall�n en el Departamento de Forestal de California, recuerda que 11 miembros en su tripulaci�n de 15 cayeron enfermo.
"Literalmente viv�as en el humo.
Despu�s de un tiempo, estabas tosiendo todo el tiempo ", dijo.
Inmediatamente despu�s de los incendios de 1987, Ford y otros investigadores de Johns Hopkins y el Departamento de Salud de California aumentaron los estudios.
"Es el viejo clich� de c�mo no pones esa se�al de tr�fico en una intersecci�n peligrosa hasta que hay mucha gente asesinada", dijo Wiest.
Ahora 46 y trabajando en su 28� temporada de incendios, Linane se retirar� en unos a�os.
Mirando hacia atr�s, dice, "Cuando el bombeo de la adrenalina y est�s salvando propiedades y vidas y importantes recursos naturales, parece que vale la pena.
Pero cuando me retiro y estoy tosiendo y amordazando, puede ser otra historia.
"El m�dico aqu� recomend� que tomara pruebas pulmonares completas.
Le dije: 'S�, s�, claro'.
Pero nunca lo hice.
Me temo que me dir� que realmente hay algo mal.
Todos tenemos miedo de hacerlo.
Ninguno de nosotros quiere saber cu�nta funci�n pulmonar realmente hemos perdido ".
Yosemite dispara: mientras las llamas se desaceleraron, la preocupaci�n se mantuvo alta sobre posibles nuevas tormentas de rayos.
A3 C�mo los venenos en el humo afectan a los bomberos Los bomberos de tierra salvaje est�n expuestos a una variedad de sustancias venenosas en el humo que respiran.
Cuando el material o el cepillo del bosque se incendia, la combusti�n incompleta convierte la vegetaci�n inofensiva en un aluvi�n de productos qu�micos peligrosos.
Los venenos: 1. Mon�xido de carbono: este gas sin olor invisible ataca el cerebro y el sistema nervioso de un bombero, causando desorientaci�n temporal, juicio deteriorado y tiempos de reacci�n m�s lentos.
Tambi�n pone estr�s extremo en el coraz�n.
2. Compuestos inorg�nicos: incluidos el plomo y el azufre, estos materiales var�an ampliamente en el humo, dependiendo del contenido del suelo.
El plomo, que se encuentra en altas concentraciones en el humo del sur de California, los incendios forestales, puede causar da�o neurol�gico.
3. Aldeh�dos: estos fuertes irritantes, especialmente acrole�na y formaldeh�do, se encuentran en altas concentraciones en el humo.
Se cree que causan gran parte de las sibilancias, la tos y la irritaci�n de los ojos sufridos por los bomberos de la tierra salvaje.
Muchos de ellos tambi�n son carcin�genos.
4. Particulaciones: estas peque�as piezas de carbono negro se alojan profundamente en los pulmones, posiblemente causando asma, bronquitis cr�nica y c�ncer.
Las part�culas m�s peque�as, que son las m�s peligrosas para los pulmones, se encuentran en incendios ardientes, donde los bomberos pasan la mayor parte de su tiempo.
5. Ozono: este potente qu�mico se forma durante los incendios cuando hay una fuerte luz solar y el humo est� atrapado por patrones clim�ticos estancados.
Las pruebas humanas muestran ozono, que tambi�n es el ingrediente principal del smog, obstaculiza la funci�n pulmonar, mientras que las pruebas de animales indican que puede causar enfermedades respiratorias cr�nicas.
6. �cidos org�nicos: incluido el �cido f�rmico y el �cido ac�tico, estos productos qu�micos son irritantes poderosos para los pulmones, los ojos y la garganta.
The Protection: Bandanna: Los bomberos de la tierra salvaje no usan nada m�s que una tela delgada en la cara para proteger sus pulmones del humo.
Los funcionarios de bomberos dicen que no han encontrado equipos respiratorios livianos que sea efectivo y seguro para las tripulaciones, que tienen que caminar cuesta arriba con 60 libras de equipo.
Equipo: el equipo est�ndar generalmente incluye un hosepack, casco, gafas, botas y un mono de retraso de fuego.
Purificador de aire: tripulaciones de bomberos en Lawrence Livermore National Laboratory usa purificadores de aire de 1 libra.
El aire ahumado pasa a trav�s de filtros que eliminan peque�as part�culas de holl�n que pueden alojarse en los pulmones.
Sin embargo, los gases como el mon�xido de carbono no se filtran.
Los funcionarios de bomberos nacionales y estatales permanecen desconfiados de la
Purificadores porque no brindan protecci�n completa contra el humo.
Hidrocarburos arom�ticos polinucleares: estos compuestos, que se creen que son agentes que causan c�ncer, se unen a part�culas de holl�n en incendios de tierras salvajes.
Los expertos no est�n seguros de si los compuestos causan da�o gen�tico en las c�lulas sangu�neas.
Fuentes: Estaci�n de Investigaci�n Intermountain del Servicio Forestal del USDA, Departamento de Servicios de Salud de California y la Escuela de Higiene y Salud P�blica de la Universidad Johns Hopkins, Laboratorio Nacional de Lawrence Livermore National


