Aqu� hay una breve mirada a los desarrollos de incendios forestales en los estados occidentales: alrededor de 2.1 millones de acres arden de 30 incendios, dijo la Oficina Federal de Gesti�n de Tierras.
Sue Mitchell, una portavoz de BLM, dijo que no hubo incendios activos o en llamas y todos menos unos pocos bomberos han sido retirados del mercado de las incendios.
Dos nuevos incendios aclararon el mi�rcoles en el Bosque Nacional de Sierra, uno de m�s de 1,800 acres al oeste del Parque Nacional Kings Canyon;el otro cerca de Mariposa, fuera del Parque Nacional Yosemite.
El Blaze Mariposa destruy� al menos una casa y creci� a 250 acres.
Los equipos de bomberos se movilizaron en el Valle Estelar del este de Idaho despu�s del fuego de 6.800 acres Trail Creek, avivado por vientos cambiantes, rompieron una l�nea de control y corrieron a una milla de ranchos y campos de granos.
El incendio fue el m�s amenazante de 17 incendios importantes ardiendo en m�s de 70,000 acres.
M�s de 2.500 bomberos, respaldados por el personal y el equipo de la Guardia Nacional, estaban en las l�neas.
El incendio de la barra Eagle de 7,900 acres cerca de Hells Canyon en la l�nea de Oregon se quem� fuera de control para el d�a 12.
La oferta de miembros de Fresh Fire Crew disminuye a medida que los trabajadores ingresan a su tercera semana de servicio.
El Servicio Forestal de EE. UU. Predeci� el clima c�lido y seco m�s all� del fin de semana del D�a del Trabajo.
Los tres incendios m�s grandes de Montana se arden principalmente en �reas silvestres: el incendio de 51,000 acres en el arroyo Canyon en el desierto de chivo expiatorio en el noroeste de Montana;y el fuego Storm Creek de 43,000 acres y el fuego Hellroaring Creek de 42,000 acres en el desierto de Absaroka-Beartooth al norte del Parque Nacional Yellowstone.
El incendio de Canyon Creek est� ardiendo hacia varios ranchos fuera del desierto, mientras que los bomberos est�n tratando de evitar que los otros dos incendios se quemen hacia el sur en el parque.
El gobernador Ted Schwinden apel� al p�blico el mi�rcoles para evitar la recreaci�n al aire libre durante el fin de semana del D�a del Trabajo para limitar el riesgo de incendio.
El gobernador dijo que 5,000 bomberos estaban luchando contra incendios en 208,000 acres en Montana, y 173,000 acres quemados en incendios anteriores.
Puso el costo en $ 15 millones.
El Departamento de Forestaci�n de Oreg�n anunci� el mi�rcoles que 3.8 millones de acres de bosques privados, estatales y otros en el este de Oreg�n estar�an cerrados hasta un aviso adicional debido al peligro de incendio extremo y escasos recursos para combatir nuevos incendios.
El mayor incendio forestal de Oregon, el Fuego de Butte de Tepee, ha quemado 36,000 acres de hierba y madera en el Bosque Nacional Wallowa-Whitman y el �rea de Recreaci�n Nacional del Ca��n del Infierno.
Se espera que el fuego crezca a 59,000 acres antes de que alcance las l�neas de fuego.
El Servicio Forestal comenz� a entrenar al primero de los 700 bomberos de respaldo para deletrear equipos cansados en Oregon y Washington.
Los bomberos de Utah controlaron un incendio de 525 acres cerca del embalse de Pineview del norte de Utah y un incendio de cepillo de 100 acres en Butterfield Canyon, y esperaba controlar un incendio de 60 acres en el este de Utah por esta noche.
Una compa��a de productos forestales cerr� 400,000 acres al p�blico debido al peligro de incendio y el Servicio Forestal de los Estados Unidos prohibi� los fogatas fuera de los campamentos designados en el Bosque Nacional de Wenatchee.
Los equipos cortaron senderos alrededor de los principales incendios a lo largo de la cordillera Kettle River en la Reserva India de Colville y en el Bosque Nacional de Colville.
M�s de 1.900 personas fueron asignadas a l�neas de incendio y deberes de limpieza en el estado de Washington.
El incendio m�s grande del estado ard�a en la reserva india, donde se han quemado 9,250 acres de madera y cepillo.
Los incendios han quemado m�s de 23,000 acres en el estado.
La entrada sur del Parque Nacional de Yellowstone puede volver a abrir hoy, ya que se pronostica que los vientos se calmar�n.
Los bomberos humedecen los edificios en la caba�a de Canyon y el complejo del hotel y movieron los cepillos y los �rboles muertos fuera del camino del fuego de la bifurcaci�n norte de 109,000 acres.
Ocho incendios est�n quemando m�s de una quinta parte de los 2.2 millones de acres del parque.
El incendio m�s grande, el Clover-Mist, creci� en casi 40,000 acres a 231,000 acres.
Al sur en el Bosque Nacional Bridger-Teton, los fuertes vientos empujaron a los 42,000 acres Huck Fire 7,000 acres al este el martes.
Tanto el Fuego de Huck como el Fuego de 112,500 Mink Creek tambi�n est�n creciendo hacia el norte hacia Yellowstone.


