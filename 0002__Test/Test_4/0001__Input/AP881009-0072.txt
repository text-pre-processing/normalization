Ben Johnson tom� esteroides a sabiendas y los cercanos al corredor tambi�n estaban al tanto de ello, su compa�era velocista ol�mpica canadiense Angella Issajenko fue citado en una entrevista publicada el domingo.
Johnson despojado de su medalla de oro ol�mpica en la final de 100 metros despu�s de que dio positivo por el esteroides prohibido Stanozolol _ tambi�n estaba tomando esteroides cuando estableci� un r�cord mundial de sprint el verano pasado en Roma, el peri�dico de Toronto Star cit� a la Sra. Issajenko como dijo.
La Sra. Issajenko, quien tambi�n compiti� en los Juegos Ol�mpicos en Se�l, Corea del Sur, fue citado diciendo que tambi�n tom� la droga de construcci�n muscular.
Los Juegos Ol�mpicos, que comenzaron el 17 de septiembre, terminaron el 2 de octubre.
M�s tarde, su esposo Tony dijo que la Sra. Issajenko neg� decir que ella y Johnson tomaron esteroides.
La estrella de Toronto dijo el domingo que estaba de pie junto a su historia.
La Sra. Issajenko acus� anteriormente a un terapeuta de dar a ella y a los esteroides de Johnson sin su conocimiento, pero luego se retract� de la declaraci�n.
La Sra. Issajenko en la entrevista afirm� que el Dr. George Mario (Jamie) Astaphan proporcion� esteroides y monitore� el programa con el conocimiento de Charlie Francis, entrenador del Mazda Optimist Track Club, al que ella y Johnson pertenecen.
`` No me importa m�s '', dijo la Sra. Issajenko.
`` Estoy harto de todo el toro.... Ben toma esteroides.
Tomo esteroides.Jamie (Astaphan) nos los da y Charlie no es un cient�fico, pero sabe lo que est� sucediendo ''.
Johnson, de 26 a�os, dice que nunca ha tomado drogas ilegales a sabiendas, mientras que Astaphan ha dicho que nunca ha administrado Stanozolol _ el esteroide prohibido que se encuentra en la muestra de orina del corredor despu�s de su carrera r�cord mundial de 100 metros en los Juegos Ol�mpicos.
Mientras tanto, Francis ha dicho que el resultado de la prueba `` desaf�a toda la l�gica '' y solo pudo explicarse `` por manipulaci�n deliberada del proceso de prueba ''.
La Sra. Issajenko, de 30 a�os, fue citada diciendo que ten�a conocimiento de primera mano que Johnson recibi� esteroides de Astaphan de 1984 a 1986, pero `` Ben iba a Jamie despu�s de eso ''.
Tambi�n dijo que Astaphan estaba administrando esteroides a Johnson cuando estableci� un r�cord mundial de 9.83 segundos en el sprint de 100 metros en el Campeonato Mundial en Roma.La carrera de Se�l de Johnson baj� esa marca a 9.79 segundos.
La Sra. Issajenko, la campeona canadiense de 100 y 200 metros, dijo que ha sido amenazada desde que ha presentado sus acusaciones contra el Johnson, nacido en Jamaica.
Despu�s de que Johnson dio positivo por esteroides en los Juegos Ol�mpicos, Issajenko dijo que el terapeuta Waldemar Matuszewski hab�a `` manipulado '' con ella y Johnson.La Sra. Issajenko afirm� que el terapeuta hab�a puesto esteroides anab�licos en el complejo de roce que us� para masajear sus m�sculos.
La historia fue llamada `` Nonsense '' por el m�dico de la pista ol�mpica canadiense Dr. Robert Luba e Issajenko luego retiraron sus cargos contra Matuszewski.
El Gobierno Federal ha designado al Presidente Asociado de Ontario Charles Dubin para dirigir una investigaci�n judicial sobre el asunto de Ben Johnson.
Astaphan y Francis no pudieron ser contactados de inmediato para hacer comentarios.


