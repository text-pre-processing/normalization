La etiqueta "Black Conservative", ahora firmemente adjunto al nombre de Clarence Thomas, no comienza a contar la historia de su vida, una historia estadounidense de transformaci�n.
Nacido en el corral, pobre en Georgia segregada, abandonada por su padre, Thomas y su hermano fueron criados por estrictos abuelos que cre�an la biblia que le ense�aron a nunca decir: "No puedo".
Thomas y su hermano lo hicieron en el mundo blanco.
Su hermana, criada por una t�a, tuvo cuatro hijos y se fue de bienestar.
Thomas, de 43 a�os, acredita todo lo que ha logrado a sus abuelos.
Se atragant� dos veces en la televisi�n nacional el lunes cuando los mencion�.
En un mundo hostil, le ense�aron a confiar en s� mismo.
Formaron sus puntos de vista sobre el individualismo, la raza y la sociedad, las opiniones que lo gu�an hoy.
"Me criaron para sobrevivir bajo el totalitarismo de la segregaci�n", escribi� Thomas en un art�culo para la Heritage Foundation, un instituto conservador de pol�ticas p�blicas en Washington.
"Nos criaron para sobrevivir a pesar de la oscura y opresiva nube de intolerancia sancionada gubernamentalmente.
La autosuficiencia, la seguridad "autosuficiencia y seguridad espiritual y emocional fueron nuestras herramientas para forjar y asegurar la libertad", agreg�.
"Aquellos que intentan capturar el asesoramiento diario, la supervisi�n, el sentido com�n y la visi�n de mis abuelos en un programa gubernamental se involucran en una locura".
Las creencias que han llevado a Thomas a los pasos de la Corte Suprema de los Estados Unidos lo hacen sospechar de activistas pol�ticos negros, veteranos de la lucha por hacer que el gobierno cuente por los errores que se hacen a los negros.
Sin embargo, como ha se�alado el periodista del Washington Post, Juan Williams, Thomas se basa firmemente en la tradici�n intelectual negra de Booker T. Washington, quien abog� por la educaci�n, la autosuficiencia y
El apoyo mutuo como el principal medio de avance.
Clarence Thomas naci� el 23 de junio de 1948 en Pinpoint, Georgia, una ciudad en las marismas cerca de Savannah.
Su madre, Leola, de 18 a�os en ese momento, viv�a en una casa que no ten�a fontaner�a.
Antes del segundo cumplea�os de Thomas, su padre se mud� al norte y dej� a la familia atr�s.
Su madre se volvi� a casar y su segundo esposo no quer�a a los hijos de su primer matrimonio.
Acogido por abuelos a los 7 a�os, Thomas fue enviado a vivir con sus abuelos.
Su abuelo, Myers Anderson, ten�a poca educaci�n formal.
Pero la vida le hab�a ense�ado mucho.
"Apenas pod�a leer y escribir, leer lo suficiente para leer la Biblia", dijo Thomas en una entrevista de 1983 con el Washington Post.
"Pero era un anciano duro".
�l elabor� en el documento de la Fundaci�n Heritage: "Por supuesto, pens� que mis abuelos eran demasiado r�gidos y sus expectativas eran demasiado altas.
Tambi�n pens� que eran malos a veces....Lo m�s compasivo que hicieron por nosotros fue ense�arnos c�mo valerse por nosotros mismos en un entorno hostil ".
Pero el mundo que yac�a m�s all� de los confines de la pobreza y la segregaci�n no estaba totalmente cerrado a Thomas.
Su abuelo, un cat�lico, lo inscribi� en una escuela completamente negra dirigida por la iglesia.
El lunes, Thomas tambi�n se asegur� de agradecer a "The Nuns".
Cuando era joven, quer�a convertirse en sacerdote.En 1967, fue aceptado en el Seminario de Concepci�n Inmaculada All-Blanco en Conception Junction, Mo. Estaba en un shock.
Otros seminaristas se refirieron a �l como el "punto negro en un caballo blanco".Disgustado, se fue al final de su primer a�o.
Cola con 'Black Power' Thomas asisti� a Holy Cross College en Worcester, Mass.
En la d�cada de 1960, coquete� con la pol�tica del "poder negro" y se consider� seguidor de Malcolm X.
Pero su verdadero inter�s era en la ley.
Recibi� su t�tulo de abogado de la Universidad de Yale en 1974.
Como joven abogado, Thomas trabaj� en la oficina del Fiscal General de Missouri, John C. Danforth.
M�s tarde se uni� a Monsanto Co.
La introducci�n de Thomas a Washington lleg� en 1979.
Para entonces, Danforth era un senador republicano.
Thomas, un ex dem�crata, se uni� a su personal como asistente legislativo.
En 1982, despu�s de un a�o como Secretario Asistente de Educaci�n de Derechos Civiles, Thomas fue nombrado por el presidente Reagan para dirigir la Comisi�n de Igualdad de Oportunidades de Empleo.
Su mandato en la Comisi�n, que investiga las quejas de discriminaci�n, fue controvertido.
Los cr�ticos dijeron que la teagencia se volvi� suave bajo Thomas.
Al abandonar el apoyo a los objetivos de contrataci�n y los horarios, Thomas se centr� en resolver miles de quejas de discriminaci�n individual.
Se atribuy� el cr�dito por mejorar la eficiencia.
Cuando el presidente Bush nomin� a Thomas a la Corte de Apelaciones de los Estados Unidos para el Distrito de Columbia, se pronostic� un trampol�n para la Corte Suprema, se pronostic� un proceso de confirmaci�n contenciosa.
Pero las audiencias fueron sin problemas, tal vez porque Thomas nunca estuvo entre los cr�ticos m�s abiertos de los derechos civiles de la administraci�n Reagan.
Tom� su asiento en la corte de apelaciones en marzo de 1990.
Thomas, que vive en Suburban Virginia, est� casado y tiene un hijo.
"En mi opini�n", dijo sobre su vida el lunes, "solo en Estados Unidos esto podr�a haber sido posible".


