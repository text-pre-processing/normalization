El presidente Reagan advirti� el s�bado que vetar� cualquier legislaci�n de bienestar que el Congreso le env�e que no contiene un requisito de trabajo.
`` La mejor manera de aprender a trabajar es trabajar '', dijo el presidente en un discurso de radio de fin de semana del D�a del Trabajo de su rancho de vacaciones a 20 millas al norte de aqu�.
El representante Thomas J. Downey, D-N.Y., En la respuesta de los dem�cratas, dijo que el trabajo de bienestar en el trabajo de bienestar puede ser necesario, pero dijo que tales requisitos deber�an ser controlados por los funcionarios de bienestar estatales y locales, no el gobierno federal.
Reagan se mezcl� de la reforma del bienestar con la celebraci�n del historial econ�mico de su administraci�n, diciendo que las cifras de desempleo del viernes mostr� que la tasa de desempleo `` flot� justo por encima del m�s bajo que ha sido en 14 a�os ''.
Las cifras del Departamento de Trabajo mostraron un desempleo de 5.6 por ciento, frente al 5.4 por ciento en julio y de la cifra de mayo del 5.3 por ciento, que fue un m�nimo de 14 a�os.
`` Pero todav�a hay algunos estadounidenses a los que nuestra expansi�n ha pasado _ aquellos atrapados en la trampa de bienestar '', dijo.
Para lidiar con esto, dijo, su administraci�n lanz� un programa alentando a los estados a encontrar sus propios planes para sacar a las personas de las listas de bienestar.
`` Casi la mitad de los estados han implementado o propuesto planes generalizados de reforma de bienestar que se basan en alg�n buen sentido com�n _ que la mejor manera de aprender a trabajar es trabajar '', dijo el presidente.
`` Ahora, el Congreso parece estar cerca de una decisi�n sobre la reforma del bienestar y tengo un mensaje para ellos '', dijo.
`` No aceptar� ning�n proyecto de ley de reforma de bienestar a menos que est� orientado a hacer que las personas sean independientes del bienestar ''.
Un comit� de la Conferencia de Senado de la C�mara de Representantes actualmente tiene antes de un proyecto de ley aprobado por el Senado que contiene un requisito de trabajo y una medida aprobada por la casa que no lo hace.
`` Cualquier proyecto de ley que no sea construido en torno al trabajo no es una verdadera reforma de bienestar '', dijo el presidente.
`` Si el Congreso me presenta un proyecto de ley que reemplaza el trabajo con la expansi�n del bienestar y que coloca la dignidad de la autosuficiencia a trav�s del trabajo fuera del alcance de los estadounidenses en el bienestar, usar� mi pluma de veto ''.
Downey dijo que mientras m�s personas que nunca est�n trabajando, `` El hecho es que el trabajador t�pico en Estados Unidos no est� mejor hoy que �l o ella hace 10 a�os;De hecho, las cosas han empeorado ''.
El 40 por ciento m�s pobre de las familias estadounidenses, con ingresos ajustados por la inflaci�n, est� peor hoy que hace 10 a�os;El 5 por ciento m�s rico est� mejor que hace una d�cada;Y 32.5 millones de estadounidenses permanecen sumidos en la pobreza, dijo.
El proyecto de ley de la C�mara, con programas de capacitaci�n y educaci�n, as� como beneficios para la salud y cuidado infantil, har�a que los padres de bienestar que trabajan mejor que aquellos que no lo hacen, dijo Downey, presidente interino de las formas y medios, subcomit� de asistencia p�blica y compensaci�n de desempleo.
`` S�, puede ser necesario requerir que un destinatario de bienestar trabaje, pero esos requisitos deben ser controlados por funcionarios estatales y locales que administran nuestros programas de bienestar, no bur�cratas federales '', dijo Downey.


