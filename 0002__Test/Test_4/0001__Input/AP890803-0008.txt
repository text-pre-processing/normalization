Los funcionarios advirtieron a los residentes de las Islas V�rgenes de EE. UU., Puerto Rico y las islas cercanas que atornillaran todo y se abastecieran de comida y agua el mi�rcoles cuando el hurac�n Dean retumb� en el Caribe del Este.
Dean fue mejorado de una tormenta tropical al segundo hurac�n de la temporada del Atl�ntico el mi�rcoles, y por el anochecer, el Centro Meteorol�gico Nacional en Puerto Rico inform� que los vientos del hurac�n se hab�an fortalecido a 80 mph.
Se publicaron advertencias de huracanes para las Islas Leeward desde Antigua hasta las Islas V�rgenes de EE. UU. Y Puerto Rico, dijeron los pronosticadores en el Centro Nacional de Huracanes cerca de Miami.
A la medianoche EDT, los pronosticadores informaron que el centro del hurac�n estaba en la latitud 18.5 Norte y Longitud 61.3 West, movi�ndose hacia el oeste a 15 mph.Su �ltima posici�n estaba a 310 millas al este de Puerto Rico, a 65 millas al noreste de Barbuda y a unas 240 millas al este de St. Thomas.
La tormenta se mov�a sobre aguas tropicales c�lidas a 15 mph, ligeramente hacia abajo de los 20 mph que hab�a sufrido gran parte del d�a.
Pero los pronosticadores dijeron que era posible un fortalecimiento en las pr�ximas 24 horas.
Un aviso emitido por el Servicio Meteorol�gico Nacional en San Juan para las Islas V�rgenes de los EE. UU. Y Puerto Rico aconsej� a los residentes que `` asegurar objetos sueltos o moverlos en el interior '' de la tabla o ventanas de cinta y abastecerse de suministros de emergencia como agua potable, alimentos.Eso no necesita refrigeraci�n ni bater�as.
`` Esta es una tormenta peligrosa y no debe tomarse a la ligera, a pesar de que es un hurac�n m�nimo '', dijo.
`` No te arriesgues.
Podr�a provocar lesiones o incluso muerte ''.
En San Juan, una ciudad de 1.1 millones de personas, los compradores formaron largas colas en supermercados, los trabajadores subieron ventanas de la mansi�n del gobernador y tiendas en el distrito tur�stico del viejo San Juan.
Un aviso de huracanes dijo que los informes de aviones indicaron que el centro de Dean se estaba moviendo hacia el oeste despu�s de hacer un trote temporal del noroeste.
`` Esto aumenta la amenaza para las islas de sotavento del norte '', dijo.
Los pronosticadores dijeron que se esperaba que la tormenta llegara primero a la tierra en Barbuda, la isla de Leeward m�s oriental y se mudara al noroeste hacia las Islas V�rgenes de EE. UU. Y Puerto Rico durante las pr�ximas 24 a 36 horas.
La radio del gobierno en el estado de Antigua y Barbuda de Twin Island dijo que se esperaba que Dean golpeara a Barbuda el mi�rcoles por la noche, y advirti� a los residentes que produjeran ventanas, enganchen objetos sueltos y abastecieran en el agua.
Barbuda es una isla de coral plana de 62 millas cuadradas con una poblaci�n de 1.200 y poca industria o turismo.
Se elimin� una advertencia de hurac�n para Guadalupe, al igual que una vigilancia de hurac�n para Dominica y Martinica.
En Coral Gables, Florida, el especialista en huracanes Jim Gross llam� a Dean un peque�o hurac�n, con vientos de fuerza de huracanes confinados a menos de 25 millas de su centro.
Se dijo que la lluvia de 4 a 8 pulgadas y las mareas de 2 a 4 pies por encima de lo normal era posible en el camino de la tormenta.
Las autoridades transmiten advertencias similares en varias islas cercanas.
Dean creci� de la quinta depresi�n tropical de la temporada el lunes por la ma�ana en una tormenta nombrada el lunes por la noche.
Se han formado otras tres tormentas desde que comenz� la temporada el 1 de junio.
La tormenta tropical Allison, caus� inundaciones generalizadas en Texas y Louisiana en junio.
Barry aument� el Atl�ntico abierto el mes pasado antes de disiparse.
Chantal se convirti� en hurac�n el lunes y fue degradado a una depresi�n tropical el martes por la noche despu�s de llegar a tierras en Texas.


