Entre los 29 millones de estadounidenses que votaron por el dem�crata George McGovern en 1972 se encontraba un estudiante de derecho negro de 24 a�os y agitador de un solo campus llamado Clarence Thomas.
Como Thomas explic� m�s tarde, ser republicano fue considerado como "un destino ... peor que la muerte entre los negros".
Sin embargo, en cuesti�n de semanas, Thomas se presenta ante el Comit� Judicial del Senado como candidato del presidente Bush para unirse a la Corte Suprema cada vez m�s conservadora.
La dram�tica transformaci�n pol�tica y filos�fica de Thomas revela m�s sobre el hombre que su viaje de Horatio Alger desde la pobreza rural del sur hasta la nominaci�n de la Corte Suprema.
Para los amigos, la suya es una historia de coraje, para enemigos, una historia de oportunismo.
Ira racial, letra de protesta;Las homil�as caseras de su abuelo, la disciplina que abarca el gobernante de las monjas que le ense�� en una escuela cat�lica en Savannah segregada, Georgia, la ira racial en los escritos de Richard Wright y Malcolm X, las teor�as iconoclastas de los acad�micos como Thomas como Thomas como ThomasSowell y William Barclay Allen, incluso las letras de protesta de la cantante y compositora Nina Simone, todas son partes de la historia.
Como se vislumbr� en docenas de entrevistas y decenas de miles de p�ginas de documentos que Thomas ha entregado al Comit� Judicial del Senado, estas influencias ayudaron a dar forma a un conjunto de creencias que ahora son objeto de una amarga controversia.
Thomas se enorgullece de haber apostado un curso independiente a pesar de sufrir lo que dijo que era un gran n�mero personal en amigos perdidos y condenaci�n p�blica.
Black 'clones intelectuales' "me niego a someterme a la ortodoxia racialmente despectiva que dice que todos los negros deber�an compartir la misma opini�n sobre ...Clones intelectuales ", dijo en un discurso de 1984 a los estudiantes negros de la Facultad de Derecho de Yale, donde obtuvo su t�tulo de abogado.
Thomas subraya su papel de figura minoritaria dentro de una minor�a al citar repetidamente el recuerdo po�tico de Robert Frost: "Dos caminos divergieron en una madera, y yo, tom� el que menos viaj�, y eso ha marcado toda la diferencia".
Incluso sus amigos cercanos tienen problemas para explicar por qu� Thomas tom� un camino diferente.
Cuando era joven, Clarence Thomas comparti� las actitudes liberales de muchos j�venes negros brillantes que nacieron en una Am�rica segregada y llegaron a la mayor�a de edad despu�s de los viajes en libertad, las sentadas en la sala de almuerzo y la Ley de Derechos Civiles de 1964 comenzaron a borrar los signos manifiestos de discriminaci�n racial.
Como estudiante de Holy Cross College en la turbulenta d�cada de 1960, se uni� a los manifestantes negros, llevaba una boina y una chaqueta de cuero, y decoraba su habitaci�n de dormitorio con un p�ster de Malcolm X.
Pero Thomas vino a ver sus a�os universitarios como maleados.
Su cambio hacia el derecho, o, por su cuenta, se dio vueltas a los valores conservadores, comenz� mientras era un estudiante de derecho de Yale de 1971 a 1974.
Thomas fue admitido mientras estaba en vigencia un programa de acci�n afirmativa, aunque no hay evidencia de que no hubiera entrado sin �l.
Visi�n de un amigo;Cualesquiera que sean las razones, y sus compa�eros de clase y miembros de la facultad en Yale no pueden identificar ning�n punto de inflexi�n en particular o eventos fundamentales, Thomas "se volvi� m�s conservador a medida que pasaba por el proceso de educaci�n legal", dijo Harry Singleton, un amigo.
Cuando Thomas lleg� a Jefferson City, Missouri, en 1974 para trabajar para John Danforth, ahora el principal partidario de Thomas en el Senado, entonces el Fiscal General del Estado republicano, sus actitudes se formaron en gran medida.
"Su filosof�a en ese momento era que sent�a que este pa�s estaba brindando oportunidades de personas si estaban dispuestos a trabajar y que para confiar en el gobierno estaba en la naturaleza de la servidumbre", dijo el abogado Harvey Tettlebaum, quien trabaj� con Thomas.
Varios a�os m�s tarde captur� la atenci�n del equipo de transici�n de Reagan, que ofreci� un trabajo de derechos civiles a un reacio Thomas.
Sus amigos lo instaron a no rechazar una oportunidad rara para hacer una pol�tica, y acept� trabajos sucesivos como Secretario Asistente de Educaci�n de Derechos Civiles y Presidente de la Comisi�n de Igualdad de Oportunidades en el Empleo.
Una constituci�n colorblind;En esos trabajos, Thomas comenz� a cuestionar las preferencias en empleos y educaci�n para las minor�as raciales que hist�ricamente hab�an sufrido discriminaci�n.
M�s tarde, comenz� a oponerse abiertamente a tales preferencias, denunciando las decisiones de la Corte Suprema que las confirmaron y pidieron una constituci�n de colorblind.
Algunos cr�ticos atribuyen c�nicamente su metamorfosis ideol�gica al oportunismo.
Thomas, quien se ha negado a ser entrevistado desde su nominaci�n a la Corte Suprema, no ha respondido.
Pero ha ofrecido una explicaci�n para su cambio pol�tico de coraz�n.
En notas escritas a mano de sus archivos, record� haberle dicho a sus abuelos dem�cratas por qu� se hab�a convertido en republicano.
"Todos me hicieron convertirme en republicanos", dijo a sus incr�dulos abuelos.
"�Recuerdas ... cuando me dijiste que no era correcto rogar mientras pudiera trabajar y conseguirlo yo mismo? ... Recuerda cuando me dijiste que si alguna vez equival�a a algo, ser�a el sudor de�Mi frente y mi grasa en el codo? "�Y recuerdas cuando dijiste que preferir�as morir de hambre que que alguien te d� algo, siempre que pudieras trabajar para ello?...Pol�ticamente, no ten�a otra opci�n: el �nico partido que se destacaba abiertamente a esos valores era el Partido Republicano ".


