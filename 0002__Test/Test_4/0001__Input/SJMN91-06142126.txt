El congelamiento de diciembre mat� a un cepillo pesado y millones de �rboles que ahora est�n preparados para quemar.
Las lluvias de marzo nutrieron el c�sped que pronto se secar� Tinder.
Todo eso despu�s de cinco a�os de sequ�a marcaron el paisaje.
A menos que intervenga un verano inusualmente fresco, estos son los ingredientes para lo que podr�a convertirse en una de las peores temporadas de incendios forestales en la historia de California.
"En este momento, las lluvias han provocado que todo se encienda, pero eso puede cambiar en unas pocas semanas", dijo el lunes Lisa Boyd, portavoz del Departamento de Forestaci�n y Protecci�n de Incendios de California en Sacramento.
"El estado es realmente un Tinderbox este a�o.
Todos tendr�n que tener mucho cuidado ".
En el condado de Santa Clara, las condiciones a�n no son extremas;Las tormentas de marzo probablemente retrasaron la temporada de fuego hasta mediados de junio.
Pero las tierras salvajes del condado est�n llenas de combustible pesado (cepillos y �rboles, asesinados por la sequ�a o la congelaci�n de diciembre.
"Las lluvias tard�as nos han dado una muy buena cosecha de hierba, el catalizador para encender combustibles m�s pesados", dijo el oficial de prevenci�n de incendios Dick Mauldin en la Unidad Morgan Hill Ranger del Departamento Forestal.
Particularmente vulnerables son los bosques de eucalipto que no sobrevivieron a la congelaci�n, dijo Mauldin.
"Los incendios all� van a ser particularmente calurosos, dif�ciles de sacar", dijo.
En todo el estado, seg�n el Servicio Forestal de EE. UU., El 10 por ciento de los �rboles en los 18 bosques nacionales han sido asesinados por la sequ�a o las infestaciones de insectos que gener�.
Los funcionarios forestales estatales estiman que 10 millones de �rboles, Boyd, los llamaron "de pie", han muerto en las zonas boscosas de California desde que comenz� la sequ�a.
El peor a�o de fuego de California fue 1987, cuando 900,000 acres quemaron.
El clima de verano ser� la clave de lo grave que es esta temporada de fuego.
"Si entramos en un per�odo de temperaturas en los a�os 90 y 100 y se mantiene as� durante una semana m�s o menos, existe un potencial real para incendios importantes", dijo Mauldin.
Los bomberos est�n m�s preocupados por las zonas rurales de South Bay y East Bay, donde las personas han construido casas entre la vegetaci�n pesada.
"Predicamos todos los a�os para que los propietarios tomen precauciones (como limpiar el cepillo de sus casas), y mucha gente no lo hace", dijo Mauldin.
"Piensan 'nunca me va a pasar a m�', y cuando lo hace comienzan a se�alar los dedos".
Mauldin dijo que la Unidad Morgan Hill Ranger comenzar� a manejar algunas de sus estaciones de temporada el lunes, comenzar a contratar bomberos de la temporada de verano el 3 de junio y tener todas las estaciones de fondo abiertas antes del 1 de julio.
La Unidad Morgan Hill cubre partes de los condados de Santa Clara, Alameda, Contra Costa, Stanislaus, San Joaqu�n y Merced.
En los condados de Alameda y Contra Costa, los funcionarios tambi�n se est�n preparando para un verano largo y caluroso.
"No queremos decir que esta es la peor temporada de incendios en X a�os, pero nos estamos preparando", dijo Ned Mackay, portavoz del Distrito de Parques Regionales de East Bay.
"Tenemos que estar preparados".
En todo el estado, la mayor amenaza de incendio en las monta�as costeras cubiertas de chaparral al sur de Santa B�rbara, donde el contenido de humedad de los �rboles y los arbustos es el m�s bajo registrado para esta �poca del a�o, dijo Boyd.
"Cada a�o contin�a la sequ�a, las condiciones se vuelven m�s explosivas, pero no podemos decir con certeza qu� va a suceder este a�o", dijo Boyd.
Dos de las 22 unidades de guardabosques del departamento forestal, ambas en el sur de California, ahora tienen personal para la temporada de bomberos, dijo Boyd, y se espera que todas est�n listas a mediados de junio.
California no enfrenta la amenaza de fuego sola.
Funcionarios del Centro Federal de Bomberos de Interagencia Boise en Idaho, que coordina los esfuerzos de lucha contra incendios en todo Occidente, dijeron partes de 11 estados en el Lejano Oeste, y Dakota del Norte y Minnesota, tambi�n enfrentan un gran peligro de incendios este a�o.
Precauciones de incendios forestales;El Departamento de Forestaci�n y Protecci�n de Incendios de California aconseja a los residentes de �reas rurales que tomen precauciones especiales para proteger sus hogares de los incendios forestales este verano:;Limpie al menos un espacio defendible de pincel defendible de 30 pies y hierba seca alrededor de su casa.
En algunas propiedades, como las de las cimas de la cresta, el �rea transparente debe tener hasta 100 pies de ancho.
El descanso no tiene que ser tierra desnuda, pero podr�a plantarse con vegetaci�n resistente al fuego.
Retire todas las agujas y hojas de pino de su techo, aleros y canaletas de lluvia.
Recorte las extremidades del �rbol a menos de 10 pies de su chimenea y recorte todas las extremidades muertas que cuelgan sobre su casa o garaje.
Instale un arrestado de chispas en su chimenea.
Est�n disponibles en las herreteras.
Trate los techos de tejas de madera con retardante de fuego de forma regular.
Aseg�rese de que haya un acceso adecuado para que los bomberos lleguen a su hogar y aseg�rese de que las calles del vecindario est�n claramente identificadas.
Se puede obtener m�s informaci�n de la Unidad de Prevenci�n de Incendios del Departamento de Forestaci�n y Protecci�n contra Incendios de California en Morgan Hill, (408) 779-2121.
Fuente: Departamento de Forestaci�n y Protecci�n de Incendios de California.


