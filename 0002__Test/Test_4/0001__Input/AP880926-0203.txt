Los pa�ses del Tercer Mundo dirigidos por Brasil, la naci�n en desarrollo m�s endeudada del mundo, culp� a las naciones industrializadas en parte el lunes por perpetuar su pobreza.
El ministro de Relaciones Exteriores, Roberto de Abreau Sodre de Brasil, dijo a la sesi�n de apertura de la 42a Asamblea General que el Picture Economic del Tercer Mundo estaba atenuando `` debido a la falta de progreso en las relaciones econ�micas internacionales ''.
`` Es ... triste tener en cuenta que nosotros, los hermanos estadounidenses, asi�ticos y africanos, todav�a sufrimos los mismos horrores y la misma desolaci�n que tan mal afect� a nuestros antepasados '', dijo, agregando: `` Hambre ...se est� extendiendo end�micamente por los continentes ''.
Un tema similar son� el lunes en Berl�n Occidental, donde los ministros de finanzas de 22 pa�ses desarrollados o en desarrollo exigieron `` acci�n m�s contundente '' para ayudar a los pa�ses del Tercer Mundo a pagar $ 1.2 billones en deudas.
Brasil, con $ 121 mil millones en pr�stamos extranjeros, ha estado entre los cabilderos del tercer mundo m�s s�lidos para la reestructuraci�n de la deuda y las cancelaciones.
Firm� un agrupaci�n integral de reprogramaci�n la semana pasada con sus acreedores occidentales.
Es cr�tico a la opini�n de los EE. UU. Que un fuerte desempe�o econ�mico en los pa�ses desarrollados gotear�a para ayudar al Tercer Mundo.
El Secretario de Asuntos Exteriores, Obed Y. Asamoah de Ghana, pidi� a los acreedores para que cancelen algunas deudas y reprogramaran otras.
Inst� a las naciones a trabajar juntas para aumentar los precios de los productos b�sicos para fortalecer las econom�as africanas.
`` En un mercado donde un grupo de operadores vende continuamente sus productos a bajo costo y comprando los de otros, se necesita protecci�n a los operadores d�biles y vulnerables '', dijo.
Ghana ten�a una deuda extranjera estimada el a�o pasado de $ 2.8 mil millones.
Funcionarios de las siete naciones industrializadas clave en el reino no comunista en los Estados Unidos, Alemania Occidental, Jap�n, Canad�, Francia, Italia y Gran Breta�a, han aprobado un plan para ayudar a las naciones m�s pobres del mundo, principalmente en �frica subsahariana.
Pero a los pa�ses latinoamericanos tambi�n les gustar�a m�s ayuda.
La viceministra de Asuntos Exteriores de Argentina, Susana Ruiz Cerutti, pidi� una `` nueva estrategia de desarrollo global '', incluido el perd�n de la deuda.
Argentina tiene una deuda extranjera de $ 56 mil millones.
El primer ministro Gro Harlem Brundtland de Noruega dijo que su pa�s gasta alrededor del 1.1 por ciento de su producto nacional bruto en pr�stamos y subvenciones a pa�ses en desarrollo, muy por encima del promedio de 0.34 por ciento por otros pa�ses desarrollados.
Inst� a otras naciones industrializadas a aumentar la ayuda financiera, diciendo que el desarrollo y la deuda son crisis del tercer mundo relacionadas.
`` Los pa�ses industrializados del Norte ahora deben demostrar que ven la pobreza del Tercer Mundo como su desaf�o com�n '', dijo.


