Del cuerno de �frica ha surgido el grupo de corredores de maratones m�s devastador y dominante que el mundo ha visto.
O al menos desde la �ltima vez que Etiop�a se aventur� por su aislamiento atl�tico y gan� tres medallas de oro ol�mpicas consecutivas, poniendo su sello indeleble en el marat�n.
Parece estar sucediendo nuevamente, una generaci�n despu�s de que Abebe Bikila corri� descalzo por las oscuros calles de Roma en 1960 para ganar el primero de sus dos oro ol�mpicos.
Ahora es Abebe Mekonnen, quien naci� el a�o en que Bikila gan� el marat�n ol�mpico de 1964 en Tokio y una naci�n nombr� a sus beb�s despu�s de su h�roe.
Mekonnen, un teniente de polic�a de Addis Abeba, hizo una furiosa prisa con una milla para ir, pasando a Juma Ikangaa de Tanzania y ganando el marat�n de Boston del lunes en 2 horas 9 minutos 6 segundos.
Ikangaa fue segundo en 2:09:56.
Era solo la segunda vez que un africano gan� aqu�.
Ingrid Kristiansen de Noruega gan� f�cilmente la carrera femenina en 2:24:33 despu�s de abandonar su intento de romper 2:20.Kristiansen, el titular del r�cord mundial del marat�n femenino, comenz� r�pidamente pero se desaceler� notablemente en un d�a caluroso.
Con temperaturas en los a�os 60 durante la carrera del D�a del Patriota, fue aproximadamente 20 grados m�s c�lido que el domingo y toda la semana pasada.
A�n as�, Kristiansen termin� 26� en general, que se cree que es el final m�s alto para una mujer en esta carrera.
Joan Benoit Samuelson, quien estableci� un r�cord mundial en este curso en 1983, estaba acosado con problemas f�sicos que alteraron su paso a 11 millas, y termin� novena entre las mujeres en 2:37:52.
Fue la peor actuaci�n de marat�n de su carrera.
Tambi�n fue la primera vez que Kristiansen hab�a derrotado a Samuelson en un marat�n.
En una emotiva conferencia de prensa despu�s, Samuelson, quien gan� el primer marat�n ol�mpico femenino en 1984 en Los �ngeles, con l�grimas admiti� que podr�a haber corrido su �ltimo marat�n durante alg�n tiempo.
Pero la historia de la 93� carrera de Boston del lunes fue Mekonnen y su naci�n de 42 millones, que ha resurgido como una fuerza a tener en cuenta en Marathon Running.
En dos d�as, los corredores et�opes han ganado tres maratones principales.
En Rotterdam el domingo, Belayneh Dinsamo gan� en 2:08:39.
Dinsamo tiene el r�cord mundial de 2:06:50, establecido el a�o pasado en el mismo curso.
Tambi�n el domingo, en el marat�n de la Copa Mundial en Mil�n, los et�opes terminaron 1-2Keleke Metaferia gan� en 2:10:28, y Dereje Nedi fue segundo en 2: 10: 3
Etiop�a venci� a Italia por el t�tulo del equipo de la Copa Mundial con un equipo de segunda cuerda.
Casi una docena de corredores et�opes se han desplegado en todo el mundo en este agitado per�odo de dos semanas de maratones de primavera.
Otros dos et�opes estaban en la carrera de Boston del lunes, colocando noveno y 18.
Y otro m�s, Wodajo Bulti, que ha corrido 2:08:44, es uno de los favoritos en el marat�n de Londres el pr�ximo domingo.
El legado de Etiop�a al mundo en la �ltima d�cada ha sido uno de sequ�a, hambre y guerra civil �tnica.
M�s de 1 mill�n de personas murieron en 1984-85 durante una hambruna causada por la sequ�a.
La dif�cil situaci�n de los et�opes atrap� la imaginaci�n del mundo e inspir� a los m�sicos de rock y otros a organizar conciertos ben�ficos.
Debido a las perturbaciones internas, sin embargo, poca de esa ayuda lleg� a los necesitados.
Pol�ticamente pro-sovi�tico, el gobierno marxista de Etiop�a orden� boicots de los Juegos Ol�mpicos en 1976, 1984 y 1988.
Si no hubieran boicoteado, al menos tres corredores et�opes habr�an estado entre los favoritos en el marat�n masculino en Se�l.
Sin embargo, a pesar de su participaci�n err�tica, Etiop�a tiene un orgulloso patrimonio ol�mpico, que data de 1956.
Una de las figuras m�s duraderas del marat�n fue Bikila, quien gan� la medalla de oro del marat�n en 1960 y 1964.
Etiop�a tambi�n tom� el marat�n de oro en 1968, cuando Mamo Wolde, de 36 a�os, gan� en la altitud en la Ciudad de M�xico.
Es la altitud a la que entrenan los et�opes lo que mejora su capacidad aer�bica.
Gran parte de la parte central del pa�s es monta�osa, que var�a en altitud de 6,000 a 15,000 pies.
Era la primera vez desde 1963 que un et�ope hab�a corrido en Boston y Mekonnen, de 24 a�os, aprovech� al m�ximo.
Estaba entre la manada de cuatro corredores africanos que lideraron la carrera de 6.418 participantes por 15 millas.
Alrededor de la milla 16, Mekonnen e Ikangaa despegaron, corriendo al primer lado a otro, luego con Ikangaa sosteniendo una ligera ventaja.
Y no corrieron como extra�os, porque Mekonnen hab�a vencido a Ikangaa al ganar el marat�n de Tokio del a�o pasado.
"Lo conozco muy bien como corredor", dijo Mekonnen a trav�s de un int�rprete.
"Sab�a que deber�a quedarme con �l hasta el �ltimo (dos millas).
Es un buen corredor, pero no tiene un buen final ".
Mekonnen y Kristiansen Eye ganaron $ 45,000.
John Treacy de Irlanda, quien fue tercero detr�s de Ikangaa aqu� el a�o pasado, fue tercero nuevamente en 2:10:24.
"Sab�a que hab�an salido muy duro", dijo Treacy sobre el ritmo temprano abrasador.
Hasta la mitad de camino, los hombres estaban a 2:04 ritmo, peligroso en el calor del lunes.
El ritmo obtuvo lo mejor de Saimon Robert Haali de Tanzania, quien lider� la carrera por cinco millas.
Termin� sexto.
La raza femenina ten�a solo un l�der, Kristiansen.
Ella tambi�n estableci� un incre�ble ritmo temprano.
Durante las primeras millas, antes del calor, Kristiansen estaba corriendo a un ritmo de 2:17.
En la milla 17, hab�a agregado m�s de 20 segundos a sus divisiones de millas.
Por su propio c�lculo, fue a casi 16 millas que Kristiansen sinti� el calor.
"Decid� ganar la carrera", dijo.
"Hac�a demasiado calor para establecer el r�cord mundial".
Samuelson se aferr� al segundo lugar e incluso corri� c�modamente hasta aproximadamente 11 millas, cuando se deshizo.
"Estaba preparado para el clima c�lido y ciertamente hac�a calor, pero el calor no era mi problema hoy", dijo Samuelson."Me sent� muy f�cil las primeras 11 millas, sent� que estaba en el ritmo.
Estaba justo donde quer�a estar.
"Antes de venir a Boston, tuve muchos problemas con mi cadera y mi espalda.
A unas 11 millas, fue muy r�pido.
Perd� mi paso desde ese punto.
Lisa Weidenbach fue volando junto a m� en ese momento.
Marguerite (Buist) fue poco despu�s.
Segu� pensando que lo lograr�a.
No ten�a el d�a que realmente quer�a.
Estaba debidamente humillado ".
Samuelson, que se someti� a una cirug�a de rodilla en febrero, continu� desvaneci�ndose cuando su paso vacil� y fue derrotado por tres corredores estadounidenses.
Hab�an pasado al menos ocho a�os desde que Samuelson fue golpeado en el marat�n por un estadounidense.
Buist, de Nueva Zelanda, fue segundo en 2:29:04.Kim Jones de Spokane, Washington, fue tercero en 2:29:34.Weidenbach fue quinto.
"Siempre es dif�cil ser golpeado por los estadounidenses cuando eres el campe�n estadounidense", dijo Kristiansen.
"La forma m�s f�cil de ser golpeada es abandonar una carrera. Y ella no.
Creo que es genial que haya terminado ".
Tales cumplidos hicieron poco para aplacar a Samuelson.
En la conferencia de prensa de postrace, el privado Samuelson fue tan emocional como siempre en p�blico.
Su voz se rompi� varias veces.
"He corrido algunas carreras muy buenas y creo que me quedan algunas carreras muy buenas", dijo.
"Mucha gente espera que diga que esto es todo.
No es todo.(Pero) He pasado tanto tiempo en fisioterapia como en el entrenamiento y no puedo continuar con eso.
Creo que necesito descansar indefinido.
Podr�a ser un a�o, podr�an pasar cinco a�os.
Simplemente no puedo continuar con mis responsabilidades como esposa y madre y pasar esta cantidad de tiempo en fisioterapia y entrenamiento.
Solo me tomar� un descanso ".
Samuelson, quien tiene el r�cord del marat�n de EE. UU., Dijo que disminuir� significativamente su kilometraje de entrenamiento y probablemente correr� carreras m�s cortas.
Ella dijo que ejecutar� un evento de ocho kil�metros el pr�ximo mes en Washington, D.C., quiero que todos sepan que todav�a est� all� ", dijo Samuelson, l�grimas en sus ojos.
"Voy a correr de nuevo.
Necesito ir m�s all� de este problema ".
Tienen un bloqueo en el marat�n c�mo a los maratonistas de Etiop�a les ha ido los �ltimos dos d�as: fecha
Resultado de la carrera Sunday Rotterdam Marathon Belayneh Dinsamo, domingo Mil�n, Italia Keleke Metaferia, 2:10:28, primer lugar, Dereje Nedi,> Marat�n de la Copa Mundial 2:10:36, segundo lugar.
Etiop�a gana el t�tulo del equipo del equipo el lunes de Boston Abebe Mekonnen, 2:09:06, primer lugar.


