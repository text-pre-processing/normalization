El clima nublado el s�bado amenaz� con matar el espect�culo para miles de Skygazers finlandeses y extranjeros con la esperanza de vislumbrar un eclipse solar total en esta tierra del sol de medianoche.
El pron�stico del tiempo empeor� en la noche, cuando los servicios meteorol�gicos finlandeses predijeron el clima nublado con la posibilidad de duchas para el este de Finlandia el domingo.
En la ciudad oriental de Joensuu, una transmisi�n de noticias de televisi�n el s�bado por la noche mostr� que ya estaba nublado all� con una llovizna ligera que cay�.
El eclipse solar en Finlandia comienza a las 4:03 a.m. del domingo (9:03 p.m. EDT S�bado).
En ese momento, la luna comenzar� a moverse gradualmente entre la tierra y el sol.
El eclipse total comienza a las 4:52 a.m. en Helsinki y durar� 83 segundos.
Despu�s de la fase total del eclipse, la luna se alejar�, descubriendo m�s y m�s del sol.
El eclipse termina a las 5:45 a.m. en Helsinki.
Alrededor de 10,000 personas, incluidos 3.000 extranjeros, han convergido en Joensuu, a unas 50 millas de la frontera sovi�tica.
Hay condiciones que se consideran especialmente buenas para ver el eclipse _ lo que permite el clima.
En Helsinki, la fase total del eclipse ocurrir� 16 minutos despu�s del amanecer, cuando el sol est� a solo 1 grado por encima del horizonte.
En Joensuu, a 310 millas al noreste de Helsinki, el sol estar� a 5 grados sobre el horizonte en la fase total en un mejor �ngulo para los observadores.
El sol se eleva inusualmente temprano durante el verano en las latitudes extremas del norte donde se encuentra Finlandia.
El sol sale inusualmente tarde durante el invierno.
Hace diez meses, Joensuu contrat� a un `` secretario de eclipse '' para manejar los arreglos para la afluencia esperada de visitantes.
Pero ese funcionario, Marjut Cadia, dijo que hab�a subestimado el inter�s en el evento.
`` Vendimos por completo los 10,000 anteojos especiales que hicimos para este evento, y nuestro stock adicional tambi�n est� terminado '', dijo en una entrevista televisiva el s�bado.
Durante la semana pasada, los peri�dicos, la televisi�n y la radio han estado llenos de informaci�n sobre los eclipses solares, as� como los consejos para que los espectadores no miren directamente al sol.
Algunos espectadores de Eclipse no tendr�n que preocuparse por las nubes, porque estar�n por encima de ellas.
Finnair, la aerol�nea nacional, ha organizado una docena de vuelos especiales para observadores de eclipse, y las empresas privadas con aviones peque�os proporcionar�n m�s.
El eclipse ser� total en un arco a unas 125 millas de ancho desde el norte del Mar B�ltico y el sureste de Finlandia, a trav�s de la Pen�nsula de Kola y la Uni�n Sovi�tica del Noreste, hasta las Islas Aleutianas cerca de Alaska.
Los cient�ficos realizar�n varios experimentos durante el eclipse, incluidas las mediciones de la prueba de gravedad del radio del sol.
Sin embargo, el inter�s cient�fico en este eclipse ha sido menor que en eclipses m�s largos, como el eclipse de siete minutos visto desde el norte de Kenia el 30 de junio de 1973.
Seppo Linnaluoto de la Asociaci�n Astron�mica de Ursa dijo que el mejor lugar para las observaciones de este eclipse ser� la Uni�n Sovi�tica del Nordeste.
El �ltimo eclipse total que se ver�a en Finlandia ocurri� en 1945.


