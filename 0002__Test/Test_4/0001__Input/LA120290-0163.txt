Los v�tores estallaron el s�bado en ambos lados del Canal de la Mancha cuando los trabajadores brit�nicos y franceses cavando el t�nel del canal finalmente se reunieron despu�s de noquear un pasaje lo suficientemente grande como para caminar y estrechar la mano.
"Hoy, por primera vez, los hombres pueden cruzar el canal subterr�neo", dijo el presidente franc�s Francois Mitterrand."Qu� signo tan brillante de la vitalidad de nuestros dos pa�ses".
El avance lleg� en un t�nel de servicio de 6 pies de altura que se utilizar� para mantener dos t�neles de riel a�n aburridos.
Marcaba un hito simb�lico en el proyecto de ingenier�a m�s grande de Europa.
Usando Jackhammers, Graham Fagg, de 42 a�os, de Dover, Inglaterra y Philippe Cozette, de 37 a�os, de Calais, Francia, noque� el �ltimo pie de tiza para vincular los lados brit�nicos y franceses del t�nel, que ha sido denominado ""Chunnel ".
La pareja sonriente luego abraz� las manos, abraz� e intercambi� sus banderas nacionales.
Los trabajadores en el mono observaron y aplaudieron.
"�Dios salve a la reina!"Llor� trabajadores franceses, descorrando botellas de champ�n.
"�Viva Francia!"vino la respuesta del lado brit�nico.
El apret�n de manos del s�bado se produjo tres a�os despu�s del d�a despu�s de que comenzara el t�nel en Sangatte, cerca de Calais y en Folkestone, Inglaterra.
El t�nel de $ 16.7 mil millones de canales permitir� viajar desde Par�s a Londres en tren de alta velocidad en 3 1/2 horas cuando se abra en junio de 1993.
Se espera que el viaje del tren a trav�s del t�nel tome 35 minutos, en comparaci�n con 90 minutos para cruzar el canal en ferry.
Las tarifas para el cruce submarino no se han establecido.
Pero los expertos dicen que los cargos pueden ser al menos el doble de las proyecciones de 1986 de $ 46 por persona en un autom�vil y $ 19 por pasajero de tren.
Los Tunnelers han pasado el �ltimo mes perforando los �ltimos 100 yardas de tiza con m�quinas aburridas gigantes de construcci�n estadounidense, tratando de alinear las dos mitades.
Los t�neles brit�nicos en realidad se vincularon con los franceses el 29 de octubre, cuando los trabajadores perforaron un agujero de 2 pulgadas a trav�s de la tiza en un t�nel de servicio.
La conexi�n puso fin a la separaci�n de la isla de Gran Breta�a de la Europa continental por primera vez desde la �ltima edad de hielo hace unos 8,000 a�os.
Fagg y Cozette fueron elegidos por el sorteo de entre los 3.000 trabajadores para representar a sus pa�ses en la reuni�n del s�bado.
Despu�s de ampliar el paso del tama�o de una mirilla a una ventana, colocaron sus herramientas y se dieron la mano.
"Hola", en auge Fagg.
"Hola", dijo Cozette, ri�ndose.
Michel Delebarre y Malcolm Rifkind, los ministros de transporte franc�s y brit�nico, cabalgaron en peque�os servicios de servicio por el t�nel de mantenimiento para presenciar el apret�n de manos.
Fagg y Rifkind cabalgaron a Sangatte, mientras que Cozette y Delbarre se dirigieron a Folkestone para unirse a celebraciones que duraron la noche.
Nueve trabajadores han sido asesinados durante el proyecto.
El costo ha aumentado de una estimaci�n inicial de $ 9.4 mil millones a $ 16.7 mil millones.
El t�nel se ve positivamente en Francia, donde se espera que revive las �reas del norte deprimidas econ�micamente.
Pero Gran Breta�a ha mostrado preocupaci�n por la p�rdida de su foso hist�rico del continente.
Muchos brit�nicos temen a los narcotraficantes o terroristas invadir�n su isla a trav�s del t�nel.


