El hombre de 74 a�os dijo que era "como las agujas o las u�as atrapadas" cuando la polic�a lo espos� a la espalda mientras yac�a boca abajo.
Luego la polic�a lo levant� por sus brazos y lo arrastr� de la escena.
El obispo cat�lico romano retirado George Lynch de Nueva York estaba describiendo su tratamiento por la polic�a de West Hartford, Connecticut, durante una manifestaci�n contra el aborto de 1989.
A mediados de junio, se estableci� una demanda contra el Departamento de Polic�a de Los �ngeles cuando la polic�a acord� dejar de usar un arma de artes marciales, Nunchakus, mientras arrestaba a los defensores de los antiabortistas.
Cientos han acusado que la polic�a en m�s de 50 ciudades ha utilizado fuerza excesiva para eliminar los manifestantes con la intenci�n de cerrar las instalaciones de aborto.
Los manifestantes, m�s asociados con Operation Rescue, han acusado y testificado que las t�cticas policiales utilizadas durante los �ltimos 2 a�os y medio en ciudades como Denver, Atlanta, Pittsburgh y Los �ngeles han resultado en lesiones graves y han provocado abusos sexuales contra las mujeres quehan sido arrestados.
Operation Rescue, que atrae a los participantes en gran parte de los c�rculos cat�licos y protestantes conservadores, es un movimiento nacional que organiza manifestaciones en las instalaciones de aborto.
Es la pr�ctica de los manifestantes ir a la luz cuando se les ordene mudarse, pero de lo contrario no ofrezca resistencia a la polic�a.
Las quejas persistentes y numerosas demandas contra la polic�a por Operation Rescue han adquirido un nuevo significado en los �ltimos meses a ra�z de la paliza de la polic�a de Los �ngeles al automovilista Rodney King.
Las im�genes de la polic�a de Los �ngeles balanceaban las paletas de noche en King mientras yac�a en el terreno, jugaba repetidamente en los programas nacionales de noticias, se quemaron en la conciencia nacional y condujeron a llamadas generalizadas para la investigaci�n de la brutalidad policial.
Pero las agencias gubernamentales, la prensa y los libertarios civiles han reaccionado de manera muy diferente a la operaci�n de rescate videos que muestran una aparente brutalidad policial y a los informes de abuso policial de cientos de activistas en todo el pa�s.
Una cinta de video de una demostraci�n de rescate de operaciones muestra el brazo de un hombre aparentemente rompiendo bajo la presi�n de ser levantado de manera similar a la utilizada en Lynch.
Otras escenas muestran a la polic�a aparentemente colocando dedos en las fosas nasales de un manifestante y agarrando el pecho de una manifestante para forzar el cumplimiento.
El acuerdo sobre el uso policial de Nunchakus en Los �ngeles, como los videos y fotograf�as que muestran a la polic�a utilizando t�cnicas de cumplimiento del dolor en los participantes de rescate, recibi� poca atenci�n del p�blico, los grupos de derechos civiles o la prensa.
Algunos cr�ticos dicen que la falta de atenci�n es un signo de un doble est�ndar.
La polic�a, incluido el asistente del jefe Craig Carrucci de West Hartford, niega las afirmaciones de brutalidad.
El FBI investig� las quejas de mala conducta por parte de miembros de su departamento, dijo, y "cada caso estaba cerrado.
"Llam� al cumplimiento del dolor" una herramienta v�lida "utilizada" en proporci�n directa a la cantidad de resistencia.
"Incluso Gandhi y Martin Luther King Jr., cuando fueron arrestados, cooperaron con la polic�a y los tribunales, dijo.
Poco despu�s de la paliza de Rodney King, un programa de noticias sobre ABC que ilustra la brutalidad policial mostr� una foto de la polic�a que usaba un arma de artes marciales contra una persona arrestada, pero no se mencion� que el episodio involucr� a Operation Rescue.
Del mismo modo, el CBS Evening News inform� el 27 de marzo "sobre varios aspectos de la brutalidad policial", pero no incluy� ejemplos que involucraran activistas contra el aborto, dijo un productor.
"(El abuso policial) no ha atra�do mucha atenci�n porque mucha gente no simpatiza con la operaci�n de rescate", dijo el Dr. James Fyfe, profesor de justicia en la Universidad Americana de Washington, D.C., la polic�a tambi�n puede tener una predisposici�n a usar excesivoFuerza contra los activistas contra el aborto, dijo Fyfe, un ex polic�a de la ciudad de Nueva York.
Sus t�cticas, encendidas y, en algunos casos, encaden�ndose a los edificios, son "un poco m�s de lo que la polic�a est� acostumbrada a tratar".
Dr. Philip Wogaman, profesor de �tica social cristiana en el Seminario Teol�gico Wesley en Washington, D.C. -Nieve la comparaci�n con
Otros tipos de protestas se descomponen en varios puntos.
Por ejemplo, dijo, los manifestantes de derechos civiles normalmente estaban piquetes para afirmar su derecho a comer en un mostrador de almuerzo o viajar en un autob�s, no cerrar las instalaciones para negar los derechos de los dem�s.
Ya en 1989, poco despu�s de que la Operaci�n Rescate comenz� las sentadas generalizadas para interrumpir las instalaciones de aborto, William B. Allen, entonces presidente de la Comisi�n de Derechos Civiles de EE. UU., Suena una alarma.
"Me preocupa que los manifestantes contra el aborto reciban un enjuiciamiento selectivo y un tratamiento selectivamente duro, a diferencia del recibido por otros manifestantes por otras causas", dijo Allen, quien generalmente se opone al aborto.
Colleen O'Connor, directora nacional de educaci�n p�blica de la Uni�n Americana de Libertades Civiles, y Carol Sobel, abogada principal de la ACLU en Los �ngeles, dijeron que est�n de acuerdo en que la polic�a ha abusado de los participantes de Operation Rescue.
La ACLU est� apoyando a Operation Rescue en algunas de esas situaciones, dijeron.
Pero el Grupo de Libertades Civiles est� claramente atrapado entre los derechos en conflicto.
La ACLU puede apoyar algunas de las afirmaciones de Operation Rescue de brutalidad policial, pero Sobel dijo que tambi�n ha ganado una sentencia de $ 110,000 en un caso en el que la ACLU hab�a obtenido una orden judicial contra el grupo antiaborto.
La Comisi�n de Derechos Civiles en 1989, bajo la presi�n de algunos miembros del Congreso, decidi� contra una investigaci�n de presunta brutalidad policial contra activistas contra el aborto y decidi� no pedirle al Departamento de Justicia que investigara.
Una portavoz de la comisi�n dijo que su mandato legal proh�be tratar los problemas de aborto.
Los cr�ticos de la decisi�n de la comisi�n de no investigar dicen que el problema fue, y es, la brutalidad policial, no el aborto.


