La tuberculosis vuelve a alarmar funcionarios de salud porque est� planteando nuevas amenazas para la salud con su conexi�n con el virus del SIDA.
Un aumento del 35 por ciento en la tuberculosis en 1989 en Newark, N.J., ha llamado la atenci�n de los funcionarios de salud, que hab�an estado registrando previamente con satisfacci�n una disminuci�n lenta y constante en los casos de TB en las �ltimas d�cadas.
Atribuyen el aumento nacional del 5 por ciento en los casos de TB en 1989 a los estragos del virus del SIDA, que destruye el sistema inmunitario del cuerpo y deja a las v�ctimas abiertas a la infecci�n por TB, dijo el domingo el Dr. Philip C. Hopewell del Hospital General de San Francisco.
Hopewell y otros funcionarios de salud discutieron el v�nculo entre el SIDA y la tuberculosis durante la Conferencia Mundial de Cuatro D�as sobre Salud pulmonar en Boston, que termina el mi�rcoles.
Alrededor del 4 por ciento de los estadounidenses identificados por tener el virus del SIDA tambi�n se ha diagnosticado como infectado con tuberculosis, dijo la Dra. Dixie E. Snider Jr., directora de la Divisi�n de Control de Tuberculosis en los Centros para el Control de Enfermedades en Atlanta.
La Asociaci�n Americana de Pulmones estima que 20,000 estadounidenses al a�o desarrollan TB.
En partes de �frica, donde el SIDA ya es un riesgo para la salud, la tuberculosis se ha convertido en epidemia, dijo el Dr. Annik Rouillon, directora ejecutiva de la Uni�n Internacional contra la Tuberculosis y las Enfermedades Pulmones en Par�s.
`` La combinaci�n de los dos es realmente catastr�fica '', dijo.
Snider enfatiz� que la tuberculosis, a diferencia del SIDA, es una enfermedad curable y pidi� ex�menes de TB en programas de rehabilitaci�n de drogas, prisiones u otros lugares donde se administran pruebas de SIDA.
Snider tambi�n dijo que los m�dicos deber�an administrar pruebas de TB a todas las personas que dan positivo para el virus del VIH, ya que la TB puede no ser diagnosticada f�cilmente en pacientes con SIDA.
`` Es importante que obtengamos el control de la situaci�n '', dijo.
En Wyoming, los Centros para el Control de Enfermedades no registraron nuevos casos de TB en 1989, lo que demuestra que es una condici�n que puede controlarse y curarse, seg�n el Dr. Lee B. Reichman, director de la Divisi�n Pulmonar de la Universidad de Medicina yOdontolog�a en Nueva Jersey.
`` Nunca he o�do hablar de un aumento de la magnitud visto en Nueva Jersey o, por otro lado, la esperanza generada por ning�n caso en un solo estado '', dijo Reichman.
Adem�s, a diferencia del SIDA, la TB es una enfermedad altamente contagiosa que puede propagarse por part�culas en el aire tose por una persona con TB pulmonar cl�nicamente activa no tratada.No tratada, la tuberculosis mata alrededor del 50 por ciento de sus v�ctimas en dos a�os, seg�n los Centros para el Control de Enfermedades.
Snider dijo que es necesario un contacto sostenido para la transmisi�n de la tuberculosis, y no plantear�a nuevos problemas para las v�ctimas del SIDA que ya luchan contra la discriminaci�n en trabajos y viviendas.
Sin embargo, se�al� que hab�a habido un aumento en las pruebas positivas de TB entre los trabajadores de la salud del SIDA.
La tuberculosis es causada por una bacteria que com�nmente afecta a los pulmones pero puede atacar a casi cualquier �rgano.
Durante las �ltimas tres d�cadas, ha sido prevenible y curable a trav�s de m�ltiples terapia farmacol�gica, dijo Snider.
Snider dijo que de 10 millones a 15 millones de estadounidenses han sido infectados con el germen de tuberculosis, pero solo un peque�o porcentaje de ellos desarrollan la enfermedad porque su sistema inmunitario era lo suficientemente fuerte como para evitar que la enfermedad se desarrollara.
Sin embargo, si el sistema inmunitario de una persona se ve afectado por la mala nutrici�n o debilitado por el virus del VIH, las personas pueden desarrollar TB activa.
`` La TB es conocida como un organismo oportunista '', dijo Hopewell.
Los m�dicos dijeron que los grupos minoritarios de EE. UU. Se han vuelto cada vez m�s susceptibles a la TB.
Snider dijo que ha habido un 150 por ciento aumentado en casos de TB entre los j�venes negros en la ciudad de Nueva York.
La tuberculosis puede tratarse de manera efectiva, incluso en pacientes con SIDA, subrayando los esfuerzos para evaluar a las personas para la enfermedad, dijo el m�dico.
Reichman se�al� que la mayor�a de los estadounidenses se han olvidado de los problemas de TB;De 1981 a 1984, los casos de TB disminuyeron un promedio de 6 por ciento por a�o seg�n los Centros para el Control de Enfermedades.
Pero, dijo, `` TB ha vuelto con una venganza ''.
Mientras est� asociado con la pobreza y las condiciones de vida llenas de gente, la TB a trav�s de la historia ha devastado tanto a los pobres como a los famosos.
Las v�ctimas de la TB incluyen a Henry David Thoreau, Washington Irving, Franz Kafka, Ring Lardner, Somerset Maugham y Vivien Leigh.
Se estima que 3 millones de personas al a�o mueren en todo el mundo de TB, seg�n la Asociaci�n Americana de Pulmones.


