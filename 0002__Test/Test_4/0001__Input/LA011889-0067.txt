La Oficina del Fiscal de Distrito del Condado de Los �ngeles y el FBI est�n investigando un incidente grabado en video en el que un oficial de polic�a de White Long Beach pareci� meter la cara de un hombre negro en una ventana de vidrio de plato despu�s de una parada de tr�fico de rutina.
El Ayuntamiento de Long Beach vot� el martes para pedirle a la oficina del fiscal de distrito que lanzara una investigaci�n independiente del incidente del s�bado por la noche, que fue registrado en secreto por un equipo de NBCtelevision.
Pero asistente Dist.Atty.Curt Livesay dijo que la oficina ya estaba investigando el caso a pedido del jefe de polic�a de Long Beach.
"Acordamos revisar el asunto para determinar si una investigaci�n penal es apropiada", dijo Livesay, y agreg� que su oficina espera decidir para el viernes si se merece una investigaci�n completa.
Si tal investigaci�n revela que la brutalidad estaba involucrada, se podr�an presentar cargos por delitos menores o delitos graves contra el oficial, dijo.
Tambi�n se ha llamado al FBI para determinar si los derechos civiles del hombre arrestado, Don Jackson, sargento de licencia administrativa del Departamento de Polic�a de Hawthorne, fueron violados durante su altercado con dos oficiales de Long Beach en la Carretera de la Costa del Pac�fico., dijo el portavoz Fred Reagan.
Se neg� a decir qui�n hab�a solicitado la investigaci�n federal.
"Hemos tenido una acusaci�n de una violaci�n de los derechos civiles y abrimos un boleto esta ma�ana", dijo.
Jackson y Jeff Hill, un oficial de correcciones federales fuera de servicio, se pusieron ropa vieja y sucia y condujeron a Long Beach en un sed�n alquilado de 12 a�os el s�bado por la noche mientras el equipo de televisi�n lo segu�a en una camioneta.
Los dos hombres dijeron que quer�an demostrar un problema de larga data de abuso de los miembros del grupo minoritario por parte de los agentes de polic�a de Long Beach.
La cinta completa retenida, mientras que las partes editadas de la cinta se han transmitido, los funcionarios de Long Beach han dicho que necesitan ver todo lo filmado por el equipo de NBC para avanzar con su propia investigaci�n del incidente.
Los funcionarios de NBC se han negado a lanzar la cinta completa, diciendo que violar�a la pol�tica de la compa��a para publicar im�genes sin editar.
Un abogado asistente de la ciudad de Long Beach dijo que su oficina est� considerando acciones legales para obtener las cintas.
Aunque el alcalde de Long Beach, Ernie Kell, le dijo al "show de hoy" de NBC el martes por la ma�ana que los dos polic�as hab�an sido suspendidos, el jefe de polic�a Lawrence L. Binkley dijo que los oficiales, Mark Dickey y Mark Ramsey, permanecer�n en esta etapa en esta etapa delinvestigaci�n.
Sin embargo, han sido reasignados del deber de patrulla al Oficina de Detective, dijo.
Kell admiti� m�s tarde el martes que estaba en un error en sus comentarios de "Today Show", pero dijo que favorecer�a despidir a los oficiales si se demuestra que usaban t�cticas brutales para tratar con Jackson.
Llamado 'Desafortunado' en la reuni�n del Consejo de la Ciudad del martes, el alcalde calific� el incidente "un desafortunado conjunto de circunstancias.
No toleraremos esto....Necesitamos averiguar qu� sucedi� aqu� y asegurarnos de que nunca vuelva a suceder ".
Una investigaci�n realizada por el fiscal de distrito agregar� credibilidad a la propia consideraci�n de la ciudad de las acusaciones de brutalidad, dijo Kell.
No es inusual que el fiscal de distrito busque acusaciones de brutalidad policial, dijo Livesay, estimando que la oficina asume de cuatro a seis casos de este tipo al a�o.
Al determinar si un oficial de polic�a ha usado una fuerza excesiva, los fiscales deben decidir si el oficial actu� "sin necesidad legal" al agredir o vencer a un sospechoso.
Declin� detallar lo que constituir�a una fuerza innecesaria, diciendo que ser�a una "llamada de juicio" por parte de los fiscales basados en las acciones y declaraciones de los oficiales de polic�a y sospechosos, y las lesiones sufridas.
El abogado Michael Hannon, quien representa a los dos oficiales, dijo el martes que disputar� cualquier acusaci�n de brutalidad.
Dijo que los oficiales fueron "establecidos" por activistas negros que intentaban crear una escena con la polic�a.
La polic�a no tiene comentarios en un comunicado publicado un d�a despu�s del incidente, la polic�a de Long Beach dijo que el sed�n de Jackson y Hill fue detenido por tejer la l�nea central de la carretera.
Negaron que la cabeza de Jackson fuera empujada por la ventana, diciendo que su codo rompi� el vaso.
El lunes, sin embargo, los funcionarios del departamento dejaron de publicar esa declaraci�n y dijeron que no tendr�an comentarios en espera del resultado de su investigaci�n interna.
Un portavoz del Servicio de Referencia de Abogados de Malanda de Mala Confecci�n de la Polic�a, un grupo sin fines de lucro que investiga las quejas ciudadanas contra las agencias de aplicaci�n de la ley, dijo que la cinta televisada deja en claro que Dickey empuj� la cabeza de Jackson y el brazo derecho por la ventana.
El portavoz David Lynn sostuvo que Hill, el conductor del autom�vil, no estaba violando ninguna ley de tr�nsito cuando lo detuvieron.
El grupo tambi�n se quej� de que Dickey tambi�n us� una serie de obscenidades en su conversaci�n con Jackson, quien fue contratado por sospecha de usar un lenguaje ofensivo, desafiando a un oficial a luchar y obstruir el arresto.
Fue liberado en su propio reconocimiento en espera de una aparici�n en la corte del 25 de enero.
Clarence Smith, el �nico miembro negro del Ayuntamiento de Long Beach, dijo que encontr� la cinta "impactante.
"Pero otros funcionarios de la ciudad argumentaron que las im�genes de televisi�n no eran necesariamente concluyentes porque mostraba el altercado desde un solo �ngulo y mostraba a Jackson y Dickey solo desde la cintura.
"Es realmente dif�cil saber qu� est� sucediendo debajo de la cintura", dijo el concejal Evan Anderson Braude, manteniendo que es vital que NBC libere el resto de sus videos.
Admite el error del oficial del oficial Hannon admiti� que Dickey se equivoc� al darle vida a Jackson con obscenidades, pero dijo que la maldici�n era evidencia de discusi�n, en lugar de racismo.
Dijo que los dos oficiales vieron el tejido del autom�vil dentro del carril de tr�fico y quer�an revisar al conductor en busca de embriaguez.
Aunque los dos hombres en el autom�vil eran negros, conduc�an un autom�vil viejo y vestidos con ropa en mal estado, no fueron detenidos por esas razones, dijo el abogado, y agreg� que estaban en una secci�n de la ciudad donde su apariencia no era inusual.
"Obviamente, no dicen la verdad cuando dicen que no hicieron nada para llamar la atenci�n", dijo Hannon sobre Jackson y Hill.
Dijo que los dos polic�as se preocupan por su seguridad cuando Jackson sali� sospechosamente del auto tan pronto como se detuvo, luego inmediatamente comenzaron a discutir cu�ndo Dickey le orden� que se sometiera a una b�squeda de armas.
"T�cticas policiales adecuadas" ", el oficial, utilizando t�cticas policiales adecuadas, lo empuj� (contra) el costado de un edificio y desafortunadamente, la ventana se rompi�", dijo Hannon.
"Estoy seguro de que ni el Sr. Jackson ni el oficial quer�an que la ventana se rompiera, porque era peligroso".
Dijo que Jackson ten�a un ojo para la c�mara cuando grit� cuando Dickey lo traslad� al crucero de la polic�a para arrestar.
Dijo que Dickey, que ha estado en la fuerza policial durante cuatro a�os, ten�a una queja anterior sobre su conducta, que fue investigada por el departamento y decidi� ser infundada.


