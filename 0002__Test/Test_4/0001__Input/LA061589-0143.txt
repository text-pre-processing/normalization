Ben Johnson deber�a ser despojado de su r�cord mundial en la carrera de 100 metros y deber�a otorgarse a Carl Lewis, muchos funcionarios y atletas de atletismo de EE. UU. Dijeron el mi�rcoles despu�s de Johnson admitiendo su uso de esteroides.
Johnson testific� esta semana en una investigaci�n canadiense de que su participaci�n de siete a�os con drogas ilegales para mejorar el rendimiento incluy� inyecciones antes del Campeonato Mundial de 1987 en Roma, donde estableci� el r�cord mundial existente de 9.83 segundos.
"Tendr�a que ver la evidencia, pero si estuviera drogas en el momento del Campeonato Mundial, mi pensamiento ser�a eliminar su historial", dijo Ollan Cassell, director ejecutivo del Congreso de Atletismo, la Junta Nacional de Gobierno de la Junta de Gobierno de la National.deporte.
Cassell tambi�n es vicepresidente de la Federaci�n Internacional de Atletismo Amateur, la organizaci�n mundial de gobierno.
La posibilidad de que Johnson pudiera perder su r�cord se plante� cuando la IAAF solicit� "una opini�n legal urgente" para ver qu� podr�a hacer con respecto a los registros y campeonatos de usuarios de drogas confesas.
El tema se decidir�a en el Congreso de la IAAF, programado para Barcelona, Espa�a, en septiembre.
"Las acciones ... permitir�an a la IAAF, si as� decidiera, retroactivamente retirar los resultados obtenidos y cualquier registro logrado por dichos atletas", dijo el comunicado.
Sin embargo, no estaba claro si alguna acci�n tomada por el Congreso afectar�a la marca mundial de Johnson porque se tendr�an que resolver fechas efectivas de sanciones retroactivas.
Frank Greenberg, presidente de TAC, dijo: "Esperaremos para ver qu� hace Canad�", siguiendo la investigaci�n del pa�s en la que testificaron 40 personas, entre ellas Johnson, su entrenador, Charlie Francis y su m�dico, Jamie Astaphan.
"Siento que haremos todo lo posible para abogar por que nuestro atleta, Carl Lewis, obtenga el r�cord mundial", dijo Greenberg.
Lewis tiene el segundo tiempo legal m�s r�pido de la historia, 9.92, al terminar en segundo lugar a Johnson en los Juegos Ol�mpicos de Se�l del a�o pasado.
Johnson, quien registr� 9.79 en esa carrera, perdi� el r�cord y su medalla de oro, y fue excluido de la competencia durante dos a�os despu�s de dar positivo por un esteroide anab�lico.
Hubo mucha discusi�n entre los atletas que compitieron esta semana en el Campeonato TAC sobre si a Johnson deber�a permitirse volver al deporte despu�s de completar su suspensi�n de dos a�os.
Lewis cree que deber�a.
"Pas� mucho tiempo sin decir la verdad, pero se rompi� y ha dicho la verdad", dijo Lewis, seis veces medallista de oro ol�mpico.
"Nos est� dando la oportunidad de creerle, para apoyarlo.
"Incluso lo escuch� hablar en contra de las drogas y eso es lo importante".
Al Joyner, el medallista de oro ol�mpico de 1984 en el triple salto, dijo que Johnson deber�a poder competir nuevamente, solo porque esa es la regla.
"Pero todos sus registros deber�an tomarse porque lo admiti�", agreg� Joyner.
"Mat� a muchos fan�ticos ... muchos ni�os peque�os ... y casi mat� nuestro deporte ... Puso una sombra oscura sobre eso ... por qui�n era", dijo Joyner.
El velocista Harvey Glance, tres veces ol�mpico y presidente del comit� asesor de atletas de TAC, dijo que a Johnson se le deber�a permitir regresar despu�s de dos a�os.
"Pero si admite que estaba en algo (cuando estableci� el registro), entonces deber�a ir a la siguiente persona, Carl Lewis", dijo Glance.
"Solo los registros lo mostrar�n y solo Ben lo sabe".
Johnson fue probado despu�s de su carrera por el r�cord mundial en Roma, pero los resultados fueron negativos.


