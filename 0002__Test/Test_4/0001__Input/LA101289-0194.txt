Consuelo desea poder compartir su primera comuni�n este a�o con Abuelita.
Ella extra�a mucho a su abuela.
Abuelita no era muy antigua, solo 62, pero no hab�a mucho que los m�dicos pudieran hacer para salvar su vida.
Ten�a diabetes incontrolada: sus ri�ones estaban fallando, su presi�n arterial era demasiado alta y finalmente muri� de un ataque card�aco.
Consuelo y Abuelita no son personas reales, pero su historia, sin embargo, se juega una y otra vez entre los m�s de 3 millones de latinos en el �rea metropolitana de Los �ngeles, dicen los funcionarios de salud.
La raz�n es la diabetes, un asesino silencioso que afecta a uno de cada ocho adultos latinos.
Esta tasa es m�s alta que la de cualquier grupo �tnico, excepto los indios estadounidenses.
A nivel nacional, se estima que hay m�s de 2 millones de latinos que ya tienen diabetes, muchos de ellos no diagnosticados.
Es por eso que la diabetes americana Assn.Y otros grupos de educaci�n para la salud en el �rea de Los �ngeles y California han comenzado un gran impulso para educar a los latinos sobre la diabetes.
El esfuerzo est� siendo ayudado por m�dicos interesados y por compa��as farmac�uticas, que fabrican insulina y otros medicamentos utilizados para la diabetes que no pueden controlarse por los cambios en los h�bitos alimenticios.
Al igual que la enfermedad card�aca, la diabetes de inicio en adultos m�s com�n entre los latinos es una condici�n que puede prevenirse en gran medida con la dieta y el ejercicio adecuados, dicen los m�dicos.
Ser 20% o m�s sobrepeso es uno de los principales factores de riesgo de diabetes.
"Necesitamos ingresar a las comunidades y comenzar a ense�ar a los ni�os y a los adolescentes y a los padres a hacer algunos cambios en sus h�bitos", dijo el Dr. Jaime Davidson, un m�dico de Dallas que ha encabezado los esfuerzos de educaci�n latina de la Asociaci�n Diabetes.
"Muchas personas piensan que cuando los latinos vienen aqu� (a los Estados Unidos) aumentan de peso porque contin�an comiendo la forma en que comieron en casa.
Pero de hecho, cambian ", dijo Davidson.
"Lo que sucede es que las comidas r�pidas est�n f�cilmente disponibles ... y son m�s altas en grasas y calor�as.
Y en casa no ten�an autom�vil, y aqu� en lugar de caminar tomamos el auto para ir a una cuadra.
"Eso termina en tener m�s obesidad.
Si ya tiene un grupo predispuesto de personas, como los latinos, obtienes m�s obesidad y obtienes m�s diabetes ".
Con fondos de Upjohn Co., el cap�tulo de Los �ngeles de American Diabetes Assn.patrocin� una feria de salud de diabetes en Olvera Street Plaza el 26 de agosto. De 3.000 que asistieron a la feria, 1,000 se sometieron a pruebas por diabetes.
Se puede esperar m�s de este tipo de esfuerzo y educaci�n m�s amplia entre el p�blico y los m�dicos en el futuro, dijo Janet Matkin, de la filial de la Asociaci�n de California.
La diabetes es un defecto en el sistema del cuerpo para usar az�car en la sangre.
Resulta de la falla del cuerpo para hacer insulina o, indican algunos estudios, de su incapacidad para usar la insulina disponible.
Los latinos tienen m�s del doble de probabilidades de obtener diabetes que los anglos, y la incidencia de la enfermedad aumenta con la edad.
En una encuesta realizada en San Antonio, un tercio de los latinos entre las edades de 55 y 65 a�os ten�an diabetes, dijo Davidson.
Se cree que la mayor susceptibilidad de los latinos resulta de una combinaci�n de herencia y dieta.
Del mismo modo, los indios estadounidenses tienen la mayor incidencia de diabetes de cualquier grupo �tnico en los Estados Unidos.
Tambi�n se cree que los defectos gen�ticamente controlados en el sistema de insulina del cuerpo existen tanto para ellos como para los latinos.
Adem�s, las dietas latinas pueden ser ricas en alimentos fritos altos en calor�as que promueven la obesidad y aumentan las posibilidades de obtener diabetes.
La diabetes que comienza en la infancia requiere inyecciones diarias de insulina.
Sin embargo, m�s com�nmente en los latinos, la diabetes toma la forma llamada Tipo II, que aparece por primera vez en la edad adulta y, a menudo, puede controlarse con una dieta y ejercicio adecuados.
La enfermedad tiende a aparecer a finales de los a�os 20 y principios de los 30 entre los latinos, una d�cada antes que en Anglos, dijo Davidson.
Pero, debido a que progresa lentamente, la diabetes tipo II puede no diagnosticarse durante a�os, permitiendo complicaciones como insuficiencia renal, enfermedad card�aca, ceguera y la necesidad de amputaci�n de un pie o dedo del pie.
Los latinos, que a menudo tienen menos acceso a la atenci�n m�dica regular que a los anglos, tienen m�s probabilidades de no diagnosticarse, dijo Matkin.
Los nuevos esfuerzos educativos para cambiar esa imagen son necesarios, no solo porque ayudan a las personas a mantenerse saludables, sino tambi�n porque la crisis de financiamiento de atenci�n m�dica de la naci�n solo empeorar� si el problema no se reduce, dijo Davidson.
"Estamos enfrentando un problema que va a crecer, tal vez en proporciones epid�micas", dijo Davidson.
"En algunos estados como California o Texas, si no se hace nada, vamos a pagar tanto dinero por la atenci�n m�dica de las personas de origen latino que puede ser prohibitivo".
El n�mero de diab�ticos latinos en los Estados Unidos casi se cuadruplicar� para el a�o 2000 a 8 millones, seg�n las proyecciones de Eli Lilly Co. que cuidan de ellos costar� m�s de $ 10 mil millones al a�o, incluso si la enfermedad no tiene complicaciones, la compa��aproyectos.
Sin embargo, los cambios en el estilo de vida hoy en d�a para reducir la obesidad pueden evitar que muchos de esos casos ocurran, dicen los funcionarios de salud.
Eso significa adaptar una red de educaci�n para la salud anglo orientada a las necesidades de una comunidad latina diversa que tiene una estructura social, creencias de salud y h�bitos alimenticios diferentes de la cultura mayoritaria.
Por ejemplo, el hecho de que la diabetes tipo II aparezca con frecuencia en mujeres durante o poco despu�s del embarazo brinda a los educadores de salud una oportunidad ideal para influirCentro M�dico del Condado de Los �ngeles.
Cualquier mujer latina cuya ni�a naci� en 9 libras o m�s debe considerarse en riesgo de diabetes y alterar su dieta, dijo Davidson.
Es probable que los inmigrantes recientes e incluso algunos residentes estadounidenses desde hace mucho tiempo deseen cambiar completamente de una dieta que depende de frijoles, tortillas y otros carbohidratos, se�al� Montoro, pero se pueden hacer modificaciones f�cilmente.
Uno de los m�s importantes es reducir la grasa en la dieta al eliminar los alimentos fritos, por ejemplo, al no fre�r tortillas para tacos o enchiladas.
Otras barreras para los latinos que reciben atenci�n por la diabetes son m�s sutiles, dicen los m�dicos interesados en el tema.
El principal de estos es que es menos probable que el paciente latino sea voluntario en la informaci�n o de notar s�ntomas de diabetes tempranos que el paciente anglo, dijo la Dra. Aliza Lifshitz, miembro de la junta de la Asociaci�n M�dica Hispana Americana de California.
Cualquier m�dico que practique en el sur de California debe ser consciente de eso y aprender a hacer m�s preguntas relacionadas con la diabetes a sus pacientes latinos, dijo Lifshitz.
Davidson dijo que conf�a en que los latinos responder�n a los esfuerzos de educaci�n en la diabetes.
Se�al� las recientes indicaciones de que el riesgo de enfermedad card�aca de los estadounidenses est� disminuyendo como ejemplos de c�mo pueden funcionar los programas educativos.
"Creo que si les damos la oportunidad y las herramientas que los latinos no son diferentes a nadie", dijo Davidson.
"Necesitamos darles la oportunidad de ingresar al sistema de atenci�n m�dica, y debemos darles la oportunidad de aprender que la diabetes es el mayor problema de atenci�n m�dica que enfrentar�n en el a�o 2000".
Datos sobre la diabetes Se�ales de alerta temprana de la diabetes
* Sed excesiva
* Orinaci�n frecuente, incluso tener que levantarse en medio de la noche
* Heridas que tardan m�s de lo habitual en sanar
* Aumento de peso excesivo durante el embarazo;Dar a luz a un beb� pesando m�s
de 9 libras
* Hambre extrema
* Visi�n borrosa
* P�rdida de peso dram�tica
* Picor
* Fatiga
C�mo evitar la diabetes
* Come menos para deshacerse de libras extra.
Por ejemplo, come una tortilla en lugar de tres
* Reduce las grasas: carne de asarla en lugar de fre�r, eliminar la piel de pollo
Antes de cocinarlo, no ponga tocino o chorizo en frijoles, no fre�r tortillas para
enchiladas o tacos
* Come m�s frutas y vegetales
* Haz ejercicio regularmente.
Caminar 30 minutos al d�a es la forma de ejercicio m�s f�cil y barata
* Reduce los az�cares simples como en los pasteles
* Ver un m�dico regularmente
Diabetic Diet Book El distrito de Los �ngeles de la Asociaci�n Diet�tica de California ha publicado un folleto �til, "La Dieta Diabetica", que revis� este a�o.
El folleto, escrito en ingl�s y espa�ol, est� disponible por $ 5.76 de California Dietetic Assn., Box 3506, Santa M�nica, CA 90403.
N�MEROS TELEF�NICOS
Para folletos o m�s informaci�n sobre la diabetes, llame a American Diabetes Assn.
(800) 828-8293;(213) 381-3639;o (714) 662-7940.
Complicaciones de diabetes
Seg�n la Asociaci�n Americana de Diabetes, en los Estados Unidos todos los a�os:
* 150,000 personas mueren de diabetes y sus complicaciones
* 5,000 personas se quedan ciegas por diabetes
* El 10% de todos los casos de enfermedad renal que necesitaban di�lisis fueron causadas por diabetes
* El 45% de todas las amputaciones no traum�ticas de la pierna y el pie fueron causadas por diabetes
* La enfermedad card�aca es de 2 a 4 veces m�s probable en personas con diabetes


