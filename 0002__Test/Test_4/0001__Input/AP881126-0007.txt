El estudio adicional de una hormona recientemente aislada que se encuentra en los p�ncreas de los diab�ticos puede conducir a nuevos tratamientos para la forma m�s com�n de la enfermedad, dice un cient�fico.
`` Tenemos mucha evidencia de que es probable que esta sea, si no la causa final, al menos una parte importante del proceso de la enfermedad '', dijo el bioqu�mico de Nueva Zelanda Garth Cooper.
La investigaci�n `` abre la puerta al estudio cient�fico de la enfermedad a un nivel que no era posible antes y potencialmente los mecanismos que descubrimos pueden ser muy amplios '', dijo.
Cooper, quien ha estado trabajando con cient�ficos de la Universidad de Oxford, describi� la investigaci�n hormonal esta semana en el 13 � Congreso Internacional de la Federaci�n de Diabetes en Sydney, Australia.
En su presentaci�n, Cooper dijo que la hormona, denominada `` amilina '', era normalmente indetectable pero se encontraba en altos niveles en los p�ncreas de los diab�ticos.
La amilina parece ser responsable de la obesidad, la secreci�n de insulina reducida y la efectividad reducida de la insulina observada en la diabetes tipo II, dijo.
Actualmente, la obesidad se considera un importante contribuyente a la enfermedad en lugar de un resultado de la misma.
La insulina normalmente controla el nivel de az�car en la sangre.
En la diabetes tipo II, tambi�n llamada diabetes no dependiente de insulina, la insulina del cuerpo no es efectiva y los niveles de az�car en la sangre aumentan demasiado.
Las complicaciones pueden incluir enfermedad renal, ceguera y gangrena que requiere amputaciones.
La diabetes tipo II afecta la mayor�a de los diab�ticos estimados de los naciones, seg�n la Asociaci�n Americana de Diabetes.
A menudo se puede controlar a trav�s de la dieta y el ejercicio.
Cooper dijo que los investigadores esperan desarrollar sustancias que bloqueen la secreci�n o acci�n de Amylin, abriendo la posibilidad de tratamiento.
Tambi�n dijo que los residentes esperan desarrollar una prueba para detectar la diabetes muy temprano en su desarrollo.
El nuevo trabajo es `` un hallazgo muy importante '' si la amilina realmente bloquea la insulina y aparece en cantidades anormales en diab�ticos, dijo el viernes F. Xavier Pi-Sunyer, autoridad en la diabetes tipo II.
Los cient�ficos ya sab�an de otra hormona pancre�tica que bloquea la insulina, pero no se encuentra en niveles anormales en diab�ticos, dijo Pi-Sunyer, director de la Divisi�n de Endocrinolog�a, Diabetes y Nutrici�n en el Centro del Hospital St. Luke's-Roosevelt en Nueva York.


