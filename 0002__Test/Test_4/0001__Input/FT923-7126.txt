Es ir�nico que haya una carrera sin precedentes y codiciosa para los diamantes de los mineros en Angola y Canad� precisamente en un momento en que el negocio de los diamantes est� en agitaci�n y inevitablemente se hacen preguntas sobre la capacidad del cartel m�s exitoso del mundo para mantener su agarre apretadoen el mercado.
En los territorios del noroeste de Canad�, el descubrimiento de 81 peque�os diamantes, algunos de la calidad de las gemas, ha provocado la mayor prisa por reclamos de miner�a en la historia de la industria norteamericana.
Los Stakers est�n utilizando helic�pteros porque cada �rea de reclamo es muy grande y los buscadores han reclamado cada pieza de tierra dentro de los 300 km (185 millas) del descubrimiento.
A pesar de toda esta actividad agitada, es muy poco probable que alguien encuentre suficientes diamantes grandes para que el desarrollo de la primera mina de diamantes de Canad� valga la pena.
Las probabilidades a favor de hacer una fortuna son mucho mejores para los cazadores de diamantes en Angola.
M�s de d�lares de 1 m validan los diamantes de gemas al d�a est�n siendo de contrabando de ese pa�s a la venta en Amberes.
Se estima que 50,000 excavadores de empresas privadas ya est�n trabajando y sus n�meros est�n siendo aumentados por 500 por d�a.
Este aumento de la miner�a ilegal, particularmente en la regi�n de Cuango, que produce el 80 por ciento de los diamantes de Angola y algunas de sus piedras gemas de mayor calidad, comenz� en mayo de 1991, despu�s del acuerdo de paz que permiti� la libertad de movimiento por primera vez en16 a�os.
Adem�s de los mineros experimentados, muchos soldados que no pudieron encontrar trabajos civiles han llegado a las �reas de diamantes.
El inicio de la estaci�n seca y la ca�da en los niveles del r�o desde finales de mayo de este a�o ha alentado lo que De Beers, el grupo sudafricano que domina el negocio mundial de diamantes, describe como 'una explosi�n repentina y sin precedentes en el suministro de angolianos il�citosDiamantes que llegan al mercado '.
A pesar de que hay tantos excavadores en el trabajo, se hacen preguntas sobre si podr�a haber m�s en esta explosi�n.
'�Unita, el movimiento rebelde de Angola, construy� una reserva que ahora se est� lanzando?�La Endiama, la compa��a de diamantes estatal de Angola, est� implicada de alguna manera?En el mundo turbio de diamantes que abundan tales rumores.
De Beers probablemente conoce las respuestas porque su red de inteligencia es notable.
Lo que tambi�n es notable es que todos los que luchan por los diamantes en Angola o que abandonan los cielos para apostar reclamos costosos en Canad� dan por sentado que el cartel de diamantes podr� continuar manteniendo los precios y hacer que todos sus esfuerzos valgan la pena.
El cartel ha sobrevivido en parte porque nadie necesita diamantes.
Est�n compuestos de carbono muy duro para que puedan ser �tiles para perforar agujeros en material resistente, pero hay sustitutos para este uso.Los diamantes gemas son �nicamente para la decoraci�n y no tienen un prop�sito �til.
Pero el cartel se ha asegurado de que los precios de diamantes �speros (sin cortar) hayan aumentado constantemente desde la d�cada de 1930, incluso cuando en las profundidades de la recesi�n de 1981-86 el precio de un diamante de gema de quilates, un diamante de gema quilatense cay� en el mercado minorista desde d�lares 60,000.a d�lares 10,000.
El cartel est� organizado por la organizaci�n de ventas central de De Beers en Londres, que comercializa alrededor del 80 por ciento de los diamantes �speros del mundo.
Adem�s de la propia producci�n de De Beers de Namibia y Sud�frica, la OSC maneja diamantes de gemas �speros de Angola, Australia, Botswana, Rusia, Tanzania y Zaire.
La OSC ha estado confundiendo tantos de los diamantes angole�os de contrabando posible para dejar de ser creado por Havoc en un negocio que ya sufre severamente por la demanda empapada en los Estados Unidos y Jap�n, los dos mercados m�s grandes, que comparten alrededor de dos tercios de demanda entre ellos.
De Beers dice eso, porque los tiempos son dif�ciles.Probablemente tendr� que reducir su pago anual de dividendos por primera vez desde 1981.
Tambi�n le ha dicho a los productores que reduzcan las entregas en un 25 por ciento, algo que los contratos de OSC permiten en momentos de estr�s.
De Beers libera una corriente controlada de diamantes �speros al mercado a trav�s de las 'miras' ofrecidas por la OSC diez veces al a�o a unos 160 compradores seleccionados.
Se les ofrecen cajas de diamantes, cada uno con un valor de entre d�lares 500,000 y d�lares de 25 m.
El contenido es juzgado por la OSC para equilibrar los requisitos de los compradores con la demanda del mercado.
Los compradores tienen que aceptar todos los diamantes o rechazar la caja.
Harry Oppenheimer, cuya familia controla efectivamente tanto de Beers como su grupo hermano, la Corporaci�n Anglo Americana de Sud�frica, estaba defendiendo nuevamente el Cartel la semana pasada en la apertura formal de la mina R1.1bn Venetia Diamond en Transvaal.
"La OSC no pudo, y no pudo, ejecutar un sistema monopol�stico", dijo.
'En malos momentos como este, a veces deseo que pudi�ramos.
El hecho es que el nivel de producci�n mundial de diamantes, que se lleva a cabo en muchos pa�ses, no se puede controlar.
Los precios de los diamantes no se pueden fijar artificialmente, pero deben establecerse en un nivel que permita que se equiparan la producci�n y el consumo.
'Lo que la OSC durante muchos a�os ha hecho con �xito es operar una piscina de amortiguaci�n, almacenar diamantes en malos tiempos y liquidar sus existencias cuando la demanda excede el nivel de suministro.
De esta manera, ha podido preservar un grado esencial de estabilidad en el precio de este lujo final de los diamantes gemas, con la ventaja com�n de los productores, procesadores y consumidores de este producto natural �nico '.
Todo muy altruista.
Pero De Beers obtiene grandes ganancias de su negocio de diamantes: un r�cord de d�lares de 1.24 mil millones en 1990 y m�s de d�lares 1 mil millones el a�o pasado.
El cartel casi perdi� el control del mercado a principios de la d�cada de 1980.
Era un momento de inflaci�n galopante, un d�lar d�bil y bajas tasas de inter�s.
Los comerciantes, particularmente en Israel, almacenaron diamantes en dinero prestado como cobertura contra la inflaci�n y cuando golpe� la recesi�n, tuvieron que arrojar diamantes m�s r�pido de lo que la organizaci�n de ventas central pod�a trapearlos.
Las acciones de diamantes de la OSC, por un valor de d�lares de 1 mil millones en 1980, alcanzaron d�lares de 1.9bn en 1984.
Cientos de fideicomisos de inversi�n de diamantes y comerciantes estaban en bancarrota en ese momento y Australia y Zaire desafiaron el casi monopolio de De Beers.
De Beers ha tenido un buen cuidado de que las acciones comerciales nunca m�s se hayan construido por razones especulativas, con frecuencia yendo a los bancos, los posibles financieros de las operaciones de almacenamiento, para "educarlos" sobre la forma en que funciona el mercado de diamantes.
Despu�s de que lleg� el busto.
Las ventas de diamantes en t�rminos de valor aumentaron en un 20 por ciento en 1987 y otro 37 por ciento al a�o siguiente.
Para enfriar el mercado, la OSC impuso aumentos de precios contundentes: 13.5 por ciento en mayo de 1988 y 15.5 por ciento en marzo de 1989.
Subyacente al auge fue el crecimiento fenomenal en la demanda de diamantes en Jap�n, donde la falta de espacio vital alienta a las personas a mostrar su riqueza comprando art�culos peque�os pero caros.
La OSC aument� la tendencia con una promoci�n inteligente para que hoy los anillos de compromiso de diamantes se intercambien regularmente en Jap�n, mientras que los pr�ximos matrimonios tradicionalmente se celebraron en una taza de vino de arroz.
Debe estar abierto a cuestionar si se puede encontrar otro Jap�n para levantar el mercado de diamantes a medida que termina la recesi�n actual.
Pero mientras tanto, el Sr. Julian Ogilvie Thompson, presidente de De Beers, insiste en que el control de la OSC del mercado de diamantes no est� resbalando.
"Al limpiar los suministros angole�os, podemos decir que m�s de los diamantes �speros del mundo est�n pasando por el CSO que nunca antes", dice.
"Al aplicar las cl�usulas de compras diferidas en nuestros contratos con los productores, estamos aplicando el medicamento adecuado para garantizar que la demanda coincida con la oferta y que el mercado de diamantes permanezca estable".
El futuro de las operaciones de diamantes de De Beers en Namibia se discuti� con el gobierno all� ayer.
En el pasado ha habido sugerencias de que la CDM, la subsidiaria de propiedad total de De Beers, podr�a estar al menos nacionalizada en parte.
CDM tiene operaciones mineras de diamantes sobre un �rea costera que se extiende a 100 km al norte del r�o Orange y recientemente llev� a la producci�n de las minas de Auchas y Elizabeth Bay.
El a�o pasado alcanz� la producci�n namibia
1.19m quilates, frente a 750,000.


