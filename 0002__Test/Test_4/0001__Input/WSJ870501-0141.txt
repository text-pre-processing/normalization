A fines de la d�cada de 1970, los agricultores pobres que viven en comunidades que rodean este peque�o pueblo comenzaron a reemplazar sus campos de cacao y caf� con coca para satisfacer la creciente demanda de coca�na en los Estados Unidos.
Al igual que miles de otros agricultores en el valle superior de Huallaga de Per�, descubrieron que si bien el cacao y el caf� pod�an dar a sus familias lo suficiente para comer, Coca podr�a hacerlos ricos.
�ltimamente, alguien m�s ha sido atra�do por sus grandes ganancias: las guerrillas asesinas del pa�s, el camino brillante.
Los trabajadores del desarrollo, los agricultores y los sacerdotes en el �rea dicen que los rebeldes han llegado constantemente a la alta Huallaga, un vasto tramo de jungla al noreste de Lima y la zona de cultivo de coca m�s grande del mundo.
Hasta hace poco, el camino brillante hab�a descuidado en gran medida la jungla.
Al seguir su estrategia de tratar de derrocar al gobierno elegido democr�ticamente de Per�, hab�a concentrado sus bombardeos y tiroteos en las monta�as y en Lima.
Pero las guerrillas han descubierto que pueden ganar f�cilmente el apoyo entre los agricultores en las selvas de la alta Huallaga debido a la ira generalizada generada por la campa�a antidrogas peruana financiada por Estados Unidos.
Este esfuerzo se centra en la interdicci�n del tr�fico de drogas y la erradicaci�n de las plantas de coca.
Mientras que Washington se ha apresurado a anunciar los v�nculos entre los guerrilleros y el tr�fico de drogas en otros pa�ses, los funcionarios estadounidenses minimizan la conexi�n entre las drogas y el camino brillante.
Una raz�n es la verg�enza que el esfuerzo antidrogas financiado por los Estados Unidos puede haber empujado a los productores de coca m�s cerca de las guerrillas.
Pero lo m�s importante, si se admitiera que los guerrilleros est�n en grandes cantidades en el Alto Huallaga, el ej�rcito podr�a nuevamente recibir un control de emergencia sobre la zona.
Y esto es algo que los funcionarios peruanos y estadounidenses aparentemente prefieren evitar.
El ej�rcito estuvo en vigor en el Alto Huallaga durante el per�odo declarado de un estado de emergencia, de julio de 1984 a diciembre de 1985.
Durante este per�odo, el Ej�rcito ten�a control dictatorial virtual sobre todas las operaciones militares y no militares en la regi�n.
Los funcionarios del gobierno de la polic�a antidrogas (UMOPAR) y la Unidad de Eradicaci�n de Coca (Corah) enviaron a la regi�n para eliminar la producci�n de drogas a menudo fueron confinados a sus cuarteles por el ej�rcito;Simplemente no se les permiti� continuar con su trabajo.
El ej�rcito continu� sus redadas contra el camino brillante en una parte del valle, pero los productores de drogas estaban aumentando la producci�n en varias otras partes, mientras que los funcionarios antidrogas se mantuvieron pr�cticamente bajo bloqueo.
La polic�a antidrogas, los residentes del Alto Huallaga y los funcionarios del Ministerio del Interior informan que los oficiales del Ej�rcito recibieron cientos de miles de d�lares en pagos de los narcotraficantes por su ayuda para mantener a raya a los funcionarios antidrogas.
"Es mejor para los 'narcos' cuando el ej�rcito est� aqu�", dice un oficial de Umopar.
Cuando el nuevo presidente Alan Garc�a levant� el estado de emergencia, el ej�rcito perdi� cualquier habilidad que pudo haber tenido que proteger a los productores de coca.
No solo perdi� el poder de la autoridad en la regi�n, sino que el n�mero de tropas en el valle superior de Huallaga se redujo dr�sticamente.
Las tropas fueron enviadas a otras �reas del pa�s, particularmente en la regi�n monta�osa de Ayacucho, donde las guerrillas han estado activas.
Con el impedimento del ej�rcito desaparecido, la campa�a antidrogas se propuso expulsar a los traficantes de Tingo Mar�a, su antigua sede, eliminando la coca creciendo en las inmediaciones de la ciudad.
Pero los Narcos ahora se han atrincherado en las ciudades de Tocache y Uchiza, donde la polic�a ha sido bloqueada repetidamente de los residentes armados.
Al mismo tiempo, los agricultores ahora est�n plantando coca en partes resistentes de la jungla, lejos del barrido de Corah.
Sin embargo, la p�rdida de la protecci�n del ej�rcito proporcion� al camino brillante una oportunidad de oro.
"Las guerrillas ahora se presentan como los defensores de los agricultores de Coca", dice Segundo Ram�rez, propietario de la estaci�n de radio de Aucayacu.
A cambio de su protecci�n, los guerrilleros exigen que los agricultores entreguen una quinta parte de su cultivo de coca, seg�n oficiales de UMopar, agricultores y sacerdotes.
Luego, los rebeldes procesan las hojas en pasta de coca y venden la pasta a los traficantes internacionales, generalmente colombianos, por armas y dinero, dicen fuentes locales.
El comandante de la polic�a antidrogas, el general Juan Zarate, dice que hay pocas guerrillas en el Alto Huallaga y niega que exista una alianza entre los narcotraficantes y el camino brillante.
Pero los residentes insisten en que los rebeldes operan como un gobierno de facto en la zona, incluso en Aucayacu y Tulumayo, donde la polic�a y el ej�rcito tienen puestos de avanzada.
Una actividad guerrillera importante es organizar grupos de autodefensa comunitarios contra la odada polic�a.
Veintinueve trabajadores de Corah han sido asesinados en el valle desde 1984, y es probable que el camino brillante fomente este tipo de violencia.
A principios de enero, los rebeldes aparentemente estaban detr�s de una huelga que protestaba por el programa antidrogas.
Durante tres d�as cerraron la carretera de 50 millas desde Tingo Mar�a a R�o Uchiza, el �nico camino continuo y pavimentado en el valle.
La relaci�n entre el camino brillante y la gente del valle no es del todo cordial.
Como parte de sus esfuerzos para impartir "justicia" revolucionaria entre las personas, el camino brillante ha administrado represalias brutales contra aquellos campesinos que no se ajustan a su perfil "nuevo".
Durante la huelga de enero, los guerrilleros mataron a un presunto ladr�n y un vendedor de cigarrillos de pasta de coca, seg�n un trabajador del programa de sustituci�n de cultivos que trabaja directamente con los agricultores en el �rea.
Los rebeldes tambi�n se creen responsables de los asesinatos de cuatro hombres homosexuales en noviembre pasado en Aucayacu.
El 13 de marzo en El Triumfo, una peque�a comunidad cerca de Aucayacu, ejecutaron a un hombre de 23 a�os que habl� contra una actividad organizada por guerrillas.
"Eso realmente sacudi� a las 80 familias de la comunidad", dice Enrique Pe�a, un p�rroco que ya no puede ingresar al �rea controlada por guerrillas.
Las guerrillas son responsables de asesinar a seis alcaldes en el �rea desde 1985, incluidos dos alcaldes de Aucayacu, que tiene 3.500 residentes.
El vicealcalde de Aucayacu, Luis Salazar, dice: "El capit�n del ej�rcito me ha dicho que estoy en la lista negra de Guerrillas".
El problema del gobierno para combatir la violencia actual y la influencia del camino brillante es c�mo garantizar que las tropas del ej�rcito enviadas a esta regi�n rica en coca no se corrompan por el atractivo del dinero de las drogas, como informaron en el pasado.
Este es el Catch-22 que ahora confunde a los funcionarios peruanos y estadounidenses que intentan erradicar tanto las drogas como los guerrilleros del Per�.


