LIMA, 13 de febrero (AFP) - El programa de televisi�n "Sunday Review" dijo que, seg�n una "fuente muy confiable", el final de la "guerra popular", la rendici�n de las armas y una amnist�a general ser�a la principalpuntos de un acuerdo de paz "sin vencedores o vencidos", entre el camino brillante y el gobierno peruano.
La administraci�n del presidente Alberto Fujimori ha admitido que las conversaciones, dirigidas por Abimael Guzm�n, que ha estado cumpliendo cadena perpetua desde octubre de 1992, se llevan a cabo entre los representantes del gobierno y los l�deres brillantes en prisi�n.
Sin embargo, no se han emitido detalles sobre discusiones o posibles acuerdos.
Seg�n el programa "Sunday Review", dirigido por el periodista Nicolas Lucar en la red de televisi�n Lima America Channel 4, el acuerdo de 10 puntos dice que su aspecto principal es "detener la guerra popular con sus cuatro formas de lucha: terrorismo, asesinatos selectivos, sabotaje y agitaci�n armada y propaganda ".
Otro punto se refiere a "desmantelar el ej�rcito guerrillero del pueblo con su rendici�n y la destrucci�n de sus armas".
Tambi�n incluye el autodismantil de los comit�s populares y la base de apoyo de la ruta brillante en todo el pa�s.
Se ha mencionado una amnist�a general y un proceso por el cual, "poco a poco y de acuerdo con las circunstancias, los prisioneros de guerra y los prisioneros pol�ticos ser�n liberados y sus sentencias se reducir�n".
Junto con la "mejora de las relaciones entre ambas partes", tambi�n se enfatiza que el acuerdo estar�a sin "conquistadores o vencidos", y aliviar�a el camino para el regreso de los miembros de la ruta brillante que viven en el extranjero "para recuperar el pa�s.vida social y normal ".
El acuerdo implicar�a un "cese de hostilidades hacia parientes de los miembros de la ruta brillante" y "los archivos, la biblioteca, el museo y otros s�mbolos del movimiento tambi�n ser�an devueltos".
Uno de los puntos en el acuerdo menciona la necesidad de "apoyo econ�mico e inversiones en las �reas devastadas por la guerra".
Hasta ahora, las autoridades gubernamentales no han negado ni rechazado la precisi�n de este acuerdo entre el gobierno y el camino brillante.


