John Hatch, fundador de la Fundaci�n sin fines de lucro para la asistencia comunitaria internacional, promueve la banca de la aldea para alentar a la empresa privada en el Tercer Mundo.
Lo que prev� es un golpe corporal para la pobreza mundial a trav�s de la econom�a de bootstrap.
John Hatch dice que golpe� su `` vacunaci�n de pobreza '' en su tercer mundo, mientras que p�nico en un vuelo a Bolivia.
Hab�a decidido que no pod�a cumplir con los requisitos de un trabajo de consultor�a que hab�a aceptado de la Agencia para el Desarrollo Internacional, y estaba luchando por encontrar un sustituto adecuado.
`` Y en mi segundo bourbon doble a 35,000 pies sobre los Andes, tengo que decir que fue como si este pensamiento pasara por mi cuerpo ", recuerda Hatch.
`` Ten�a mi calculadora y estaba dibujando esto y estaba haciendo n�meros, y llegu� a La Paz con un concepto completo ''.
Lo que hab�a tropezado era la banca de la aldea, un sistema de inversi�n que utiliza peque�os pr�stamos giratorios para mujeres pobres para financiar su empresa privada, permiti�ndoles crear ahorros mientras pagaba intereses y calificando para pr�stamos m�s grandes.
La serie de pr�stamos, a partir de $ 50, es renovable durante tres a�os a un techo de $ 300, lo que permite a los destinatarios alcanzar un nivel en el que puedan calificar para cr�dito comercial.
Lo que Hatch imagin� en ese avi�n hace cinco a�os se ha traducido a 400 bancos de las aldeas en nueve pa�ses con m�s de 35,000 miembros.
Las carteras totalizan $ 1.6 millones, con una tasa de reembolso del 96 por ciento.
Los bancos de las aldeas est�n operando en M�xico, Hait�, Tailandia, Guatemala, Honduras, Costa Rica, Bolivia, Per� y El Salvador.
El concepto es similar al que est� detr�s del Grameen Bank of Bangladesh: canalizar el dinero a las madres empobrecidas, principalmente, sin las demandas de los bancos comerciales para garant�as, para que puedan mejorar la vida de sus familias.
Lo que Hatch prev� es un golpe corporal para la pobreza mundial a trav�s de la econom�a de bootstrap.
Hatch, voluntario de un solo Cuerpo de Paz con un Doctorado en Desarrollo Econ�mico, es fundador y presidente de la Fundaci�n sin fines de lucro para la asistencia comunitaria internacional, que recientemente traslad� su sede de Tucson a Alexandria, VA.
�l dice que su impulso fue, y sigue siendo, los miles de ni�os en todo el mundo que mueren diariamente debido a la inanici�n, lo que �l llama el sacrificio de `` probablemente nuestro recurso m�s preciado ''.
Hatch se enfoca en llegar a las madres en la creencia de que fortalecerlas econ�micamente les permite cuidar mejor a sus hijos.
Hay `` casi una garant�a del 100 por ciento '' que el aumento de los ingresos para las mujeres entrar� en mejoras en el bienestar de sus hijos, dice Hatch.
`` Ense�amos a las madres severamente pobres c�mo crear su propio banco autogestionado, que les proporciona pr�stamos de auto empleo, les proporciona incentivos de ahorro y les proporciona un grupo de apoyo de madres como ellos, para que todos atacen el cambio enal mismo tiempo.''
Lawrence Yanovitch, coordinador del Programa de Desarrollo de la Empresa Peque�a para los Servicios de Ayuda Cat�lica en Baltimore, Maryland, dice, `` John Hatch est� cerca de la voz de los pobres.
Aunque se le ocurri� algunas herramientas econ�micas sofisticadas, ha mantenido el humanitarismo ''.
Catholic Relief Services ingres� a una empresa conjunta de cinco a�os y $ 1 mill�n hace un a�o con la Agencia de los Estados Unidos para el Desarrollo Internacional y la Fundaci�n para crear 40 bancos de las aldeas en Tailandia.
Se dirigieron al desarrollo de microempresas a trav�s de peque�os empresarios, vendedores, mujeres del mercado de aldeas, que en algunos pa�ses en desarrollo comprenden hasta el 50 por ciento del empleo.
La capitalizaci�n para un banco generalmente le costar� a una agencia de patrocinio $ 5,000.
Beryl Rupp, un funcionario del Club Rotario en Phoenix, dice que los miembros del banco compran necesidades con sus ganancias o ahorros: una silla para sentarse, ollas para cocinar, mantas, mejor comida.
Las mujeres, cuando se les pregunta, hablan del mayor beneficio en t�rminos de lo que Hatch llama `` percepciones de empoderamiento ''.
`` Dicen cosas como 'ahora soy alguien' o 'mi esposo me respeta m�s' '.
Su veh�culo para mejorar es el banco de la aldea, cada uno de las cuales est� compuesto por 30 a 50 mujeres que, a trav�s de pr�stamos que comienzan en $ 50, pueden ganar lo suficiente para proporcionar ahorros y crear capital.
Hatch dice que los pr�stamos les dan libertad para comenzar sus propios proyectos de inversi�n en o cerca del hogar.
La fundaci�n ir� a una aldea con un compa�ero de empresa conjunta que presenta al menos la mitad de la capitalizaci�n, como clubes rotativos o servicios de ayuda cat�lica, e identifica a una mujer clave.
Luego se pone en contacto con otros para asistir a una reuni�n de organizaci�n.
`` Olv�date del alcalde, olvida al sacerdote, olvida al jefe '', dice Hatch.
`` Queremos saber qui�n es la mujer m�s poderosa en ese pueblo ''.
En la reuni�n, el c�rculo de mujeres ilustr� ejemplos de trabajo r�pido que desean que los pr�stamos financien.
Puede ser vender productos, comestibles, productos horneados, criar pollos, coser o manualidades.
Cada mujer firma su nombre o proporciona una huella digital como garant�a, acordando pagar una decimosexto del pr�stamo semanalmente, m�s intereses del 1 por ciento al 3 por ciento, en l�nea con los bancos locales.
En algunos pa�ses, la tasa de reembolso ha sido del 100 por ciento.
En general, el promedio es del 97 por ciento, dice Hatch.
`` El cincuenta por ciento o m�s son analfabetos, y dir�a que el 90 por ciento de ellos no tienen idea de lo que se est�n metiendo cuando comenzamos '', dice Hatch.
`` Y, sin embargo, en 16 semanas son todos expertos.
Simplemente aprenden a medida que van ''.
Las mujeres que se unen a los miembros de la pantalla del banco y eligen a sus l�deres de los asistentes a la primera reuni�n.
Al final de la semana 16, el saldo de pr�stamo de cada miembro es cero.
Pero al mismo tiempo, cada mujer ha estado depositando ahorros semanales de al menos el 20 por ciento del pr�stamo original.
Los ahorros sirven como incentivos para pr�stamos posteriores al aumentar la l�nea de cr�dito cada vez, dijo Hatch.
El inter�s en la banca de la aldea est� aumentando.
Las Naciones Unidas han proporcionado una subvenci�n de $ 50,000 para investigar los efectos del empoderamiento de las mujeres a trav�s de la banca de la aldea en pr�cticas de salud materna e infantil.
La Fundaci�n tiene propuestas importantes pendientes con la Agencia para el Desarrollo Internacional, la Fundaci�n Ford, Citibank y el Chase Manhattan Bank, entre otros.
Rotary Club 100, una organizaci�n de Phoenix y un Club Rotary de la Ciudad de M�xico se encuentran en una empresa conjunta, ya que financi� cinco bancos de las aldeas en Guaymas, M�xico, y se lanzar�n otros cuatro en los barrios bajos de la Ciudad de M�xico.
Hatch dice que cree que la banca de la aldea tiene un papel que desempe�ar en la reducci�n de la pobreza a lo largo de la frontera mexicana y en la desaceleraci�n de la migraci�n a este pa�s.
En cuanto a si dicho sistema podr�a funcionar en los Estados Unidos, RUPP de Rotary dice: `` Francamente, es la �nica esperanza que tenemos de cambiar el sistema de bienestar ''.
`` En �ltima instancia, la salida de la pobreza es ahorros '', agrega Hatch.
`` Se debe construir sobre la parte posterior de sus propios ahorros ''.


