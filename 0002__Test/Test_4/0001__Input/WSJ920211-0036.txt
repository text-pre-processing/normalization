Los opositores a cualquier tipo de control de armas escriben apasionadamente, pero, a juzgar por sus cartas ("Sonr�e cuando dices el control de armas", 14 de enero), no leen bien.
Por ejemplo, un escritor de cartas, comentando mi contrapunto del 12 de diciembre, "el control de armas es constitucional", se sinti� obligado a instruirme que la palabra milicia significa "la gente entera", un punto que cit� tres veces y tambi�n parafraseado una vez en miBreve art�culo.
�l y los otros que habitaban en este punto tampoco leen muy bien la Segunda Enmienda.
El tema no es solo la milicia sino "una milicia bien regulada", las personas entrenadas, disciplinadas y armadas para la defensa de la comunidad bajo oficiales al mando.
Ese mismo corresponsal me acus� de pedir "la eliminaci�n de todas las armas de los ciudadanos estadounidenses a trav�s de la implementaci�n de regulaciones totales".
No dije nada as�.
Dije que aunque la Segunda Enmienda no tiene nada que ver con los usos privados de las armas, hay un derecho poco inundable pero innegable a tener un arma.
Tenemos muchos derechos no enumerados en la Constituci�n, como el derecho de casarse, conducir un autom�vil o ser due�o de un perro.
Estos derechos no se niegan solo porque la Constituci�n no los menciona;y los derechos no se desprecian por el hecho de que debemos obtener una licencia de matrimonio, licencia de conducir o una licencia de perro.
De la misma manera, el derecho a poseer un arma no se menosprecia al ser licenciado o regulado y controlado.
Un escritor de cartas que tambi�n sabe algo sobre la constituci�n, aunque no est� de acuerdo conmigo en otro punto, acord� el tema central, que la regulaci�n de la posesi�n y el uso de armas no viola la constituci�n.
Todas las cartas perdieron completamente mi punto de requerir que los propietarios de armas "se inscriban" con (no "unirse") a la Guardia Nacional.
Lo compar� con la ley actual que requiere que los hombres de 18 a�os se registren con el servicio selectivo.
El registro es muy diferente de la alistaci�n para el servicio activo.
Requerir que los propietarios de armas se registren con la Guardia Nacional proporcionar�a el tipo de escrutinio que contemplaron los proyectos de ley de Brady y Staggers y, si el Congreso se atrev�a, podr�a proporcionar una medida adicional de control sobre qui�n posee qu� tipo de armas.
Pero mi punto principal era mostrar hasta qu� punto el Congreso puede llegar en la legislaci�n de control de armas sin exceder sus poderes constitucionales, utilizando como modelo la legislaci�n del segundo Congreso, que inclu�a a la mayor�a de los autores de la Segunda Enmienda.
Otro escritor de cartas cree que he confundido el problema al llamar al deber de servir en la milicia un derecho a servir, pero si observa los debates en el primer Congreso, ver� que no hay confusi�n en absoluto, los autores de los autoresLa Segunda Enmienda estaba hablando del derecho a servir.
De acuerdo con toda la Declaraci�n de Derechos, su tema son los derechos, no los deberes.
Elbridge Gerry se opuso a la cl�usula de James Madison que exime a las personas "religiosamente escrupulosas", por el peligro de que pueda ser abusado de discriminar a los miembros de ciertas sectas, "declarar qui�nes son aquellos que son religiosamente escrupulosos", y luego mantenerlos fuera de la milicia.
El verdadero problema es que la Segunda Enmienda abord� una preocupaci�n p�blica grave, para proteger el derecho de los ciudadanos a servir como defensores de la comunidad en tiempos de peligro, no los usos personales de las armas.
Y dado que la proliferaci�n de armas en manos de los delincuentes se ha convertido en una calamidad nacional, como atestiguan los oficiales de polic�a de todo el pa�s, es esencial que recuperemos el verdadero prop�sito de la Segunda Enmienda.
Simple y simple, no es barrera para el control sensible de las armas.
Robert A. Goldwin Scholar American Enterprise Institute Washington


