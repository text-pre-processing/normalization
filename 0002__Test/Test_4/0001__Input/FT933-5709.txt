El presidente del hombre, Bill Clinton, ha elegido dirigir la Fuerza de Tarea para ayudar a obtener su pacto de libre comercio con Canad� y M�xico Ratified puede carecer de experiencia en los �mbitos del comercio internacional.
Pero viene a Washington bien preparado para el tipo de trampa pol�tica de la trastienda que se requerir� para persuadir a un Congreso dividido para que lo apruebe.
William Daley es hermano menor y asesor principal del alcalde de Chicago, Richard M Daley, y la descendencia de la legendaria m�quina democr�tica 'Boss', el difunto Richard J Daley.
A diferencia de su hermano de mal genio y muy p�blico, Bill Daley, de 45 a�os, es un operador detr�s de escena que habla suavemente con conexiones importantes en Washington y lazos de larga data con mano de obra organizada.
Nunca se ha postulado para un cargo pol�tico.
Daley acept� la posici�n del TLCAN despu�s de ser aprobado para un puesto de gabinete y rechazar una oferta para ser el principal negociador de Clinton en el acuerdo general sobre tarifas y comercio en Ginebra.
Presidi� la campa�a electoral de Clinton en Illinois, donde entreg� el 48 por ciento de los votos a Clinton, por encima del promedio nacional, y contin�a como un partidario tangible.
M�s recientemente, organiz� una elegante cena de Chicago para el presidente que recaud� d�lares 1 m (libras 670,000).
Sin embargo, el nombramiento del Sr. Daley para presidir el grupo de trabajo de la Casa Blanca sobre el Tratado de Libre Comercio de Am�rica del Norte no es una recompensa pol�tica.
Las encuestas recientes muestran que hasta dos tercios del pueblo estadounidense no apoyan el TLCAN, y la oposici�n de base alimentada por grupos laborales est� creciendo.
El propio Sr. Clinton reserv� su apoyo al acuerdo, negociado bajo la administraci�n de George Bush, hasta que se alcanzaron los acuerdos secundarios que se ocuparon de los problemas ambientales y laborales el mes pasado.
Muchos dem�cratas en el Congreso, incluidos algunos en la poderosa delegaci�n de Illinois, no est�n comprometidos.
Con el acuerdo destinado a desentra�arse si no es ratificado antes del 1 de enero, la batalla por el apoyo del Congreso ser� feroz, r�pida y visible.
El Sr. Daley fue elegido para el trabajo, dicen los observadores de Chicago, porque ha demostrado que puede influir en los votos y est� dispuesto a correr el riesgo de perder una batalla controvertida.
'Bill Daley no es ne�fito de Washington.
Clinton necesita a alguien con sentido pol�tico para mover esto a trav�s del Congreso, y Daley tiene las conexiones '', dice un compa�ero cabildero de Washington.
La oposici�n al TLCAN proviene de las circunscripciones democr�ticas centrales, lo que lo convierte en un posible bache pol�tico para Clinton.
Los talentos del Sr. Daley han sido convocados, dicen los expertos, para convencer a los dem�cratas moderados de respaldar el acuerdo.
El presidente necesita 218 votos en la C�mara para ratificar el TLCAN.
La administraci�n cuenta con 125 republicanos a favor del acuerdo.
Se espera que Clinton Ally y su compa�ero dem�crata de Chicago, el Sr. Dan Rostenkowski, presidente del Comit� de Medios de la C�mara de Representantes, entregue a los 20 dem�cratas en su comit�.
Sin embargo, el Sr. Rostenkowski est� bajo investigaci�n por el uso inadecuado de sus beneficios de la oficina de correos y enfrenta un futuro inquietado.
Por el cargo aproximado, el Sr. Daley tendr� que marcar a 73 votos de representantes democr�ticos de los estados que se destacan para obtener el m�ximo del acuerdo: aquellos a lo largo de la frontera con M�xico y aquellos con grandes intereses agr�colas, de productos de consumo o exportaci�n manufactureros.
Su m�sculo trasero al estilo de Chicago ser� un contrapeso invisible para las t�cticas p�blicas del oponente m�s destacado del TLCAN, el Sr. Ross Perot.
La campa�a populista anti-NAFTA del Sr. Perot promociona el 'sonido de succi�n gigante' que se escuchar� a medida que los trabajos con belleza del TLCA fluyan al sur de la frontera de Texas.
"Es una yuxtaposici�n muy interesante de los estilos", dice el comentarista pol�tico de Chicago, Bruce Dumont.
'Bill Daley es como un bombardero sigiloso.
Es posible que no lo veas, pero ver�s los resultados.
Daley ha tomado cuatro meses de su bufete de abogados de Chicago para liderar el impulso del TLCAN.
Recientemente se uni� a la firma despu�s de un per�odo de tres a�os como presidente del Banco Amalgamado de Chicago, que fue fundada por el sindicato de trabajadores textiles amalgamados en 1922.
Aunque pas� a propiedad privada en 1966, su junta todav�a est� dominada por funcionarios sindicales de alto perfil.
Clinton puede estar depositando en los lazos de la Uni�n del Sr. Daley para ayudar al esfuerzo del TLCAN.
Sin embargo, el Sr. Daley, al empujar el TLCAN, corre el riesgo de alienar a sus partidarios sindicales.
Jim Jontz, ex congresista de Indiana y director de la campa�a comercial de ciudadanos anti-NAFTA, dijo: 'No s� por qu� Bill Daley querr�a estar en una posici�n como esa.
Simplemente est� mal.


