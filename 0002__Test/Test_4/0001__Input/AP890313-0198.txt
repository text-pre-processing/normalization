Dentro de una peque�a casa autom�tica, Joannepierluissi levant� la manga cuando la enfermera Mary P�rez insert� una aguja en la vena sobre su antebrazo, arrastrando sangre en un tubo para una prueba de diabetes.
Mientras sus hijas observaban, Pierluissi, de 24 a�os, dijo que era para ellos, tanto como para ella, que aceptaba ser probada para el mortal asesino de los hispanos.
`` Estaba preocupado porque dijeron que una t�a m�a lo ten�a y solo quer�a venir para el chequeo.
Toda nuestra familia va a pasar por el programa para asegurarse de que si tenemos que hacer algo al respecto ".
Doce millones de estadounidenses tienen alguna forma de diabetes, pero es m�s frecuente entre las minor�as, especialmente los nativos americanos, los negros e hispanos.
Los hispanos tienen tres veces m�s probabilidades de desarrollar diabetes que la poblaci�n general, y el 40 por ciento de las 700,000 v�ctimas en Texas son mexicoamericanos.
M�s de 150,000 estadounidenses mueren de diabetes cada a�o;Otras 150,000 muertes est�n relacionadas con la diabetes, seg�n la Asociaci�n Americana de Diabetes.
Nadie sabe realmente lo que lo desencadena, pero los investigadores creen que los hispanos podr�an tener la llave.
San Antonio, la novena ciudad m�s grande de la naci�n, con una poblaci�n que es 50 por ciento hispana, se est� convirtiendo en la base de los estudios de diabetes.
Los investigadores llevan una casa m�vil personalizada a los vecindarios para verificar al azar los hispanos y los anglos para la enfermedad, lo que priva el cuerpo de la insulina y puede provocar complicaciones que afectan el coraz�n, los ri�ones, los ojos y los nervios.
El maquillaje hispano de San Antonio llev� al Dr. Ralph DeFronzo a abandonar su prestigioso puesto como investigador de diabetes de la Universidad de Yale y persuadir a su equipo de cuatro miembros para que se traslade al Centro de Ciencias de la Salud de la Universidad de Texas.
Un epidemi�logo en el Centro, el Dr. Michael Stern, ha dedicado 10 a�os al estudio de la diabetes hispana y dirigi� el estudio de base de la diabetes tipo II.
El tipo II, la forma m�s com�n, se desarrolla principalmente en adultos obesos mayores de 40 a�os que tambi�n pueden tener antecedentes familiares de la enfermedad.
En los diab�ticos obesos, el cuerpo tiene demasiada insulina porque est� quemando m�s grasas que los az�cares.
La diabetes tipo I generalmente se desarrolla entre los adolescentes y requiere que tengan inyecciones diarias de insulina.
Stern dijo que los estudios familiares de pacientes diab�ticos lo est�n poniendo m�s cerca de encontrar el gen que desencadena la enfermedad.
Un marcador gen�tico podr�a identificar a las personas susceptibles, lo que podr�a conducir a una prueba de detecci�n, dijo.
`` Entonces podr�a salir y concentrarse en los susceptibles gen�ticos y puede ser m�s intenso en sus recomendaciones y tambi�n podr�a estudiar ese grupo ''.
Stern cree que si las personas ejerc�an m�s y com�an menos alimentos saturados de grasa comunes a las dietas de los hispanos de bajos ingresos, menos obtendr�a la enfermedad.
`` Usamos el t�rmino doble riesgo para los mexicoamericanos '', dijo.
`` No sabemos por qu�, cuando obtienen diabetes, tienen una forma m�s severa de la enfermedad _ si es una diferencia biol�gica o es que no reciben una buena atenci�n m�dica.
`` Pero lo interesante es que los mexicoamericanos de ingresos altos no tienen el mismo riesgo que los mexicoamericanos de bajos ingresos.
Puede ser que el gen est� all�, pero por alguna raz�n no puede expresarse en los mexicoamericanos de ingresos altos.
`` Adem�s, los mexicoamericanos tienden a tener m�s grasa corporal en el torso superior y podemos ver que se relacione con la diabetes ''.
Entre 1979 y 1988, Stern y su personal estudiaron a m�s de 5,000 personas y descubrieron que 387 de 2,905 hispanos ten�an la enfermedad, o 13.3 por ciento, en comparaci�n con solo 87 de 1,780 anglos, o 4.8 por ciento.
Los investigadores creen que las dietas de los hispanos pobres de alimentos baratos y procesados, la falta de ejercicio y la atenci�n m�dica poco frecuente _ debido a la pobreza o un sesgo cultural contra los m�dicos, aumenta su riesgo de adquirir diabetes.
El estudio est� en su etapa de seguimiento, para ver si los pacientes con diabetes diagnosticados han cambiado su estilo de vida y han buscado atenci�n m�dica.
Teresa Castro, de 54 a�os, cuyo esposo diab�tico muri� a los 37 a�os, pas� por la proyecci�n hace ocho a�os.
Le dijeron que debido a su peso, 254 libras, ten�a hipertensi�n y estaba en riesgo de diabetes.
Los m�dicos la pusieron en una dieta estricta y baja en grasa y perdi� 26 libras.
`` Fui a la proyecci�n porque llamaron y dijeron que era gratis.
Es por eso que fui a eso, porque ser pobre no pod�a permitirme ir al m�dico para este tipo de chequeo '', dijo.
`` Mi madre tiene diabetes y me dicen que tambi�n podr�a tener diabetes, pero no s� demasiado al respecto.
`` Me siento bien, pero me dicen que un a�o puedes estar bien y al a�o siguiente, puede ser totalmente diferente ''.
DeFronzo, quien en 1988 fue elegido como el principal investigador de diabetes por asociaciones de diabetes canadienses y japonesas, dice que su unidad en el Centro de Ciencias de la Salud intentar� usar muchos de los pacientes de Stern para la investigaci�n.
Eso incluir� el trabajo para Lipha Chemicals, lo que hace un medicamento antidiab�tico llamado metaforim que mejora la capacidad del cuerpo para responder a la insulina.
`` El problema con los diab�ticos tipo II no es que no hagan suficiente insulina;No responden a la insulina '', dijo DeFronzo.
`` Lo que nos gustar�a hacer es hacerlos m�s receptivos y este medicamento lo har� ''.
La droga se usa ampliamente en Europa, Canad� y M�xico y debe ser aprobada por la Administraci�n de Alimentos y Medicamentos en varios a�os para uso de los Estados Unidos, dijo.
Educar a los ni�os en edad escolar primaria sobre dietas saludables ayudar�a a reducir el n�mero de casos de diabetes, dijo Defronzo.
`` Si tienes una madre de 65 a�os que pesa 220 libras y le dices que salga y trote cinco millas el lunes, mi�rcoles y viernes, se reir� de ti.
`` Entonces tienes que dise�ar un programa de ejercicios que sea compatible con el estilo de vida del paciente y es algo que pueden hacer ''.


