El Banco Mundial vincular� el volumen de pr�stamos con la fortaleza de los esfuerzos de un pa�s para combatir la pobreza, seg�n una directiva operativa con el personal emitido hoy por el Sr. Lewis Preston, presidente del banco.
El v�nculo entre los pr�stamos y el alivio de la pobreza forma parte de un nuevo impulso para hacer que la pobreza alivie la misi�n central del banco en la d�cada de 1990.
El cambio en las prioridades tambi�n se refleja en un compromiso de hacer evaluaciones integrales de la naturaleza y el alcance de la pobreza en el Tercer Mundo, lo que permite que el banco dise�e pol�ticas m�s efectivas para combatir la pobreza.
En la directiva, Preston dice que la reducci�n de la pobreza es "el punto de referencia por el cual se medir� nuestro desempe�o como instituci�n de desarrollo".
Agrega que las nuevas instrucciones al personal est�n destinadas a "garantizar que estas pol�ticas se reflejen completamente en las operaciones del banco".
El banco tambi�n publica un manual que contiene ejemplos de mejores pr�cticas pasadas sobre la reducci�n de la pobreza.
El banco dice que las evaluaciones de pobreza deber�an estar disponibles para la mayor�a de los pa�ses en desarrollo dentro de dos a�os.
Estos formar�an la base para un "enfoque colaborativo para la reducci�n de la pobreza por parte de los funcionarios del pa�s y el banco".
La directiva indica un intento de imponer una forma de 'condicionalidad social' a los pa�ses prestados.
'El compromiso gubernamental m�s fuerte con la reducci�n de la pobreza garantiza un mayor apoyo;Por el contrario, el compromiso m�s d�bil con la reducci�n de la pobreza garantiza menos apoyo '', dice.
El �nfasis de Preston en la pobreza es una reacci�n a las pol�ticas bancarias en la d�cada de 1980, cuando el objetivo era mejorar la eficiencia econ�mica en los pa�ses en desarrollo.
La nueva directiva dice que los pr�stamos de ajuste estructural en la �ltima d�cada "eclipsaron los objetivos de reducci�n de la pobreza del banco".
El banco tambi�n est� reaccionando a nuevas pruebas que sugieren que el n�mero de pobres en los pa�ses en desarrollo aumentar� durante la d�cada de 1990, en lugar de estabilizarse como se esperaba.
Pobreza su juez, p�gina 34.


