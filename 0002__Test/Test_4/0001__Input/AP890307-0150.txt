Los telescopios solares produjeron vistas a las manchas solares productoras de bengalas y monta�as siluetas en la luna el martes cuando las multitudes se reunieron para ver un eclipse solar parcial visible en todo el oeste de Am�rica del Norte.
`` Hubo una delicia infantil con la maravilla de la naturaleza '', dijo Ed Krupp, director del Observatorio Griffith en Los �ngeles, donde 600 ni�os y al menos otras 400 personas observaron la luna bloquear el 37 por ciento de la superficie del sol a las 10:50 a.m. PST.
`` Ciertamente era una multitud animada '', dijo.
`` Hab�a un ambiente de festival ''.
`` Es bueno '', dijo el fabricante de mapas Jan Mayne, quien se encontraba entre docenas de personas que observaban el eclipse a trav�s de dos tipos de telescopios solares en el Instituto de Tecnolog�a de California en Pasadena.
Cuando la luna pas� entre la tierra y el sol para causar el eclipse parcial, los que observan a trav�s de los telescopios de Caltech pod�an ver monta�as en el borde de la luna siluada contra el sol.
Tambi�n visibles estaban los chorros de gas en la superficie del sol y un grupo gigante de manchas solares que el lunes produjo el brote solar m�s intenso, una explosi�n de calor y radiaci�n en 1984.
`` Hab�a una vista impresionante de ese gran grupo de manchas solares '', dijo Krupp.
El eclipse era visible en cierta medida al oeste de una l�nea diagonal que se extend�a aproximadamente desde Mazatlan, M�xico, noreste de Dallas y Chicago.
Las vistas eran mejores m�s al oeste y al norte.
Pero debido a que el eclipse era parcial, la mayor�a de la gente no not� la ligera atenuaci�n de la luz solar.
`` No se puede distinguir entre un eclipse parcial y una alerta de smog de segunda etapa '', brome� un observador en Caltech.
Varias personas vieron el eclipse a trav�s de los cascos del soldador en el centro de Anchorage, Alaska, donde la luna oscureci� el 80 por ciento del sol a las 9:13 a.m. AST.
En San Francisco, la luna eclips� el 46 por ciento del sol a las 9:52 a.m., pero el evento no fue visible debido a las nubes que fueron tan gruesas que retrasaron los vuelos llegando al aeropuerto.
Se observ� una ligera atenuaci�n en Seattle, donde los cielos parcialmente nublados permitieron un vistazo al eclipse del 56 por ciento a las 10:10 a.m. PST.
El porcentaje del sol bloqueado y el tiempo de eclipse m�ximo en otros lugares incluyeron el 52 por ciento a las 11:28 a.m. MST en Edmonton, Alberta;46 por ciento a las 11:09 a.m. MST en Boise, Idaho;36 por ciento a las 11:10 a.m. MST en Salt Lake City;35 por ciento a las 9:58 a.m. PST en Las Vegas;25 por ciento a las 11:17 a.m. MST en Denver;15 por ciento a las 12:42 p.m.CST en Minneapolis, y un miserable 3 por ciento a las 12:44 p.m.CST en Milwaukee.
Temiendo que la gente sufriera da�os oculares durante el eclipse, los cient�ficos advirtieron contra mirar el sol directamente o a trav�s de filtros inadecuados, incluidos vidrio ahumado y pel�culas o filtros fotogr�ficos.
Alan Macrobert, portavoz de Sky & amp;La revista Telescope, dijo que hubo 245 lesiones oculares conocidas en los Estados Unidos despu�s de un eclipse en 1970, pero las advertencias sobre el peligro redujeron el n�mero a tres durante un eclipse parcial de 1984.
Desde cualquier lugar en la Tierra, se produce un eclipse parcial cada varios a�os, dijo Ken Libbrecht, como profesor asistente de astrof�sica en Caltech.
Los eclipses solares totales son visibles desde cualquier ubicaci�n sola aproximadamente una vez cada cuatro siglos, aunque son visibles cada dos a�os desde alg�n lugar de la Tierra, dijo.
El pr�ximo eclipse total ocurrir� el 11 de julio de 1991, barriendo a trav�s de Hawai, el Oc�ano Pac�fico, la baja Baja California en M�xico, la costa oeste de Am�rica Central y finalmente Colombia y Brasil, dijo Macrobert.


