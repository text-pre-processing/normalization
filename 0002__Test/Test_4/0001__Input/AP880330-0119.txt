Si las dos partes que intentan forzar cambios en el censo de 1990 se presentan, los resultados casi se equilibrar�an entre s�, dijo un experto en poblaci�n el mi�rcoles.
La Oficina del Censo est� bajo presi�n para excluir a los extranjeros ilegales de su conteo nacional de cabeza.
Tradicionalmente, cuenta con todos los que viven en el pa�s.
Los grupos que han presentado una demanda para ignorar a los extraterrestres sostienen grandes concentraciones de ellos podr�an dar lugar a que algunos estados ganen asientos en la C�mara de Representantes a expensas de otros estados.
Mientras tanto, otros grupos quieren que se incrementen los totales del censo final para dar cuenta de las personas que pueden pasarse por alto en el censo _ con mayor frecuencia a los negros e hispanos que viven en �reas urbanas.
En juego est�n los 435 esca�os en la casa, que se distribuyen entre los estados sobre la base de la poblaci�n.
`` Si ambas partes se salen con la suya, el �nico cambio ser�a un flip-flop de un asiento de California a Georgia '', dijo William O'Hare, director de estudios de pol�ticas de la Oficina de Referencia de Poblaci�n Independiente.
O'Hare le dijo a una conferencia de desayuno para los miembros del Congreso del Nordeste y del Medio Oeste que estima que su regi�n perder� 14 esca�os en la casa despu�s del censo de 1990.
Eso continuar�a una tendencia evidente en las �ltimas d�cadas, se�al�.
Utilizando estimaciones del n�mero de extranjeros ilegales y minor�as subcuitadas, dijo que eliminar el �nico grupo y agregar al otro har�a poca diferencia a largo plazo.
El �nico cambio, dijo, ser�a que California ganar�a cinco asientos nuevos en lugar de seis, mientras que Georgia agregar�a dos en lugar de solo uno.
`` Eso es f�cil de entender, ya que hay tantos extraterrestres indocumentados en California '', coment�.
El estudio de O'Hare de los posibles cambios en los asientos de la casa _ basado en proyecciones de 1990 sin ajustes _ requiere que California sea el gran ganador, agregando seis asientos de la casa, seguido de Florida con una ganancia de cuatro y Texas agregando tres.
Se espera que recoja un asiento cada uno son Virginia, Carolina del Norte, Georgia y Arizona.
Por otro lado, Nueva York perder�a tres asientos.
Los estados que perdieron dos cada uno ser�an Pennsylvania, Ohio, Illinois y Michigan.
Se espera que pierda un asiento de la casa son Massachusetts, Wisconsin, Iowa, Kansas, West Virginia y Montana.


