Puede tener su tamale y comerlo tambi�n, dice Rachel Torres, educadora de diabetes en el Hospital San Jos� y coordinador del Programa de Educaci�n Hispana de la Sociedad Diabetes del Valle de Santa Clara.
El secreto para comer alimentos mexicanos y otros hispanos mientras mantiene una dieta saludable est� en la preparaci�n de la comida, dijo.
A partir de este verano, Torres y otros profesionales de la salud ofrecer�n clases de nutrici�n y ejercicio orientadas a los hispanos en un esfuerzo por educarlos sobre los peligros de la diabetes.
Seg�n las estad�sticas del Departamento de Salud del Estado, la incidencia de diabetes en la poblaci�n hispana es tres veces mayor que la de la poblaci�n general.
Los expertos en salud creen que la dieta hispana y la predisposici�n gen�tica pueden ser factores contribuyentes.
"Nuestra comida es una de las m�s ricas en t�rminos de sabor y nutrici�n", dijo Torres.
"Pero es la forma en que preparamos la comida que arruina las cosas para nosotros".
En sus clases, Torres dijo que instruir� a sus alumnos para que preparen tamales, frijoles refritos y otros platos hispanos tradicionales que usan aceite poliinsaturado en lugar de las recetas m�s tradicionales que requieren el uso de mantenimiento.
Y en lugar de comer cuatro tortillas de una sola vez, recomienda que se los coman durante todo el d�a.
Aunque los cursos de diab�ticos hispanos y sus familias han sido ofrecidos por la Sociedad Diab�tica durante casi tres a�os, las lecciones sobre los beneficios de la buena nutrici�n y el ejercicio para combatir la enfermedad se ofrecer�n por primera vez.
Sue Ann Kelly, directora de educaci�n de la Sociedad Diabetes, dijo que las clases pueden ser las primeras orientadas espec�ficamente al estilo de vida hispano.
"Es una sensaci�n poderosa que tenemos nuestros educadores cuando descubren todos los problemas culturales que entran en juego cuando se trata de diabetes", dijo.
Si est�s interesado;El pr�ximo curso para los diab�ticos hispanos y sus familias ser� de 9 a.m. al mediod�a del s�bado en
Alexian Brothers Hospital, 225 N. Jackson Ave. La inscripci�n comenzar� a las 8:30 a.m. Las clases son gratuitas.
Para obtener m�s informaci�n, llame al (408) 287-3785.
Para aquellos que no pueden llegar a clase, la Sociedad Diabetes del Condado de Santa Clara ofrece el folleto biling�e, "Comer Bien para Vivir Mejor", un men� de comida mexicana de siete d�as para diab�ticos, as� como aquellos que desean comer sano.


