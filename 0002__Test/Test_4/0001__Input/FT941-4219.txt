El 6 de mayo, la reina y el presidente Mitterrand declarar�n el t�nel del canal abierto.
Esa deber�a ser la se�al inicial para una recuperaci�n en el mercado inmobiliario en Kent y el noreste de Francia.
Por el momento, los agentes informan m�s consultas, pero los mercados en ambos pa�ses siguen siendo silenciosos y los precios bajos, especialmente en Francia.
Esa es una excelente raz�n a largo plazo para comprar.
Los altos precios iniciales de Le Shuttle para un veh�culo y sus pasajeros eventualmente disminuir�n creando una competencia de precios entre ferrocarril, ferry, catamar�n y aerodeslizadores.
Los futuros compradores con visi�n de futuro que miran cerca de Calais incluyen transportistas que desean establecer una base en el continente y empresarios asi�ticos que ven una ventaja en estar cerca de B�lgica.
John Hart, autor de una casa en Francia, se�ala: 'No es una prisa.
El t�nel no significa mucho para los continentales.
La mayor parte del comercio de ferry es de Gran Breta�a a Francia.
Los brit�nicos buscan principalmente casas, en lugar de apartamentos, aunque los compradores franceses y holandeses tambi�n est�n en el mercado.
Las consultas sobre la propiedad del norte de Francia se retomaron justo antes de Navidad, dice Maggie Kelly, de los agentes franceses L'Abri-Tanique en Hesdin.
La creciente confianza en el mercado del Reino Unido est� estimulando lentamente el inter�s en la propiedad francesa.
Los compradores del Reino Unido ahora pueden vender su casa del Reino Unido para poner dinero en una residencia principal o una segunda casa en Francia.
Los precios han dejado de caer en el norte de Francia y las esperanzas son altas de que el t�nel, las autopistas y los nuevos ferrocarriles resucitar�n a la regi�n.
Los brit�nicos que compraron en 1989 o 1990 pueden encontrar un buen momento para vender, si no pagaron demasiado en ese momento y han renovado sus propiedades desde entonces.
Pero algunos pagaron demasiado.
Se pueden encontrar recortes de precios espectaculares.
Por ejemplo, una abad�a gloriosa y recuperada con cinco acres en Tortefontaine, con un pasillo del siglo XII y muchas dependencias, est� a la venta en alrededor de FFR750,000 (libras 86,000) de agentes latitudes o L'Abri-Tanique.
Hace cuatro o cinco a�os cost� FFR2.5M (despu�s de ser puesto en el mercado en FFR4M).
Un molino cerca de Montreuil, que cost� FFR2M en 1989, se vendi� el a�o pasado por FFR500,000.
Los bancos franceses y los proveedores de hipotecas lo han llevado 'en la barbilla', dijo Kelly, y algunos bancos m�s peque�os en Calais no est�n muy contentos cuando los brit�nicos buscan pr�stamos.
Sin embargo, demuestre que tiene el efectivo y hay gangas que se deben tener.
Una casa en Hesdin, o una casa de campo cercana, se retiran bien y son mucho m�s baratas que sus equivalentes ingleses.
Una peque�a casa en Montreuil, una ciudad amurallada con calles empedradas, cuesta FFR280,000 de La residencia.
Se puede comprar una peque�a granja parcialmente restaurada cerca de Montreuil para FFR135,000 en una casa en Francia.
Treinta minutos en coche desde Boulogne, un granjero largo, bajo, de estilo normandos, a la madrugada, est� a la venta en FFR515,250, y a 50 minutos de distancia, otro cuesta FFR436,800 (reducido de FFR650,000).
Cerca del agente de San Omer Cote de Opale est� vendiendo un castillo del siglo XVIII con alas agregadas en 1908 para FFR4.46m.
LA Residence ofrece otro con 28 camas de berros y 12 hect�reas (30 acres) en buenas condiciones para FFR2.6M y un piso en la ciudad para FFR380,000.
En Hesdin, Wine Society, un grupo de entusiastas del vino con sede en ingl�s, tiene una salida para los miembros donde pueden recoger vinos recomendados por la sociedad sin impuestos en el Reino Unido.
Cuando Kelly ve a los conductores de Range Rover y Jaguar recolectando sus casos de vino en Hesdin, le gustar�a que se queden en su oficina a 100 metros de distancia y elijan una casa tambi�n.
Latitudes tiene en sus libros una casa de la ciudad del siglo XVIII con patio interior para FFR1.3m, un recorte cuando piensas lo que pagar�as en Par�s.
Cerca de Fruges es una f�brica de agua para FFR900,000 y, en el valle del Ternoise, una casa de ladrillo del siglo XVIII que necesita trabajo est� disponible para solo FFR450,000.
En la costa, Boulogne es una ciudad inteligente y agradable preferible a Calais.
Tiene buenas tiendas, incluida la tienda de quesos de Philippe Olivier, y restaurantes.
El favorito franc�s es Le Touquet, sigue siendo un lugar elegante para que los parisinos pasen el fin de semana.
Las tiendas abren en invierno los s�bados y domingos, y cierran los mi�rcoles y los jueves por la ma�ana.
Puedes jugar al golf y al tenis, montar, ir al casino y vivir en un elegante dominio en el FORET.
Como en Deauville, el prestigio de la ciudad ha mantenido los precios.Penny Zoldan, de Latitudes, tiene un piso en Le Touquet y lo ve como una buena base para los extranjeros.
Cerca, en Hardelot, hay muchas parcelas de construcci�n a la venta al lado de los dos campos de golf (consulte las latitudes).
Fuera de Le Touquet, una casa en Francia ofrece un castillo completo con fortificaciones de pared y t�neles para FFR1.3m.
En el campo Beyond Dieppe, una casa normanda t�pica, restaurada e incluyendo la mayor�a de los muebles y una caba�a, est� a la venta por libras 125,000 de Domus en el extranjero.
Hacia Par�s, fuera de la autoraute de Calais, Philip Hawkes est� vendiendo el Chateau de Pronleroy del siglo XVIII por FFR13M.
Egerton y Knight Frank & amp;Rutley ofrece el m�s reciente Chateau de la Chaussee, cerca de Chantilly, el centro de las carreras francesas, en FFR19m.
En el otro extremo del t�nel, Cluttons informa que los belgas y los holandeses se registraron en la oficina de Canterbury para caba�as de �poca por alrededor de libras de 200,000, y franceses y belgas en la oficina de Folkestone, donde David Parry informa que los �rabes est�n interesados en bloques de pisos en el mar del mar en el mar delantero..
'El t�nel tiene m�s impacto psicol�gico que cualquier cosa.
Los vendedores lo ven como una ventaja.
Los compradores a�n no ven eso.
Una vez que el t�nel est� abierto, dijo Parry, la industria se sentir� atra�da por el �rea y la gente se mudar�.
El centro y este de Kent necesita un est�mulo, pero puede llevar cuatro o cinco a�os antes de que haya evidencia de nuevas oportunidades en el �rea.
Un apartamento en el Grand at Folkestone, el antiguo hotel, est� a la venta desde Cluttons por libras 89,500.
A lo largo del M2, Strutt & amp;Parker ofrece 52 St Margaret's Street, Rochester, una casa de finales del siglo XVII de Grado II*, por libras 190,000, y un Tudor (1508) y Georgian Kentish Hall, Cobrahamsole Hall en Sheldwich cerca de Faversham por libras 275,000.
Lane Fox est� vendiendo la granja Manor de Manor en Milstead, cerca de Sittingbourne, por libras 275,000.
En el antiguo puerto de oferta de Cinque, Strutt & amp;Parker y Bright & amp;Bright Offer Woodbine, una casa georgiana con jard�n amurallado y estudio, por libras 330,000.
Canterbury Office de Cluttons y Weatherall Green & amp;Smith est� vendiendo Highland Court, en Bridge, una casa majestuosa columna por libras 750,000.
Los miopes est�n comprando ahora, a ambos lados de La Mancha.
El valor en Francia es formidable.
Y no est� mal en Kent.
M�s informaci�n en Francia: L'Hasside, Hesdin (21 81 59 79);Calificaci�n de �palos, Le Touquet (21 05 21 05);Knight Frank & amp;Rutley, Par�s (1) 42 96 88 88;Philip Hawkes, Par�s (1) 42 68 11 11.
Y en Inglaterra: Bright & amp;Bright, Deal (0304-374071);Cluttons, Canterbury (0227-457441) y Folkestone (0303-850 422);Domus en el extranjero (071-431-4692);Egerton (071-493-0676);Una casa en Francia (081-959-5182);Lane Fox, Sevenoaks (0732-459 900);Latitudes (081-958-5485);La residencia, Ruislip (0895-622020);Strutt & amp;Parker, Canterbury (0227-451123);Weatherall Green & amp;Smith (071-405-6944).


